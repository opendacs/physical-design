/* 
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : veri_str.hpp
 * Summary  : verilog syntax analyzer true node class : string
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2013.1.28
 */

#ifndef PARSER_VERILOG_VERI_STR_HPP_
#define PARSER_VERILOG_VERI_STR_HPP_

#include "veri_node.hpp"

#include <vector>
#include <string>

namespace rtl
{

/*
 * class VeriStmt :
 *     Verilog syntax analyzer true node : Statement
 */
class VeriStr : public VeriNode
{
  typedef std::string  String;
  typedef const String CString;

  public:
    VeriStr( Uint idx, int line, int col, VeriMod *mod )
      :  VeriNode( idx, vtSTR, vtSTR, line, col, mod )
    {}

    virtual ~VeriStr( void ) { clear(); }

    // accessors
    CString& str( void ) const { return _str; }

    // mutators
    void set_str( CString& str ) { _str = str; }

    virtual void clean_up( void );

    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  private:
    String _str;
};

};
#endif // PARSER_VERILOG_VERI_STMT_HPP_
