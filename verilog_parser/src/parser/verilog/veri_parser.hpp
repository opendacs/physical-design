/* 
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : veri_parser.hpp
 * Summary  : verilog syntax analyzer class
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2012.10.28
 */

#ifndef PARSER_VERILOG_VERI_PARSER_HPP_
#define PARSER_VERILOG_VERI_PARSER_HPP_

#include "singleton.hpp"

#include <string>
#include <sstream>
#include <vector>
#include <list>
#include <map>

#include "veri_node_pool.hpp"

#include "veri_node.hpp"
#include "veri_node_pool.hpp"
#include "veri_ast_node.hpp"
#include "veri_err_token.hpp"
#include "veri_ast_drawer.hpp"

extern int verilogparse( void );

namespace rtl
{

using pavio_singleton::Singleton;

class Cfg_mgr;
class Src_mgr;

class VeriMod;
class VeriModInstance;
class VeriGateInstance;
class VeriModContainer;
class VeriVarContainer;
class VeriNum;
class VeriErr;

/*
 * class VeriParser :
 *     verilog syntax-analyzer class deals with verilog HDL source code
 *   syntax-analysis.
 *
 *     Need to call int init() before parse.
 *     Need to call int parse() to parse source code.
 *     Has void clean_up() function to clean up inner data, back to status after
 *   init().
 *   
 */

// M: VeriMod* , P: VeriParser*
#define FOREACH_ALL_MODULES(M, P) for(auto (M) : (P)->get_modules()->all_modules())

class VeriParser : public Singleton<VeriParser>
{
  friend class Singleton<VeriParser>;

  typedef VeriModContainer          ModContainer;
  typedef VeriVarContainer          VarContainer;
  typedef VeriAstDrawer             AstDrawer;
  typedef VeriNodePool              NodePool;

  typedef std::string               String;
  typedef const String              CString;
  typedef std::list<String>         SList;
  typedef std::list<int>            IList;
  typedef unsigned int              Uint;
  typedef std::vector<Uint>         UVec;

  typedef std::vector< VeriNode * > NodeVec;
  typedef std::list< VeriNode * >   NodeList;
  typedef std::map< String, VeriNode * >    NodeMap;

  public:
    // initialize before using
    int init( void );

    // set top module
    void set_top_module( CString& mod_name ) { _top_mod_name = mod_name; }

    // get top module
    CString& get_top_module( void ) { return  _top_mod_name; }

    // get all modules
    ModContainer* get_modules( void ) { return _mods; }

    // parse from src_mgr :: code
    int parse( void );
    
    // back to status after initialization
    void clean_up( void );

    // dump syntree to dot file
    void dump_ast( CString& dir );

    // get Verilog-module by name
    VeriMod *module( CString& name );

    CString& get_err_info( void ) const { return _err->get_err_info( ); }
    int has_err( void ) const { return _err->has_err( ); }

  public: // open for lex
    int  get_cur_length(       ) const { return _length;  }
    void set_cur_length(int i  )       { _length = (i >= 0 ? i : 0); }
    void set_cur_s     (char *s)       { _s = s;          }
    char * get_s       (       ) const { return _s;       }

  public: // open for yacc

    /* expression */
    VeriNode* gen_dummy_port( int line, int col );
    VeriNode* gen_port_ref( int line, int col, const char *port_name );
    VeriNode* gen_primary( const char *str, int line, int col, int primary_type );
    VeriNode* gen_range  ( VeriNode *left, int rtype, VeriNode *right );
    VeriNode* gen_array  ( VeriNode *id, VeriNode *range );
    VeriNode* gen_caller ( VeriNode *name, VeriNode *call_exp );
    VeriNode* gen_unary  ( int op, int line, int col, VeriNode *exp1 );
    VeriNode* gen_binary ( VeriNode *exp1, int op, int line, int col,
                           VeriNode *exp2 );
    VeriNode* gen_mintypmax( VeriNode *exp1, int line, int col,
                            VeriNode *exp2, VeriNode *exp3 );
    VeriNode* gen_ques_exp( VeriNode *ques, int line, int col,
                            VeriNode *choice1, VeriNode *choice2 );
    VeriNode* gen_multi  ( VeriNode *exp1, VeriNode *exp2,
                           int mtype, int line, int col );
    VeriNode* gen_concat_exp( VeriNode *mexp );

    VeriNode* gen_delay_exp( int line, int col, VeriNode *d1, VeriNode *dextra );
    VeriNode* gen_drive_exp( int line, int col, int drive_from, int drive_to);

    VeriNode* gen_signed_exp( int line, int col, VeriNode *exp );
    VeriNode* gen_unsigned_exp( int line, int col, VeriNode *exp );

    /* statement */
    VeriNode* gen_loop_stmt( int line, int col, VeriNode *init,
                             VeriNode *judge, VeriNode *iter,
                             VeriNode *content );
    VeriNode* gen_func_loop_stmt( int line, int col, VeriNode *init,
                             VeriNode *judge, VeriNode *iter,
                             VeriNode *content );
    VeriNode* gen_default_case_item( int line, int col, VeriNode *item );
    VeriNode* gen_case_item( VeriNode *label, VeriNode *item );
    VeriNode* gen_case_item_list( VeriNode *item );
    VeriNode* append_case_item_list( VeriNode *list, VeriNode *item );
    VeriNode* gen_case_stmt( int line, int col, int case_type,
                             VeriNode *switch_value, VeriNode *item );

    VeriNode* gen_func_default_case_item( int line, int col, VeriNode *item );
    VeriNode* gen_func_case_item( VeriNode *label, VeriNode *item );
    VeriNode* gen_func_case_stmt( int line, int col, int case_type,
                                  VeriNode *switch_value, VeriNode *item );
    VeriNode* gen_func_case_item_list( VeriNode *item );
    VeriNode* append_func_case_item_list( VeriNode *list, VeriNode *item );

    VeriNode* gen_func_cond_stmt( int line, int col, VeriNode *if_exp,
                                  VeriNode *if_stmt, VeriNode *else_stmt );
    VeriNode* gen_cond_stmt( int line, int col, VeriNode *if_exp,
                             VeriNode *if_stmt, VeriNode *else_stmt );

    VeriNode* gen_proc_timing_control_stmt( VeriNode *event_control,
                                            VeriNode *statement );
    VeriNode* gen_disable_stmt( const char *label, int line, int col );
    VeriNode* gen_event_stmt( int line, int col, int is_all,
                              VeriNode *event_list );

    VeriNode* gen_func_blocking_assign_stmt( VeriNode *assign_exp );
    VeriNode* gen_sys_caller_stmt( int line, int col, const char *name,
                                   VeriNode *call_exp );

    VeriNode* gen_dummy_stmt( int line, int col );
    VeriNode* gen_seq_block_stmt( int line, int col, const char *name,
                                  int label_line, int label_col,
                                  VeriNode *decl_list, VeriNode *stmt_list );
    VeriNode* gen_func_seq_block_stmt( int line, int col, const char *name,
                                      int label_line, int label_col,
                                      VeriNode *decl_list, VeriNode *stmt_list );

    VeriNode* gen_stmt_list( VeriNode *item );
    VeriNode* append_stmt_list( VeriNode *list, VeriNode *item );
    VeriNode* gen_func_stmt_list( VeriNode *item );
    VeriNode* append_func_stmt_list( VeriNode *list, VeriNode *item );

    VeriNode* gen_mod_item_list( VeriNode *item );
    VeriNode* append_mod_item_list( VeriNode *list, VeriNode *item );

    VeriNode* gen_block_or_non_or_caller_stmt( VeriNode *node );
    VeriNode* gen_blocking_stmt( VeriNode *lval, VeriNode *event,
                                 VeriNode *rval );
    VeriNode* gen_non_blocking_stmt( VeriNode *lval, VeriNode *event,
                                     VeriNode *rval );

    VeriNode* gen_continuous_assign_stmt( int line, int col,
                            VeriNode *delay, VeriNode *drive, VeriNode *exp );

    VeriNode* gen_init_stmt( int line, int col, VeriNode *stmt );
    VeriNode* gen_always_stmt( int line, int col, VeriNode *stmt );

    /* instantiation */
    VeriNode* gen_dummy_inst( int line, int col );
    VeriNode* gen_gen_loop_stmt( int line, int col, VeriNode *init,
                             VeriNode *judge, VeriNode *iter,
                             const char * label, int label_line,
                             int label_col, VeriNode *content );

    VeriNode* gen_default_gen_case_item( int line, int col, VeriNode *item );
    VeriNode* gen_gen_case_item( VeriNode *label, VeriNode *item );
    VeriNode* gen_gen_case_item_list( VeriNode *item );
    VeriNode* append_gen_case_item_list( VeriNode *list, VeriNode *item );
    VeriNode* gen_gen_case_stmt( int line, int col,
                             VeriNode *switch_value, VeriNode *item );

    VeriNode* gen_gen_cond_stmt( int line, int col, VeriNode *if_exp,
                                 VeriNode *if_stmt, VeriNode *else_stmt );
    VeriNode* gen_gen_item_list( VeriNode *item );
    VeriNode* append_gen_item_list( VeriNode *list, VeriNode *item );
    VeriNode* gen_gen_block_stmt( int line, int col, const char *name,
                                  int label_line, int label_col,
                                  VeriNode *item_list );

    VeriNode* gen_mod_instantiation( int line, int col, const char * mod_name,
                                     VeriNode *param, VeriNode *list );
    VeriNode* gen_gate_instantiation( int line, int col, 
                                int gate_type, VeriNode *drive,
                                VeriNode *delay, VeriNode *list );

    VeriModInstance* gen_mod_instance( VeriNode *mod_isnt );
    VeriGateInstance* gen_gate_instance( VeriNode *gate_inst );

    /* declaration */
    VeriNode* gen_tf_declaration( int line, int col, int inout, int type,
                                  int is_signed, VeriNode *range,
                                  VeriNode *port_ids );
    VeriNode* gen_tp_list( VeriNode *port );
    VeriNode* append_tp_list( VeriNode *list, VeriNode *port );
    VeriNode* gen_fp_list( VeriNode *port );
    VeriNode* append_fp_list( VeriNode *list, VeriNode *port );
    VeriNode* gen_task_item_list( VeriNode *item );
    VeriNode* append_task_item_list( VeriNode *list, VeriNode *item );
    VeriNode* gen_block_item_list( VeriNode *item );
    VeriNode* append_block_item_list( VeriNode *list, VeriNode *item );
    VeriNode* gen_func_item_list( VeriNode *item );
    VeriNode* append_func_item_list( VeriNode *list, VeriNode *item );
    VeriNode* gen_param_decl_list( VeriNode *item );
    VeriNode* append_param_decl_list( VeriNode *list, VeriNode *item );
    VeriNode* gen_port_decl_list( VeriNode *item );
    VeriNode* append_port_decl_list( VeriNode *list, VeriNode *item );
    VeriNode* gen_task_decl( int line, int col, int is_auto, const char *tname,
                             VeriNode *task_port, VeriNode *task_item,
                             VeriNode *stmt );
    VeriNode* gen_func_decl( int line, int col, int is_auto, int is_signed,
                             int is_int, VeriNode *range, const char *name,
                             VeriNode *port_list, VeriNode *item_decl,
                             VeriNode *stmt );
    VeriNode* gen_var_decl( int line, int col, int inout, int type,
                            int is_signed, int vector_scalar, VeriNode *drive,
                            VeriNode *range, VeriNode *delay, VeriNode * list );
    VeriNode* gen_param_decl( int line, int col, int is_signed, int is_int,
                              int is_local, VeriNode *range, VeriNode *list );
    VeriNode* gen_int_decl( int line, int col, VeriNode *list );
    VeriNode* gen_genvar_decl( int line, int col, VeriNode *list );

    VeriNode* gen_mod( int line, int col, const char *name, VeriNode *param,
                       VeriNode *port, VeriNode *item_list );

  public: // error manipulation
    void include_file( int line, CString& fname )
    {
      _err->include_file( line, fname );
      _fname.push_back( fname );
      _fline.push_back( line );
    }

    void exclude_file( )
    {
      _err->exclude_file( );
      _fname.pop_back();
      _fline.pop_back();
    }

    void back_to_file( int line )
    {
      _err->back_to_file( line );
      _fline.back() = line;
    }

    void append_err( int lvl, int line, int col, int err );

  public:  // node pool manipulation

    Uint next_id   ( void    ) { return _node_pool.next_id( ); }
    int  toss_node ( Uint id ) { return _node_pool.toss_node( id ); }

    int  insert_node ( VeriNode *node )
    { return _node_pool.insert( node ); }

  protected:
    int clear_syntree( void );

    int append_module( VeriMod *mod );
    int prep_for_new_mod( void );

    String int2string(int n)
    {
      std::stringstream f;
      f << n;
      return f.str();
    }

    // back to status after construction
    void clear( void );

  private:
    VeriParser();
    ~VeriParser();

    // manager ptrs
    Cfg_mgr  *_cmgr;
    Src_mgr  *_smgr;
    VeriErr  *_err;
    SList     _fname;
    IList     _fline;
    String    _top_mod_name;

    NodeList  _mod_instantiations;

    // for flex and bison
    char     *_s;
    Uint      _length;
    VeriMod  *_mod;

    // data lib
    ModContainer *_mods;
    NodePool      _node_pool;

    // utilities
    AstDrawer     _drawer;
};

};

extern rtl::VeriParser *veri_parser;

#endif // PARSER_VERILOG_VERI_PARSER_HPP_
