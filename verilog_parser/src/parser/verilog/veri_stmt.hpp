/* 
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : veri_stmt.hpp
 * Summary  : verilog syntax analyzer true node class : statement
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2013.1.28
 */

#ifndef PARSER_VERILOG_VERI_STMT_HPP_
#define PARSER_VERILOG_VERI_STMT_HPP_

#include "veri_node.hpp"
#include "veri_stmt.hpp"

#include <vector>
#include <string>
#include <list>

namespace rtl
{

/*
 * class VeriStmt :
 *     Verilog syntax analyzer true node : Statement
 */
class VeriStmt : public VeriNode
{
  public:
    VeriStmt( Uint idx, int stmt_type, int func_task_flag, int line, int col,
             VeriMod *mod )
      :  VeriNode( idx, vtSTMT, stmt_type, line, col, mod )
      , _func_task_flag( func_task_flag )
    {}

    virtual ~VeriStmt( void ) {};

    // accessors
    int func_task_flag( void ) const { return _func_task_flag; }
    int is_dummy( void ) const
    {
      if( detail_type() == vsSTMT_DUMMY )
        return IS_TRUE;
      return IS_FALSE;
    }

    // mutators
    void set_func_task_flag( int flag ) { _func_task_flag = flag; }

    virtual void clean_up( void ) = 0;
    virtual int  check_child_validation( void ) = 0;

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue ) = 0;
    virtual int  pretty_dot_label( Ofstream& ofile, int depth ) = 0;

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info ) = 0;
    virtual int  push_for_replacement( NodeVec& mstack ) = 0;
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl ) = 0;

  protected:
    VeriStmt( Uint idx, const VeriStmt *stmt_copy )
      :  VeriNode( idx, stmt_copy )
      , _func_task_flag( stmt_copy->_func_task_flag )
    {}

    int check_func_child_exp_validation( VeriNode *child, int flag );
    int check_child_stmt( VeriNode * child, int flag );

  private:
    int  _func_task_flag;
};

/*
 * class VeriDummyExp :
 *     Verilog syntax analyzer true node : Dummy statement
 */
class VeriDummyStmt : public VeriStmt
{
  public:
    VeriDummyStmt( Uint idx, int line, int col,
             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_DUMMY, vsBOTH, line, col, mod )
    {};

    virtual ~VeriDummyStmt( void ) { clear(); };

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    VeriDummyStmt( Uint idx, const VeriDummyStmt *copy )
      :  VeriStmt( idx, copy )
    {}

  private:
};

/*
 * class VeriLoopStmt :
 *     Verilog syntax analyzer true node : Loop statement
 *   Loop statement has four children
 *     1, initialization expression
 *     2, judge expression
 *     3, iteration expression
 *     4, content statement
 */
class VeriLoopStmt : public VeriStmt
{
  public:
    VeriLoopStmt( Uint idx, int line, int col, VeriNode *init, VeriNode *judge,
                  VeriNode *iter, VeriNode *content, VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_LOOP, vsNONE_FUNC, line, col, mod )
      , _init( init )
      , _judge( judge )
      , _iter( iter )
      , _content( content )
    {
    };

    virtual ~VeriLoopStmt( void ) { clear(); };

    // accessors
    VeriNode * init   ( void ) { return _init;    }
    VeriNode * judge  ( void ) { return _judge;   }
    VeriNode * iter   ( void ) { return _iter;    }
    VeriNode * content( void ) { return _content; }
    NodeList & gen_content( void ) { return _gen_content; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_start_and_condition( NodeVec& mstack );
    int decide_loop( NodeVec& mstack );
    int repeat_loop( void );
    int clear_content( void );

  protected:
    VeriLoopStmt( Uint idx, const VeriLoopStmt *copy )
      :  VeriStmt( idx, copy )
      , _init( NULL )
      , _judge( NULL )
      , _iter( NULL )
      , _content( NULL )
    {}

   int check_child_validation_assign( VeriNode *child );

  private:
    VeriNode *_init;
    VeriNode *_judge;
    VeriNode *_iter;
    VeriNode *_content;

    NodeList  _gen_iter;
    NodeList  _gen_judge;
    NodeList  _gen_content;
};

/*
 * class VeriFuncLoopStmt :
 *     Verilog syntax analyzer true node : Function Loop statement
 *   Function Loop statement has four children
 *     1, initialization expression
 *     2, judge expression
 *     3, iteration expression
 *     4, content  function-statement
 */
class VeriFuncLoopStmt : public VeriStmt
{
  public:
    VeriFuncLoopStmt( Uint idx, int line, int col, VeriNode *init,
                      VeriNode *judge, VeriNode *iter,
                      VeriNode *content,
                           VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_FUNC_LOOP, vsFUNC, line, col, mod )
      , _init( init )
      , _judge( judge )
      , _iter( iter )
      , _content( content )
    {
    };

    virtual ~VeriFuncLoopStmt( void ) { clear(); };

    // accessors
    VeriNode * init   ( void ) { return _init;    }
    VeriNode * judge  ( void ) { return _judge;   }
    VeriNode * iter   ( void ) { return _iter;    }
    VeriNode * content( void ) { return _content; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_start_and_condition( NodeVec& mstack );
    int decide_loop( NodeVec& mstack );
    int repeat_loop( void );
    int clear_content( void );

  protected:
    VeriFuncLoopStmt( Uint idx, const VeriFuncLoopStmt *copy )
      :  VeriStmt( idx, copy )
      , _init( NULL )
      , _judge( NULL )
      , _iter( NULL )
      , _content( NULL )
    {}

   int check_child_validation_assign( VeriNode *child );

  private:
    VeriNode *_init;
    VeriNode *_judge;
    VeriNode *_iter;
    VeriNode *_content;

    NodeList  _gen_iter;
    NodeList  _gen_judge;
    NodeList  _gen_content;
};

class VeriFuncDefaultCaseItem : public VeriStmt
{
  friend class VeriFuncCaseStmt;

  public:
    VeriFuncDefaultCaseItem( Uint idx, int line, int col, VeriNode *stmt,
             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_FUNC_DEFAULT_CASE_ITEM, vsFUNC, line, col, mod )
      , _stmt( stmt )
    {}

    virtual ~VeriFuncDefaultCaseItem( void ) { clear(); }

    // accessors
    VeriNode * stmt( void ) { return _stmt; }

    // mutators
    void set_stmt( VeriNode *node ) { _stmt = node; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_stmt( NodeVec& mstack );

  protected:
    VeriFuncDefaultCaseItem( Uint idx, const VeriFuncDefaultCaseItem *copy )
      :  VeriStmt( idx, copy )
      , _stmt( NULL )
    {}

  private:
    VeriNode *_stmt;
};

class VeriFuncCaseItem : public VeriStmt
{
  friend class VeriFuncCaseStmt;
  friend class VeriFuncCaseItemList;

  public:
    VeriFuncCaseItem( Uint idx, int line, int col,
                      VeriNode *stmt, VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_FUNC_CASE_ITEM, vsFUNC, line, col, mod )
      , _stmt( stmt )
    {}

    virtual ~VeriFuncCaseItem( void ) { clear(); }

    // accessors
    NodeList & label( void ) { return _label; }
    VeriNode * stmt ( void ) { return _stmt;  }

    // mutators
    void append_label( VeriNode *node ) { _label.push_back( node ); }
    void set_stmt ( VeriNode *node ) { _stmt  = node; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_labels( NodeVec& mstack );
    int push_stmt( NodeVec& mstack );

  protected:
    VeriFuncCaseItem( Uint idx, const VeriFuncCaseItem *copy )
      :  VeriStmt( idx, copy )
      , _stmt( NULL )
    {}

  private:
    NodeList  _label;
    VeriNode *_stmt;
};

class VeriFuncCaseItemList : public VeriStmt
{
  friend class VeriFuncCaseStmt;

  public:
    VeriFuncCaseItemList( Uint idx, int line, int col ,
             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_FUNC_CASE_ITEM_LIST, vsFUNC, line, col, mod )
      , _default_item( NULL )
    {}

    virtual ~VeriFuncCaseItemList( void ) { clear(); }

    // accessors
    VeriNode* default_item( void ) { return _default_item; }
    NodeList& other_item  ( void ) { return _other_item;   }

    // mutators
    int  set_default( VeriNode * node );
    void append_item( VeriNode * node ) { _other_item.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_labels( NodeVec& mstack );

  protected:
    VeriFuncCaseItemList( Uint idx, const VeriFuncCaseItemList *copy )
      :  VeriStmt( idx, copy )
      , _default_item( NULL )
    {}

  private:
    VeriNode *_default_item;
    NodeList  _other_item;

};

class VeriFuncCaseStmt : public VeriStmt
{
  public:
    VeriFuncCaseStmt( Uint idx, int line, int col, int case_type,
                      VeriNode *switch_value, VeriNode * item_list,
                           VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_FUNC_CASE_STMT, vsFUNC, line, col, mod )
      , _case_type( case_type )
      , _switch( switch_value )
      , _item_list( item_list )
      , _choice( NULL )
    {}

    virtual ~VeriFuncCaseStmt( void ) { clear(); }

    // accessors
    int case_type( void ) const { return _case_type; }
    VeriNode * switch_value( void ) { return _switch; }
    VeriNode * item_list( void ) { return _item_list; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_conditions( NodeVec& mstack );
    int prep_all_labels( void );
    int decide_case( NodeVec& mstack );

  protected:
    VeriFuncCaseStmt( Uint idx, const VeriFuncCaseStmt *copy )
      :  VeriStmt( idx, copy )
      , _case_type( copy->_case_type )
      , _switch( NULL )
      , _item_list( NULL )
      , _choice( NULL )
    {}

  private:
    int        _case_type;
    VeriNode * _switch;
    VeriNode * _item_list;

    NodeList   _all_labels;
    NodeList   _all_exps;

    VeriNode * _choice;
};

class VeriDefaultCaseItem : public VeriStmt
{
  friend class VeriCaseStmt;

  public:
    VeriDefaultCaseItem( Uint idx, int line, int col, VeriNode *stmt,
             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_DEFAULT_CASE_ITEM, vsNONE_FUNC, line, col, mod )
      , _stmt( stmt )
    {}

    virtual ~VeriDefaultCaseItem( void ) { clear(); }

    // accessors
    VeriNode * stmt( void ) { return _stmt; }

    // mutators
    void set_stmt( VeriNode *node ) { _stmt = node; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_stmt( NodeVec& mstack );

  protected:
    VeriDefaultCaseItem( Uint idx, const VeriDefaultCaseItem *copy )
      :  VeriStmt( idx, copy )
      , _stmt( NULL )
    {}

  private:
    VeriNode *_stmt;
};

class VeriCaseItem : public VeriStmt
{
  friend class VeriCaseItemList;
  friend class VeriCaseStmt;

  public:
    VeriCaseItem( Uint idx, int line, int col,
                  VeriNode *stmt, VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_CASE_ITEM, vsNONE_FUNC, line, col, mod )
      , _stmt( stmt )
    {}

    virtual ~VeriCaseItem( void ) { clear(); }

    // accessors
    NodeList & label( void ) { return _label; }
    VeriNode * stmt ( void ) { return _stmt;  }

    // mutators
    void append_label( VeriNode *node ) { _label.push_back( node ); }
    void set_stmt ( VeriNode *node ) { _stmt  = node; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_labels( NodeVec& mstack );
    int push_stmt( NodeVec& mstack );

  protected:
    VeriCaseItem( Uint idx, const VeriCaseItem *copy )
      :  VeriStmt( idx, copy )
      , _stmt( NULL )
    {}

  private:
    NodeList  _label;
    VeriNode *_stmt;
};

class VeriCaseItemList : public VeriStmt
{
  friend class VeriCaseStmt;

  public:
    VeriCaseItemList( Uint idx, int line, int col,
             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_CASE_ITEM_LIST, vsNONE_FUNC, line, col, mod )
      , _default_item( NULL )
    {}

    virtual ~VeriCaseItemList( void ) { clear(); }

    // accessors
    VeriNode* default_item( void ) { return _default_item; }
    NodeList& other_item  ( void ) { return _other_item;   }

    // mutators
    int  set_default( VeriNode * node );
    void append_item( VeriNode * node ) { _other_item.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_labels( NodeVec& mstack );

  protected:
    VeriCaseItemList( Uint idx, const VeriCaseItemList *copy )
      :  VeriStmt( idx, copy )
      , _default_item( NULL )
    {}

  private:
    VeriNode *_default_item;
    NodeList  _other_item;

};

class VeriCaseStmt : public VeriStmt
{
  public:
    VeriCaseStmt( Uint idx, int line, int col, int case_type,
                  VeriNode *switch_value, VeriNode * item_list,
                  VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_CASE, vsNONE_FUNC, line, col, mod )
      , _case_type( case_type )
      , _switch( switch_value )
      , _item_list( item_list )
      , _choice( NULL )
    {}

    virtual ~VeriCaseStmt( void ) { clear(); }

    // accessors
    int case_type( void ) const { return _case_type; }
    VeriNode * switch_value( void ) { return _switch; }
    VeriNode * item_list( void ) { return _item_list; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_conditions( NodeVec& mstack );
    int prep_all_labels( void );
    int decide_case( NodeVec& mstack );

  protected:
    VeriCaseStmt( Uint idx, const VeriCaseStmt *copy )
      :  VeriStmt( idx, copy )
      , _case_type( copy->_case_type )
      , _switch( NULL )
      , _item_list( NULL )
      , _choice( NULL )
    {}

  private:
    int        _case_type;
    VeriNode * _switch;
    VeriNode * _item_list;

    NodeList   _all_labels;
    NodeList   _all_exps;

    VeriNode * _choice;
};

class VeriFuncCondStmt : public VeriStmt
{
  public:
    VeriFuncCondStmt( Uint idx, int line, int col, VeriNode *if_exp,
                      VeriNode *if_stmt, VeriNode *else_stmt,
                           VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_FUNC_COND, vsFUNC, line, col, mod )
      , _if_exp( if_exp )
      , _if_stmt( if_stmt )
      , _else_stmt( else_stmt )
      , _choice( NULL )
    {}

    virtual ~VeriFuncCondStmt( void ) { clear(); }

    // accessors
    VeriNode * if_exp   ( void ) { return _if_exp; }
    VeriNode * if_stmt  ( void ) { return _if_stmt; }
    VeriNode * else_stmt( void ) { return _else_stmt; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_condition( NodeVec& mstack );
    int choose_children( NodeVec& mstack );

  protected:
    VeriFuncCondStmt( Uint idx, const VeriFuncCondStmt *copy )
      :  VeriStmt( idx, copy )
      , _if_exp( NULL )
      , _if_stmt( NULL )
      , _else_stmt( NULL )
      , _choice( NULL )
    {}

  private:
    VeriNode * _if_exp;
    VeriNode * _if_stmt;
    VeriNode * _else_stmt;

    VeriNode * _choice;
};

class VeriCondStmt : public VeriStmt
{
  public:
    VeriCondStmt( Uint idx, int line, int col, VeriNode *if_exp,
                  VeriNode *if_stmt, VeriNode *else_stmt,
                       VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_COND, vsNONE_FUNC, line, col, mod )
      , _if_exp( if_exp )
      , _if_stmt( if_stmt )
      , _else_stmt( else_stmt )
      , _choice( NULL )
    {}

    virtual ~VeriCondStmt( void ) { clear(); }

    // accessors
    VeriNode * if_exp   ( void ) { return _if_exp; }
    VeriNode * if_stmt  ( void ) { return _if_stmt; }
    VeriNode * else_stmt( void ) { return _else_stmt; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_condition( NodeVec& mstack );
    int choose_children( NodeVec& mstack );

  protected:
    VeriCondStmt( Uint idx, const VeriCondStmt *copy )
      :  VeriStmt( idx, copy )
      , _if_exp( NULL )
      , _if_stmt( NULL )
      , _else_stmt( NULL )
      , _choice( NULL )
    {}

  private:
    VeriNode * _if_exp;
    VeriNode * _if_stmt;
    VeriNode * _else_stmt;

    VeriNode * _choice;
};

class VeriProcTimeContStmt : public VeriStmt
{
  public:
    VeriProcTimeContStmt( Uint idx, int line, int col, VeriNode *control,
                          VeriNode *stmt,
                               VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_PROC_TIME_CONTROL, vsNONE_FUNC, line, col, mod )
      , _control( control )
      , _stmt( stmt )
    {}

    virtual ~VeriProcTimeContStmt( void ) { clear(); }

    // accessors
    VeriNode * control( void ) { return _control; }
    VeriNode * stmt   ( void ) { return _stmt; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriProcTimeContStmt( Uint idx, const VeriProcTimeContStmt *copy )
      :  VeriStmt( idx, copy )
      , _control( NULL )
      , _stmt( NULL )
    {}

  private:
    VeriNode * _control;
    VeriNode * _stmt;
};

class VeriDisableStmt : public VeriStmt
{
  public:
    VeriDisableStmt( Uint idx, int line, int col, CString& label,
             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_DISABLE, vsBOTH, line, col, mod )
      , _label( label )
      , _break_stmt( NULL )
    {}

    virtual ~VeriDisableStmt( void ) { clear(); }

    // accessors
    CString& label( void ) const { return _label; }
    VeriNode * break_stmt( void ) { return _break_stmt; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriDisableStmt( Uint idx, const VeriDisableStmt *copy )
      :  VeriStmt( idx, copy )
      , _label( copy->_label )
      , _break_stmt( NULL )
    {}

  private:
    String     _label;
    VeriNode * _break_stmt;
};

class VeriEventStmt : public VeriStmt
{
  public:
    VeriEventStmt( Uint idx, int line, int col, int is_all, VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_EVENT, vsBOTH, line, col, mod )
      , _is_all( is_all )
    {}

    virtual ~VeriEventStmt( void ) { clear(); }

    // accessors
    int is_all( void ) const { return _is_all; }
    NodeList & event_list( void ) { return _event_list; }

    // mutators
    void append_event( VeriNode *node ) { _event_list.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriEventStmt( Uint idx, const VeriEventStmt *copy )
      :  VeriStmt( idx, copy )
      , _is_all( copy->_is_all )
    {}

  private:
    int      _is_all;
    NodeList _event_list;
};

class VeriSysCallerStmt : public VeriStmt
{
  public:
    VeriSysCallerStmt( Uint idx, int line, int col,
                       CString& caller, VeriNode *arg,
                            VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_SYS_CALL, vsBOTH, line, col, mod )
      , _caller( caller )
      , _arg( arg )
    {}

    virtual ~VeriSysCallerStmt( void ) { clear(); }

    // accessors
    CString& caller( void ) const { return _caller; }
    VeriNode * arg ( void ) const { return _arg;    }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int process_sys_call( void );

  protected:
    VeriSysCallerStmt( Uint idx, const VeriSysCallerStmt *copy )
      :  VeriStmt( idx, copy )
      , _caller( copy->_caller )
      , _arg( NULL )
    {}

  private:
    String    _caller;
    VeriNode *_arg;
};

class VeriCallerStmt : public VeriStmt
{
  public:
    VeriCallerStmt( Uint idx, int line, int col, VeriNode *caller,
             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_CALL, vsBOTH, line, col, mod )
      , _caller( caller )
    {}

    virtual ~VeriCallerStmt( void ) { clear(); }

    // accessors
    VeriNode * caller( void ) const { return _caller; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int process_caller( void );

  protected:
    VeriCallerStmt( Uint idx, const VeriCallerStmt *copy )
      :  VeriStmt( idx, copy )
      , _caller( NULL )
    {}

  private:
    VeriNode *_caller;
};

class VeriSeqBlock : public VeriStmt
{
  public:
    VeriSeqBlock( Uint idx, int line, int col, CString& label, int label_line,
                  int label_col, VeriNode *decl_list, VeriNode *stmt_list,
                  VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_SEQ_BLOCK, vsNONE_FUNC, line, col , mod)
      , _label( label )
      , _label_line( label_line )
      , _label_col( label_col )
      , _decl_list( decl_list )
      , _stmt_list( stmt_list )
    {}

    virtual ~VeriSeqBlock( void ) { clear(); }

    // accessors
    CString  & label    ( void ) { return _label; }
    VeriNode * decl_list( void ) { return _decl_list; }
    VeriNode * stmt_list( void ) { return _stmt_list; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int process_label( void );
    int push_children( NodeVec& mstack );
    int end_label( void );

  protected:
    VeriSeqBlock( Uint idx, const VeriSeqBlock *copy )
      :  VeriStmt( idx, copy )
      , _label( copy->_label )
      , _label_line( copy->_label_line )
      , _label_col( copy->_label_col )
      , _decl_list( NULL )
      , _stmt_list( NULL )
    {}

  private:
    String     _label;
    int        _label_line;
    int        _label_col;
    VeriNode * _decl_list;
    VeriNode * _stmt_list;
};

class VeriFuncSeqBlock : public VeriStmt
{
  public:
    VeriFuncSeqBlock( Uint idx, int line, int col, CString& label,
                      int label_line, int label_col, VeriNode *decl_list,
                      VeriNode *stmt_list, VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_FUNC_SEQ_BLOCK, vsFUNC, line, col, mod )
      , _label( label )
      , _label_line( label_line )
      , _label_col( label_col )
      , _decl_list( decl_list )
      , _stmt_list( stmt_list )
    {}

    virtual ~VeriFuncSeqBlock( void ) { clear(); }

    // accessors
    CString  & label    ( void ) { return _label; }
    VeriNode * decl_list( void ) { return _decl_list; }
    VeriNode * stmt_list( void ) { return _stmt_list; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int process_label( void );
    int push_children( NodeVec& mstack );
    int end_label( void );

  protected:
    VeriFuncSeqBlock( Uint idx, const VeriFuncSeqBlock *copy )
      :  VeriStmt( idx, copy )
      , _label( copy->_label )
      , _label_line( copy->_label_line )
      , _label_col( copy->_label_col )
      , _decl_list( NULL )
      , _stmt_list( NULL )
    {}

  private:
    String     _label;
    int        _label_line;
    int        _label_col;
    VeriNode * _decl_list;
    VeriNode * _stmt_list;
};

class VeriStmtList : public VeriStmt
{
  public:
    VeriStmtList( Uint idx, int line, int col,
             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_STMT_LIST, vsNONE_FUNC, line, col, mod )
    {}

    virtual ~VeriStmtList( void ) { clear(); }

    // accessors
    NodeList & stmt_list( void ) { return _stmt_list; }

    // mutators
    void append_stmt( VeriNode * node ) { _stmt_list.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriStmtList( Uint idx, const VeriStmtList *copy )
      :  VeriStmt( idx, copy )
    {}

  private:
    NodeList _stmt_list;
};

class VeriFuncStmtList : public VeriStmt
{
  public:
    VeriFuncStmtList( Uint idx, int line, int col,
             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_FUNC_STMT_LIST, vsFUNC, line, col, mod )
    {}

    virtual ~VeriFuncStmtList( void ) { clear(); }

    // accessors
    NodeList & stmt_list( void ) { return _stmt_list; }

    // mutators
    void append_stmt( VeriNode * node ) { _stmt_list.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriFuncStmtList( Uint idx, const VeriFuncStmtList *copy )
      :  VeriStmt( idx, copy )
    {}

  private:
    NodeList _stmt_list;
};

class VeriFuncBlockAssignStmt : public VeriStmt
{
  typedef std::vector<int> IVec;

  public:
    VeriFuncBlockAssignStmt( Uint idx, int line, int col, VeriNode *lval,
                             VeriNode *rval, VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_FUNC_BLOCKING, vsBOTH, line, col, mod )
      , _lval( lval )
      , _rval( rval )
    {}

    virtual ~VeriFuncBlockAssignStmt( void ) { clear(); }

    // accessors
    VeriNode * lval( void ) { return _lval; }
    VeriNode * rval( void ) { return _rval; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int process_func_blocking_assign( void );

  protected:
    VeriFuncBlockAssignStmt( Uint idx, const VeriFuncBlockAssignStmt *copy )
      :  VeriStmt( idx, copy )
      , _lval( NULL )
      , _rval( NULL )
    {}

  private:
    VeriNode * _lval;
    VeriNode * _rval;
};

class VeriBlockStmt : public VeriStmt
{
  typedef std::vector<int> IVec;

  public:
    VeriBlockStmt( Uint idx, int line, int col,
                   VeriNode *lval, VeriNode *event, VeriNode *rval,
                        VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_BLOCKING, vsNONE_FUNC, line, col, mod )
      , _lval( lval )
      , _event( event )
      , _rval( rval )
    {}

    virtual ~VeriBlockStmt( void ) { clear(); }

    // accessors
    VeriNode * lval( void ) { return _lval; } 
    VeriNode * rval( void ) { return _rval; } 
    VeriNode * event( void ) { return _event; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int process_blocking_assignment( void );

  protected:
    VeriBlockStmt( Uint idx, const VeriBlockStmt *copy )
      :  VeriStmt( idx, copy )
      , _lval( NULL )
      , _event( NULL )
      , _rval( NULL )
    {}

  private:
    VeriNode * _lval;
    VeriNode * _event;
    VeriNode * _rval;
};

class VeriNonBlockStmt : public VeriStmt
{
  typedef std::vector<int> IVec;

  public:
    VeriNonBlockStmt( Uint idx, int line, int col,
                      VeriNode *lval, VeriNode *event, VeriNode *rval,
                           VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_NONBLOCKING, vsNONE_FUNC, line, col, mod )
      , _lval( lval )
      , _event( event )
      , _rval( rval )
    {}

    virtual ~VeriNonBlockStmt( void ) { clear(); }

    // accessors
    VeriNode * lval( void ) { return _lval; } 
    VeriNode * rval( void ) { return _rval; } 
    VeriNode * event( void ) { return _event; } 

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int process_non_blocking_assignment( void );

  protected:
    VeriNonBlockStmt( Uint idx, const VeriNonBlockStmt *copy )
      :  VeriStmt( idx, copy )
      , _lval( NULL )
      , _event( NULL )
      , _rval( NULL )
    {}

  private:
    VeriNode * _lval;
    VeriNode * _event;
    VeriNode * _rval;
};

/*
 * Expression should be Assignment-Expression
 * Delay should be Delay-Expression
 * Drive-Strength should be Drive-Expression
 */
class VeriContAssignStmt : public VeriStmt
{
  public:
    VeriContAssignStmt( Uint idx, int line, int col,
                        VeriNode *delay, VeriNode *drive, VeriNode *exp,
                             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_CONT_ASSIGN, vsNONE_FUNC, line, col, mod )
      , _delay( delay )
      , _drive_strength( drive )
      , _exp( exp )
    {}

    virtual ~VeriContAssignStmt( void ) { clear(); }

    // accessors
    VeriNode * exp  ( void ) { return _exp; } 
    VeriNode * delay( void ) { return _delay; } 
    VeriNode * drive_strength( void ) { return _drive_strength; } 

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriContAssignStmt( Uint idx, const VeriContAssignStmt *copy )
      :  VeriStmt( idx, copy )
      , _delay( NULL )
      , _drive_strength( NULL )
      , _exp( NULL )
    {}

  private:
    VeriNode *_delay;
    VeriNode *_drive_strength;
    VeriNode *_exp;
};

class VeriInitStmt : public VeriStmt
{
  public:
    VeriInitStmt( Uint idx, int line, int col, VeriNode *stmt,
             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_INIT, vsNONE_FUNC, line, col, mod )
      , _stmt( stmt )
    {}

    virtual ~VeriInitStmt( void ) { clear(); }

    // accessors
    VeriNode * stmt( void ) { return _stmt; } 

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriInitStmt( Uint idx, const VeriInitStmt *copy )
      :  VeriStmt( idx, copy )
      , _stmt( NULL )
    {}

  private:
    VeriNode *_stmt;
};

class VeriAlwaysStmt : public VeriStmt
{
  public:
    VeriAlwaysStmt( Uint idx, int line, int col, VeriNode *stmt,
             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_ALWAYS, vsNONE_FUNC, line, col, mod )
      , _stmt( stmt )
    {}

    virtual ~VeriAlwaysStmt( void ) { clear(); }

    // accessors
    VeriNode * stmt( void ) { return _stmt; } 

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriAlwaysStmt( Uint idx, const VeriAlwaysStmt *copy )
      :  VeriStmt( idx, copy )
      , _stmt( NULL )
    {}

  private:
    VeriNode *_stmt;
};

class VeriModuleItemList : public VeriStmt
{
  public:
    VeriModuleItemList( Uint idx, int line, int col,
             VeriMod *mod )
      :  VeriStmt( idx, vsSTMT_MOD_ITEM_LIST, vsNONE_FUNC, line, col, mod )
    {}

    virtual ~VeriModuleItemList() { clear(); }

    // accessors
    NodeList & item_list( void ) { return _item_list; }

    // mutators
    void append_item( VeriNode * node ) { _item_list.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriModuleItemList( Uint idx, const VeriModuleItemList *copy )
      :  VeriStmt( idx, copy )
    {}

  private:
    NodeList _item_list;
};

};
#endif // PARSER_VERILOG_VERI_STMT_HPP_
