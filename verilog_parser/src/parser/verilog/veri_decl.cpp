/*
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : veri_decl.cpp
 * Summary  : verilog syntax analyzer :: declaration
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2012.11.20
 */

#include <assert.h>

#include "general_err.hpp"
#include "veri_err_token.hpp"

#include "veri_symbol_table.hpp"

#include "veri_num.hpp"
#include "veri_exp.hpp"
#include "veri_decl.hpp"
#include "veri_tfmg.hpp"

#include "veri_module.hpp"

#include "veri_parser.hpp"

#include <cmath>

namespace rtl
{

    /****************************
     * Tf-Declaration Statement *
     ****************************/

void
VeriTfDecl::clean_up( void )
{
  set_pstatus( vSTART_S );
}

int
VeriTfDecl::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;

  if( _range != NULL )
  {
    mqueue.push( _range );
    iqueue.push( depth );
  }

  NodeList::iterator it = _port_ids.begin();
  for( ; it != _port_ids.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }

  return OK;
}

int
VeriTfDecl::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_var_type( _net_type );
  ofile << dot_str_in_out( _inout );
  ofile << dot_str_is_tf( "SIGNED", _is_signed );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _range );
  NodeList::iterator it = _port_ids.begin();
  for( ; it != _port_ids.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriTfDecl::check_child_validation( void )
{
  if( _range != NULL )
  {
    if( _range->detail_type() != veEXP_RANGE )
    {
      veri_err->append_err( ERR_ERROR, _range->line(), _range->col(),
                            vCHILD_TYPE_TF_DECL );
      return vCHILD_TYPE_TF_DECL;
    }
  }

  NodeList::iterator it = _port_ids.begin();
  for( ; it != _port_ids.end(); ++it )
  {
    if( (*it)->detail_type() != veEXP_PRI )
    {
      veri_err->append_err( ERR_ERROR, (*it)->line(),
                            (*it)->col(), vCHILD_TYPE_TF_DECL );
      return vCHILD_TYPE_TF_DECL;
    }
  }

  return OK;
}

int
VeriTfDecl::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vDECL_S );
      return re;
      break;

    case vDECL_S:
      re = declare_tf_symbol( );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return OK;
      break;
  }
}

int
VeriTfDecl::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _port_ids.rbegin();
  for( ; it != _port_ids.rend(); ++it )
  {
    //(*it)->clean_up();
    (*it)->set_is_decl( IS_TRUE );
    (*it)->set_is_trash( IS_TRUE );
    (*it)->set_scope( scope() );
    mstack.push_back( *it );
  }

  if( _range != NULL )
  {
    //_range->clean_up();
    _range->set_is_trash( IS_TRUE );
    _range->set_scope( scope() );
    mstack.push_back( _range );
  }
  return OK;
}

int
VeriTfDecl::declare_tf_symbol( void )
{
  int dstart = 0;
  int dend   = 0;

  if( _range != NULL )
  {
    VeriRangeExp *range = static_cast<VeriRangeExp *>(_range);
    dstart = range->range_from();
    dend   = range->range_to();
  }

  VeriPrimaryExp *pexp = NULL;
  VeriTfVar *symbol = NULL;
  NodeList::iterator inl = _port_ids.begin();
  for( ; inl != _port_ids.end(); ++inl )
  {
    pexp = static_cast<VeriPrimaryExp *>(*inl);
    symbol = reg_tfvar_symbol( (*inl)->line(), (*inl)->col(),
                              pexp->str(), dstart, dend, *inl );
    pexp->set_var( symbol );
  }

  return OK;
}

VeriTfVar *
VeriTfDecl::reg_tfvar_symbol( int line, int col, CString& name, int dstart,
                              int dend, VeriNode *decl )
{
  VeriNode * node = scope()->value_same_level( name );
  if( node != NULL )
  {
    veri_err->append_err( ERR_ERROR, decl, vTF_PORT_REDEFINE );
    return NULL;
  }

  VeriNum *init = NULL;
  init = new VeriNum( veri_parser->next_id(), decl->line(),
                      decl->col(), birth_mod());
  init->init_val( _is_signed, abs(dend-dstart)+1 );
  init->set_father( decl );
  veri_parser->insert_node( init );

  VeriTfVar * var = new VeriTfVar( veri_parser->next_id(), line, col,
                                   name, _inout, _net_type, _is_signed,
                                   decl, dstart, dend, init, birth_mod() );
  veri_parser->insert_node( var );

  var->set_father( this );
  var->set_scope( scope() );

  scope()->insert( name, var );
  return var;
}

int
VeriTfDecl::push_for_replacement( NodeVec& mstack )
{
  if( _range != NULL )
    mstack.push_back( _range );

  NodeList::iterator it = _port_ids.begin();
  for( ; it != _port_ids.end(); ++it )
    mstack.push_back( *it );

  return OK;
}

int
VeriTfDecl::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriTfDecl *tmp = new VeriTfDecl( idx, this );

  // begin replacement
  NodeList::iterator it = _port_ids.begin();
  NodeList::iterator itl = inl; 
  for( ; it != _port_ids.end(); ++it )
  {
    tmp->_port_ids.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  if( _range != NULL )
  {
    tmp->_range = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
  }

  *inl = tmp;
  return OK;
}

    /****************************
     * Task-Port-List Statement *
     ****************************/

void
VeriTaskPortList::clean_up( void )
{
  set_pstatus( vSTART_S );
}

int
VeriTaskPortList::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;
  NodeList::iterator it = _task_port.begin();
  for( ; it != _task_port.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }
  return _task_port.size();
}

int
VeriTaskPortList::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  NodeList::iterator it = _task_port.begin();
  for( ; it != _task_port.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriTaskPortList::check_child_validation( void )
{
  return OK;
}

int
VeriTaskPortList::check_last_child_validation( void )
{
  VeriNode * last = _task_port.back();

  if( last == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),col(), vCHILD_TYPE_TASK_PORT_LIST );
    return vCHILD_TYPE_TASK_PORT_LIST;
  }
   
  if( last->detail_type() != vsDECL_TF_DECL )
  {
    veri_err->append_err( ERR_ERROR, last->line(),
                          last->col(), vCHILD_TYPE_TASK_PORT_LIST );
    return vCHILD_TYPE_TASK_PORT_LIST;
  }
  return OK;
}

int
VeriTaskPortList::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return do_nothing( );
      break;
  }
}

int
VeriTaskPortList::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _task_port.rbegin();
  for( ; it != _task_port.rend(); ++it )
  {
    //(*it)->clean_up();
    (*it)->set_is_decl( IS_TRUE );
    (*it)->set_is_trash( IS_TRUE );
    (*it)->set_scope( scope() );
    mstack.push_back( *it );
  }
  return OK;
}

int
VeriTaskPortList::do_nothing( void )
{
  return OK;
}

int
VeriTaskPortList::push_for_replacement( NodeVec& mstack )
{
  NodeList::iterator it = _task_port.begin();
  for( ; it != _task_port.end(); ++it )
    mstack.push_back( *it );

  return OK;
}

int
VeriTaskPortList::replace_node( Uint idx, NodeList& node_list,
                                NodeList::iterator& inl)
{
  // spawn duplicant
  VeriTaskPortList *tmp = new VeriTaskPortList( idx, this );

  // begin replacement
  NodeList::iterator it = _task_port.begin();
  NodeList::iterator itl = inl; 
  for( ; it != _task_port.end(); ++it )
  {
    tmp->_task_port.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  *inl = tmp;
  return OK;
}

    /****************************
     * Func-Port-List Statement *
     ****************************/

void
VeriFuncPortList::clean_up( void )
{
  set_pstatus( vSTART_S );
}

int
VeriFuncPortList::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;
  NodeList::iterator it = _func_port.begin();
  for( ; it != _func_port.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }
  return _func_port.size();
}

int
VeriFuncPortList::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  NodeList::iterator it = _func_port.begin();
  for( ; it != _func_port.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriFuncPortList::check_child_validation( void )
{
  return OK;
}

int
VeriFuncPortList::check_last_child_validation( void )
{
  VeriNode * last = _func_port.back();
  if( last == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),col(), vCHILD_TYPE_FUNC_PORT_LIST );
    return vCHILD_TYPE_FUNC_PORT_LIST;
  }

  if( last->detail_type() != vsDECL_TF_DECL )
  {
    veri_err->append_err( ERR_ERROR, last->line(),
                          last->col(), vCHILD_TYPE_FUNC_PORT_LIST );
    return vCHILD_TYPE_FUNC_PORT_LIST;
  }

  VeriTfDecl *tf = static_cast<VeriTfDecl *>(last);
  if( tf->inout() != vpIN )
  {
    veri_err->append_err( ERR_ERROR, last->line(),
                          last->col(), vCHILD_TYPE_FUNC_PORT_LIST );
    return vCHILD_TYPE_FUNC_PORT_LIST;
  }

  return OK;
}

int
VeriFuncPortList::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return do_nothing( );
      break;
  }
}

int
VeriFuncPortList::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _func_port.rbegin();
  for( ; it != _func_port.rend(); ++it )
  {
    //(*it)->clean_up();
    (*it)->set_is_decl( IS_TRUE );
    (*it)->set_is_trash( IS_TRUE );
    (*it)->set_scope( scope() );
    mstack.push_back( *it );
  }
  return OK;
}

int
VeriFuncPortList::do_nothing( void )
{
  return OK;
}

int
VeriFuncPortList::push_for_replacement( NodeVec& mstack )
{
  NodeList::iterator it = _func_port.begin();
  for( ; it != _func_port.end(); ++it )
    mstack.push_back( *it );

  return OK;
}

int
VeriFuncPortList::replace_node( Uint idx, NodeList& node_list,
                                NodeList::iterator& inl)
{
  // spawn duplicant
  VeriFuncPortList *tmp = new VeriFuncPortList( idx, this );

  // begin replacement
  NodeList::iterator it = _func_port.begin();
  NodeList::iterator itl = inl; 
  for( ; it != _func_port.end(); ++it )
  {
    tmp->_func_port.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  *inl = tmp;
  return OK;
}

    /****************************************
     * Task-Item-Declaration-List Statement *
     ****************************************/

void
VeriTaskItemDeclList::clean_up( void )
{
  set_pstatus( vSTART_S );
}

int
VeriTaskItemDeclList::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;
  NodeList::iterator it = _task_item.begin();
  for( ; it != _task_item.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }
  return _task_item.size();
}

int
VeriTaskItemDeclList::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  NodeList::iterator it = _task_item.begin();
  for( ; it != _task_item.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriTaskItemDeclList::check_child_validation( void )
{
  return OK;
}

int
VeriTaskItemDeclList::check_last_child_validation( void )
{
  VeriNode * last = _task_item.back();
  if( last == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_TASK_ITEM_DECL_LIST );
    return vCHILD_TYPE_TASK_ITEM_DECL_LIST;
  }
   
  if( last->tok_type() != vtDECL )
  {
    veri_err->append_err( ERR_ERROR, last->line(),
                          last->col(), vCHILD_TYPE_TASK_ITEM_DECL_LIST );
    return vCHILD_TYPE_TASK_ITEM_DECL_LIST;
  }
  return OK;
}

int
VeriTaskItemDeclList::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return do_nothing( );
      break;
  }
}

int
VeriTaskItemDeclList::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _task_item.rbegin();
  for( ; it != _task_item.rend(); ++it )
  {
    //(*it)->clean_up();
    (*it)->set_is_decl( IS_TRUE );
    (*it)->set_is_trash( IS_TRUE );
    (*it)->set_scope( scope() );
    mstack.push_back( *it );
  }
  return OK;
}

int
VeriTaskItemDeclList::do_nothing( void )
{
  return OK;
}

int
VeriTaskItemDeclList::push_for_replacement( NodeVec& mstack )
{
  NodeList::iterator it = _task_item.begin();
  for( ; it != _task_item.end(); ++it )
    mstack.push_back( *it );

  return OK;
}

int
VeriTaskItemDeclList::replace_node( Uint idx, NodeList& node_list,
                                    NodeList::iterator& inl)
{
  // spawn duplicant
  VeriTaskItemDeclList *tmp = new VeriTaskItemDeclList( idx, this );

  // begin replacement
  NodeList::iterator it = _task_item.begin();
  NodeList::iterator itl = inl; 
  for( ; it != _task_item.end(); ++it )
  {
    tmp->_task_item.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  *inl = tmp;
  return OK;
}

    /*****************************************
     * Block-Item-Declaration-List Statement *
     *****************************************/

void
VeriBlockItemDeclList::clean_up( void )
{
  set_pstatus( vSTART_S );
}

int
VeriBlockItemDeclList::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;
  NodeList::iterator it = _block_item.begin();
  for( ; it != _block_item.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriBlockItemDeclList::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  NodeList::iterator it = _block_item.begin();
  for( ; it != _block_item.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriBlockItemDeclList::check_child_validation( void )
{
  return OK;
}

int
VeriBlockItemDeclList::check_last_child_validation( void )
{
  VeriNode * last = _block_item.back();
  if( last == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_BLOCK_ITEM_DECL_LIST );
    return vCHILD_TYPE_BLOCK_ITEM_DECL_LIST;
  }
  if( last->tok_type() != vtDECL )
  {
    veri_err->append_err( ERR_ERROR, last->line(),
                          last->col(), vCHILD_TYPE_BLOCK_ITEM_DECL_LIST );
    return vCHILD_TYPE_BLOCK_ITEM_DECL_LIST;
  }
  return OK;
}

int
VeriBlockItemDeclList::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return do_nothing( );
      break;
  }
}

int
VeriBlockItemDeclList::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _block_item.rbegin();
  for( ; it != _block_item.rend(); ++it )
  {
    //(*it)->clean_up();
    (*it)->set_is_decl( IS_TRUE );
    (*it)->set_is_trash( IS_TRUE );
    (*it)->set_scope( scope() );
    mstack.push_back( *it );
  }
  return OK;
}

int
VeriBlockItemDeclList::do_nothing( void )
{
  return OK;
}

int
VeriBlockItemDeclList::push_for_replacement( NodeVec& mstack )
{
  NodeList::iterator it = _block_item.begin();
  for( ; it != _block_item.end(); ++it )
    mstack.push_back( *it );

  return OK;
}

int
VeriBlockItemDeclList::replace_node( Uint idx, NodeList& node_list,
                                     NodeList::iterator& inl)
{
  // spawn duplicant
  VeriBlockItemDeclList *tmp = new VeriBlockItemDeclList( idx, this );

  // begin replacement
  NodeList::iterator it = _block_item.begin();
  NodeList::iterator itl = inl; 
  for( ; it != _block_item.end(); ++it )
  {
    tmp->_block_item.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  *inl = tmp;
  return OK;
}

    /********************************************
     * Function-Item-Declaration-List Statement *
     ********************************************/

void
VeriFuncItemDeclList::clean_up( void )
{
  set_pstatus( vSTART_S );
}

int
VeriFuncItemDeclList::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;
  NodeList::iterator it = _func_item.begin();
  for( ; it != _func_item.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriFuncItemDeclList::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  NodeList::iterator it = _func_item.begin();
  for( ; it != _func_item.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriFuncItemDeclList::check_child_validation( void )
{
  return OK;
}

int
VeriFuncItemDeclList::check_last_child_validation( void )
{
  VeriNode * last = _func_item.back();
  if( last == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_FUNC_ITEM_DECL_LIST );
    return vCHILD_TYPE_FUNC_ITEM_DECL_LIST;
  }
  if( last->tok_type() != vtDECL )
  {
    veri_err->append_err( ERR_ERROR, last->line(),
                          last->col(), vCHILD_TYPE_FUNC_ITEM_DECL_LIST );
    return vCHILD_TYPE_FUNC_ITEM_DECL_LIST;
  }
  return OK;
}

int
VeriFuncItemDeclList::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return do_nothing( );
      break;
  }
}

int
VeriFuncItemDeclList::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _func_item.rbegin();
  for( ; it != _func_item.rend(); ++it )
  {
    //(*it)->clean_up( );
    (*it)->set_is_decl( IS_TRUE );
    (*it)->set_is_trash( IS_TRUE );
    (*it)->set_scope( scope() );
    mstack.push_back( *it );
  }
  return OK;
}

int
VeriFuncItemDeclList::do_nothing( void )
{
  return OK;
}

int
VeriFuncItemDeclList::push_for_replacement( NodeVec& mstack )
{
  NodeList::iterator it = _func_item.begin();
  for( ; it != _func_item.end(); ++it )
    mstack.push_back( *it );

  return OK;
}

int
VeriFuncItemDeclList::replace_node( Uint idx, NodeList& node_list,
                                     NodeList::iterator& inl)
{
  // spawn duplicant
  VeriFuncItemDeclList *tmp = new VeriFuncItemDeclList( idx, this );

  // begin replacement
  NodeList::iterator it = _func_item.begin();
  NodeList::iterator itl = inl; 
  for( ; it != _func_item.end(); ++it )
  {
    tmp->_func_item.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  *inl = tmp;
  return OK;
}

    /********************
     * Task-Declaration *
     ********************/

void
VeriTaskDecl::clean_up( void )
{
  set_pstatus( vSTART_S );
  _act_port_num = 0;
  _act_ports.clear();
}

int
VeriTaskDecl::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;

  if( _task_port != NULL )
  {
    mqueue.push( _task_port );
    iqueue.push( depth );
  }
  if( _item_decl != NULL )
  {
    mqueue.push( _item_decl );
    iqueue.push( depth );
  }

  mqueue.push( _stmt );
  iqueue.push( depth );

  return OK;
}

int
VeriTaskDecl::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_name( _task_name );
  ofile << dot_str_is_tf( "AUTO", _is_auto );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _task_port );
  ofile << dot_str_conn( idx(), _item_decl );
  ofile << dot_str_conn( idx(), _stmt );

  return OK;
}

int
VeriTaskDecl::check_child_validation( void )
{
  if( _task_name.empty() )
  {
    veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_TASK_DECL );
    return vCHILD_TYPE_TASK_DECL;
  }

  if( _task_port != NULL )
  {
    if( _task_port->detail_type() != vsDECL_TASK_PORT_LIST )
    {
      veri_err->append_err( ERR_ERROR, _task_port->line(),
                            _task_port->col(), vCHILD_TYPE_TASK_DECL );
      return vCHILD_TYPE_TASK_DECL;
    }
  }

  if( _item_decl != NULL )
  {
    if( _item_decl->tok_type() != vtDECL )
    {
      veri_err->append_err( ERR_ERROR, _item_decl->line(),
                            _item_decl->col(), vCHILD_TYPE_TASK_DECL );
      return vCHILD_TYPE_TASK_DECL;
    }
  }

  if( _stmt == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_TASK_DECL );
    return vCHILD_TYPE_TASK_DECL;
  }
   
  if( _stmt->tok_type() != vtSTMT )
  {
    veri_err->append_err( ERR_ERROR, _stmt->line(),
                          _stmt->col(), vCHILD_TYPE_TASK_DECL );
    return vCHILD_TYPE_TASK_DECL;
  }

  return OK;
}

int
VeriTaskDecl::search_for_symbol( NodeVec& /*seq*/, String& /*help_info*/ )
{
  return reg_task_symbol( );
}

int
VeriTaskDecl::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_ports( mstack );
      set_pstatus( vGET_CALL_EXP_S );
      return re;
      break;

    case vGET_CALL_EXP_S :
      re = get_call_exp( );
      if( re != OK ) return re;

      if(    father() != NULL
          && father()->detail_type() != veEXP_CALLER )
      {
        set_pstatus( vEND_S );
        return reg_task_symbol( );
      }
      else
      {
        set_pstatus( vEND_S );
        return push_contents( mstack );
      }
      break;

    default:
      return finish_scope( );
      break;
  }
}

int
VeriTaskDecl::reg_task_symbol( void )
{
  VeriNode * node_out = scope()->floater()->value_same_level( _task_name );
  VeriNode * node = scope()->value_same_level( _task_name );
  if( node != NULL || node_out != NULL )
  {
    if( node->tok_type( ) == vtTASK && node_out->tok_type( ) == vtTASK )
    {
      VeriTask *task     = static_cast<VeriTask *>(node);
      VeriTask *task_out = static_cast<VeriTask *>(node_out);
      
      if( task->task() != this || task_out->task() != this )
      {
        veri_err->append_err( ERR_ERROR, this, vTASK_REDEFINE );
        return vTASK_REDEFINE;
      }
      else
        return OK;
    }
    else
    {
      veri_err->append_err( ERR_ERROR, this, vTASK_REDEFINE );
      return vTASK_REDEFINE;
    }
  }

  VeriTask * symbol = new VeriTask( veri_parser->next_id(), line(), col(),
                                    _task_name, this, birth_mod() ); 
  veri_parser->insert_node( symbol );
  symbol->set_father( this );
  symbol->set_scope( scope() );

  scope()->insert( _task_name, symbol );
  scope()->floater()->insert( _task_name, symbol );
  return OK;
}

int
VeriTaskDecl::push_ports( NodeVec& mstack )
{
  mstack.push_back( this );

  set_scope( scope()->append_scope( _task_name, vTASK_SCOPE ) );

  if( _item_decl != NULL )
  {
    //_item_decl->clean_up( );
    _item_decl->set_is_decl( IS_TRUE );
    _item_decl->set_is_trash( IS_TRUE );
    _item_decl->set_scope( scope( ) );
    mstack.push_back( _item_decl );
  }

  if( _task_port != NULL )
  {
    //_task_port->clean_up( );
    _task_port->set_is_decl( IS_TRUE );
    _task_port->set_is_trash( IS_TRUE );
    _task_port->set_scope( scope() );
    mstack.push_back( _task_port );
  }

  return OK;
}

int
VeriTaskDecl::get_call_exp( void )
{
  assert( father() != NULL );
  if( father()->detail_type() != veEXP_CALLER )
    return OK;

  int re = prep_ports_for_check_type( );
  if( re != OK )
    return re;

  VeriCallerExp *caller = static_cast<VeriCallerExp *>(father());
  if( caller->arg() == NULL )
    return OK;

  if( caller->arg()->detail_type() != veEXP_MULTI )
    return OK;

  VeriMultiExp *arg = static_cast<VeriMultiExp *>(caller->arg());
  if( arg->exp_num( ) != _act_port_num )
  {
    veri_err->append_err( ERR_ERROR, this, vSEC_CALLER_PORT_MISMATCH );
    return vSEC_CALLER_PORT_MISMATCH;
  }

  NodeList &port_exp = arg->exp();

  NodeList::iterator iexp  = port_exp.begin();
  NodeList::iterator iport = _act_ports.begin();

  VeriExp *exp = NULL;
  for( ; iexp != port_exp.end(); ++iexp, ++iport )
  {
    if( *iexp == NULL || *iport == NULL )
    {
      veri_err->append_err( ERR_ERROR, father(), vSEC_CALLER_PORT_MISMATCH );
      return vSEC_CALLER_PORT_MISMATCH;
    }

    exp = static_cast<VeriExp *>(*iexp);

    // check type match
    re = check_type_ref( exp, *iport, IS_FALSE );
    if( re != OK )
      return re; 
  }
  return OK;
}

int
VeriTaskDecl::prep_ports_for_check_type( void )
{
  if( _task_port != NULL )
  {
    if( _task_port->detail_type( ) != vsDECL_TASK_PORT_LIST )
      return vSEC_CALLER_PORT_MISMATCH;

    VeriTaskPortList * port_list
      = static_cast<VeriTaskPortList *>(_task_port);
    NodeList &tf_decl_list = port_list->task_port();

    NodeList::iterator idecl = tf_decl_list.begin();
    NodeList::iterator iport;

    VeriPrimaryExp *pexp = NULL;
    for( ; idecl != tf_decl_list.end(); ++idecl )
    {
      NodeList &port_ids = (static_cast<VeriTfDecl *>(*idecl))->port_ids();
      iport = port_ids.begin();
      for( ; iport != port_ids.end(); ++iport )
      {
        pexp = static_cast<VeriPrimaryExp *>(*iport);
        if( pexp->var() == NULL && pexp->calc_value( ) == NULL )
        {
          veri_err->append_err( ERR_ERROR, this, vSEC_CALLER_PORT_NOT_COMPLETE );
          return vSEC_CALLER_PORT_NOT_COMPLETE;
        }
        _act_ports.push_back( pexp->var() );
        _act_port_num++;
      }
    }
  }
  else
  {
    if( _item_decl->detail_type( ) != vsDECL_TASK_ITEM_DECL_LIST )
      return OK;

    VeriTaskItemDeclList * item_list
      = static_cast<VeriTaskItemDeclList *>(_item_decl);
    NodeList &decl_list = item_list->task_item();

    NodeList::iterator idecl = decl_list.begin();
    NodeList::iterator iport;

    VeriPrimaryExp *pexp = NULL;
    for( ; idecl != decl_list.end(); ++idecl )
    {
      if( (*idecl)->detail_type( ) == vsDECL_TF_DECL )
      {
        NodeList &port_ids = (static_cast<VeriTfDecl *>(*idecl))->port_ids();
        iport = port_ids.begin();
        for( ; iport != port_ids.end(); ++iport )
        {
          pexp = static_cast<VeriPrimaryExp *>(*iport);
          if( pexp->var() == NULL && pexp->calc_value( ) == NULL )
          {
            veri_err->append_err( ERR_ERROR, this, vSEC_CALLER_PORT_NOT_COMPLETE );
            return vSEC_CALLER_PORT_NOT_COMPLETE;
          }
          _act_ports.push_back( pexp->var() );
          _act_port_num++;
        }
      }
    }
  }
  return OK;
}

/*
 *  exp and var should not be null, garanteed by caller
 */
int
VeriTaskDecl::check_type_ref( VeriExp *exp, VeriNode *port, int no_assign )
{
  int re = OK;

  VeriVar    * var      = NULL;
  VeriTfVar  * tfvar    = NULL;
  VeriParam  * param    = NULL;
  VeriInt    * int_var  = NULL;
  VeriGenVar * genvar   = NULL;
  VeriVar    * svar     = NULL;
  VeriParam  * sparam   = NULL;
  VeriTfVar  * stfvar   = NULL;
  VeriInt    * sint_var = NULL;
  VeriGenVar * sgenvar  = NULL;

  switch( port->tok_type( ) )
  {
    case vtVAR    : var     = static_cast<VeriVar    *>(port); break;
    case vtTFVAR  : tfvar   = static_cast<VeriTfVar  *>(port); break;
    case vtPARAM  : param   = static_cast<VeriParam  *>(port); break;
    case vtINT    : int_var = static_cast<VeriInt    *>(port); break;
    case vtGENVAR : genvar  = static_cast<VeriGenVar *>(port); break;
    default : return vSEC_CALLER_PORT_MISMATCH;
  }

  IVec dummy_dimen;

  switch( exp->detail_type( ) )
  {
    case veEXP_PRI :
    {
      VeriPrimaryExp *pexp = static_cast<VeriPrimaryExp *>(exp);
      if( pexp->var( ) == NULL )
      {
        if( pexp->pri_type( ) == veNUM )
        {
          switch( port->tok_type( ) )
          {
            case vtVAR :
              if( var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(), pexp->calc_value() );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;

            case vtTFVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(), pexp->calc_value() );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(), pexp->calc_value() );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                param->set_val( num );
              }
              return OK;
              break;

            case vtGENVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(), pexp->calc_value() );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( int_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(), pexp->calc_value() );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;
          }
        }
        return OK;
        break;
      }

      switch( pexp->var()->tok_type( ) )
      {
        case vtVAR :
          svar = static_cast<VeriVar *>(pexp->var());
          switch( port->tok_type( ) )
          {
            case vtVAR :
              if( var->range_depth( ) != svar->range_depth( ) )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = svar->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;

            case vtTFVAR :
              if( svar->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = svar->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( svar->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = svar->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                param->set_val( num );
              }
              return OK;
              break;

            case vtGENVAR :
              if( svar->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = svar->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( int_var->range_depth( ) != svar->range_depth( ) )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE && svar->range_depth( ) == 0 )
              {
                VeriNum *num = svar->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;
          }
          break;

        case vtTFVAR :
          stfvar = static_cast<VeriTfVar *>(pexp->var());
          switch( port->tok_type( ) )
          {
            case vtVAR :
              if( var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = stfvar->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;

            case vtTFVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = stfvar->get_val( );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = stfvar->get_val( );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                param->set_val( num );
              }
              return OK;
              break;

            case vtGENVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = stfvar->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( int_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = stfvar->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;
          }
          break;

        case vtPARAM :
          sparam = static_cast<VeriParam *>(pexp->var());
          switch( port->tok_type( ) )
          {
            case vtVAR :
              if( var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sparam->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;

            case vtTFVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = sparam->get_val( );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = sparam->get_val( );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                param->set_val( num );
              }
              return OK;
              break;

            case vtGENVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = sparam->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( int_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sparam->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;
          }
          break;

        case vtINT :
          sint_var = static_cast<VeriInt *>(pexp->var());
          switch( port->tok_type( ) )
          {
            case vtVAR :
              if( var->range_depth( ) != sint_var->range_depth( ) )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE && sint_var->range_depth( ) == 0 )
              {
                VeriNum *num = sint_var->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;

            case vtTFVAR :
              if( sint_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sint_var->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( sint_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sint_var->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                param->set_val( num );
              }
              return OK;
              break;

            case vtGENVAR :
              if( sint_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sint_var->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( int_var->range_depth( ) != sint_var->range_depth( ) )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE && sint_var->range_depth( ) == 0 )
              {
                VeriNum *num = sint_var->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;
          }
          break;

        case vtGENVAR :
          sgenvar = static_cast<VeriGenVar *>(pexp->var());
          switch( port->tok_type( ) )
          {
            case vtVAR :
              if( var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sgenvar->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;

            case vtTFVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = sgenvar->get_val( );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = sgenvar->get_val( );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                param->set_val( num );
              }
              return OK;
              break;

            case vtGENVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = sgenvar->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( int_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sgenvar->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;
          }
          break;

        default:
          break;
      }
      break;
    }

    case veEXP_UNA :
    case veEXP_BIN :
    case veEXP_SIGN :
    case veEXP_UNSIGN :
    case veEXP_MCONCAT :
    {
      switch( port->tok_type( ) )
      {
        case vtVAR :
        {
          if( var->range_depth( ) == 0 )
          {
            return OK;
          }
          else
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          break;
        }

        case vtTFVAR :
          if( no_assign == IS_FALSE )
          {
            VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value( ) );
            veri_parser->insert_node( num );
            num->set_father( tfvar );
            tfvar->set_val( num );
          }
          return OK;
          break;

        case vtPARAM :
          if( no_assign == IS_FALSE )
          {
            VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value( ) );
            veri_parser->insert_node( num );
            num->set_father( tfvar );
            param->set_val( num );
          }
          return OK;
          break;
        
        case vtGENVAR :
          if( no_assign == IS_FALSE )
          {
            VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value( ) );
            veri_parser->insert_node( num );
            num->set_father( tfvar );
            genvar->set_val( num );
          }
          return OK;
          break;

        case vtINT :
          if( var->range_depth( ) == 0 )
          {
            if( no_assign == IS_FALSE )
            {
              VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value( ) );
              veri_parser->insert_node( num );
              num->set_father( tfvar );
              int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
            }
            return OK;
          }
          else
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          break;

        default:
          return vSEC_CALLER_PORT_MISMATCH;
      }
      break;
    }

    case veEXP_ARRAY :
    {
      VeriArrayExp *aexp = static_cast<VeriArrayExp *>(exp);
      VeriPrimaryExp *pexp = static_cast<VeriPrimaryExp *>(aexp->id());

      if( pexp->var() != NULL )
        return vSEC_CALLER_PORT_MISMATCH;

      switch( port->tok_type( ) )
      {
        case vtPARAM : case vtGENVAR :
          veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
          return vSEC_CALLER_PORT_MISMATCH;

        case vtVAR :
          if( aexp->range_depth( ) > var->range_depth( ) )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          return OK;
          break;
        
        case vtINT :
          if( aexp->range_depth( ) > int_var->range_depth( ) )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          else
          {
            if( no_assign == IS_FALSE )
            {
              VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value( ) );
              veri_parser->insert_node( num );
              num->set_father( tfvar );
              int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
            }
            return OK;
          }
          break;
      }
      return OK;
      break;
    }

    case veEXP_NAMED_PARAM :
    {
      VeriNamedParamExp * nexp = static_cast<VeriNamedParamExp *>(exp);
      switch( port->tok_type( ) )
      {
        case vtVAR :
          if( var->name( ) != nexp->ref() )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          return OK;
          break;

        case vtTFVAR :
          if( tfvar->name( ) != nexp->ref() )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          return OK;
         break;

        case vtPARAM :
          if( param->name( ) != nexp->ref() )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          return OK;
         break;
        
        case vtGENVAR :
          if( genvar->name( ) != nexp->ref() )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          return OK;
          break;

        case vtINT :
          if( int_var->name( ) != nexp->ref() )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          return OK;
          break;
      }

      VeriExp *inner_exp = static_cast<VeriExp *>(nexp->port_exp());
      return check_type_ref( inner_exp, port, IS_FALSE );
      break;
    }

    case veEXP_CALLER :
    {
      VeriCallerExp *cexp = static_cast<VeriCallerExp *>(exp);
      if( cexp->call_type() == vtTASK_CALL )
      {
        veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
        return vSEC_CALLER_PORT_MISMATCH;
      }
      else
      {
        assert( cexp->hive()->detail_type() == vsDECL_FUNC_DECL );
        VeriFuncDecl *func = static_cast<VeriFuncDecl *>(cexp->hive());
        if( func->ret_value() != NULL )
        {
          VeriFunc *ret_value = func->ret_value();
          switch( port->tok_type( ) )
          {
            case vtVAR :
            {
              if( var->range_depth( ) == 0 )
              {
                return OK;
              }
              else
              {
                veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
                return vSEC_CALLER_PORT_MISMATCH;
              }
              break;
            }

            case vtTFVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(),
                                            ret_value->get_val( ) );
                veri_parser->insert_node( num );
                num->set_father( tfvar );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(),
                                            ret_value->get_val( ) );
                veri_parser->insert_node( num );
                num->set_father( tfvar );
                param->set_val( num );
              }
              return OK;
              break;
            
            case vtGENVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(),
                                            ret_value->get_val( ) );
                veri_parser->insert_node( num );
                num->set_father( tfvar );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( var->range_depth( ) == 0 )
              {
                if( no_assign == IS_FALSE )
                {
                  VeriNum *num = new VeriNum( veri_parser->next_id(),
                                              ret_value->get_val( ) );
                  veri_parser->insert_node( num );
                  num->set_father( tfvar );
                  int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
                }
                return OK;
              }
              else
              {
                veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
                return vSEC_CALLER_PORT_MISMATCH;
              }
              break;

            default:
              return vSEC_CALLER_PORT_MISMATCH;
          }
        }
        else
        {
           veri_err->append_err( ERR_ERROR, exp, vSEC_FUNC_NO_RET );
           return vSEC_FUNC_NO_RET;
        }
      }
    }
    break;

    case veEXP_MINTY :
    {
      VeriMintypmaxExp *pmax = static_cast<VeriMintypmaxExp *>(exp);
      VeriExp *exp1 = static_cast<VeriExp *>(pmax->exp_child1( ) );
      VeriExp *exp2 = static_cast<VeriExp *>(pmax->exp_child2( ) );
      VeriExp *exp3 = static_cast<VeriExp *>(pmax->exp_child3( ) );

      re = check_type_ref( exp1, port, IS_TRUE );
      if( re != OK )
        return re;

      re = check_type_ref( exp2, port, IS_TRUE );
      if( re != OK )
        return re;
         
      return check_type_ref( exp3, port, IS_TRUE );
      break;
    }

    case veEXP_QUES :
    {
      VeriQuesExp *ques = static_cast<VeriQuesExp *>(exp);
      VeriExp  *choice  = static_cast<VeriExp *>(ques->choice ( ) );
      VeriExp  *choice1 = static_cast<VeriExp *>(ques->choice1( ) );
      VeriExp  *choice2 = static_cast<VeriExp *>(ques->choice2( ) );

      if( choice != NULL )
        return check_type_ref( choice, port, IS_FALSE );
      else
      {
        re = check_type_ref( choice1, port, IS_TRUE );
        if( re != OK )
          return re;
           
        return check_type_ref( choice2, port, IS_TRUE );
      }
      break;
    }

    case veEXP_MULTI :
    {
      VeriMultiExp *mexp = static_cast<VeriMultiExp *>(exp);
      NodeList & exp_list = mexp->exp();
      NodeList::iterator it = exp_list.begin();
      for( ; it != exp_list.end(); ++it )
      {
        re = check_type_ref( static_cast<VeriExp *>(*it), port, IS_TRUE );
        if( re != OK )
          return re;
      }
      return OK;
      break;
    }

    default:
      return OK;
      break;
  }
  return OK;
}

int
VeriTaskDecl::push_contents( NodeVec& mstack )
{
  mstack.push_back( this );

  //_stmt->clean_up( );
  _stmt->set_is_trash( IS_TRUE );
  _stmt->set_scope( scope() );
  mstack.push_back( _stmt );

  return OK;
}

int
VeriTaskDecl::finish_scope( void )
{
  scope()->finish_scope();
  return OK;
}

int
VeriTaskDecl::push_for_replacement( NodeVec& mstack )
{
  if( _task_port != NULL )
    mstack.push_back( _task_port );

  if( _item_decl != NULL )
    mstack.push_back( _item_decl );

  mstack.push_back( _stmt );

  return OK;
}

int
VeriTaskDecl::replace_node( Uint idx, NodeList& node_list,
                                     NodeList::iterator& inl)
{
  // spawn duplicant
  VeriTaskDecl *tmp = new VeriTaskDecl( idx, this );

  // begin replacement
  NodeList::iterator itl = inl; 
  tmp->_stmt = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );
  itl = inl;

  if( _item_decl != NULL )
  {
    tmp->_item_decl = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  if( _task_port != NULL )
  {
    tmp->_task_port = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
  }

  *inl = tmp;
  return OK;
}

    /************************
     * Function-Declaration *
     ************************/

void
VeriFuncDecl::clean_up( void )
{
  set_pstatus( vSTART_S );
  _ret = NULL;
  _act_ports.clear();
  _act_port_num = 0;
}

int
VeriFuncDecl::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;
  if( _range     != NULL )
  {
    mqueue.push( _range     );
    iqueue.push( depth     );
  }

  if( _port_list != NULL )
  {
    mqueue.push( _port_list );
    iqueue.push( depth     );
  }

  if( _item_decl != NULL )
  {
    mqueue.push( _item_decl );
    iqueue.push( depth     );
  }

  mqueue.push( _stmt );
  iqueue.push( depth );
  return OK;
}

int
VeriFuncDecl::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_name( _func_name );
  ofile << dot_str_is_tf( "AUTO", _is_auto );
  ofile << dot_str_is_tf( "SIGNED", _is_signed );
  ofile << dot_str_is_tf( "INT", _is_int );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _range );
  ofile << dot_str_conn( idx(), _port_list );
  ofile << dot_str_conn( idx(), _item_decl );
  ofile << dot_str_conn( idx(), _stmt );

  return OK;
}

int
VeriFuncDecl::check_child_validation( void )
{
  if( _func_name.empty() )
  {
    veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_FUNC_DECL );
    return vCHILD_TYPE_FUNC_DECL;
  }

  if( _range != NULL )
  {
    if( _range->detail_type() != veEXP_RANGE )
    {
      veri_err->append_err( ERR_ERROR, _range->line(),
                          _range->col(), vCHILD_TYPE_FUNC_DECL );
      return vCHILD_TYPE_FUNC_DECL;
    }

    VeriRangeExp *rexp = static_cast<VeriRangeExp *>(_range);
    if(rexp->rtype() != vrSEMI )
    {
      veri_err->append_err( ERR_ERROR, _range->line(),
                          _range->col(), vCHILD_TYPE_FUNC_DECL );
      return vCHILD_TYPE_FUNC_DECL;
    }
  }

  if( _port_list != NULL )
  {
    if( _port_list->detail_type() != vsDECL_FUNC_PORT_LIST )
    {
      veri_err->append_err( ERR_ERROR, _port_list->line(),
                          _port_list->col(), vCHILD_TYPE_FUNC_DECL );
      return vCHILD_TYPE_FUNC_DECL;
    }
  }

  if( _item_decl != NULL )
  {
    if( _item_decl->tok_type() != vtDECL )
    {
      veri_err->append_err( ERR_ERROR, _item_decl->line(),
                          _item_decl->col(), vCHILD_TYPE_FUNC_DECL );
      return vCHILD_TYPE_FUNC_DECL;
    }
  }

  if( _stmt == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_FUNC_DECL );
    return vCHILD_TYPE_FUNC_DECL;
  }
   
  if( _stmt->tok_type() != vtSTMT )
  {
    veri_err->append_err( ERR_ERROR, _stmt->line(),
                          _stmt->col(), vCHILD_TYPE_FUNC_DECL );
    return vCHILD_TYPE_FUNC_DECL;
  }

  return OK;
}

int
VeriFuncDecl::search_for_symbol( NodeVec& /*seq*/, String& /*help_info*/ )
{
  return reg_func_symbol( );
}

int
VeriFuncDecl::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_ports( mstack );
      set_pstatus( vGET_CALL_EXP_S );
      return re;
      break;

    case vGET_CALL_EXP_S :
      re = get_call_exp( );
      if( re != OK ) return re;

      if(    father() != NULL
          && father()->detail_type() != veEXP_CALLER )
      {
        set_pstatus( vEND_S );
        return reg_func_symbol( );
      }
      else
      {
        set_pstatus( vEND_S );
        return push_contents( mstack );
      }
      break;

    default:
      return finish_scope( );
      break;
  }
}

int
VeriFuncDecl::reg_func_symbol( void )
{
  VeriNode * node_out = scope()->floater()->value_same_level( _func_name );
  VeriNode * node = scope()->value_same_level( _func_name );

  if( node != NULL || node_out != NULL )
  {
    if( node->tok_type( ) == vtFUNC && node_out->tok_type( ) == vtFUNC )
    {
      VeriFunc *func     = static_cast<VeriFunc *>(node);
      VeriFunc *func_out = static_cast<VeriFunc *>(node_out);
      
      if( func->func() != this || func_out->func() != this )
      {
        veri_err->append_err( ERR_ERROR, this, vFUNC_REDEFINE );
        return vFUNC_REDEFINE;
      }
    }
    else
    {
      veri_err->append_err( ERR_ERROR, this, vFUNC_REDEFINE );
      return vFUNC_REDEFINE;
    }
  }

  int dstart = 0;
  int dend   = 31;

  if( _range != NULL )
  {
    VeriRangeExp *range = static_cast<VeriRangeExp *>(_range);
    dstart = range->range_from();
    dend   = range->range_to();
  }

  VeriNum *init = new VeriNum( veri_parser->next_id( ), line(),
                               col(), birth_mod() );

  init->init_val( _is_signed, abs(dend-dstart)+1 );

  init->set_father( this );
  veri_parser->insert_node( init );

  VeriFunc * symbol = new VeriFunc( veri_parser->next_id(), line(), col(),
                                    _func_name, this, dstart, dend,
                                    _is_signed, _is_auto, init,
                                    birth_mod() ); 
  veri_parser->insert_node( symbol );
  symbol->set_father( this );
  symbol->set_scope( scope() );

  if( node == NULL && node_out == NULL )
  {
    scope()->insert( _func_name, symbol );
    scope()->floater()->insert( _func_name, symbol );
  }
  else
  {
    assert( node != NULL && node_out != NULL );
    veri_parser->toss_node( node->idx( ) );
    scope()->replace_value_same_level( _func_name, symbol );
    scope()->floater()->replace_value_same_level( _func_name, symbol );
  }

  return OK;
}

int
VeriFuncDecl::push_ports( NodeVec& mstack )
{
  mstack.push_back( this );

  set_scope( scope()->append_scope( _func_name, vFUNC_SCOPE ) );

  if( _item_decl != NULL )
  {
    //_item_decl->clean_up( );
    _item_decl->set_is_decl( IS_TRUE );
    _item_decl->set_is_trash( IS_TRUE );
    _item_decl->set_scope( scope() );
    mstack.push_back( _item_decl );
  }

  if( _port_list != NULL )
  {
    //_port_list->clean_up( );
    _port_list->set_is_decl( IS_TRUE );
    _port_list->set_is_trash( IS_TRUE );
    _port_list->set_scope( scope( ) );
    mstack.push_back( _port_list );
  }

  if( _range != NULL )
  {
    //_range->clean_up( );
    _range->set_is_decl( IS_TRUE );
    _range->set_is_trash( IS_TRUE );
    _range->set_scope( scope( ) );
    mstack.push_back( _range );
  }

  return OK;
}

int
VeriFuncDecl::get_call_exp( void )
{
  assert( father() != NULL );
  if( father()->detail_type() != veEXP_CALLER )
    return OK;

  int re = prep_ports_for_check_type( );
  if( re != OK )
    return re;

  VeriCallerExp *caller = static_cast<VeriCallerExp *>(father());
  if( caller->arg() == NULL )
    return OK;

  if( caller->arg()->detail_type() != veEXP_MULTI )
    return OK;

  VeriMultiExp *arg = static_cast<VeriMultiExp *>(caller->arg());
  if( arg->exp_num( ) != _act_port_num )
  {
    veri_err->append_err( ERR_ERROR, this, vSEC_CALLER_PORT_MISMATCH );
    return vSEC_CALLER_PORT_MISMATCH;
  }

  NodeList &port_exp = arg->exp();

  NodeList::iterator iexp  = port_exp.begin();
  NodeList::iterator iport = _act_ports.begin();

  VeriExp *exp = NULL;
  VeriVar *var = NULL;
  for( ; iexp != port_exp.end(); ++iexp, ++iport )
  {
    if( *iexp == NULL || *iport == NULL )
    {
      veri_err->append_err( ERR_ERROR, father(), vSEC_CALLER_PORT_MISMATCH );
      return vSEC_CALLER_PORT_MISMATCH;
    }

    exp = static_cast<VeriExp *>(*iexp);
    var = static_cast<VeriVar *>(*iport);

    // check type match
    re = check_type_ref( exp, var, IS_FALSE );
    if( re != OK )
      return re;
  }
  return OK;
}

int
VeriFuncDecl::prep_ports_for_check_type( void )
{
  if( _port_list != NULL )
  {
    if( _port_list->detail_type( ) != vsDECL_FUNC_PORT_LIST )
      return OK;

    VeriFuncPortList * port_list
      = static_cast<VeriFuncPortList *>(_port_list);
    NodeList &tf_decl_list = port_list->func_port();

    NodeList::iterator idecl = tf_decl_list.begin();
    NodeList::iterator iport;

    VeriPrimaryExp *pexp = NULL;
    for( ; idecl != tf_decl_list.end(); ++idecl )
    {
      NodeList &port_ids = (static_cast<VeriTfDecl *>(*idecl))->port_ids();
      iport = port_ids.begin();
      for( ; iport != port_ids.end(); ++iport )
      {
        pexp = static_cast<VeriPrimaryExp *>(*iport);
        if( pexp->var() == NULL && pexp->calc_value( ) == NULL )
        {
          veri_err->append_err( ERR_ERROR, this, vSEC_CALLER_PORT_NOT_COMPLETE );
          return vSEC_CALLER_PORT_NOT_COMPLETE;
        }
         _act_ports.push_back( pexp->var() );
        _act_port_num++;
      }
    }
  }
  else
  {
    if( _item_decl->detail_type( ) != vsDECL_FUNC_ITEM_DECL_LIST )
      return OK;

    VeriFuncItemDeclList * item_list
      = static_cast<VeriFuncItemDeclList *>(_item_decl);
    NodeList &decl_list = item_list->func_item();

    NodeList::iterator idecl = decl_list.begin();
    NodeList::iterator iport;

    VeriPrimaryExp *pexp = NULL;
    for( ; idecl != decl_list.end(); ++idecl )
    {
      if( (*idecl)->detail_type( ) == vsDECL_TF_DECL )
      {
        NodeList &port_ids = (static_cast<VeriTfDecl *>(*idecl))->port_ids();
        iport = port_ids.begin();
        for( ; iport != port_ids.end(); ++iport )
        {
          pexp = static_cast<VeriPrimaryExp *>(*iport);
          if( pexp->var() == NULL && pexp->calc_value( ) == NULL )
          {
            veri_err->append_err( ERR_ERROR, this, vSEC_CALLER_PORT_NOT_COMPLETE );
            return vSEC_CALLER_PORT_NOT_COMPLETE;
          }
          _act_ports.push_back( pexp->var() );
          _act_port_num++;
        }
      }
    }
  }

  return OK;
}

/*
 *  exp and var should not be null, garanteed by caller
 */
int
VeriFuncDecl::check_type_ref( VeriExp *exp, VeriNode *port, int no_assign )
{
  int re = OK;

  VeriVar    * var      = NULL;
  VeriTfVar  * tfvar    = NULL;
  VeriParam  * param    = NULL;
  VeriInt    * int_var  = NULL;
  VeriGenVar * genvar   = NULL;

  VeriVar    * svar     = NULL;
  VeriParam  * sparam   = NULL;
  VeriTfVar  * stfvar   = NULL;
  VeriInt    * sint_var = NULL;
  VeriGenVar * sgenvar  = NULL;

  switch( port->tok_type( ) )
  {
    case vtVAR    : var     = static_cast<VeriVar    *>(port); break;
    case vtTFVAR  : tfvar   = static_cast<VeriTfVar  *>(port); break;
    case vtPARAM  : param   = static_cast<VeriParam  *>(port); break;
    case vtINT    : int_var = static_cast<VeriInt    *>(port); break;
    case vtGENVAR : genvar  = static_cast<VeriGenVar *>(port); break;
    default : return vSEC_CALLER_PORT_MISMATCH;
  }

  IVec dummy_dimen;

  switch( exp->detail_type( ) )
  {
    case veEXP_PRI :
    {
      VeriPrimaryExp *pexp = static_cast<VeriPrimaryExp *>(exp);
      if( pexp->var( ) == NULL )
      {
        if( pexp->pri_type( ) == veNUM )
        {
          switch( port->tok_type( ) )
          {
            case vtVAR :
              if( var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(), pexp->calc_value() );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;

            case vtTFVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(), pexp->calc_value() );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(), pexp->calc_value() );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                param->set_val( num );
              }
              return OK;
              break;

            case vtGENVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(), pexp->calc_value() );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( int_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(), pexp->calc_value() );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;
          }
        }
        return OK;
        break;
      }

      switch( pexp->var()->tok_type( ) )
      {
        case vtVAR :
          svar = static_cast<VeriVar *>(pexp->var());
          switch( port->tok_type( ) )
          {
            case vtVAR :
              if( var->range_depth( ) != svar->range_depth( ) )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = svar->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;

            case vtTFVAR :
              if( svar->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = svar->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( svar->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = svar->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                param->set_val( num );
              }
              return OK;
              break;

            case vtGENVAR :
              if( svar->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = svar->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( int_var->range_depth( ) != svar->range_depth( ) )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE && svar->range_depth( ) == 0 )
              {
                VeriNum *num = svar->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;
          }
          break;

        case vtTFVAR :
          stfvar = static_cast<VeriTfVar *>(pexp->var());
          switch( port->tok_type( ) )
          {
            case vtVAR :
              if( var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = stfvar->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;

            case vtTFVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = stfvar->get_val( );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = stfvar->get_val( );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                param->set_val( num );
              }
              return OK;
              break;

            case vtGENVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = stfvar->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( int_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = stfvar->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;
          }
          break;

        case vtPARAM :
          sparam = static_cast<VeriParam *>(pexp->var());
          switch( port->tok_type( ) )
          {
            case vtVAR :
              if( var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sparam->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;

            case vtTFVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = sparam->get_val( );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = sparam->get_val( );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                param->set_val( num );
              }
              return OK;
              break;

            case vtGENVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = sparam->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( int_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sparam->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;
          }
          break;

        case vtINT :
          sint_var = static_cast<VeriInt *>(pexp->var());
          switch( port->tok_type( ) )
          {
            case vtVAR :
              if( var->range_depth( ) != sint_var->range_depth( ) )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE && sint_var->range_depth( ) == 0 )
              {
                VeriNum *num = sint_var->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;

            case vtTFVAR :
              if( sint_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sint_var->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( sint_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sint_var->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                param->set_val( num );
              }
              return OK;
              break;

            case vtGENVAR :
              if( sint_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sint_var->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( int_var->range_depth( ) != sint_var->range_depth( ) )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE && sint_var->range_depth( ) == 0 )
              {
                VeriNum *num = sint_var->get_one_num( dummy_dimen, IS_TRUE, 0, 0 );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;
          }
          break;

        case vtGENVAR :
          sgenvar = static_cast<VeriGenVar *>(pexp->var());
          switch( port->tok_type( ) )
          {
            case vtVAR :
              if( var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sgenvar->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;

            case vtTFVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = sgenvar->get_val( );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = sgenvar->get_val( );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                param->set_val( num );
              }
              return OK;
              break;

            case vtGENVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = sgenvar->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( int_var->range_depth( ) != 0 )
              {
                veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
                return vSEC_MOD_PORT_MISMATCH;
              }
              else if( no_assign == IS_FALSE )
              {
                VeriNum *num = sgenvar->get_val( );
                veri_parser->insert_node( num );
                num->set_scope( exp->scope( ) );
                num->set_father( port );
                int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
              }
              return OK;
              break;
          }
          break;

        default:
          break;
      }
      break;
    }

    case veEXP_UNA :
    case veEXP_BIN :
    case veEXP_SIGN :
    case veEXP_UNSIGN :
    case veEXP_MCONCAT :
    {
      switch( port->tok_type( ) )
      {
        case vtVAR :
        {
          if( var->range_depth( ) == 0 )
          {
            return OK;
          }
          else
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          break;
        }

        case vtTFVAR :
          if( no_assign == IS_FALSE )
          {
            VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value( ) );
            veri_parser->insert_node( num );
            num->set_father( tfvar );
            tfvar->set_val( num );
          }
          return OK;
          break;

        case vtPARAM :
          if( no_assign == IS_FALSE )
          {
            VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value( ) );
            veri_parser->insert_node( num );
            num->set_father( tfvar );
            param->set_val( num );
          }
          return OK;
          break;
        
        case vtGENVAR :
          if( no_assign == IS_FALSE )
          {
            VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value( ) );
            veri_parser->insert_node( num );
            num->set_father( tfvar );
            genvar->set_val( num );
          }
          return OK;
          break;

        case vtINT :
          if( var->range_depth( ) == 0 )
          {
            if( no_assign == IS_FALSE )
            {
              VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value( ) );
              veri_parser->insert_node( num );
              num->set_father( tfvar );
              int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
            }
            return OK;
          }
          else
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          break;

        default:
          return vSEC_CALLER_PORT_MISMATCH;
      }
      break;
    }

    case veEXP_ARRAY :
    {
      VeriArrayExp *aexp = static_cast<VeriArrayExp *>(exp);
      VeriPrimaryExp *pexp = static_cast<VeriPrimaryExp *>(aexp->id());

      if( pexp->var() != NULL )
        return vSEC_CALLER_PORT_MISMATCH;

      switch( port->tok_type( ) )
      {
        case vtPARAM : case vtGENVAR :
          veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
          return vSEC_CALLER_PORT_MISMATCH;

        case vtVAR :
          if( aexp->range_depth( ) > var->range_depth( ) )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          return OK;
          break;
        
        case vtINT :
          if( aexp->range_depth( ) > int_var->range_depth( ) )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          else
          {
            if( no_assign == IS_FALSE )
            {
              VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value( ) );
              veri_parser->insert_node( num );
              num->set_father( tfvar );
              int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
            }
            return OK;
          }
          break;
      }
      return OK;
      break;
    }

    case veEXP_NAMED_PARAM :
    {
      VeriNamedParamExp * nexp = static_cast<VeriNamedParamExp *>(exp);
      switch( port->tok_type( ) )
      {
        case vtVAR :
          if( var->name( ) != nexp->ref() )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          return OK;
          break;

        case vtTFVAR :
          if( tfvar->name( ) != nexp->ref() )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          return OK;
         break;

        case vtPARAM :
          if( param->name( ) != nexp->ref() )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          return OK;
         break;
        
        case vtGENVAR :
          if( genvar->name( ) != nexp->ref() )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          return OK;
          break;

        case vtINT :
          if( int_var->name( ) != nexp->ref() )
          {
            veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
            return vSEC_CALLER_PORT_MISMATCH;
          }
          return OK;
          break;
      }

      VeriExp *inner_exp = static_cast<VeriExp *>(nexp->port_exp());
      return check_type_ref( inner_exp, port, IS_FALSE );
      break;
    }

    case veEXP_CALLER :
    {
      VeriCallerExp *cexp = static_cast<VeriCallerExp *>(exp);
      if( cexp->call_type() == vtTASK_CALL )
      {
        veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
        return vSEC_CALLER_PORT_MISMATCH;
      }
      else
      {
        assert( cexp->hive()->detail_type() == vsDECL_FUNC_DECL );
        VeriFuncDecl *func = static_cast<VeriFuncDecl *>(cexp->hive());
        if( func->ret_value() != NULL )
        {
          VeriFunc *ret_value = func->ret_value();
          switch( port->tok_type( ) )
          {
            case vtVAR :
            {
              if( var->range_depth( ) == 0 )
              {
                return OK;
              }
              else
              {
                veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
                return vSEC_CALLER_PORT_MISMATCH;
              }
              break;
            }

            case vtTFVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(),
                                            ret_value->get_val( ) );
                veri_parser->insert_node( num );
                num->set_father( tfvar );
                tfvar->set_val( num );
              }
              return OK;
              break;

            case vtPARAM :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(),
                                            ret_value->get_val( ) );
                veri_parser->insert_node( num );
                num->set_father( tfvar );
                param->set_val( num );
              }
              return OK;
              break;
            
            case vtGENVAR :
              if( no_assign == IS_FALSE )
              {
                VeriNum *num = new VeriNum( veri_parser->next_id(),
                                            ret_value->get_val( ) );
                veri_parser->insert_node( num );
                num->set_father( tfvar );
                genvar->set_val( num );
              }
              return OK;
              break;

            case vtINT :
              if( var->range_depth( ) == 0 )
              {
                if( no_assign == IS_FALSE )
                {
                  VeriNum *num = new VeriNum( veri_parser->next_id(),
                                              ret_value->get_val( ) );
                  veri_parser->insert_node( num );
                  num->set_father( tfvar );
                  int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
                }
                return OK;
              }
              else
              {
                veri_err->append_err( ERR_ERROR, exp, vSEC_CALLER_PORT_MISMATCH );
                return vSEC_CALLER_PORT_MISMATCH;
              }
              break;

            default:
              return vSEC_CALLER_PORT_MISMATCH;
          }
        }
        else
        {
           veri_err->append_err( ERR_ERROR, exp, vSEC_FUNC_NO_RET );
           return vSEC_FUNC_NO_RET;
        }
      }
    }
    break;

    case veEXP_MINTY :
    {
      VeriMintypmaxExp *pmax = static_cast<VeriMintypmaxExp *>(exp);
      VeriExp *exp1 = static_cast<VeriExp *>(pmax->exp_child1( ) );
      VeriExp *exp2 = static_cast<VeriExp *>(pmax->exp_child2( ) );
      VeriExp *exp3 = static_cast<VeriExp *>(pmax->exp_child3( ) );

      re = check_type_ref( exp1, port, IS_TRUE );
      if( re != OK )
        return re;

      re = check_type_ref( exp2, port, IS_TRUE );
      if( re != OK )
        return re;
         
      return check_type_ref( exp3, port, IS_TRUE );
      break;
    }

    case veEXP_QUES :
    {
      VeriQuesExp *ques = static_cast<VeriQuesExp *>(exp);
      VeriExp  *choice  = static_cast<VeriExp *>(ques->choice ( ) );
      VeriExp  *choice1 = static_cast<VeriExp *>(ques->choice1( ) );
      VeriExp  *choice2 = static_cast<VeriExp *>(ques->choice2( ) );

      if( choice != NULL )
        return check_type_ref( choice, port, IS_FALSE );
      else
      {
        re = check_type_ref( choice1, port, IS_TRUE );
        if( re != OK )
          return re;
           
        return check_type_ref( choice2, port, IS_TRUE );
      }
      break;
    }

    case veEXP_MULTI :
    {
      VeriMultiExp *mexp = static_cast<VeriMultiExp *>(exp);
      NodeList & exp_list = mexp->exp();
      NodeList::iterator it = exp_list.begin();
      for( ; it != exp_list.end(); ++it )
      {
        re = check_type_ref( static_cast<VeriExp *>(*it), port, IS_TRUE );
        if( re != OK )
          return re;
      }
      return OK;
      break;
    }

    default:
      return OK;
      break;
  }
  return OK;
}


int
VeriFuncDecl::push_contents( NodeVec& mstack )
{
  mstack.push_back( this );

  //_stmt->clean_up( );
  _stmt->set_is_trash( IS_TRUE );
  _stmt->set_scope( scope( ) );
  mstack.push_back( _stmt );

  return OK;
}

int
VeriFuncDecl::finish_scope( void )
{
  scope()->finish_scope();
  return OK;
}

int
VeriFuncDecl::push_for_replacement( NodeVec& mstack )
{
  if( _range != NULL )
    mstack.push_back( _range );

  if( _port_list != NULL )
    mstack.push_back( _port_list );

  if( _item_decl != NULL )
    mstack.push_back( _item_decl );

  mstack.push_back( _stmt );

  return OK;
}

int
VeriFuncDecl::replace_node( Uint idx, NodeList& node_list,
                            NodeList::iterator& inl)
{
  // spawn duplicant
  VeriFuncDecl *tmp = new VeriFuncDecl( idx, this );

  // begin replacement
  NodeList::iterator itl = inl; 

  tmp->_stmt = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );
  itl = inl;

  if( _item_decl != NULL )
  {
    tmp->_item_decl = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  if( _port_list != NULL )
  {
    tmp->_port_list = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  if( _range != NULL )
  {
    tmp->_range = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
  }

  *inl = tmp;
  return OK;
}

    /************************
     * Variable-Declaration *
     ************************/

void
VeriVarDecl::clean_up( void )
{
  set_pstatus( vSTART_S );
}

int
VeriVarDecl::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;

  if( _drive != NULL )
  {
    mqueue.push( _drive );
    iqueue.push( depth );
  }

  if( _range != NULL )
  {
    mqueue.push( _range );
    iqueue.push( depth );
  }

  if( _delay != NULL )
  {
    mqueue.push( _delay );
    iqueue.push( depth );
  }

  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriVarDecl::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_in_out( _inout );
  ofile << dot_str_var_type( _net_type );
  ofile << dot_str_is_tf( "SIGNED", _is_signed );
  ofile << dot_str_vector_scalar( _vec_scalar );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _drive );
  ofile << dot_str_conn( idx(), _range );
  ofile << dot_str_conn( idx(), _delay );
  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriVarDecl::check_child_validation( void )
{
  switch( _inout )
  {
    case vpIN :
    case vpINOUT :
      switch( _net_type )
      {
        case vvREG:
        case vvTIME:
        case vvINT:
          veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_VAR_DECL );
          return vCHILD_TYPE_VAR_DECL;

        default:
          break;
      }
      break;

    case vpOUT :
      switch( _net_type )
      {
        case vvTIME:
        case vvINT:
          if( _is_signed == IS_TRUE || _range != NULL )
          {
            veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_VAR_DECL );
            return vCHILD_TYPE_VAR_DECL;
          }
          break;

        default:
          break;
      }
      break;

    default:
      break;
  }

  if( _drive != NULL )
  {
    if( _drive->detail_type() != veEXP_DRIVE )
    {
      veri_err->append_err( ERR_ERROR, _drive->line(),
                           _drive->col(), vCHILD_TYPE_VAR_DECL );
      return vCHILD_TYPE_VAR_DECL;
    }
  }

  if( _range != NULL )
  {
    if( _range->detail_type() != veEXP_RANGE )
    {
      veri_err->append_err( ERR_ERROR, _range->line(),
                           _range->col(), vCHILD_TYPE_VAR_DECL );
      return vCHILD_TYPE_VAR_DECL;
    }
  }

  if( _delay != NULL )
  {
    if( _delay->detail_type() != veEXP_DELAY )
    {
      veri_err->append_err( ERR_ERROR, _delay->line(),
                           _delay->col(), vCHILD_TYPE_VAR_DECL );
      return vCHILD_TYPE_VAR_DECL;
    }
  }

  return OK;
}

int
VeriVarDecl::check_last_child_validation( void )
{
  VeriNode * last = _vars.back();
  if( last != NULL )
  {
    switch( last->detail_type() )
    {
      case veEXP_PRI :
      case veEXP_ARRAY:
      case veEXP_ASSIGN :
        return OK;
        break;

      default:
        veri_err->append_err( ERR_ERROR, last->line(),
                        last->col(), vCHILD_TYPE_VAR_DECL );
        return vCHILD_TYPE_VAR_DECL;
        break;
    }
  }

  veri_err->append_err( ERR_ERROR, last->line(),
                        last->col(), vCHILD_TYPE_VAR_DECL );
  return vCHILD_TYPE_VAR_DECL;
}

int
VeriVarDecl::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vDECL_S );
      return re;
      break;

    case vDECL_S :
      return declare_var_symbols( );
      break;

    default :
      return do_nothing( );
      break;
  }
}

int
VeriVarDecl::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _vars.rbegin();
  for( ; it != _vars.rend(); ++it )
  {
    //(*it)->clean_up( );
    (*it)->set_is_decl( IS_TRUE );
    (*it)->set_is_trash( IS_TRUE );
    (*it)->set_scope( scope() );
    static_cast<VeriExp *>(*it)->set_signed( _is_signed );
    mstack.push_back( *it );
  }

  if( _delay != NULL )
  {
    //_delay->clean_up( );
    _delay->set_is_trash( IS_TRUE );
    _delay->set_scope( scope() );
    mstack.push_back( _delay );
  }

  if( _range != NULL )
  {
    //_range->clean_up( );
    _range->set_is_trash( IS_TRUE );
    _range->set_scope( scope() );
    mstack.push_back( _range );
  }

  if( _drive != NULL )
  {
    //_drive->clean_up( );
    _drive->set_is_trash( IS_TRUE );
    _drive->set_scope( scope() );
    mstack.push_back( _drive );
  }

  return OK;
}

VeriVar *
VeriVarDecl::reg_var_symbol( int line, int col, CString& name, short drive_from,
                             short drive_to, int dstart, int dend,
                             IList& range_from, IList& range_to,
                             VeriNode *decl, VeriNum **init, int total_cnt )
{
  VeriNode * node = scope()->value_same_level( name );
  if( node != NULL )
  {
    switch( node->tok_type() )
    {
      case vtFUNC     : case vtTASK      :
      case vtMOD_INST : case vtGATE_INST :
        veri_err->append_err( ERR_ERROR, decl, vVAR_TFMG_REDEFINE );
        return NULL;
        break;

      case vtSYS_VAR  : case vtVAR : case vtPARAM :
      case vtGENVAR   : case vtINT : case vtTFVAR :
        veri_err->append_err( ERR_ERROR, decl, vVAR_VAR_REDEFINE );
        return NULL;
        break;

      default:
        veri_err->append_err( ERR_ERROR, decl, vVAR_UNKNOWN_REDEFINE );
        return NULL;
        break;
    }
  }

  VeriVar * var = new VeriVar( veri_parser->next_id(), line, col,
                               name, _inout, _net_type, _is_signed, _vec_scalar,
                               drive_from, drive_to, dstart, dend,
                               decl, init, total_cnt, birth_mod() );
  veri_parser->insert_node( var );

  var->set_father( this );
  var->set_scope( scope() );

  IList::iterator irf = range_from.begin();
  IList::iterator irt = range_to.begin();

  for( ; irf != range_from.end(); ++irf, ++irt )
    var->add_range( *irf, *irt );

  scope()->insert( name, var );
  return var;
}

int
VeriVarDecl::declare_var_symbols( void )
{
  short drive_from = DEFAULT_DRIVE_FROM;
  short drive_to   = DEFAULT_DRIVE_TO;
  int   dstart     = RANGE_DEFAULT;
  int   dend       = RANGE_DEFAULT;

  IList vec_range_from;
  IList vec_range_to;

  if( _drive != NULL )
  {
    VeriDriveExp *drive = static_cast<VeriDriveExp *>(_drive);
    drive_from = drive->drive_from( );
    drive_to   = drive->drive_to( );
  }

  if( _range != NULL )
  {
    VeriRangeExp *range = static_cast<VeriRangeExp *>(_range);
    dstart = range->range_from();
    dend   = range->range_to();
  }

  int             total_cnt  = 1;
  VeriPrimaryExp *pri_exp    = NULL;
  VeriArrayExp   *array_exp  = NULL;
  VeriAssignExp  *assign_exp = NULL;
  VeriNum       **init       = NULL;
  VeriExp        *lval       = NULL;
  VeriExp        *rval       = NULL;
  VeriVar        *symbol     = NULL;

  IList::iterator iil;
  NodeList::iterator inl = _vars.begin();
  for( ; inl != _vars.end(); ++inl )
  {
    switch( (*inl)->detail_type() )
    {
      case veEXP_PRI :
        pri_exp = static_cast<VeriPrimaryExp *>(*inl);

        init = new VeriNum*[total_cnt];
        init[0] = new VeriNum( veri_parser->next_id(), pri_exp->line(),
                               pri_exp->col(), birth_mod());
        init[0]->init_val( _is_signed, abs(dend-dstart)+1 );

        init[0]->set_father( pri_exp );
        veri_parser->insert_node( init[0] );

        symbol = reg_var_symbol( (*inl)->line(), (*inl)->col(), pri_exp->str(),
                                  drive_from, drive_to, dstart, dend,
                                  vec_range_from, vec_range_to,
                                  *inl, init, total_cnt );

        pri_exp->set_var( symbol );
        break;

      case veEXP_ARRAY :
      {
        array_exp = static_cast<VeriArrayExp *>(*inl);
        if( array_exp->not_calc( ) == IS_TRUE )
        {
          veri_err->append_err( ERR_ERROR, array_exp, vSEC_VAR_RANGE_NOT_CALC );
          return vSEC_VAR_RANGE_NOT_CALC;
        }

        IList &irange_from = array_exp->range_from();
        IList &irange_to   = array_exp->range_to();
        IList::iterator iil = irange_from.begin();
        for( ; iil != irange_from.end(); ++iil )
          vec_range_from.push_back( *iil );
        for( iil = irange_to.begin(); iil != irange_to.end(); ++iil )
          vec_range_to.push_back( *iil );

        init = new VeriNum*[total_cnt];
        init[0] = new VeriNum( veri_parser->next_id(), array_exp->line(),
                               array_exp->col(), birth_mod());
        init[0]->init_val( _is_signed, abs(dend-dstart)+1 );
        init[0]->set_father( pri_exp );
        veri_parser->insert_node( init[0] );

        symbol = reg_var_symbol( (*inl)->line(), (*inl)->col(),
                                 array_exp->name(), drive_from, drive_to,
                                 dstart, dend, vec_range_from, vec_range_to,
                                 *inl, init, total_cnt );
        pri_exp = static_cast<VeriPrimaryExp *>(array_exp->id());
        pri_exp->set_var( symbol );

        vec_range_from.clear();
        vec_range_to.clear();

        break;
      }

      case veEXP_ASSIGN :
        assign_exp = static_cast<VeriAssignExp *>(*inl);
        lval = static_cast<VeriExp *>(assign_exp->lval());
        rval = static_cast<VeriExp *>(assign_exp->rval());

        if( lval->detail_type() == veEXP_PRI )
        {
          init = new VeriNum*[total_cnt];
          init[0] = new VeriNum( veri_parser->next_id(), rval->calc_value());
          init[0]->set_father( this );
          init[0]->set_scope( scope() );
          veri_parser->insert_node( init[0] );

          pri_exp = static_cast<VeriPrimaryExp *>(*inl);
          symbol = reg_var_symbol( (*inl)->line(), (*inl)->col(),
                                    pri_exp->str(), 
                                    drive_from, drive_to, dstart, dend,
                                    vec_range_from, vec_range_to, *inl,
                                    init, total_cnt );
          pri_exp->set_var( symbol );
          init = NULL;
        }
        else if( lval->detail_type( ) == veEXP_ARRAY )
        {
          array_exp = static_cast<VeriArrayExp *>(lval);
          if( array_exp->not_calc() == IS_TRUE )
          {
            veri_err->append_err( ERR_ERROR, array_exp,
                                  vSEC_VAR_RANGE_NOT_CALC );
            return vSEC_VAR_RANGE_NOT_CALC;
          }

          IList &irange_from = array_exp->range_from();
          IList &irange_to   = array_exp->range_to();
          IList::iterator iil = irange_from.begin();
          for( ; iil != irange_from.end(); ++iil )
            vec_range_from.push_back( *iil );
          for( iil = irange_to.begin(); iil != irange_to.end(); ++iil )
            vec_range_to.push_back( *iil );

          /* TODO: calculate total cnt for initialization */
          /* pseudo down heer */
          init = new VeriNum*[total_cnt];
          init[0] = new VeriNum( veri_parser->next_id(), rval->calc_value() );
          init[0]->set_father( this );
          init[0]->set_scope( scope() );
          veri_parser->insert_node( init[0] );

          symbol = reg_var_symbol( (*inl)->line(), (*inl)->col(),
                                    array_exp->name(),
                                    drive_from, drive_to, dstart, dend,
                                    vec_range_from, vec_range_to, *inl,
                                    init, total_cnt );
          pri_exp = static_cast<VeriPrimaryExp *>(array_exp->id());
          pri_exp->set_var( symbol );

          init = NULL;
          total_cnt = 1;
          vec_range_from.clear();
          vec_range_to.clear();
        }
        break;

      default:
        break;
    }
  }
  return OK;
}

int
VeriVarDecl::do_nothing( void )
{
  return OK;
}

int
VeriVarDecl::push_for_replacement( NodeVec& mstack )
{
  if( _drive != NULL )
    mstack.push_back( _drive );

  if( _range != NULL )
    mstack.push_back( _range );

  if( _delay != NULL )
    mstack.push_back( _delay );

  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
    mstack.push_back( *it );

  return OK;
}

int
VeriVarDecl::replace_node( Uint idx, NodeList& node_list,
                                     NodeList::iterator& inl)
{
  // spawn duplicant
  VeriVarDecl *tmp = new VeriVarDecl( idx, this );

  // begin replacement
  NodeList::iterator itl = inl; 
  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
  {
    tmp->_vars.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  if( _delay != NULL )
  {
    tmp->_delay = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  if( _range != NULL )
  {
    tmp->_range = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  if( _drive != NULL )
  {
    tmp->_drive = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
  }

  *inl = tmp;
  return OK;
}

    /*************************
     * Parameter-Declaration *
     *************************/

void
VeriParamDecl::clean_up( void )
{
  set_pstatus( vSTART_S );
}

int
VeriParamDecl::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;
  
  if( _range != NULL )
  {
    mqueue.push( _range );
    iqueue.push( depth );
  }

  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriParamDecl::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_is_tf( "SIGNED", _is_signed );
  ofile << dot_str_is_tf( "INT", _is_int );
  ofile << dot_str_is_tf( "LOCAL", _is_local );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _range );
  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriParamDecl::check_child_validation( void )
{
  if( _range != NULL )
  {
    if( _range->detail_type() != veEXP_RANGE )
    {
      veri_err->append_err( ERR_ERROR, _range->line(),
                          _range->col(), vCHILD_TYPE_PARAM_DECL );
      return vCHILD_TYPE_PARAM_DECL;
    }
  }

  return OK;
}

int
VeriParamDecl::check_last_child_validation( void )
{
  VeriNode * last = _vars.back();
  if( last == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_PARAM_DECL );
    return vCHILD_TYPE_PARAM_DECL;
  }

  if(    last->detail_type() != veEXP_PRI
      && last->detail_type() != veEXP_ASSIGN )
  {
    veri_err->append_err( ERR_ERROR, last->line(),
                        last->col(), vCHILD_TYPE_PARAM_DECL );
    return vCHILD_TYPE_PARAM_DECL;
  }
  return OK;
}

int
VeriParamDecl::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vDECL_S );
      return re;
      break;

    case vDECL_S:
      return declare_param_symbols( );
      break;

    default:
      return do_nothing( );
      break;
  }
}

int
VeriParamDecl::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _vars.rbegin();
  for( ; it != _vars.rend(); ++it )
  {
    //(*it)->clean_up( );
    (*it)->set_is_decl( IS_TRUE );
    (*it)->set_is_trash( IS_TRUE );
    (*it)->set_scope( scope() );
    mstack.push_back( *it );
    static_cast<VeriExp *>(*it)->set_signed( _is_signed );
  }

  if( _range != NULL )
  {
    //_range->clean_up( );
    _range->set_is_trash( IS_TRUE );
    _range->set_scope( scope() );
    mstack.push_back( _range );
  }

  return OK;
}

VeriParam *
VeriParamDecl::reg_param_symbol( int line, int col, VeriNode *decl,
                                 CString& name, int dstart, int dend,
                                 VeriNum  *init )
{
  VeriNode * node = scope()->value_same_level( name );
  if( node != NULL )
  {
    switch( node->tok_type() )
    {
      case vtPARAM:
      {
        VeriParam *tparam = static_cast<VeriParam *>(node);
        if( tparam->is_head_override() )
        {
          tparam->set_is_int( _is_int );
          tparam->set_is_local( _is_local );
          tparam->set_is_signed( _is_signed );
          tparam->set_dstart( dstart );
          tparam->set_dend( dend );
          tparam->set_dlen( abs(dend - dstart)+1 );
          return tparam;
        }
        else
        {
          veri_err->append_err( ERR_ERROR, decl, vPARAM_VAR_REDEFINE );
          return NULL;
        }
        break;
      }

      case vtFUNC     : case vtTASK      :
      case vtMOD_INST : case vtGATE_INST :
        veri_err->append_err( ERR_ERROR, decl, vPARAM_TFMG_REDEFINE );
        return NULL;
        break;

      case vtSYS_VAR  : case vtVAR : case vtTFVAR :
      case vtGENVAR   : case vtINT :
        veri_err->append_err( ERR_ERROR, decl, vPARAM_VAR_REDEFINE );
        return NULL;
        break;

      default:
        veri_err->append_err( ERR_ERROR, decl, vPARAM_UNKNOWN_REDEFINE );
        return NULL;
        break;
    }
  }

  VeriParam * param = new VeriParam( veri_parser->next_id(), line, col,
                                     name, _is_int, _is_local, _is_signed,
                                     decl, dstart, dend, init, birth_mod() );
  veri_parser->insert_node( param );
  param->set_father( this );
  param->set_scope( scope() );

  scope()->insert( name, param );
  return param;
}

int
VeriParamDecl::declare_param_symbols( void )
{
  int dstart = 0;
  int dend   = 0;

  if( _range != NULL )
  {
    VeriRangeExp *range = static_cast<VeriRangeExp *>(_range);
    dstart = range->range_from();
    dend   = range->range_to();
  }

  VeriPrimaryExp *pexp = NULL;
  VeriAssignExp  *aexp = NULL;
  VeriExp        *rval = NULL;
  VeriNum        *init = NULL;
  VeriParam      *symbol = NULL;

  NodeList::iterator inl = _vars.begin();
  for( ; inl != _vars.end(); ++inl )
  {
    if( (*inl)->detail_type() == veEXP_ASSIGN )
    {
      aexp = static_cast<VeriAssignExp *>(*inl);
      rval = static_cast<VeriExp *>(aexp->rval());

      if(    aexp->lval()->detail_type() == veEXP_PRI
          && rval->calc_value()  != NULL )
      {
        init = new VeriNum( veri_parser->next_id(), rval->calc_value() );
        init->set_father( this );
        init->set_scope( scope() );
        veri_parser->insert_node( init );

        pexp = static_cast<VeriPrimaryExp *>(aexp->lval());
        symbol = reg_param_symbol( (*inl)->line(), (*inl)->col(), *inl,
                                   pexp->str(), dstart, dend, init );
        pexp->set_var( symbol );

        init = NULL;
      }
    }
  }
  return OK;
}

int
VeriParamDecl::do_nothing( void )
{
  return OK;
}

int
VeriParamDecl::push_for_replacement( NodeVec& mstack )
{
  if( _range != NULL )
    mstack.push_back( _range );

  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
    mstack.push_back( *it );

  return OK;
}

int
VeriParamDecl::replace_node( Uint idx, NodeList& node_list,
                                     NodeList::iterator& inl)
{
  // spawn duplicant
  VeriParamDecl *tmp = new VeriParamDecl( idx, this );

  // begin replacement
  NodeList::iterator itl = inl; 
  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
  {
    tmp->_vars.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  if( _range != NULL )
  {
    tmp->_range = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
  }

  *inl = tmp;
  return OK;
}

    /***********************
     * Integer-Declaration *
     ***********************/

void
VeriIntDecl::clean_up( void )
{
  set_pstatus( vSTART_S );
}

int
VeriIntDecl::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;

  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriIntDecl::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriIntDecl::check_child_validation( void )
{
  return OK;
}

int
VeriIntDecl::check_last_child_validation( void )
{
  VeriNode * last = _vars.back();
  if( last != NULL )
  {
    switch( last->detail_type() )
    {
      case veEXP_PRI :
      case veEXP_ARRAY:
      case veEXP_ASSIGN :
        return OK;
        break;

      default:
        veri_err->append_err( ERR_ERROR, last->line(),
                              last->col(), vCHILD_TYPE_INT_DECL );
        return vCHILD_TYPE_INT_DECL;
        break;
    }
  }

  veri_err->append_err( ERR_ERROR, last->line(),
                        last->col(), vCHILD_TYPE_INT_DECL );
  return vCHILD_TYPE_INT_DECL;
}

int
VeriIntDecl::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vDECL_S );
      return re;
      break;

    case vDECL_S:
      return declare_int_symbols( );
      break;

    default:
      return do_nothing( );
      break;
  }
}

int
VeriIntDecl::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _vars.rbegin();
  for( ; it != _vars.rend(); ++it )
  {
    //(*it)->clean_up( );
    (*it)->set_is_decl( IS_TRUE );
    (*it)->set_is_trash( IS_TRUE );
    (*it)->set_scope( scope() );
    mstack.push_back( *it );
  }

  return OK;
}

VeriInt *
VeriIntDecl::reg_int_symbol( int line, int col, VeriNode *decl, CString& name,
                             IList& range_from, IList& range_to, VeriNum **init,
                             int total_cnt )
{
  VeriNode * node = scope()->value_same_level( name );
  if( node != NULL )
  {
    switch( node->tok_type() )
    {
      case vtFUNC     : case vtTASK      :
      case vtMOD_INST : case vtGATE_INST :
        veri_err->append_err( ERR_ERROR, decl, vINT_TFMG_REDEFINE );
        return NULL;
        break;

      case vtSYS_VAR  : case vtVAR : case vtPARAM :
      case vtGENVAR   : case vtINT : case vtTFVAR :
        veri_err->append_err( ERR_ERROR, decl, vINT_VAR_REDEFINE );
        return NULL;
        break;

      default:
        veri_err->append_err( ERR_ERROR, decl, vINT_UNKNOWN_REDEFINE );
        return NULL;
        break;
    }
  }

  VeriInt * int_var = new VeriInt( veri_parser->next_id(), line, col,
                                   name, decl, init, total_cnt, birth_mod() );
  int_var->set_father( this );
  int_var->set_scope( scope() );
  veri_parser->insert_node( int_var );

  IList::iterator irf = range_from.begin();
  IList::iterator irt = range_to.begin();

  for( ; irf != range_from.end(); ++irf, ++irt )
    int_var->add_range( *irf, *irt );

  scope()->insert( name, int_var );
  return int_var;
}

int
VeriIntDecl::declare_int_symbols( void )
{
  IList vec_range_from;
  IList vec_range_to;

  VeriPrimaryExp *pri_exp    = NULL;
  VeriArrayExp   *array_exp  = NULL;
  VeriAssignExp  *assign_exp = NULL;
  VeriNum       **init       = NULL;
  VeriExp        *lval       = NULL;
  VeriExp        *rval       = NULL;
  VeriInt        *symbol     = NULL;

  IList::iterator iil;
  NodeList::iterator inl = _vars.begin();
  int total_cnt = 1;
  for( ; inl != _vars.end(); ++inl )
  {
    switch( (*inl)->detail_type() )
    {
      case veEXP_PRI :
        pri_exp = static_cast<VeriPrimaryExp *>(*inl);

        init = new VeriNum*[total_cnt];
        init[0] = new VeriNum( veri_parser->next_id(), pri_exp->line(),
                               pri_exp->col(), birth_mod());
        init[0]->init_val( IS_TRUE, 32 );
        init[0]->set_father( pri_exp );
        veri_parser->insert_node( init[0] );

        symbol = reg_int_symbol( (*inl)->line(), (*inl)->col(), *inl,
                                  pri_exp->str(), vec_range_from,
                                  vec_range_to, init, total_cnt );
        pri_exp->set_var( symbol );
        break;

      case veEXP_ARRAY :
      {
        array_exp = static_cast<VeriArrayExp *>(*inl);
        if( array_exp->not_calc() == IS_TRUE )
        {
          veri_err->append_err( ERR_ERROR, array_exp, vSEC_VAR_RANGE_NOT_CALC );
          return vSEC_VAR_RANGE_NOT_CALC;
        }

        IList &irange_from = array_exp->range_from();
        IList &irange_to   = array_exp->range_to();
        IList::iterator iil = irange_from.begin();
        for( ; iil != irange_from.end(); ++iil )
          vec_range_from.push_back( *iil );
        for( iil = irange_to.begin(); iil != irange_to.end(); ++iil )
          vec_range_to.push_back( *iil );

        init = new VeriNum*[total_cnt];
        init[0] = new VeriNum( veri_parser->next_id(), array_exp->line(),
                               array_exp->col(), birth_mod());
        init[0]->init_val( IS_TRUE, 32 );
        init[0]->set_father( pri_exp );
        veri_parser->insert_node( init[0] );

        symbol = reg_int_symbol( (*inl)->line(), (*inl)->col(),
                                 *inl, array_exp->name(),
                                 vec_range_from, vec_range_to,
                                 init, total_cnt );
        pri_exp = static_cast<VeriPrimaryExp *>(array_exp->id());
        pri_exp->set_var( symbol );

        vec_range_from.clear();
        vec_range_to.clear();

        break;
      }

      case veEXP_ASSIGN :
        assign_exp = static_cast<VeriAssignExp *>(*inl);
        lval = static_cast<VeriExp *>(assign_exp->lval());
        rval = static_cast<VeriExp *>(assign_exp->rval());

        if( lval->detail_type() == veEXP_PRI )
        {
          init = new VeriNum*[total_cnt];
          init[0] = new VeriNum( veri_parser->next_id(), rval->calc_value());
          init[0]->set_father( this );
          veri_parser->insert_node( init[0] );

          pri_exp = static_cast<VeriPrimaryExp *>(*inl);
          symbol = reg_int_symbol( (*inl)->line(), (*inl)->col(),
                                    *inl, pri_exp->str(),
                                    vec_range_from, vec_range_to,
                                    init, total_cnt );
          pri_exp->set_var( symbol );
          init = NULL;
        }
        else if( lval->detail_type( ) == veEXP_ARRAY )
        {
          array_exp = static_cast<VeriArrayExp *>(lval);
          if( array_exp->not_calc() == IS_TRUE )
          {
            veri_err->append_err( ERR_ERROR, array_exp,
                                  vSEC_VAR_RANGE_NOT_CALC );
            return vSEC_VAR_RANGE_NOT_CALC;
          }

          IList &irange_from = array_exp->range_from();
          IList &irange_to   = array_exp->range_to();
          IList::iterator iil = irange_from.begin();
          for( ; iil != irange_from.end(); ++iil )
            vec_range_from.push_back( *iil );
          for( iil = irange_to.begin(); iil != irange_to.end(); ++iil )
            vec_range_to.push_back( *iil );

          /* TODO: calculate total cnt for initialization */
          /* pseudo down heer */
          init = new VeriNum*[total_cnt];
          init[0] = new VeriNum( veri_parser->next_id(), rval->calc_value() );
          init[0]->set_father( this );
          veri_parser->insert_node( init[0] );

          symbol = reg_int_symbol( (*inl)->line(), (*inl)->col(),
                                    *inl, array_exp->name(),
                                     vec_range_from, vec_range_to,
                                     init, total_cnt );
          pri_exp = static_cast<VeriPrimaryExp *>(array_exp->id());
          pri_exp->set_var( symbol );

          init = NULL;
          total_cnt = 1;
          vec_range_from.clear();
          vec_range_to.clear();
        }
        break;

      default:
        break;
    }
  }
  return OK;
}

int
VeriIntDecl::do_nothing( void )
{
  return OK;
}

int
VeriIntDecl::push_for_replacement( NodeVec& mstack )
{
  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
    mstack.push_back( *it );

  return OK;
}

int
VeriIntDecl::replace_node( Uint idx, NodeList& node_list,
                           NodeList::iterator& inl)
{
  // spawn duplicant
  VeriIntDecl *tmp = new VeriIntDecl( idx, this );

  // begin replacement
  NodeList::iterator itl = inl; 
  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
  {
    tmp->_vars.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  *inl = tmp;
  return OK;
}

    /**********************
     * GenVar-Declaration *
     **********************/

void
VeriGenVarDecl::clean_up( void )
{
  set_pstatus( vSTART_S );
}

int
VeriGenVarDecl::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;
  
  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriGenVarDecl::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriGenVarDecl::check_child_validation( void )
{
  return OK;
}

int
VeriGenVarDecl::check_last_child_validation( void )
{
  VeriNode * last = _vars.back();
  if( last == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_GENVAR_DECL );
    return vCHILD_TYPE_GENVAR_DECL;
  }

  if(    last->detail_type() != veEXP_PRI
      && last->detail_type() != veEXP_ASSIGN )
  {
    veri_err->append_err( ERR_ERROR, last->line(),
                        last->col(), vCHILD_TYPE_GENVAR_DECL );
    return vCHILD_TYPE_GENVAR_DECL;
  }
  return OK;
}

int
VeriGenVarDecl::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vDECL_S );
      return re;
      break;

    case vDECL_S:
      return declare_genvar_symbols( );
      break;

    default:
      return do_nothing( );
      break;
  }
}

int
VeriGenVarDecl::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _vars.rbegin();
  for( ; it != _vars.rend(); ++it )
  {
    //(*it)->clean_up( );
    (*it)->set_is_decl( IS_TRUE );
    (*it)->set_is_trash( IS_TRUE );
    (*it)->set_scope( scope() );
    mstack.push_back( *it );
  }

  return OK;
}

VeriGenVar *
VeriGenVarDecl::reg_genvar_symbol( int line, int col, VeriNode *decl,
                                   CString& name, VeriNum *init )
{
  VeriNode * node = scope()->value_same_level( name );
  if( node != NULL )
  {
    switch( node->tok_type() )
    {
      case vtPARAM:
      {
        veri_err->append_err( ERR_ERROR, decl, vGENVAR_VAR_REDEFINE );
        return NULL;
        break;
      }

      case vtFUNC     : case vtTASK      :
      case vtMOD_INST : case vtGATE_INST :
        veri_err->append_err( ERR_ERROR, decl, vGENVAR_TFMG_REDEFINE );
        return NULL;
        break;

      case vtSYS_VAR  : case vtVAR : case vtTFVAR :
      case vtGENVAR   : case vtINT :
        veri_err->append_err( ERR_ERROR, decl, vGENVAR_VAR_REDEFINE );
        return NULL;
        break;

      default:
        veri_err->append_err( ERR_ERROR, decl, vGENVAR_UNKNOWN_REDEFINE );
        return NULL;
        break;
    }
  }

  VeriGenVar * genvar = new VeriGenVar( veri_parser->next_id(), line, col,
                                     name, decl, init, birth_mod() );
  genvar->set_father( this );
  genvar->set_scope( scope() );
  veri_parser->insert_node( genvar );
  scope()->insert( name, genvar );
  return genvar;
}

int
VeriGenVarDecl::declare_genvar_symbols( void )
{
  VeriPrimaryExp *pexp = NULL;
  VeriAssignExp  *aexp = NULL;
  VeriExp        *rval = NULL;
  VeriGenVar     *symbol = NULL;
  VeriNum        *init = NULL;

  NodeList::iterator inl = _vars.begin();
  for( ; inl != _vars.end(); ++inl )
  {
    if( (*inl)->detail_type() == veEXP_ASSIGN )
    {
      aexp = static_cast<VeriAssignExp *>(*inl);
      rval = static_cast<VeriExp *>(aexp->rval());

      if(    aexp->lval()->detail_type() == veEXP_PRI
          && rval->calc_value()  != NULL )
      {
        pexp = static_cast<VeriPrimaryExp *>(aexp->lval());

        init = new VeriNum( veri_parser->next_id(), rval->calc_value() );
        init->set_father( this );
        init->set_scope( scope() );
        veri_parser->insert_node( init );

        symbol = reg_genvar_symbol( (*inl)->line(), (*inl)->col(),
                                     *inl, pexp->str(), init );
        pexp->set_var( symbol );
      }
    }
    else if( (*inl)->detail_type() == veEXP_PRI )
    {
      pexp = static_cast<VeriPrimaryExp *>(*inl);

      init = new VeriNum( veri_parser->next_id(), pexp->line(), pexp->col(), birth_mod());
      init->init_val( IS_TRUE, 32 );
      init->set_father( pexp );
      veri_parser->insert_node( init );

      symbol = reg_genvar_symbol( (*inl)->line(), (*inl)->col(),
                                   *inl, pexp->str(), init );
      pexp->set_var( symbol );
    }
  }
  return OK;
}

int
VeriGenVarDecl::do_nothing( void )
{
  return OK;
}

int
VeriGenVarDecl::push_for_replacement( NodeVec& mstack )
{
  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
    mstack.push_back( *it );

  return OK;
}

int
VeriGenVarDecl::replace_node( Uint idx, NodeList& node_list,
                              NodeList::iterator& inl)
{
  // spawn duplicant
  VeriGenVarDecl *tmp = new VeriGenVarDecl( idx, this );

  // begin replacement
  NodeList::iterator itl = inl; 
  NodeList::iterator it = _vars.begin();
  for( ; it != _vars.end(); ++it )
  {
    tmp->_vars.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  *inl = tmp;
  return OK;
}


    /****************************************
     * Parameter-Declaration-List Statement *
     ****************************************/

void
VeriParamDeclList::clean_up( void )
{
  set_pstatus( vSTART_S );
}

int
VeriParamDeclList::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;
  NodeList::iterator it = _param_list.begin();
  for( ; it != _param_list.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriParamDeclList::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  NodeList::iterator it = _param_list.begin();
  for( ; it != _param_list.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriParamDeclList::check_child_validation( void )
{
  return OK;
}

int
VeriParamDeclList::check_last_child_validation( void )
{
  VeriNode * last = _param_list.back();
  if( last == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_PARAM_DECL_LIST );
    return vCHILD_TYPE_PARAM_DECL_LIST;
  }

  if( last->detail_type() != vsDECL_PARAM )
  {
    veri_err->append_err( ERR_ERROR, last->line(),
                        last->col(), vCHILD_TYPE_PARAM_DECL_LIST );
    return vCHILD_TYPE_PARAM_DECL_LIST;
  }
  return OK;
}

int
VeriParamDeclList::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return do_nothing( );
      break;
  }
}

int
VeriParamDeclList::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _param_list.rbegin();
  for( ; it != _param_list.rend(); ++it )
  {
    //(*it)->clean_up( );
    (*it)->set_is_decl( IS_TRUE );
    (*it)->set_is_trash( IS_TRUE );
    (*it)->set_scope( scope() );
    mstack.push_back( *it );
  }
  return OK;
}

int
VeriParamDeclList::do_nothing( void )
{
  return OK;
}

int
VeriParamDeclList::push_for_replacement( NodeVec& mstack )
{
  NodeList::iterator it = _param_list.begin();
  for( ; it != _param_list.end(); ++it )
    mstack.push_back( *it );

  return OK;
}

int
VeriParamDeclList::replace_node( Uint idx, NodeList& node_list,
                                     NodeList::iterator& inl)
{
  // spawn duplicant
  VeriParamDeclList *tmp = new VeriParamDeclList( idx, this );

  // begin replacement
  NodeList::iterator itl = inl; 
  NodeList::iterator it = _param_list.begin();
  for( ; it != _param_list.end(); ++it )
  {
    tmp->_param_list.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  *inl = tmp;
  return OK;
}

    /****************************************
     * Port-Declaration-List Statement *
     ****************************************/

void
VeriPortDeclList::clean_up( void )
{
  set_pstatus( vSTART_S );
}

int
VeriPortDeclList::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;
  NodeList::iterator it = _port_list.begin();
  for( ; it != _port_list.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriPortDeclList::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  NodeList::iterator it = _port_list.begin();
  for( ; it != _port_list.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriPortDeclList::check_child_validation( void )
{
  return OK;
}

int
VeriPortDeclList::check_last_child_validation( void )
{
  VeriNode * last = _port_list.back();
  if( last == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_PORT_DECL_LIST );
    return vCHILD_TYPE_PORT_DECL_LIST;
  }
   
  if( last->detail_type() != vsDECL_VAR )
  {
    veri_err->append_err( ERR_ERROR, last->line(),
                        last->col(), vCHILD_TYPE_PORT_DECL_LIST );
    return vCHILD_TYPE_PORT_DECL_LIST;
  }
  return OK;
}

int
VeriPortDeclList::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return do_nothing( );
      break;
  }
}

int
VeriPortDeclList::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _port_list.rbegin();
  for( ; it != _port_list.rend(); ++it )
  {
    //(*it)->clean_up( );
    (*it)->set_is_decl( IS_TRUE );
    (*it)->set_is_trash( IS_TRUE );
    (*it)->set_scope( scope() );
    mstack.push_back( *it );
  }
  return OK;
}

int
VeriPortDeclList::do_nothing( void )
{
  return OK;
}

int
VeriPortDeclList::push_for_replacement( NodeVec& mstack )
{
  NodeList::iterator it = _port_list.begin();
  for( ; it != _port_list.end(); ++it )
    mstack.push_back( *it );

  return OK;
}

int
VeriPortDeclList::replace_node( Uint idx, NodeList& node_list,
                                     NodeList::iterator& inl)
{
  // spawn duplicant
  VeriPortDeclList *tmp = new VeriPortDeclList( idx, this );

  // begin replacement
  NodeList::iterator itl = inl; 
  NodeList::iterator it = _port_list.begin();
  for( ; it != _port_list.end(); ++it )
  {
    tmp->_port_list.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  *inl = tmp;
  return OK;
}

};

