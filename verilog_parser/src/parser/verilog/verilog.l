%{
/*
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : verilog.l
 * Summary  : verilog syntax analyzer
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2012.10.28
 */

//#define YYDEBUG 1

#include <cstdio>
#include <cstring>
#include <ctype.h>
#include <string>
#include <unistd.h>
#include "veri_parser.hpp"
#include "verilog_yacc.hpp"

int yylex();
void count( void );
void count_line( void );
void yyerror( char * );
                    
void determin_line_cd( void );

int veriloglex_destroy( void );
int veri_col = 0;
int veri_lno = 1;
int just_end = 0;

int veri_include_line;
int veri_include_directive;
std::string veri_include_fname;

int verilog_parser_input( char *buf, int max )
{
  using rtl::VeriParser;
  if( veri_parser->get_cur_length() == 0 )
    return 0;

  if( max > veri_parser->get_cur_length() )
    max = veri_parser->get_cur_length();

  memcpy( buf, veri_parser->get_s(), max );
  veri_parser->set_cur_s( veri_parser->get_s() + max );
  veri_parser->set_cur_length( veri_parser->get_cur_length() - max );
  return max;
}

void verilog_parser_output( char * /*s*/, int /*size*/ )
{
}

#undef YY_INPUT
#define YY_INPUT(b, r, m)  ( r = verilog_parser_input(b,m) )

#undef YY_OUTPUT
#define YY_OUTPUT(str, size) (verilog_parser_output(str, size))

#define GET_CONTENT (veriloglval.symbol.content = strdup(yytext))

//%option prefix="verilog"
//%option nodefault
//%option batch
%}

D   [0-9_]
PD  [0-9]
L   [a-zA-Z_]
AL  [a-zA-Z_$]
H   [a-fA-F0-9_]
E   [Ee][+-]?{D}+
WHITE [ \t\b\r\f\n]
SPACE [ \t\b\r\f]
NONWHITE [^ \t\b\r\f\n]
NONID [^0-9_a-zA-Z$]


S    [sS]
BASE [hHoOdDbB]
XD   [0-9_a-fA-FXZxz]
XPD  [0-9a-fA-FXZxz]

%x _LINE_A_
%x _LINE_B_
%x _LINE_C_

%x _OMIT_

%x _DUMMY_PORT_A
%x _DUMMY_PORT_B
%x _DUMMY_PORT_C
%x _DUMMY_PORT_D

%%

    /*     Important */
"\0"               { veriloglex_destroy(); return MY_EOF;  }

<INITIAL>"`line"   { count_line(); BEGIN _LINE_A_;         }

<_LINE_A_>{PD}+    {
                     count_line();
                     veri_include_line = atoi(yytext);
                     BEGIN _LINE_B_;
                   }

<_LINE_B_>\".*?\"  {
                     count_line();
                     veri_include_fname = yytext;
                     BEGIN _LINE_C_;
                   }

<_LINE_C_>{PD}+{SPACE}*"\n" {
                     count_line();
                     veri_include_directive = atoi(yytext);
                     determin_line_cd();
                     BEGIN INITIAL;
                   }

<_LINE_A_>[\f\r\t\b ]+ { count(); }
<_LINE_B_>[\f\r\t\b ]+ { count(); }
<_LINE_C_>[\f\r\t\b ]+ { count(); }

<_LINE_A_>"\n" { count(); veri_lno++; BEGIN INITIAL; }
<_LINE_B_>"\n" { count(); veri_lno++; BEGIN INITIAL; }

<_LINE_A_>.    { BEGIN INITIAL; }
<_LINE_B_>.    { BEGIN INITIAL; }
<_LINE_C_>.    { BEGIN INITIAL; }

    /* general stuff */
<*>"\n"         { count(); veri_lno++;    }
<*>{SPACE}      { count();                }

    /* concat operators */
"{"     { count(); return '{';  }
"}"     { count(); return '}';  }

    /* Operators */
"("{WHITE}*"*"{WHITE}*")"   { count(); return vEVENT_ALL; }
"("{WHITE}*","              { yyless(1); count();
                              BEGIN _DUMMY_PORT_A; return '(' ;
                            }
","{WHITE}*","              { yyless(1); count();
                              BEGIN _DUMMY_PORT_A; return ','; 
                            }
","{WHITE}*")"              { yyless(1); count();
                              BEGIN _DUMMY_PORT_C; return ',';
                            }

<_DUMMY_PORT_A>{WHITE}*","  { yyless(strlen(yytext)-1); count();
                              BEGIN _DUMMY_PORT_B; return vDUMMY_PORT;
                            }

<_DUMMY_PORT_B>","          { count(); BEGIN INITIAL; return ','; } 

<_DUMMY_PORT_C>{WHITE}*")"  { yyless(strlen(yytext)-1); count();
                              BEGIN _DUMMY_PORT_D; return vDUMMY_PORT;
                            }

<_DUMMY_PORT_D>")"          { count(); BEGIN INITIAL; return ')'; }


"+"     { count(); return '+';  }
"-"     { count(); return '-';  }
"*"     { count(); return '*';  }
"/"     { count(); return '/';  }
"%"     { count(); return '%';  }
">"     { count(); return '>';  }
"<"     { count(); return '<';  }
"!"     { count(); return '!';  }
"~"     { count(); return '~';  }
"&"     { count(); return '&';  }
"|"     { count(); return '|';  }
"^"     { count(); return '^';  }
"?"     { count(); return '?';  }
"$"     { count(); return '$';  }
"="     { count(); return '=';  }

":"     { count(); return ':';  }
"#"     { count(); return '#';  }
"["     { count(); return '[';  }
"]"     { count(); return ']';  }
"("     { count(); return '(';  }
")"     { count(); return ')';  }
";"     { count(); return ';';  }
","     { count(); return ',';  }
"."     { count(); return '.';  }
"@"     { count(); return '@';  }

"&&"    { count(); return vANDAND;    }
"||"    { count(); return vOROR;      }
"<="    { count(); return vLE;        }
">="    { count(); return vGE;        }
"<<"    { count(); return vLOGIC_LS;  }
">>"    { count(); return vLOGIC_RS;  }
"=="    { count(); return vLOGIC_EQ;  }
"!="    { count(); return vLOGIC_INEQ;}
"^~"    { count(); return vXNOR;      }
"~^"    { count(); return vXNOR;      }
"~&"    { count(); return vNAND;      }
"~|"    { count(); return vNOR;       }
"**"    { count(); return vPOW;       }
"->"    { count(); return vNOT_SUPPORTED;    }
"=>"    { count(); return vNOT_SUPPORTED;    }
"*>"    { count(); return vNOT_SUPPORTED;    }
"+:"    { count(); return vRANGE_ADD; }
"-:"    { count(); return vRANGE_SUB; }
"(*"    { count(); return vATTR_L;   }
"*)"    { count(); return vATTR_R;   }

"==="   { count(); return vCASE_EQ;   }
"!=="   { count(); return vCASE_INEQ; }
"<<<"   { count(); return vARITH_LS;  }
">>>"   { count(); return vARITH_RS;  }
"&&&"   { count(); return vNOT_SUPPORTED; }

   /*     Keywords */

","{WHITE}*"input"{NONID}   { yyless(1); count(); return vPORT_DELIM;  }
","{WHITE}*"inout"{NONID}   { yyless(1); count(); return vPORT_DELIM;  }
","{WHITE}*"output"{NONID}  { yyless(1); count(); return vPORT_DELIM;  }

"specify"               { count(); BEGIN _OMIT_;   }
<_OMIT_>"endspecify"    { count(); BEGIN INITIAL;  }

"always"        { count(); return vALWAYS;    }
"and"           { count(); return vAND_ASCII; }
"assign"        { count(); return vASSIGN;    }
"automatic"     { count(); return vAUTO;      }
"begin"         { count(); return vBEGIN;     }
"buf"           { count(); return vBUF_ASCII; }
"bufif0"        { count(); return vBUFIF_A;   }
"bufif1"        { count(); return vBUFIF_B;   }
"case"          { count(); return vCASE;      }
"casex"         { count(); return vCASEX;     }
"casez"         { count(); return vCASEZ;     }
"cell"          { count(); return vCELL;      }
"cmos"          { count(); return vNOT_SUPPORTED;      }
"config"        { count(); return vCONFIG;    }
"deassign"      { count(); return vNOT_SUPPORTED;  }
"default"       { count(); return vDEFAULT;   }
"defparam"      { count(); return vNOT_SUPPORTED;  }
"design"        { count(); return vDESIGN;    }
"disable"       { count(); return vDISABLE;   }
"edge"          { count(); return vNOT_SUPPORTED;      }
"else"          { count(); return vELSE;      }
"end"           { count(); return vEND;       }
"endcase"       { count(); return vENDCASE;   }
"endconfig"     { count(); return vENDCONFIG; }
"endfunction"   { count(); return vENDFUNC;   }
"endgenerate"   { count(); return vENDGEN;    }
"endmodule"     { count(); return vENDMODULE; }
"endprimitive"  { count(); return vNOT_SUPPORTED;    }
"endtable"      { count(); return vNOT_SUPPORTED;  }
"endtask"       { count(); return vENDTASK;   }
"event"         { count(); return vNOT_SUPPORTED;     }
"for"           { count(); return vFOR;       }
"force"         { count(); return vNOT_SUPPORTED;     }
"forever"       { count(); return vNOT_SUPPORTED;   }
"fork"          { count(); return vNOT_SUPPORTED;      }
"function"      { count(); return vFUNC;      }
"generate"      { count(); return vGEN;       }
"genvar"        { count(); return vGENVAR;    }
"highz0"        { count(); return vHIGHZ_A;   }
"highz1"        { count(); return vHIGHZ_B;   }
"if"            { count(); return vIF;        }
"ifnone"        { count(); return vNOT_SUPPORTED;    }
"-incdir"       { count(); return vINC_DIR;   }
"initial"       { count(); return vINIT;      }
"inout"         { count(); return vINOUT;     }
"input"         { count(); return vINPUT;     }
"instance"      { count(); return vINSTANCE;  }
"integer"       { count(); return vINT;       }
"join"          { count(); return vNOT_SUPPORTED;      }
"large"         { count(); return vNOT_SUPPORTED;     }
"liblist"       { count(); return vLIBLIST;   }
"library"       { count(); return vLIB;       }
"localparam"    { count(); return vLOCAL_PARAM;  }
"macromodule"   { count(); return vNOT_SUPPORTED; }
"medium"        { count(); return vNOT_SUPPORTED;    }
"module"        { count(); return vMODULE;    }
"nand"          { count(); return vNAND_ASCII;}
"negedge"       { count(); return vNEGEDGE;   }
"nmos"          { count(); return vNOT_SUPPORTED;      }
"nor"           { count(); return vNOR_ASCII; }
"not"           { count(); return vNOT_ASCII; }
"notif0"        { count(); return vNOTIF_A;   }
"notif1"        { count(); return vNOTIF_B;   }
"or"            { count(); return vOR_ASCII;  }
"output"        { count(); return vOUTPUT;    }
"parameter"     { count(); return vPARAMETER; }
"pmos"          { count(); return vNOT_SUPPORTED;      }
"posedge"       { count(); return vPOSEDGE;   }
"primitive"     { count(); return vNOT_SUPPORTED; }
"pull0"         { count(); return vPULL_A;    }
"pull1"         { count(); return vPULL_B;    }
"pulldown"      { count(); return vNOT_SUPPORTED;  }
"pullup"        { count(); return vNOT_SUPPORTED;    }
"noshowcancelled"     { count(); return vNOT_SUPPORTED;  }
"pulsestyle_onevent"  { count(); return vNOT_SUPPORTED;  }
"pulsestyle_ondetect" { count(); return vNOT_SUPPORTED; }
"rcmos"         { count(); return vNOT_SUPPORTED;     }
"real"          { count(); return vNOT_SUPPORTED;      }
"realtime"      { count(); return vNOT_SUPPORTED;  }
"reg"           { count(); return vREG;       }
"release"       { count(); return vNOT_SUPPORTED;   }
"repeat"        { count(); return vNOT_SUPPORTED;    }
"rnmos"         { count(); return vNOT_SUPPORTED;     }
"rpmos"         { count(); return vNOT_SUPPORTED;     }
"rtran"         { count(); return vNOT_SUPPORTED;     }
"rtranif0"      { count(); return vNOT_SUPPORTED; }
"rtranif1"      { count(); return vNOT_SUPPORTED; }
"scalared"      { count(); return vSCALARED;  }
"showcancelled" { count(); return vNOT_SUPPORTED;  }
"signed"        { count(); return vSIGNED;    }
"small"         { count(); return vNOT_SUPPORTED;     }
"specparam"     { count(); return vSPEC_PARAM;}
"strong0"       { count(); return vSTRONG_A;  }
"strong1"       { count(); return vSTRONG_B;  }
"supply0"       { count(); return vSUPPLY_A;  }
"supply1"       { count(); return vSUPPLY_B;  }
"table"         { count(); return vNOT_SUPPORTED;     }
"task"          { count(); return vTASK;      }
"time"          { count(); return vTIME;      }
"tran"          { count(); return vNOT_SUPPORTED;      }
"tranif0"       { count(); return vNOT_SUPPORTED;  }
"tranif1"       { count(); return vNOT_SUPPORTED;  }
"tri"           { count(); return vTRI;       }
"tri0"          { count(); return vNOT_SUPPORTED;     }
"tri1"          { count(); return vNOT_SUPPORTED;     }
"triand"        { count(); return vTRIAND;    }
"trior"         { count(); return vTRIOR;     }
"trireg"        { count(); return vNOT_SUPPORTED;    }
"unsigned"      { count(); return vNOT_SUPPORTED;  }
"use"           { count(); return vUSE;       }
"vectored"      { count(); return vVECTORED;  }
"wait"          { count(); return vNOT_SUPPORTED;      }
"wand"          { count(); return vWAND;      }
"weak0"         { count(); return vWEAK_A;    }
"weak1"         { count(); return vWEAK_B;    }
"while"         { count(); return vNOT_SUPPORTED;     }
"wire"          { count(); return vWIRE;      }
"wor"           { count(); return vWOR;       }
"xnor"          { count(); return vXNOR_ASCII;}
"xor"           { count(); return vXOR_ASCII; }

   /* System Parts */
"PATHPULSE$" { count(); return vPATHPULSE;     }
"$signed"    { count(); return vSIGN_FUNC;     }
"$unsigned"  { count(); return vUNSIGN_FUNC;   }
"$setup"     { count(); return vNOT_SUPPORTED; }
"$hold"      { count(); return vNOT_SUPPORTED; }
"$setuphold" { count(); return vNOT_SUPPORTED; }
"$recovery"  { count(); return vNOT_SUPPORTED; }
"$removal"   { count(); return vNOT_SUPPORTED; }
"$recrem"    { count(); return vNOT_SUPPORTED; }
"$skew"      { count(); return vNOT_SUPPORTED; }
"$timeskew"  { count(); return vNOT_SUPPORTED; }
"$fullskew"  { count(); return vNOT_SUPPORTED; }
"$period"    { count(); return vNOT_SUPPORTED; }
"$width"     { count(); return vNOT_SUPPORTED; }
"$nochange"  { count(); return vNOT_SUPPORTED; }

   /* identifiers */
\".*?\"            { count(); GET_CONTENT; return vSTR;       }

"$"{L}({AL}|{PD})* { count(); GET_CONTENT; return vSYS_IDENT; }

{L}({AL}|{PD})*    { count(); GET_CONTENT; return vIDENT;     }

"\\"{NONWHITE}+    { count(); GET_CONTENT; return vIDENT;     }

{PD}?{D}+          { count(); GET_CONTENT; return vNUMBER;    }

({PD}?{D}*)?"'"{S}?{BASE}{SPACE}*{XD}+ { count(); GET_CONTENT; return vNUMBER; }

<_OMIT_>.    { count(); }
.            { count(); return ' '; }

%%

int yywrap(void)
{
  return 1;
}

void count(void)
{
  int i;

  for (i = 0; yytext[i] != '\0'; i++)
  {
    if (yytext[i] == '\n')
      veri_col = 0;
    else
      veri_col++;
  }
  veriloglval.symbol.col = veri_col;
  veriloglval.symbol.line = veri_lno;

//  ECHO;
}

void count_line(void)
{
  int i;

  for (i = 0; yytext[i] != '\0'; i++)
  {
    if (yytext[i] == '\n')
      veri_col = 0;
    else
      veri_col++;
  }

//  ECHO;
}

void determin_line_cd( )
{
  switch( veri_include_directive )
  {
    case 0:
      if( just_end == 1 )
      {
        veri_parser->back_to_file( veri_include_line );
        just_end = 0;
      }
      break;

    case 1:  // enter new included file
      veri_parser->include_file( veri_lno, veri_include_fname );
      break;

    case 2:  // end of included file
      veri_parser->exclude_file( );
      just_end = 1;
      break;

    default:
      break;
  }
  veri_lno = veri_include_line;
}
