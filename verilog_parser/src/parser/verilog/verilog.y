%{
/*
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : verilog.y
 * Summary  : verilog syntax analyzer
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2012.10.28
 */
#include <cstdlib>
#include <cstring>

#include "general_err.hpp"
#include "verilog_token.hpp"
#include "veri_node.hpp"
#include "veri_parser.hpp"

using rtl::OK;
using rtl::IS_TRUE;
using rtl::IS_FALSE;
using rtl::VeriAstNode;
using rtl::VeriNode;
using rtl::VeriParser;

#define YYINITDEPTH 1000
#define YYMAXDEPTH 50000

extern char yytext[];

int yylex();
extern int veriloglex_destroy( void );
extern int veri_col;
extern int veri_lno;

 /* RESPONCE in an error */
void yyerror(const char * /*str*/ )
{
  using pavio_singleton::Singleton;
  using rtl::VeriParser;
  veriloglex_destroy( );
}

%}

%union
{
  class VeriNode *tok;

  struct
  {
    int   col;
    int   line;
    char *content;
  }symbol;

  struct
  {
    int  col;
    int  line;
    int  extra[3];
    class VeriNode *node[3];
  }extra_tok;
}

%token MY_EOF
%token<symbol> vPORT_DELIM vEVENT_ALL vDUMMY_PORT
%token<symbol> vSIGN_FUNC vUNSIGN_FUNC

%token<symbol> vNOT_SUPPORTED

%token<symbol> vALWAYS vAND_ASCII vASSIGN vAUTO vBEGIN vBUF_ASCII vBUFIF_A
%token<symbol> vBUFIF_B vCASE vCASEX vCASEZ vCELL vCONFIG vDEFAULT
%token<symbol> vDESIGN vDISABLE vELSE vEND vENDCASE vENDCONFIG vENDFUNC
%token<symbol> vENDGEN vENDMODULE vENDSPEC vENDTASK
%token<symbol> vFOR vFUNC vGEN vGENVAR vHIGHZ_A vHIGHZ_B vIF
%token<symbol> vINC_DIR vINIT vINOUT vINPUT vINSTANCE vINT
%token<symbol> vLIBLIST vLIB vLOCAL_PARAM vMODULE vNAND_ASCII
%token<symbol> vNEGEDGE vNOR_ASCII vNOT_ASCII vNOTIF_A
%token<symbol> vNOTIF_B vOR_ASCII vOUTPUT vPARAMETER
%token<symbol> vPOSEDGE vPULL_A vPULL_B vREG vTRIAND vTRIOR
%token<symbol> vSCALARED vSIGNED vSPECIFY vSPEC_PARAM
%token<symbol> vSTRONG_A vSTRONG_B vSUPPLY_A vSUPPLY_B vTASK vTIME vTRI
%token<symbol> vUSE vVECTORED vWAND vWEAK_A vWEAK_B vWIRE vWOR
%token<symbol> vXNOR_ASCII vXOR_ASCII

%token<symbol> vIDENT vNUMBER vSYS_IDENT vCONCAT_IDENT vUNSIGNED_NUM

%token<symbol> vARITH_LS vARITH_RS vANDAND vXNOR vNAND vNOR vPOW
%token<symbol> vOROR vLE vGE vLOGIC_LS vLOGIC_RS vLOGIC_EQ vLOGIC_INEQ
%token<symbol> vCASE_EQ vCASE_INEQ

%token<symbol> '+' '=' '-' '*' '/' '%' '>' '<' '!' '~' '&' '|' '^' '?'

%token<symbol> '#' '@' '[' ']' '{' '}' ';' ',' '.' '(' ')' ':'
%token<symbol> vATTR_L vATTR_R
%token<symbol> vPRIMITIVE vENDPRI

%token<symbol> vSTR vFILE
%token<symbol> vRANGE_ADD vRANGE_SUB

  /* system part */
%token<symbol> vPATHPULSE
  
  /* priorities */
%nonassoc LOWER_THAN_ELSE
%nonassoc vELSE

%nonassoc LOWER_THAN_A
%nonassoc LOWER_THAN_B
%right '='
%left ',' vOR_ASCII
%right '?' ':'
%left vRANGE_ADD vRANGE_SUB
%left vOROR
%left vANDAND
%left '|'
%left '^'
%left '&'
%left vLOGIC_EQ vLOGIC_INEQ vCASE_EQ vCASE_INEQ
%left vGE vLE '<' '>'
%left vARITH_LS vARITH_RS vLOGIC_LS vLOGIC_RS
%left '+' '-'
%left '%' '*' '/'
%left vNAND vPOW vNOR vXNOR
%left UOR
%left UAND
%left UNEG
%left UNAND
%left UNOR
%left UXNOR
%left UXOR
%left ULNOT
%left UADD
%left UMINUS
%left '(' ')' '[' ']'
%nonassoc '.'
%nonassoc vATTR_L vATTR_R

  /* start symbol */
%type <tok> verilog_synthesis_unit

/* A.1 Source test */
/* A.1.1 Library source text */

/* A.1.2 Config source text */

/* A.1.3 Module and primitive source text */
%type <tok> source_text module_declaration
%type <extra_tok> module_port_part
%type <tok> module_item_list

/* A.1.4 Module parameters and ports */
%type <tok> param_decl_list
%type <tok> port_decl_list port_declaration
%type <extra_tok> port_type
%type <extra_tok> in_or_out

/* A.1.5 Module items */
%type <tok> module_item module_or_gen_item
%type <tok> module_or_gen_item_declaration

/* A.2 Declarations */
/* A.2.1 Declaration types */
/* A.2.1.1 Module parameter declarations */
%type <tok> param_declaration specparam_declaration
%type <extra_tok> param_declarator

/* A.2.1.2 Port declarations */
/* A.2.1.3 Type declarations */
%type <tok> genvar_declaration int_declaration
%type <tok> net_declaration reg_declaration
%type <extra_tok> vector_or_scalar
%type <extra_tok> net_decl_a net_decl_b

/* A.2.2 Declaration data types */
/* A.2.2.1 Net and variable types */
%type <extra_tok> net_type

/* A.2.2.2 Strengths */
%type <tok> drive_strength
%type <extra_tok> strength0 strength1

/* A.2.2.3 Delays */
%type <tok> delay3

/* A.2.3 Declaration lists */
%type <tok> specparam_assignment_list

/* A.2.4 Declaration assignments */
%type <tok> specparam_assignment

/* A.2.5 Declaration ranges */
/* A.2.6 Function declarations */
%type <tok> func_declaration func_item_declaration func_port_list
%type <tok> func_item_decl_list block_item_decl_list
%type <extra_tok> func_declarator

/* A.2.7 Task declarations */
%type <tok> task_declaration task_item_decl_list task_item_declaration
%type <tok> task_port_list tf_declaration
%type <extra_tok> tf_port_declarator

/* A.2.8 Block item declarations */
%type <tok> block_item_declaration

/* A.3 Primitive instances */
/* A.3.1 Primitive instantiation and instances */
%type <tok> gate_instantiation

/* A.3.2 Primitive strengths */
/* A.3.3 Primitive terminals */

/* A.3.4 Primitive gate and switch types */
%type <extra_tok> gatetype

/* A.4 Module and generated instantiation */
/* A.4.1 Module instantiation */
%type <tok> module_instantiation

/* A.4.2 Generated instantiation */
%type <tok> generated_instantiation gen_item_or_null gen_item gen_item_list
%type <tok> gen_cond_statement genvar_case_item gen_loop_statement
%type <tok> gen_block gen_case_statement
%type <tok> genvar_case_item_list

/* A.5 UDP declaration and instantiation */
/* A.5.1 UDP declaration */
%type <tok> udp_declaration

/* A.5.2 UDP ports */
/* A.5.3 UDP body */
/* A.5.4 UDP instantiation */

/* A.6 Behavioral statements */
/* A.6.1 Continuous assignment statements */
%type <tok> continuous_assign

/* A.6.2 Procedural blocks and assignments */
%type <tok> blocking_assignment
%type <tok> nonblocking_assignment func_stmt_or_null

/* A.6.3 Parallel and sequential blocks */
%type <tok> func_seq_block seq_block
%type <tok> func_stmt_list stmt_list

/* A.6.4 Statements */
%type <tok> statement stmt_or_null func_statement

/* A.6.5 Timing control statements */
%type <tok> event_control disable_statement
%type <tok> procedural_timing_control_statement

/* A.6.6 Conditioinal statements */
%type <tok> cond_statement func_cond_statement

/* A.6.7 Case statements */
%type <tok> case_statement case_item case_item_list
%type <tok> func_case_statement func_case_item func_case_item_list

/* A.6.8 Looping statements */
%type <tok> func_loop_statement loop_statement

/* A.6.9 Task enable statements */
%type <tok> sys_caller

/* A.7 Specify section */
/* A.7.1 Specify block declaration */
/* A.7.2 Specify path declaration */
/* A.7.3 Specify block terminals */
/* A.7.4 Specify path delays */
/* A.7.5 System timing checks */
/* A.7.5.1 System timing check commands */
/* A.7.5.2 System timing check command arguments */
/* A.7.5.3 System timing check event definitions */

/* A.8 Expressions */
/* A.8.1 Concatenations */
%type <tok> concat multi_concat

/* A.8.2 Function calls */
/* A.8.3 Expressions */
%type <tok> expression assignment_expression postfix_expression
%type <tok> unary_expression pow_expression multiplicative_expression
%type <tok> additive_expression shift_expression relational_expression
%type <tok> equality_expression and_expression xor_expression
%type <tok> bit_or_expression logical_and_expression logical_or_expression
%type <tok> conditional_expression
%type <tok> mintypmax_expression
%type <tok> range

/* A.8.4 Primaries */
%type <tok> primary

/* A.8.5 Expression left_side values */
/* A.8.6 Operators */
/* A.8.7 Numbers */

/* A.9 General */
/* A.9.l Attributes */
%type <tok> attr_inst_list attr_instance

/* A.9.2 Comments */
/* A.9.3 Identifiers */

%type <tok> error

%start verilog_synthesis_unit

%%

/* Whole */
verilog_synthesis_unit
  : source_text MY_EOF
    {
      YYACCEPT;
    }
  ;

/* A.1 Source text */
/* A.1.1 Library source text */
/* A.1.3 Module and primitive source text */

/* udp_declaration not supported */
source_text
  : module_declaration
    {
    }
  | udp_declaration
    {
    }
  | source_text module_declaration
    {
    }
  | source_text udp_declaration
    {
    }
  ;

/* module and endmodule status added to ignore # */
module_declaration
  : vMODULE vIDENT ';' vENDMODULE
    {
      veri_parser->gen_mod( $1.line, $1.col, $2.content, NULL, NULL, NULL );
      free($2.content);
    }
  | vMODULE vIDENT module_port_part ';' vENDMODULE
    {
      veri_parser->gen_mod( $1.line, $1.col, $2.content, $3.node[0],
                            $3.node[1], NULL );
      free($2.content);
    }
  | vMODULE vIDENT ';' module_item_list vENDMODULE
    {
      veri_parser->gen_mod( $1.line, $1.col, $2.content, NULL, NULL, $4 );
      free($2.content);
    }
  | vMODULE vIDENT module_port_part ';' module_item_list
    vENDMODULE
    {
      veri_parser->gen_mod( $1.line, $1.col, $2.content, $3.node[0],
                            $3.node[1], $5 );
      free($2.content);
    }
  | attr_inst_list vMODULE vIDENT ';' vENDMODULE
    {
      veri_parser->gen_mod( $2.line, $2.col, $3.content, NULL, NULL, NULL );
      free($3.content);
    }
  | attr_inst_list vMODULE vIDENT module_port_part ';' vENDMODULE
    {
      veri_parser->gen_mod( $2.line, $2.col, $3.content, $4.node[0],
                            $4.node[1], NULL );
      free($3.content);
    }
  | attr_inst_list vMODULE vIDENT ';' module_item_list vENDMODULE
    {
      veri_parser->gen_mod( $2.line, $2.col, $3.content, NULL, NULL, $5 );
      free($3.content);
    }
  | attr_inst_list vMODULE vIDENT module_port_part ';' module_item_list
    vENDMODULE
    {
      veri_parser->gen_mod( $2.line, $2.col, $3.content, $4.node[0],
                            $4.node[1], $6 );
      free($3.content);
    }
  ;

module_port_part
  : '(' ')'
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.node[0] = NULL;
      $$.node[1] = NULL;
    }
  | '(' expression ')'
    {
      /* arg-list or .arg()-list */
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.node[0] = NULL;
      $$.node[1] = $2;
    }
  | '(' port_decl_list ')'
    {
      /* arg-list or .arg()-list */
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.node[0] = NULL;
      $$.node[1] = $2;
    }
  | '#' '(' param_decl_list ')'
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.node[0] = $3;
      $$.node[1] = NULL;
    }
  | '#' '(' param_decl_list ')' '(' ')'
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.node[0] = $3;
      $$.node[1] = NULL;
    }
  | '#' '(' param_decl_list ')' '(' expression ')'
    {
      /* arg-list or .arg()-list */
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.node[0] = $3;
      $$.node[1] = $6;
    }
  | '#' '(' param_decl_list ')' '(' port_decl_list ')'
    {
      /* arg-list or .arg()-list */
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.node[0] = $3;
      $$.node[1] = $6;
    }
  ;

module_item_list
  : module_item
    {
      $$ = veri_parser->gen_mod_item_list( $1 );
    }
  | module_item_list module_item
    {
      $$ = veri_parser->append_mod_item_list( $1, $2 );
    }
  ;

/* A.1.4 Module parameters and ports */
param_decl_list
  : param_declaration
    {
      $$ = veri_parser->gen_param_decl_list( $1 );
    }
  | param_decl_list ',' param_declaration
    {
      $$ = veri_parser->append_param_decl_list( $1, $3 );
    }
  ;


port_decl_list
  : port_declaration
    {
      $$ = veri_parser->gen_port_decl_list( $1 );
    }
  | port_decl_list vPORT_DELIM port_declaration
    {
      $$ = veri_parser->append_port_decl_list( $1, $3 );
    }
  ;


port_declaration
  : in_or_out expression ';'
    {
      $$ = veri_parser->gen_var_decl( $1.line, $1.col, $1.extra[0],
                                           vvWIRE, IS_FALSE, vpSCALAR, NULL,
                                           NULL, NULL, $2 );
    }
  | in_or_out port_type expression ';'
    {
      $$ = veri_parser->gen_var_decl( $1.line, $1.col, $1.extra[0],
                                           $2.extra[0], $2.extra[1], vpSCALAR,
                                           NULL, NULL, NULL,
                                           $3 );
    }
  | attr_inst_list in_or_out expression ';'
    {
      $$ = veri_parser->gen_var_decl( $2.line, $2.col, $2.extra[0],
                                           vvWIRE, IS_FALSE, vpSCALAR, NULL,
                                           NULL, NULL, $3 );
    }
  | attr_inst_list in_or_out port_type expression ';'
    {
      $$ = veri_parser->gen_var_decl( $2.line, $2.col, $2.extra[0],
                                           $3.extra[0], $3.extra[1], vpSCALAR,
                                           NULL, NULL, NULL,
                                           $4 );
    }
  | in_or_out range expression ';'
    {
      $$ = veri_parser->gen_var_decl( $1.line, $1.col, $1.extra[0],
                                           vvWIRE, IS_FALSE, vpSCALAR, NULL,
                                           $2, NULL, $3 );
    }
  | in_or_out port_type range expression ';'
    {
      $$ = veri_parser->gen_var_decl( $1.line, $1.col, $1.extra[0],
                                           $2.extra[0], $2.extra[1], vpSCALAR,
                                           NULL, $3, NULL,
                                           $4 );
    }
  | attr_inst_list in_or_out range expression ';'
    {
      $$ = veri_parser->gen_var_decl( $2.line, $2.col, $2.extra[0],
                                           vvWIRE, IS_FALSE, vpSCALAR, NULL,
                                           $3, NULL, $4 );
    }
  | attr_inst_list in_or_out port_type range expression ';'
    {
      $$ = veri_parser->gen_var_decl( $2.line, $2.col, $2.extra[0],
                                           $3.extra[0], $3.extra[1], vpSCALAR,
                                           NULL, $4, NULL,
                                           $5 );
    }
  ;

in_or_out
  : vINPUT
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpIN;
    }
  | vOUTPUT
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpOUT;
    }
  | vINOUT
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpINOUT;
    }
  ;

port_type
  : vSIGNED
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvWIRE;
      $$.extra[1] = IS_TRUE;
    }
  | net_type
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = $1.extra[0];
      $$.extra[1] = IS_FALSE;
    }
  | net_type vSIGNED
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = $1.extra[0];
      $$.extra[1] = IS_TRUE;
    }
  | vREG
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvREG;
      $$.extra[1] = IS_FALSE;
    }
  | vREG vSIGNED
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvREG;
      $$.extra[1] = IS_TRUE;
    }
  | vINT
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvINT;
      $$.extra[1] = IS_FALSE;
    }
  | vTIME
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvTIME;
      $$.extra[1] = IS_FALSE;
    }
  ;

/* A.1.5 Module items */
/* compress module_item and non_module_item here */
module_item
  : port_declaration
    {
      $$ = $1;
    }
  | module_or_gen_item
    {
      $$ = $1;
    }
  | generated_instantiation
    {
      $$ = $1;
    }
  | param_declaration
    {
      $$ = $1;
    }
  | specparam_declaration
    {
      $$ = $1;
    }
  | attr_inst_list module_or_gen_item
    {
      $$ = $2;
    }
  | attr_inst_list generated_instantiation
    {
      $$ = $2;
    }
  | attr_inst_list param_declaration
    {
      $$ = $2;
    }
  | attr_inst_list specparam_declaration
    {
      $$ = $2;
    }
  | error ';'
    {
      $$ = veri_parser->gen_dummy_stmt( veri_lno, veri_col );
    }
  ;

module_or_gen_item
  : module_or_gen_item_declaration
    {
      $$ = $1;
    }
  | continuous_assign
    {
      $$ = $1;
    }
  | gate_instantiation
    {
      $$ = $1;
    }
  | module_instantiation
    {
      $$ = $1;
    }
  | vINIT statement
    {
      $$ = veri_parser->gen_init_stmt( $1.line, $1.col, $2 );
    }
  | vALWAYS statement
    {
      $$ = veri_parser->gen_always_stmt( $1.line, $1.col, $2 );
    }
  ;

module_or_gen_item_declaration
  : net_declaration
    {
      $$ = $1;
    }
  | reg_declaration
    {
      $$ = $1;
    }
  | int_declaration
    {
      $$ = $1;
    }
  | genvar_declaration
    {
      $$ = $1;
    }
  | task_declaration
    {
      $$ = $1;
    }
  | func_declaration
    {
      $$ = $1;
    }
  ;

/* A.2 Declaration */

/* A.2.1 Declaration types */

/* A.2.1.1 Module parameter declarations */
param_declarator
  : vLOCAL_PARAM
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = IS_FALSE;
      $$.extra[2] = IS_TRUE;
    }
  | vPARAMETER
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = IS_FALSE;
      $$.extra[2] = IS_FALSE;
    }
  | vLOCAL_PARAM vSIGNED
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_TRUE;
      $$.extra[1] = IS_FALSE;
      $$.extra[2] = IS_TRUE;
    }
  | vLOCAL_PARAM vINT
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = IS_TRUE;
      $$.extra[2] = IS_TRUE;
    }
  | vPARAMETER vSIGNED
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_TRUE;
      $$.extra[1] = IS_FALSE;
      $$.extra[2] = IS_FALSE;
    }
  | vPARAMETER vINT
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = IS_TRUE;
      $$.extra[2] = IS_FALSE;
    }
  ;

param_declaration
  : param_declarator expression ';'
    {
      /* expression-list or .arg()-list */
      $$ = veri_parser->gen_param_decl( $1.line, $1.col, $1.extra[0],
                                             $1.extra[1], $1.extra[2],
                                             NULL, $2 );
    }
  | param_declarator range expression ';'
    {
      /* expression-list or .arg()-list */
      $$ = veri_parser->gen_param_decl( $1.line, $1.col, $1.extra[0],
                                             $1.extra[1], $1.extra[2],
                                             $2, $3 );
    }
  ;

specparam_declaration
  : vSPEC_PARAM expression ';'
    {
      // vIDENT = mintypmax-or-range-expression
      // const-mintypmax-expression
    }
  | vSPEC_PARAM specparam_assignment_list ';'
    {
      // first could be range[a:b]
    }
  | vSPEC_PARAM range expression ';'
    {
      // vIDENT = mintypmax-or-range-expression
      // const-mintypmax-expression
    }
  | vSPEC_PARAM range specparam_assignment_list ';'
    {
      // first could be range[a:b]
    }
  ;

/* A.2.1.2 Port declarations */
/* A.2.1.3 Type declaratioins */
genvar_declaration
  : vGENVAR expression ';'
    {
      // genvar-identifier-list
      $$ = veri_parser->gen_genvar_decl( $1.line, $1.col, $2 );
    }
  ;

int_declaration
  : vINT expression ';'
    {
      // var-identifier-list -> expression
      $$ = veri_parser->gen_int_decl( $1.line, $1.col, $2 );
    }
  ;

net_declaration
  : net_type expression ';'
    {
      // { vIDENT + dimension_list } _list
      // or .id() -list
      $$ = veri_parser->gen_var_decl( $1.line, $1.col, vpNO_IO, $1.extra[0],
                                           IS_FALSE, vpSCALAR, NULL, NULL,
                                           NULL, $2  );
    }
  | net_type net_decl_a expression ';'
    {
      // { vIDENT + dimension_list } _list
      // or .id() -list
      $$ = veri_parser->gen_var_decl( $1.line, $1.col, vpNO_IO, $1.extra[0],
                                           $2.extra[0], $2.extra[1],
                                           NULL, $2.node[0],
                                           $2.node[1], $3  );
    }
  | net_type net_decl_b expression ';'
    {
      // const-assignment-exp, type-1
      // or .id() -list
      $$ = veri_parser->gen_var_decl( $1.line, $1.col, vpNO_IO, $1.extra[0],
                                           $2.extra[0], $2.extra[1],
                                           $2.node[2], $2.node[0],
                                           $2.node[1], $3  );
    }
  ;

net_decl_a
  : range
    {
      $$.line = $1->line();
      $$.col  = $1->col();
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = vpSCALAR;
      $$.node[0] = $1;
      $$.node[1] = NULL;
    }
  | vSIGNED
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_TRUE;
      $$.extra[1] = vpSCALAR;
      $$.node[0] = NULL;
      $$.node[1] = NULL;
    }
  | vector_or_scalar
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = $1.extra[0];
      $$.node[0] = NULL;
      $$.node[1] = NULL;
    }
  | delay3
    {
      $$.line = $1->line();
      $$.col  = $1->col();
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = vpSCALAR;
      $$.node[0] = NULL;
      $$.node[1] = $1;
    }
  | vSIGNED range
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_TRUE;
      $$.extra[1] = vpSCALAR;
      $$.node[0] = $2;
      $$.node[1] = NULL;
    }
  | vector_or_scalar range
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = $1.extra[0];
      $$.node[0] = $2;
      $$.node[1] = NULL;
    }
  | vSIGNED delay3
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_TRUE;
      $$.extra[1] = vpSCALAR;
      $$.node[0] = NULL;
      $$.node[1] = $2;
    }
  | vector_or_scalar delay3
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = $1.extra[0];
      $$.node[0] = NULL;
      $$.node[1] = $2;
    }
  | vSIGNED range delay3
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_TRUE;
      $$.extra[1] = vpSCALAR;
      $$.node[0] = $2;
      $$.node[1] = $3;
    }
  | vector_or_scalar range delay3
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = $1.extra[0];
      $$.node[0] = $2;
      $$.node[1] = $3;
    }
  ;

net_decl_b
  : drive_strength
    {
      $$.line = $1->line();
      $$.col  = $1->col();
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = vpSCALAR;
      $$.node[0] = NULL;
      $$.node[1] = NULL;
      $$.node[2] = $1;
    }
  | drive_strength net_decl_a
    {
      $$.line = $1->line();
      $$.col  = $1->col();
      $$.extra[0] = $2.extra[0];
      $$.extra[1] = $2.extra[1];
      $$.node[0] = $2.node[0];
      $$.node[1] = $2.node[1];
      $$.node[2] = $1;
    }
  ;

vector_or_scalar
  : vVECTORED
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpVECTOR;
    }
  | vSCALARED
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpSCALAR;
    }
  ;

reg_declaration
  : vREG expression ';'
    {
      // var-identifier-list -> expression
      $$ = veri_parser->gen_var_decl( $1.line, $1.col, vpNO_IO, vvREG,
                                      IS_FALSE, vpSCALAR, NULL, NULL,
                                      NULL, $2  );
    }
  | vREG vSIGNED expression ';'
    {
      // var-identifier-list -> expression
      $$ = veri_parser->gen_var_decl( $1.line, $1.col, vpNO_IO, vvREG,
                                           IS_TRUE, vpSCALAR, NULL, NULL,
                                           NULL, $3 );
    }
  | vREG range expression ';'
    {
      // var-identifier-list -> expression
      $$ = veri_parser->gen_var_decl( $1.line, $1.col, vpNO_IO, vvREG,
                                           IS_FALSE, vpSCALAR, NULL, $2,
                                           NULL, $3  );
    }
  | vREG vSIGNED range expression ';'
    {
      // var-identifier-list -> expression
      $$ = veri_parser->gen_var_decl( $1.line, $1.col, vpNO_IO, vvREG,
                                           IS_TRUE, vpSCALAR, NULL, $3,
                                           NULL, $4  );
    }
  ;

/* A.2.2 Declaration data types */

/* A.2.2.1 Net and variable types */
net_type
  : vSUPPLY_A
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvSUPPLY0;
    }
  | vSUPPLY_B
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvSUPPLY1;
    }
  | vTRI
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvTRI;
    }
  | vTRIAND
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvTRIAND;
    }
  | vTRIOR
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvTRIOR;
    }
  | vWIRE
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvWIRE;
    }
  | vWAND
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvWAND;
    }
  | vWOR
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvWOR;
    }
  ;

/* A.2.2.2 Strengths */
drive_strength
  : '(' strength0 ',' strength1 ')'
    {
      $$ = veri_parser->gen_drive_exp( $1.line, $1.col, $2.extra[0],
                                            $4.extra[0] );
    }
  | '(' strength1 ',' strength0 ')'
    {
      $$ = veri_parser->gen_drive_exp( $1.line, $1.col, $2.extra[0],
                                            $4.extra[0] );
    }
  | '(' strength0 ',' vHIGHZ_A ')'
    {
      $$ = veri_parser->gen_drive_exp( $1.line, $1.col, $2.extra[0],
                                            vpHIGHZ0 );
    }
  | '(' strength1 ',' vHIGHZ_B ')'
    {
      $$ = veri_parser->gen_drive_exp( $1.line, $1.col, $2.extra[0],
                                            vpHIGHZ1 );
    }
  | '(' vHIGHZ_A ',' strength1 ')'
    {
      $$ = veri_parser->gen_drive_exp( $1.line, $1.col, vpHIGHZ0,
                                            $4.extra[0] );
    }
  | '(' vHIGHZ_B ',' strength0 ')'
    {
      $$ = veri_parser->gen_drive_exp( $1.line, $1.col, vpHIGHZ1,
                                            $4.extra[0] );
    }
  ;

strength0
  : vSUPPLY_A
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpSUPPLY0;
    }
  | vSTRONG_A
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpSTRONG0;
    }
  | vPULL_A
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpPULL0;
    }
  | vWEAK_A
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpWEAK0;
    }
  ;

strength1
  : vSUPPLY_B
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpSUPPLY1;
    }
  | vSTRONG_B
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpSTRONG1;
    }
  | vPULL_B
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpPULL1;
    }
  | vWEAK_B
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vpWEAK1;
    }
  ;

/* A.2.2.3 Delays */
delay3
  : '#' expression
    {
      $$ = veri_parser->gen_delay_exp( $1.line, $1.col, $2, NULL );
    }
  | '#' '(' expression ',' expression ')'
    {
      $$ = veri_parser->gen_delay_exp( $1.line, $1.col, $3, $5 );
    }
  ;

/* A.2.3 Declaration lists */
specparam_assignment_list
  : specparam_assignment
    {
    }
  | specparam_assignment_list ',' specparam_assignment
    {
    }
  ;

/* A.2.4 Declaration assignments */
specparam_assignment
  : vPATHPULSE ';'
    {
    }
  ;

/* A.2.5 Declaration ranges */
/* A.2.6 Function declarations */
func_declaration
  : vFUNC vIDENT ';'
    func_item_decl_list func_statement vENDFUNC
    {
      // expression is id
      $$ = veri_parser->gen_func_decl( $1.line, $1.col, IS_FALSE, 
                                  IS_FALSE, IS_FALSE, NULL, $2.content,
                                  NULL, $4, $5 );
      free( $2.content );
    }
  | vFUNC vIDENT '(' func_port_list ')' ';'
    block_item_decl_list func_statement vENDFUNC
    {
      // expression is id
      $$ = veri_parser->gen_func_decl( $1.line, $1.col, IS_FALSE,
                                  IS_FALSE, IS_FALSE, NULL, $2.content,
                                  $4, $7, $8 );
      free( $2.content );
    }
  | vFUNC range vIDENT ';'
    func_item_decl_list func_statement vENDFUNC
    {
      // last exp is id
      $$ = veri_parser->gen_func_decl( $1.line, $1.col, IS_FALSE,
                                  IS_FALSE, IS_FALSE, $2, $3.content,
                                  NULL, $5, $6 );
      free( $3.content );
    }
  | vFUNC range vIDENT '(' func_port_list ')' ';'
    block_item_decl_list func_statement vENDFUNC
    {
      // last exp is id
      $$ = veri_parser->gen_func_decl( $1.line, $1.col, IS_FALSE,
                                  IS_FALSE, IS_FALSE, $2, $3.content,
                                  $5, $8, $9 );
      free( $3.content );
    }
  | vFUNC func_declarator vIDENT ';'
    func_item_decl_list func_statement vENDFUNC
    {
      // expression is id
      $$ = veri_parser->gen_func_decl( $1.line, $1.col, $2.extra[0],
                                  $2.extra[1], $2.extra[2], NULL, $3.content,
                                  NULL, $5, $6 );
      free( $3.content );
    }
  | vFUNC func_declarator vIDENT '(' func_port_list ')' ';'
    block_item_decl_list func_statement vENDFUNC
    {
      // expression is id
      $$ = veri_parser->gen_func_decl( $1.line, $1.col, $2.extra[0],
                                  $2.extra[1], $2.extra[2], NULL, $3.content,
                                  $5, $8, $9 );
      free( $3.content );
    }
  | vFUNC func_declarator range vIDENT ';'
    func_item_decl_list func_statement vENDFUNC
    {
      // last exp is id
      $$ = veri_parser->gen_func_decl( $1.line, $1.col, $2.extra[0],
                                  $2.extra[1], $2.extra[2], $3, $4.content,
                                  NULL, $6, $7 );
      free( $4.content );
    }
  | vFUNC func_declarator range vIDENT '(' func_port_list ')' ';'
    block_item_decl_list func_statement vENDFUNC
    {
      // last exp is id
      $$ = veri_parser->gen_func_decl( $1.line, $1.col, $2.extra[0],
                                  $2.extra[1], $2.extra[2], $3, $4.content,
                                  $6, $9, $10 );
      free( $4.content );
    }
  ;

func_declarator
  : vINT
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = IS_FALSE;
      $$.extra[2] = IS_TRUE;
    }
  | vSIGNED
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = IS_TRUE;
      $$.extra[2] = IS_FALSE;
    }
  | vSIGNED vINT
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_FALSE;
      $$.extra[1] = IS_TRUE;
      $$.extra[2] = IS_TRUE;
    }
  | vAUTO
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_TRUE;
      $$.extra[1] = IS_FALSE;
      $$.extra[2] = IS_FALSE;
    }
  | vAUTO vINT
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_TRUE;
      $$.extra[1] = IS_FALSE;
      $$.extra[2] = IS_TRUE;
    }
  | vAUTO vSIGNED
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_TRUE;
      $$.extra[1] = IS_TRUE;
      $$.extra[2] = IS_FALSE;
    }
  | vAUTO vSIGNED vINT
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = IS_TRUE;
      $$.extra[1] = IS_TRUE;
      $$.extra[2] = IS_TRUE;
    }
  ;

func_item_decl_list
  : func_item_declaration
    {
      $$ = veri_parser->gen_func_item_list( $1 );
    }
  | func_item_decl_list func_item_declaration
    {
      $$ = veri_parser->append_func_item_list( $1, $2 );
    }
  ;

block_item_decl_list
  : block_item_declaration
    {
      $$ = veri_parser->gen_block_item_list( $1 );
    }
  | block_item_decl_list block_item_declaration
    {
      $$ = veri_parser->append_block_item_list( $1, $2 );
    }
  ;

func_item_declaration
  : block_item_declaration
    {
      $$ = $1;
    }
  | tf_declaration ';'
    {
      $$ = $1;
    }
  ;

func_port_list
  : tf_declaration
    {
      $$ = veri_parser->gen_fp_list( $1 );
    }
  | func_port_list vPORT_DELIM tf_declaration
    {
      $$ = veri_parser->append_fp_list( $1, $3 );
    }
  ;

/* A.2.7 Task declarations */
task_declaration
  : vTASK vIDENT ';' statement vENDTASK
    {
      $$ = veri_parser->gen_task_decl( $1.line, $1.col, IS_FALSE,
                                  $2.content, NULL, NULL, $4 );
      free($2.content);
    }
  | vTASK vIDENT ';' task_item_decl_list statement vENDTASK
    {
      $$ = veri_parser->gen_task_decl( $1.line, $1.col, IS_FALSE,
                                  $2.content, NULL, $4, $5 );
      free($2.content);
    }
  | vTASK vAUTO vIDENT ';' statement vENDTASK
    {
      $$ = veri_parser->gen_task_decl( $1.line, $1.col, IS_TRUE,
                                  $3.content, NULL, NULL, $5 );
      free($3.content);
    }
  | vTASK vAUTO vIDENT ';' task_item_decl_list statement vENDTASK
    {
      $$ = veri_parser->gen_task_decl( $1.line, $1.col, IS_TRUE,
                                  $3.content, NULL, $5, $6 );
      free($3.content);
    }
  | vTASK vIDENT '(' task_port_list ')' ';' statement vENDTASK
    {
      $$ = veri_parser->gen_task_decl( $1.line, $1.col, IS_FALSE,
                                  $2.content, $4, NULL, $7 );
      free($2.content);
    }
  | vTASK vIDENT '(' task_port_list ')' ';'
    block_item_decl_list statement vENDTASK
    {
      $$ = veri_parser->gen_task_decl( $1.line, $1.col, IS_FALSE,
                                  $2.content, $4, $7, $8 );
      free($2.content);
    }
  | vTASK vAUTO vIDENT '(' task_port_list ')' ';'
    statement vENDTASK
    {
      $$ = veri_parser->gen_task_decl( $1.line, $1.col, IS_TRUE,
                                  $3.content, $5, NULL, $8 );
      free($3.content);
    }
  | vTASK vAUTO vIDENT '(' task_port_list ')' ';'
    block_item_decl_list statement vENDTASK
    {
      $$ = veri_parser->gen_task_decl( $1.line, $1.col, IS_TRUE,
                                  $3.content, $5, $8, $9 );
      free($3.content);
    }
  ;

task_item_decl_list
  : task_item_declaration
    {
      $$ = veri_parser->gen_task_item_list( $1 );
    }
  | task_item_decl_list task_item_declaration
    {
      $$ = veri_parser->append_task_item_list( $1, $2 );
    }
  ;

task_item_declaration
  : block_item_declaration
    {
      $$ = $1;
    }
  | tf_declaration
    {
      $$ = $1;
    }
  ;

task_port_list
  : tf_declaration
    {
      $$ = veri_parser->gen_tp_list( $1 );
    }
  | task_port_list vPORT_DELIM tf_declaration
    {
      $$ = veri_parser->append_tp_list( $1, $3 );
    }
  ;

tf_port_declarator
  : vSIGNED
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvWIRE;
      $$.extra[1] = IS_TRUE;
    }
  | vREG
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvREG;
      $$.extra[1] = IS_FALSE;
    }
  | vREG vSIGNED
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvREG;
      $$.extra[1] = IS_TRUE;
    }
  | vINT
    {
      $$.line = $1.line;
      $$.col  = $1.col;
      $$.extra[0] = vvINT;
      $$.extra[1] = IS_FALSE;
    }
  ;

tf_declaration
  : in_or_out expression
    {
      // port-identifier-list
      $$ = veri_parser->gen_tf_declaration( $1.line, $1.col, $1.extra[0],
                                           vvWIRE, IS_FALSE, NULL, $2 );
    }
  | in_or_out tf_port_declarator expression
    {
      // port-identifier-list
      $$ = veri_parser->gen_tf_declaration( $1.line, $1.col, $1.extra[0],
                                           $2.extra[0], $2.extra[1], NULL,
                                           $3 );
    }
  | attr_inst_list in_or_out expression
    {
      // port-identifier-list
      $$ = veri_parser->gen_tf_declaration( $2.line, $2.col, $2.extra[0],
                                           vvWIRE, IS_FALSE, NULL, $3 );
    }
  | attr_inst_list in_or_out tf_port_declarator expression
    {
      // port-identifier-list
      $$ = veri_parser->gen_tf_declaration( $2.line, $2.col, $2.extra[0],
                                           $3.extra[0], $3.extra[1], NULL,
                                           $4 );
    }
  | in_or_out range expression
    {
      // port-identifier-list
      $$ = veri_parser->gen_tf_declaration( $1.line, $1.col, $1.extra[0],
                                           vvWIRE, IS_FALSE, $2, $3 );
    }
  | in_or_out tf_port_declarator range expression
    {
      // port-identifier-list
      $$ = veri_parser->gen_tf_declaration( $1.line, $1.col, $1.extra[0],
                                           $2.extra[0], $2.extra[1], $3,
                                           $4 );
    }
  | attr_inst_list in_or_out range expression
    {
      // port-identifier-list
      $$ = veri_parser->gen_tf_declaration( $2.line, $2.col, $2.extra[0],
                                           vvWIRE, IS_FALSE, $3,
                                           $4 );
    }
  | attr_inst_list in_or_out tf_port_declarator range expression
    {
      // port-identifier-list
      $$ = veri_parser->gen_tf_declaration( $2.line, $2.col, $2.extra[0],
                                           $3.extra[0], $3.extra[1], $4,
                                           $5 );
    }
  ;

/* A.2.8 Block item declarations */
block_item_declaration
  : reg_declaration
    {
      $$ = $1;
    }
  | int_declaration
    {
      $$ = $1;
    }
  | param_declaration
    {
      $$ = $1;
    }
  | attr_inst_list reg_declaration
    {
      $$ = $2;
    }
  | attr_inst_list int_declaration
    {
      $$ = $2;
    }
  | attr_inst_list param_declaration
    {
      $$ = $2;
    }
  ;

/* A.3 Primitive instances */
/* A.3.1 Primitive instantiation and instances */

/*
 *   expression is gate-instance-list, with brackets-exp-list,
 * or caller-expression-list
 *
 *   TODO: check '( ')' and gate-specific-correction
 */
gate_instantiation
  : gatetype expression ';'
    {
      $$ = veri_parser->gen_gate_instantiation( $1.line, $1.col,
                               $1.extra[0], NULL, NULL, $2 );
    }
  | gatetype drive_strength expression ';'
    {
      $$ = veri_parser->gen_gate_instantiation( $1.line, $1.col,
                               $1.extra[0], $2, NULL, $3 );
    }
  | gatetype delay3 expression ';'
    {
      $$ = veri_parser->gen_gate_instantiation( $1.line, $1.col,
                               $1.extra[0], NULL, $2, $3 );
    }
  | gatetype drive_strength delay3 expression ';'
    {
      $$ = veri_parser->gen_gate_instantiation( $1.line, $1.col,
                               $1.extra[0], $2, $3, $4 );
    }
  ;

/* A.3.2 Primitive strengths */
/* A.3.3 Primitive terminals */

/* A.3.4 Primitive gate and switch types */
gatetype
  : vBUFIF_A
    {
      $$.line  = $1.line;
      $$.col   = $1.col;
      $$.extra[0] = vpBUFFIF0;
    }
  | vBUFIF_B
    {
      $$.line  = $1.line;
      $$.col   = $1.col;
      $$.extra[0] = vpBUFFIF1;
    }
  | vNOTIF_A
    {
      $$.line  = $1.line;
      $$.col   = $1.col;
      $$.extra[0] = vpNOTIF0;
    }
  | vNOTIF_B
    {
      $$.line  = $1.line;
      $$.col   = $1.col;
      $$.extra[0] = vpNOTIF1;
    }
  | vAND_ASCII
    {
      $$.line  = $1.line;
      $$.col   = $1.col;
      $$.extra[0] = vpAND;
    }
  | vNAND_ASCII
    {
      $$.line  = $1.line;
      $$.col   = $1.col;
      $$.extra[0] = vpNAND;
    }
  | vOR_ASCII
    {
      $$.line  = $1.line;
      $$.col   = $1.col;
      $$.extra[0] = vpOR;
    }
  | vNOR_ASCII
    {
      $$.line  = $1.line;
      $$.col   = $1.col;
      $$.extra[0] = vpNOR;
    }
  | vXOR_ASCII
    {
      $$.line  = $1.line;
      $$.col   = $1.col;
      $$.extra[0] = vpXOR;
    }
  | vXNOR_ASCII
    {
      $$.line  = $1.line;
      $$.col   = $1.col;
      $$.extra[0] = vpXNOR;
    }
  | vBUF_ASCII
    {
      $$.line  = $1.line;
      $$.col   = $1.col;
      $$.extra[0] = vpBUF;
    }
  | vNOT_ASCII
    {
      $$.line  = $1.line;
      $$.col   = $1.col;
      $$.extra[0] = vpNOT;
    }
  ;

/* A.4 Module and generated instantiation */

/* A.4.1 Module instantiation */
/* module-instantiation status added for ignoring # */
module_instantiation
  : vIDENT expression ';'
    {
      $$ = veri_parser->gen_mod_instantiation( $1.line, $1.col,
                                   $1.content, NULL, $2 );
      free($1.content);
    }
  | vIDENT '#' '(' expression ')' expression ';'
    {
      /* second is param-val-assignment */
      /* third is module-instance-list */
      $$ = veri_parser->gen_mod_instantiation( $1.line, $1.col,
                                   $1.content, $4, $6 );
      free($1.content);
    }
  ;

/* A.4.2 Generated instantiation */
generated_instantiation
  : vGEN vENDGEN
    {
      $$ = veri_parser->gen_dummy_inst( $1.line, $1.col );
    }
  | vGEN gen_item_list vENDGEN
    {
      $$ = $2;
    }
  ;

gen_item_list
  : gen_item
    {
      $$ = veri_parser->gen_gen_item_list( $1 );
    }
  | gen_item_list gen_item
    {
      $$ = veri_parser->append_gen_item_list( $1, $2 );
    }
  ;

gen_item_or_null
  : gen_item
    {
      $$ = $1;
    }
  | ';'
    {
      $$ = veri_parser->gen_dummy_inst( $1.line, $1.col );
    }
  ;

gen_item
  : gen_cond_statement
    {
      $$ = $1;
    }
  | gen_case_statement
    {
      $$ = $1;
    }
  | gen_loop_statement
    {
      $$ = $1;
    }
  | gen_block
    {
      $$ = $1;
    }
  | module_or_gen_item
    {
      $$ = $1;
    }
  ;

gen_cond_statement
  : vIF '(' expression ')' gen_item_or_null %prec LOWER_THAN_ELSE
    {
      // const-expression
      // 1 expression only
      $$ = veri_parser->gen_gen_cond_stmt( $1.line, $1.col, $3,
                                                $5, NULL );
    }
  | vIF '(' expression ')' gen_item_or_null vELSE gen_item_or_null
    {
      // const-expression
      // 1 expression only
      $$ = veri_parser->gen_gen_cond_stmt( $1.line, $1.col, $3,
                                                $5, $7 );
    }
  ;

gen_case_statement
  : vCASE '(' expression ')' genvar_case_item_list vENDCASE
    {
      // const-expression
      // 1 expression only
      $$ = veri_parser->gen_gen_case_stmt( $1.line, $1.col, 
                                                $3, $5 );
    }
  ;

genvar_case_item_list
  : genvar_case_item
    {
      $$ = veri_parser->gen_gen_case_item_list( $1 );
    }
  | genvar_case_item_list genvar_case_item
    {
      $$ = veri_parser->append_gen_case_item_list( $1, $2 );
    }
  ;

genvar_case_item
  : expression ':' gen_item_or_null
    {
      // const-exp-list
      $$ = veri_parser->gen_gen_case_item( $1, $3 );
    }
  | vDEFAULT gen_item_or_null
    {
      $$ = veri_parser->gen_default_gen_case_item( $1.line, $1.col,
                                                        $2 );
    }
  | vDEFAULT ':' gen_item_or_null
    {
      $$ = veri_parser->gen_default_gen_case_item( $1.line, $1.col,
                                                        $3 );
    }
  ;

gen_loop_statement
  : vFOR '(' expression ';' expression ';' expression ')'
    vBEGIN ':' vIDENT vEND
    {
      // const-expression
      // 1 expression only
      $$ = veri_parser->gen_gen_loop_stmt( $1.line, $1.col, $3, $5,
                                $7, $11.content, $11.line, $11.col, NULL );
      free($11.content);
    }
  | vFOR '(' expression ';' expression ';' expression ')'
    vBEGIN ':' vIDENT gen_item_list vEND
    {
      // const-expression
      // 1 expression only
      $$ = veri_parser->gen_gen_loop_stmt( $1.line, $1.col, $3, $5,
                                $7, $11.content, $11.line, $11.col, $12 );
      free($11.content);
    }
  ;

gen_block
  : vBEGIN vEND
    {
      $$ = veri_parser->gen_gen_block_stmt( $1.line, $1.col, NULL, -1, -1,
                                            NULL );
    }
  | vBEGIN gen_item_list vEND
    {
      $$ = veri_parser->gen_gen_block_stmt( $1.line, $1.col, NULL, -1, -1,
                                            $2 );
    }
  | vBEGIN ':' vIDENT vEND
    {
      $$ = veri_parser->gen_gen_block_stmt( $1.line, $1.col, $3.content,
                                            $3.line, $3.col, NULL );
      free($3.content);
    }
  | vBEGIN ':' vIDENT gen_item_list vEND
    {
      $$ = veri_parser->gen_gen_block_stmt( $1.line, $1.col, $3.content,
                                            $3.line, $3.col, $4 );
      free($3.content);
    }
  ;

/* A.5 UDP declaration and instantiation */
/* A.5.1 UDP declaration */
udp_declaration
  : vPRIMITIVE vENDPRI
    {
    }
  | attr_inst_list vPRIMITIVE vENDPRI
    {
    }
  ;

/* A.5.2 UDP ports */
/* A.5.3 UDP body */
/* A.5.4 UDP instantiation */

/* A.6 Behavioral statements */

/* A.6.1 Continuous assignment statements */
continuous_assign
  : vASSIGN expression ';'
    {
      $$ = veri_parser->gen_continuous_assign_stmt( $1.line, $1.col,
                                    NULL, NULL, $2 );
    }
  | vASSIGN drive_strength expression ';'
    {
      $$ = veri_parser->gen_continuous_assign_stmt( $1.line, $1.col,
                                    NULL, $2, $3 );
    }
  | vASSIGN delay3 expression ';'
    {
      $$ = veri_parser->gen_continuous_assign_stmt( $1.line, $1.col,
                                    $2, NULL, $3 );
    }
  | vASSIGN drive_strength delay3 expression ';'
    {
      $$ = veri_parser->gen_continuous_assign_stmt( $1.line, $1.col,
                                    $2, $3, $4 );
    }
  ;

/* A.6.2 Procedural blocks and assignments */
blocking_assignment
  : expression '=' event_control expression
    {
      // 1 expression only
      $$ = veri_parser->gen_blocking_stmt( $1, $3, $4 );
    }
  ;

nonblocking_assignment
  : expression vLE event_control expression
    {
      // 1 expression only
      $$ = veri_parser->gen_non_blocking_stmt( $1, $3, $4);
    }
  ;

func_stmt_or_null
  : func_statement
    {
      $$ = $1;
    }
  | ';'
    {
      $$ = veri_parser->gen_dummy_stmt( $1.line, $1.col );
    }
  ;

/* A.6.3 Parallel and sequential blocks */
func_seq_block
  : vBEGIN vEND
    {
      $$ = veri_parser->gen_func_seq_block_stmt( $1.line, $1.col, NULL,
                                                 -1, -1, NULL, NULL );
    }
  | vBEGIN func_stmt_list vEND
    {
      $$ = veri_parser->gen_func_seq_block_stmt( $1.line, $1.col, NULL,
                                                 -1, -1, NULL, $2 );
    }
  | vBEGIN ':' vIDENT vEND
    {
      $$ = veri_parser->gen_func_seq_block_stmt( $1.line, $1.col, $3.content,
                                                 $3.line, $3.col, NULL, NULL );
      free($3.content);
    }
  | vBEGIN ':' vIDENT func_stmt_list vEND
    {
      $$ = veri_parser->gen_func_seq_block_stmt( $1.line, $1.col, $3.content,
                                                 $3.line, $3.col, NULL, $4 );
      free($3.content);
    }
  | vBEGIN ':' vIDENT block_item_decl_list vEND
    {
      $$ = veri_parser->gen_func_seq_block_stmt( $1.line, $1.col, $3.content,
                                                 $3.line, $3.col, $4, NULL );
      free($3.content);
    }
  | vBEGIN ':' vIDENT block_item_decl_list func_stmt_list vEND
    {
      $$ = veri_parser->gen_func_seq_block_stmt( $1.line, $1.col, $3.content,
                                                 $3.line, $3.col, $4, $5 );
      free($3.content);
    }
  ;

func_stmt_list
  : func_statement
    {
      $$ = veri_parser->gen_func_stmt_list( $1 );
    }
  | func_stmt_list func_statement
    {
      $$ = veri_parser->append_func_stmt_list( $1, $2 );
    }
  ;

stmt_list
  : statement
    {
      $$ = veri_parser->gen_stmt_list( $1 );
    }
  | stmt_list statement
    {
      $$ = veri_parser->append_stmt_list( $1, $2 );
    }
  ;

seq_block
  : vBEGIN vEND
    {
      $$ = veri_parser->gen_seq_block_stmt( $1.line, $1.col, NULL,
                                            -1, -1, NULL, NULL );
    }
  | vBEGIN stmt_list vEND
    {
      $$ = veri_parser->gen_seq_block_stmt( $1.line, $1.col, NULL,
                                            -1, -1, NULL, $2 );
    }
  | vBEGIN ':' vIDENT vEND
    {
      $$ = veri_parser->gen_seq_block_stmt( $1.line, $1.col, $3.content,
                                            $3.line, $3.col, NULL, NULL );
      free($3.content);
    }
  | vBEGIN ':' vIDENT stmt_list vEND
    {
      $$ = veri_parser->gen_seq_block_stmt( $1.line, $1.col, $3.content,
                                            $3.line, $3.col, NULL, $4 );
      free($3.content);
    }
  | vBEGIN ':' vIDENT block_item_decl_list vEND
    {
      $$ = veri_parser->gen_seq_block_stmt( $1.line, $1.col, $3.content,
                                            $3.line, $3.col, $4, NULL );
      free($3.content);
    }
  | vBEGIN ':' vIDENT block_item_decl_list stmt_list vEND
    {
      $$ = veri_parser->gen_seq_block_stmt( $1.line, $1.col, $3.content,
                                            $3.line, $3.col, $4, $5 );
      free($3.content);
    }
  ;

/* A.6.4 Statements */
statement
  : expression ';'
    {
      /* '=' is blocking, '<=' is non-blocking */
      $$ = veri_parser->gen_block_or_non_or_caller_stmt( $1 );
    }
  | blocking_assignment ';'
    {
      $$ = $1;
    }
  | case_statement
    {
      $$ = $1;
    }
  | cond_statement
    {
      $$ = $1;
    }
  | disable_statement
    {
      $$ = $1;
    }
  | loop_statement
    {
      $$ = $1;
    }
  | nonblocking_assignment ';'
    {
      $$ = $1;
    }
  | procedural_timing_control_statement
    {
      $$ = $1;
    }
  | seq_block
    {
      $$ = $1;
    }
  | sys_caller
    {
      $$ = $1;
    }
  | attr_inst_list blocking_assignment ';'
    {
      $$ = $2;
    }
  | attr_inst_list case_statement
    {
      $$ = $2;
    }
  | attr_inst_list cond_statement
    {
      $$ = $2;
    }
  | attr_inst_list disable_statement
    {
      $$ = $2;
    }
  | attr_inst_list loop_statement
    {
      $$ = $2;
    }
  | attr_inst_list nonblocking_assignment ';'
    {
      $$ = $2;
    }
  | attr_inst_list procedural_timing_control_statement
    {
      $$ = $2;
    }
  | attr_inst_list seq_block
    {
      $$ = $2;
    }
  | attr_inst_list sys_caller
    {
      $$ = $2;
    }
  ;

stmt_or_null
  : statement
    {
      $$ = $1;
    }
  | ';'
    {
      $$ = veri_parser->gen_dummy_stmt( $1.line, $1.col );
    }
  | attr_inst_list ';'
    {
      $$ = veri_parser->gen_dummy_stmt( $2.line, $2.col );
    }
  ;

func_statement
  : expression ';'
    {
      // function-blocking-assignment
      // 1 expression only
      $$ = veri_parser->gen_func_blocking_assign_stmt( $1 );
    }
  | func_case_statement
    {
      $$ = $1;
    }
  | func_cond_statement
    {
      $$ = $1;
    }
  | func_loop_statement
    {
      $$ = $1;
    }
  | func_seq_block
    {
      $$ = $1;
    }
  | disable_statement
    {
      $$ = $1;
    }
  | sys_caller
    {
      /* only for task */
      $$ = $1;
    }
  | attr_inst_list expression ';'
    {
      // function-blocking-assignment
      // 1 expression only
      $$ = veri_parser->gen_func_blocking_assign_stmt( $2 );
    }
  | attr_inst_list func_case_statement
    {
      $$ = $2;
    }
  | attr_inst_list func_cond_statement
    {
      $$ = $2;
    }
  | attr_inst_list func_loop_statement
    {
      $$ = $2;
    }
  | attr_inst_list func_seq_block
    {
      $$ = $2;
    }
  | attr_inst_list disable_statement
    {
      $$ = $2;
    }
  | attr_inst_list sys_caller
    {
      /* only for task */
      $$ = $2;
    }
  ;

/* A.6.5 Timing control statements */
event_control
  : '@' vIDENT
    {
      $$ = veri_parser->gen_event_stmt( $1.line, $1.col, IS_FALSE,
                         veri_parser->gen_primary( $2.content, $2.line,
                                                   $2.col, veVAR ));
      free($2.content);
    }
  | '@' '(' expression ')'
    {
      /* event expression check, only id or event expression list */
      $$ = veri_parser->gen_event_stmt( $1.line, $1.col,
                                             IS_FALSE, $3 );
    }
  | '@' '*'
    {
      $$ = veri_parser->gen_event_stmt( $1.line, $1.col, IS_TRUE, NULL );
    }
  | '@' vEVENT_ALL
    {
      $$ = veri_parser->gen_event_stmt( $1.line, $1.col, IS_TRUE, NULL );
    }
  ;

disable_statement
  : vDISABLE vIDENT ';'
    {
      // hier_block_identifier
      // original --> hier-block-identifier or hier-task-identifier
      // ori --> hier identifier
      $$ = veri_parser->gen_disable_stmt( $2.content, $1.line, $1.col );
      free($2.content);
    }
  ;

procedural_timing_control_statement
  : event_control stmt_or_null
    {
      $$ = veri_parser->gen_proc_timing_control_stmt( $1, $2 );
    }
  ;

/* A.6.6 Conditional statements */
cond_statement
  : vIF '(' expression ')' stmt_or_null %prec LOWER_THAN_ELSE
    {
      // 1 expression only
      $$ = veri_parser->gen_cond_stmt( $1.line, $1.col, $3,
                                            $5, NULL );
    }
  | vIF '(' expression ')' stmt_or_null vELSE stmt_or_null
    {
      // 1 expression only
      $$ = veri_parser->gen_cond_stmt( $1.line, $1.col, $3,
                                            $5, $7 );
    }
  ;

func_cond_statement
  : vIF '(' expression ')' func_stmt_or_null %prec LOWER_THAN_ELSE
    {
      // 1 expression only
      $$ = veri_parser->gen_func_cond_stmt( $1.line, $1.col, $3,
                                                 $5, NULL );
    }
  | vIF '(' expression ')' func_stmt_or_null vELSE func_stmt_or_null
    {
      // 1 expression only
      $$ = veri_parser->gen_func_cond_stmt( $1.line, $1.col, $3,
                                                 $5, $7 );
    }
  ;

/* A.6.7 Case statements */
case_statement
  : vCASE  '(' expression ')' case_item_list vENDCASE
    {
      // 1 expression only
      $$ = veri_parser->gen_case_stmt( $1.line, $1.col, vcCASE,
                                            $3, $5 );
    }
  | vCASEZ '(' expression ')' case_item_list vENDCASE
    {
      // 1 expression only
      $$ = veri_parser->gen_case_stmt( $1.line, $1.col, vcCASEZ,
                                            $3, $5 );
    }
  | vCASEX '(' expression ')' case_item_list vENDCASE
    {
      // 1 expression only
      $$ = veri_parser->gen_case_stmt( $1.line, $1.col, vcCASEX,
                                            $3, $5 );
    }
  ;

case_item_list
  : case_item
    {
      $$ = veri_parser->gen_case_item_list( $1 );
    }
  | case_item_list case_item
    {
      $$ = veri_parser->append_case_item_list( $1, $2 );
    }
  ;

case_item
  : expression ':' stmt_or_null
    {
      $$ = veri_parser->gen_case_item( $1, $3 );
    }
  | vDEFAULT stmt_or_null
    {
      $$ = veri_parser->gen_default_case_item( $1.line, $1.col, $2 );
    }
  | vDEFAULT ':' stmt_or_null
    {
      $$ = veri_parser->gen_default_case_item( $1.line, $1.col, $3 );
    }
  ;

func_case_statement
  : vCASE  '(' expression ')' func_case_item_list vENDCASE
    {
      // 1 expression only
      $$ = veri_parser->gen_func_case_stmt( $1.line, $1.col, vcCASE,
                                                 $3, $5 );
    }
  | vCASEZ '(' expression ')' func_case_item_list vENDCASE
    {
      // 1 expression only
      $$ = veri_parser->gen_func_case_stmt( $1.line, $1.col, vcCASEZ,
                                                 $3, $5 );
    }
  | vCASEX '(' expression ')' func_case_item_list vENDCASE
    {
      // 1 expression only
      $$ = veri_parser->gen_func_case_stmt( $1.line, $1.col, vcCASEX,
                                                 $3, $5 );
    }
  ;

func_case_item_list
  : func_case_item
    {
      $$ = veri_parser->gen_func_case_item_list( $1 );
    }
  | func_case_item_list func_case_item
    {
      $$ = veri_parser->append_func_case_item_list( $1, $2 );
    }
  ;

func_case_item
  : expression ':' func_stmt_or_null
    {
      $$ = veri_parser->gen_func_case_item( $1, $3 );
    }
  | vDEFAULT func_stmt_or_null
    {
      $$ = veri_parser->gen_func_default_case_item( $1.line, $1.col, $2 );
    }
  | vDEFAULT ':' func_stmt_or_null
    {
      $$ = veri_parser->gen_func_default_case_item( $1.line, $1.col, $3 );
    }
  ;

/* A.6.8 Looping statements */
func_loop_statement
  : vFOR '(' expression ';' expression ';' expression ')'
    func_statement
    {
      // 1 expression only
      $$ = veri_parser->gen_func_loop_stmt( $1.line, $1.col, $3,
                                    $5, $7, $9 );
    }
  ;

loop_statement
  : vFOR '(' expression ';' expression ';' expression ')'
    statement
    {
      // 1 expression only
      $$ = veri_parser->gen_loop_stmt( $1.line, $1.col, $3,
                                    $5, $7, $9 );
    }
  ;

/* A.6.9 Task enable statements */
  /* combined by caller */

/* A.7 Specify section */
/* specify block is ignored */
/* A.7.1 Specify block declaration */
/* A.7.2 Specify path declaration */
/* A.7.3 Specify block terminals */
/* A.7.4 Specify path delays */
/* A.7.5 System timing checks  */
/* A.7.5.1 System timing check commands  */
/* A.7.5.2 System timing check command arguments  */
/* A.7.5.3 System timing check event definitions */

/* A.8 Expressions */

/* A.8.1 Concatenations */
concat
  : '{' expression '}'
    {
      // could be const
      $$ = veri_parser->gen_concat_exp( $2 );
    }
  ;

multi_concat
  : '{' expression concat '}'
    {
      // const + non-const, two structures
      // 1 const-expression only 
      $$ = veri_parser->gen_binary( $2, vtMULTI_CONCAT, $1.line,
                                         $1.col, $3 );
    }
  ;

/* A.8.2 Function calls */
/* combined by caller */
sys_caller
  : vSYS_IDENT ';'
    {
      $$ = veri_parser->gen_sys_caller_stmt( $1.line, $1.col, $1.content, NULL );
    }
  | vSYS_IDENT '(' expression ')' ';'
    {
      $$ = veri_parser->gen_sys_caller_stmt( $1.line, $1.col,
                                             $1.content, $3 );
    }
  ;

/* A.8.3 Expressions */
expression
  : assignment_expression
    {
      $$ = $1;
    }
  | vDUMMY_PORT
    {
      $$ = veri_parser->gen_dummy_port( $1.line, $1.col );
    }
  | expression ',' expression
    {
      $$ = veri_parser->gen_multi( $1, $3, vemCOMMA_EXP, $2.line, $2.col );
    }
  | expression vOR_ASCII expression
    {
      /* for event only */
      $$ = veri_parser->gen_multi( $1, $3, vemEVENT_EXP, $2.line, $2.col );
    }
  ;

postfix_expression
  : primary
    {
      $$ = $1;
    }
  | postfix_expression '(' ')'
    {
      $$ = veri_parser->gen_caller( $1, NULL );
    }
  | postfix_expression '(' expression ')'
    {
      /* could be const */
      $$ = veri_parser->gen_caller( $1, $3 );
    }
  | postfix_expression range
    {
      /* could be range or array or dimension ... */
      $$ = veri_parser->gen_array( $1, $2 );
    }
  ;

range
  : '[' expression ']'
    {
      $$ = veri_parser->gen_range( $2, vrSINGLE, NULL );
    }
  | '[' expression ':' expression ']'
    {
      $$ = veri_parser->gen_range( $2, vrSEMI, $4 );
    }
  | '[' expression vRANGE_ADD expression ']'
    {
      $$ = veri_parser->gen_range( $2, vrRAN_ADD, $4 );
    }
  | '[' expression vRANGE_SUB expression ']'
    {
      $$ = veri_parser->gen_range( $2, vrRAN_SUB, $4 );
    }
  ;

unary_expression
  : postfix_expression %prec LOWER_THAN_A
    {
      $$ = $1;
    }
  | '+'   unary_expression %prec UADD
    {
      $$ = veri_parser->gen_unary( vtUADD, $1.line, $1.col, $2 );
    }
  | '-'   unary_expression %prec UMINUS
    {
      $$ = veri_parser->gen_unary( vtUSUB, $1.line, $1.col, $2 );
    }
  | '~'   unary_expression %prec UNEG
    {
      $$ = veri_parser->gen_unary( vtUNEG, $1.line, $1.col, $2 );
    }
  | '&'   unary_expression %prec UAND
    {
      $$ = veri_parser->gen_unary( vtUAND, $1.line, $1.col, $2 );
    }
  | '|'   unary_expression %prec UOR
    {
      $$ = veri_parser->gen_unary( vtUOR, $1.line, $1.col, $2 );
    }
  | vNAND unary_expression %prec UNAND
    {
      $$ = veri_parser->gen_unary( vtUNAND, $1.line, $1.col, $2 );
    }
  | vNOR  unary_expression %prec UNOR
    {
      $$ = veri_parser->gen_unary( vtUNOR, $1.line, $1.col, $2 );
    }
  | vXNOR unary_expression %prec UXNOR
    {
      $$ = veri_parser->gen_unary( vtUXNOR, $1.line, $1.col, $2 );
    }
  | '!'   unary_expression %prec ULNOT
    {
      $$ = veri_parser->gen_unary( vtUNOT, $1.line, $1.col, $2 );
    }
  | '^'   unary_expression %prec UXOR
    {
      $$ = veri_parser->gen_unary( vtUXOR, $1.line, $1.col, $2 );
    }
  | vPOSEDGE unary_expression
    {
      /* event */
      $$ = veri_parser->gen_unary( vtUPOSE, $1.line, $1.col, $2 );
    }
  | vNEGEDGE unary_expression
    {
      /* event */
      $$ = veri_parser->gen_unary( vtUNEGE, $1.line, $1.col, $2 );
    }
  | vSIGN_FUNC '(' expression ')'
    {
      $$ = veri_parser->gen_signed_exp( $1.line, $1.col, $3 );
    }
  | vUNSIGN_FUNC '(' expression ')'
    {
      $$ = veri_parser->gen_unsigned_exp( $1.line, $1.col, $3 );
    }
  ;

pow_expression
  : unary_expression %prec LOWER_THAN_A
    {
      $$ = $1;
    }
  | pow_expression vPOW  unary_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBPOW, $2.line,
                                         $2.col, $3 );
    }
  | pow_expression vNAND unary_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBNAND, $2.line,
                                         $2.col, $3 );
    }
  | pow_expression vNOR  unary_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBNOR, $2.line,
                                         $2.col, $3 );
    }
  | pow_expression vXNOR unary_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBXNOR, $2.line,
                                         $2.col, $3 );
    }
  ;

multiplicative_expression
  : pow_expression %prec LOWER_THAN_A
    {
      $$ = $1;
    }
  | multiplicative_expression '%' pow_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBMOD, $2.line,
                                         $2.col, $3 );
    }
  | multiplicative_expression '*' pow_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBMUL, $2.line,
                                         $2.col, $3 );
    }
  | multiplicative_expression '/' pow_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBDIV, $2.line,
                                         $2.col, $3 );
    }
  ;

additive_expression
  : multiplicative_expression
    {
      $$ = $1;
    }
  | additive_expression '+' multiplicative_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBADD, $2.line,
                                         $2.col, $3 );
    }
  | additive_expression '-' multiplicative_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBSUB, $2.line,
                                         $2.col, $3 );
    }
  ;

shift_expression
  : additive_expression %prec LOWER_THAN_A
    {
      $$ = $1;
    }
  | shift_expression vLOGIC_RS additive_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBLRS, $2.line,
                                         $2.col, $3 );
    }
  | shift_expression vLOGIC_LS additive_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBLLS, $2.line,
                                         $2.col, $3 );
    }
  | shift_expression vARITH_RS additive_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBARS, $2.line,
                                         $2.col, $3 );
    }
  | shift_expression vARITH_LS additive_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBALS, $2.line,
                                         $2.col, $3 );
    }
  ;

relational_expression
  : shift_expression
    {
      $$ = $1;
    }
  | relational_expression '<' shift_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBLT, $2.line,
                                         $2.col, $3 );
    }
  | relational_expression '>' shift_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBGT, $2.line,
                                         $2.col, $3 );
    }
  | relational_expression vLE shift_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBLE, $2.line,
                                         $2.col, $3 );
    }
  | relational_expression vGE shift_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBGE, $2.line,
                                         $2.col, $3 );
    }
  ;

equality_expression
  : relational_expression %prec LOWER_THAN_A
    {
      $$ = $1;
    }
  | equality_expression vLOGIC_EQ   relational_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBLEQ, $2.line,
                                         $2.col, $3 );
    }
  | equality_expression vLOGIC_INEQ relational_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBLIEQ, $2.line,
                                         $2.col, $3 );
    }
  | equality_expression vCASE_EQ   relational_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBCEQ, $2.line,
                                         $2.col, $3 );
    }
  | equality_expression vCASE_INEQ relational_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBCIEQ, $2.line,
                                         $2.col, $3 );
    }
  ;

and_expression
  : equality_expression
    {
      $$ = $1;
    }
  | and_expression '&' equality_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBSAND, $2.line,
                                         $2.col, $3 );
    }
  ;

xor_expression
  : and_expression %prec LOWER_THAN_A
    {
      $$ = $1;
    }
  | xor_expression '^' and_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBSXOR, $2.line,
                                         $2.col, $3 );
    }
  ;

bit_or_expression
  : xor_expression %prec LOWER_THAN_A
    {
      $$ = $1;
    }
  | bit_or_expression '|' xor_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBSOR, $2.line,
                                         $2.col, $3 );
    }
  ;

logical_and_expression
  : bit_or_expression %prec LOWER_THAN_A
    {
      $$ = $1;
    }
  | logical_and_expression vANDAND bit_or_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBDAND, $2.line,
                                         $2.col, $3 );
    }
  ;

logical_or_expression
  : logical_and_expression
    {
      $$ = $1;
    }
  | logical_or_expression vOROR logical_and_expression
    {
      $$ = veri_parser->gen_binary( $1, vtBDOR, $2.line, $2.col, $3);
    }
  ;

conditional_expression
  : logical_or_expression
    {
      $$ = $1;
    }
  | logical_or_expression '?' expression ':' conditional_expression
    {
      $$ = veri_parser->gen_ques_exp( $1, $2.line, $2.col, $3, $5 );
    }
  ;

assignment_expression
  : conditional_expression
    {
      $$ = $1;
    }
  | unary_expression '=' assignment_expression
    {
      /* first expression should be id */
      $$ = veri_parser->gen_binary( $1, vtASSIGN, $2.line,
                                         $2.col, $3 );
    }
  ;

mintypmax_expression
  : expression ':' expression ':' expression
    {
      $$ = veri_parser->gen_mintypmax( $1, $2.line, $2.col, $3, $5 );
    }
  ;

/* A.8.4 Primaries */

/* concat hier_identifier-exp-bracket-list due to r/s conflicts */
primary
  : vNUMBER
    {
      /* could be const */
      $$ = veri_parser->gen_primary( $1.content, $1.line, $1.col, veNUM );
      free( $1.content );
    }
  | vSTR
    {
      /* could be const */
      $$ = veri_parser->gen_primary( $1.content, $1.line, $1.col, veSTR );
      free( $1.content );
    }
  | vIDENT
    {
      /* could be const */
      $$ = veri_parser->gen_primary( $1.content, $1.line, $1.col, veVAR );
      free($1.content);
    }
  | '.' vIDENT
    {
      $$ = veri_parser->gen_port_ref( $1.line, $1.col, $2.content );
      free( $2.content );
    }
  | concat
    {
      /* could be const */
      $$ = $1;
    }
  | multi_concat
    {
      /* could be const */
      $$ = $1;
    }
  | '(' expression ')'
    {
      $$ = $2;
    }
  | '(' mintypmax_expression ')'
    {
      /* could be const */
      $$ = $2;
    }
  ;

/* A.8.5 Expression left_side values */
/* A.8.6 Operators */
/* A.8.7 Numbers */

/* A.9 General */
/* A.9.l Attributes */

attr_inst_list
  : attr_instance
    {
    }
  | attr_inst_list attr_instance
    {
    }
  ;

attr_instance
  : vATTR_L expression vATTR_R
    {
    }
  ;

/* A.9.2 Comments */

/* A.9.3 Identifiers */
%%
