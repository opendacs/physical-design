/*
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : veri_parser.cpp
 * Summary  : verilog syntax-analyzer functions
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2012.10.28
 */

#include <assert.h>

#include <cstring>

#include "verilog_yacc.h"

#include "general_err.hpp"
#include "veri_err_token.hpp"
#include "verilog_token.hpp"

#include "cfg_mgr.hpp"
#include "src_mgr.hpp"

#include "veri_ast_node.hpp"
#include "veri_err.hpp"

#include "veri_node.hpp"
#include "veri_decl.hpp"
#include "veri_exp.hpp"
#include "veri_inst.hpp"
#include "veri_stmt.hpp"
#include "veri_module.hpp"

#include "veri_symbol_table.hpp"
#include "veri_mod_container.hpp"
#include "veri_parser.hpp"

rtl::VeriParser *veri_parser = NULL;

namespace rtl
{

#define debug_verilog_parser_

VeriParser::VeriParser()
  : _top_mod_name("")
  , _s(NULL)
  , _length(0)
  , _mod(NULL)
  , _mods(NULL)
{
  _cmgr = Cfg_mgr::get_instance();
  _smgr = Src_mgr::get_instance();
  _err  = VeriErr::get_instance();

  veri_parser = this;
}

VeriParser::~VeriParser()
{
  clear();
}

int
VeriParser::init( void )
{
  _s      = _smgr->src();
  _length = _smgr->src_len();
  _fname.push_back( _smgr->fname() );
  _fline.push_back( 0 );

  _node_pool.init( _cmgr->node_pool_size(), _cmgr->node_pool_hash_mod() ); 

  _mods = new ModContainer;
  _mods->init( _cmgr->module_container_size() );
  return OK;
}

void
VeriParser::clean_up( void )
{
  _fname.clear();
  _fline.clear();

  _s          = NULL;
  _length     = 0;

  _node_pool.clean_up();

  _err->clean_up();
  if( _mods != NULL )
    _mods->clean_up( );

  clear_syntree();
}

void
VeriParser::clear( void )
{
  _fname.clear();
  _fline.clear();

  _s          = NULL;
  _length     = 0;

  _node_pool.clear();
  _err->clean_up();

  if( _mods != NULL )
    delete _mods;
  _mods = NULL;

  clear_syntree();
}

int
VeriParser::parse( void )
{
  _mod = new VeriMod( _node_pool.next_id( ) );
  _mod_instantiations.clear();

  _err->reset_file( );
  _err->include_file( 0, _fname.back() );

  int re = verilogparse( );
  if( _mod != NULL )
    delete _mod;
  _mod = NULL;

  if( _err->has_err( ) == IS_TRUE )
    return ERR;
  
  _mods->parse_modules( _top_mod_name );

  return re;
}

int
VeriParser::append_module( VeriMod *mod )
{
  VeriMod * t = _mods->module( mod->name() );
  if( t != NULL )
  {
    String other = int2string(t->line());
    _err->append_err( ERR_ERROR, mod->line(), mod->col(),
                      vMOD_MODULE_REDEFINE, other );
    delete _mod;
    _mod = NULL;
    return vMOD_MODULE_REDEFINE;
  }
  else
    _mods->insert( mod->name(), _mod );

  return OK;
}

VeriMod *
VeriParser::module( CString& name )
{
  return _mods->module( name );
}

int
VeriParser::prep_for_new_mod( void )
{
  _mod = new VeriMod( _node_pool.next_id( ) );
  _mod_instantiations.clear();
  return OK;
}

/*
VeriNode *
VeriParser::gen_sys_var( const char *name, int line, int col )
{
  VeriNode * node = new VeriSysVar( _node_pool.next_id( ), line, col, name );
  _node_pool.insert( node );
  return node;
}
*/

VeriNode *
VeriParser::gen_dummy_port( int line, int col )
{
  VeriDummyPort * port = new VeriDummyPort( _node_pool.next_id( ), line, col, _mod );
  _node_pool.insert( port );
  return port;
}

VeriNode *
VeriParser::gen_port_ref( int line, int col, const char *port_name)
{
//  if( port_ref == NULL || port_ref->detail_type() != veEXP_PRI )
//  {
//    return port_ref;
//  }

//  VeriPrimaryExp * pexp = static_cast<VeriPrimaryExp *>(port_ref);
//  VeriPortRefExp * ref = new VeriPortRefExp( _node_pool.next_id( ), line, col,
//                                             pexp->str(), _mod );
//  _node_pool.insert( ref );
  VeriPortRefExp * ref = new VeriPortRefExp( _node_pool.next_id( ), line, col,
                                             port_name, _mod );
  _node_pool.insert( ref );

  return ref;
}

VeriNode *
VeriParser::gen_primary( const char *str, int line, int col, int primary_type )
{
  String iname = "";
  if( str != NULL )
    iname = str;

  VeriExp * exp
    = new VeriPrimaryExp( _node_pool.next_id( ), line, col,
                          iname, primary_type, _mod );
  _node_pool.insert( exp );
  return exp;
}

VeriNode *
VeriParser::gen_range( VeriNode *left, int rtype, VeriNode *right )
{
  VeriExp * exp
    = new VeriRangeExp( _node_pool.next_id( ), left->line(), left->col(),
                        left, rtype, right, _mod );
  _node_pool.insert( exp );
  left->set_father( exp );
  if( right != NULL )
    right->set_father( exp );
  return exp;
}

VeriNode *
VeriParser::gen_array( VeriNode *id, VeriNode *range )
{
  VeriArrayExp *array = NULL;
  if( id->tok_type() == vtEXP && range->tok_type() == vtEXP )
  {
    switch( id->detail_type() )
    {
      case veEXP_ARRAY :
        if( range->detail_type() == veEXP_RANGE )
        {
          static_cast<VeriArrayExp *>(id)->append_range( range );
          range->set_father( id );
        }
        array = static_cast<VeriArrayExp *>(id);
        break;

      case veEXP_PRI :
        array = new VeriArrayExp( _node_pool.next_id( ), id->line(),
                                  id->col(), id, _mod );
        _node_pool.insert( array );
        id->set_father( array );

        if( range->detail_type() == veEXP_RANGE )
        {
          array->append_range( range );
          range->set_father( array );
        }

        break;

      default:
        break;
    }
  }
  return array;
}

VeriNode *
VeriParser::gen_caller( VeriNode *name, VeriNode *call_exp )
{
  VeriExp *exp = NULL;
  VeriMultiExp *arg = NULL;
  if(     name->tok_type() != vtEXP
      || (call_exp != NULL && call_exp->tok_type() != vtEXP) )
  { /* not okay */;}
  else
  {
    if( call_exp != NULL )
    {
      switch( call_exp->detail_type() )
      {
        case veEXP_NAMED_PARAM :
          arg = new VeriMultiExp( _node_pool.next_id( ),
                                  vemPORT_EXP, call_exp->line(),
                                  call_exp->col(), _mod );
          _node_pool.insert( arg );
          arg->append_child( call_exp );
          call_exp->set_father( arg );
          break;

        case veEXP_PRI :
        case veEXP_UNA :
        case veEXP_ARRAY :
        case veEXP_BIN :
        case veEXP_MCONCAT :
        case veEXP_SIGN :
        case veEXP_UNSIGN :
          arg = new VeriMultiExp( _node_pool.next_id( ),
                                  vemCOMMA_EXP, call_exp->line(),
                                  call_exp->col(), _mod );
          _node_pool.insert( arg );
          arg->append_child( call_exp );
          call_exp->set_father( arg );
          break;

        case veEXP_CALLER :
        {
          VeriCallerExp * caller = static_cast<VeriCallerExp *>(call_exp);
          if( caller->call_type() == vtTASK_CALL )
          {
            arg = new VeriMultiExp( _node_pool.next_id( ),
                                    vemCOMMA_EXP, call_exp->line(),
                                    call_exp->col(), _mod );
            _node_pool.insert( arg );
            arg->append_child( call_exp );
            call_exp->set_father( arg );
          }
          break;
        }

        case veEXP_MULTI :
        {
          VeriMultiExp * mexp = static_cast<VeriMultiExp *>(call_exp);
          switch( mexp->multi_type() )
          {
            case vemCOMMA_EXP :
            case vemPORT_EXP :
            case vemCONCAT_EXP :
            case vemPORT_INST_EXP :
              arg = static_cast<VeriMultiExp *>(call_exp);
              break;

            default:
              break;
          }
          break;
        }

        default:
          break;
      }
    }
    switch( name->detail_type())
    {
      case veEXP_PRI :
      case veEXP_ARRAY :
        exp = new VeriCallerExp( _node_pool.next_id( ),
                                 name->line(), name->col(),
                                 name, arg, _mod );
        _node_pool.insert( exp );
        name->set_father( exp );
        break;

      case veEXP_PORT_REF :
      {
        VeriPortRefExp *npexp = static_cast<VeriPortRefExp *>(name);
        exp = new VeriNamedParamExp( _node_pool.next_id( ),
                                    name->line(), name->col(),
                                    npexp->port_ref(), arg, _mod );
        _node_pool.insert( exp );
        break;
      }

      default:
        break;
    }
  }
  return exp;
}

VeriNode *
VeriParser::gen_unary( int op, int line, int col, VeriNode *exp1 )
{
  VeriExp *exp = NULL;
  int re;
  switch( op )
  {
    /* normal unary expression */
    case vtUADD  :  case vtUSUB  :  case vtUNEG  :  case vtUAND  :
    case vtUOR   :  case vtUNAND :  case vtUNOR  :  case vtUXNOR :
    case vtUNOT  :  case vtUXOR  :
      exp = new VeriUnaryExp( _node_pool.next_id( ), line, col, op, exp1, _mod );
      _node_pool.insert( exp );
      re = exp->check_child_validation();
      if( re == OK && exp1 != NULL )
        exp1->set_father( exp );
      break;

    /* event expression */
    case vtUPOSE :  case vtUNEGE :
      exp = new VeriEventExp( _node_pool.next_id( ), line, col, op, exp1, _mod );
      _node_pool.insert( exp );
      re = exp->check_child_validation();
      if( re == OK && exp1 != NULL )
        exp1->set_father( exp );
      break;

    default:
      break;
  }
  return exp;
}

VeriNode *
VeriParser::gen_binary( VeriNode *exp1, int op,
                        int line, int col, VeriNode *exp2 )
{
  VeriExp *exp = NULL;
  int re;
  switch( op )
  {
    /* normal binary expression */
    case vtBPOW  :  case vtBNAND :  case vtBNOR  :  case vtBXNOR :
    case vtBMOD  :  case vtBMUL  :  case vtBDIV  :  case vtBADD  :
    case vtBSUB  :  case vtBLRS  :  case vtBLLS  :  case vtBARS  :
    case vtBALS  :  case vtBLT   :  case vtBGT   :  case vtBLE   :
    case vtBGE   :  case vtBLEQ  :  case vtBLIEQ :  case vtBCEQ  :
    case vtBCIEQ :  case vtBSAND :  case vtBSXOR :  case vtBSOR  :
    case vtBDAND :  case vtBDOR  :
      exp = new VeriBinaryExp( _node_pool.next_id( ), line, col, exp1, op, exp2, _mod );
      _node_pool.insert( exp );
      re  = exp->check_child_validation();
      if( re == OK )
      {
        exp1->set_father( exp );
        exp2->set_father( exp );
      }
      break;

    /* assign expression */
    case vtASSIGN:
      exp = new VeriAssignExp( _node_pool.next_id( ), line, col, exp1, exp2, _mod );
      _node_pool.insert( exp );
      re  = exp->check_child_validation();
      if( re == OK )
      {
        exp1->set_father( exp );
        exp2->set_father( exp );
      }
      break;

    /* multiple concatenation is added here */
    case vtMULTI_CONCAT:
      exp = new VeriMultiConcatExp( _node_pool.next_id( ), line, col, exp1, exp2, _mod );
      _node_pool.insert( exp );
      re  = exp->check_child_validation();
      if( re == OK )
      {
        exp1->set_father( exp );
        exp2->set_father( exp );
      }
      break;

    default:
      break;
  }
  return exp;
}

VeriNode *
VeriParser::gen_mintypmax( VeriNode *exp1, int line, int col,
                         VeriNode *exp2, VeriNode *exp3 )
{
  VeriExp *exp = new VeriMintypmaxExp( _node_pool.next_id( ), line, col,
                                     exp1, exp2, exp3, _mod );
  _node_pool.insert( exp );
  int re = exp->check_child_validation();
  if( re == OK )
  {
    exp1->set_father( exp );
    exp2->set_father( exp );
    exp3->set_father( exp );
  }

  return exp;
}

VeriNode *
VeriParser::gen_ques_exp( VeriNode *ques, int line, int col,
                         VeriNode *choice1, VeriNode *choice2 )
{
  VeriExp *exp = new VeriQuesExp( _node_pool.next_id( ), line, col,
                                  ques, choice1, choice2, _mod );
  _node_pool.insert( exp );
  int re = exp->check_child_validation();
  if( re == OK )
  {
    ques->set_father( exp );
    choice1->set_father( exp );
    choice2->set_father( exp );
  }

  return exp;
}

VeriNode *
VeriParser::gen_multi( VeriNode *exp1, VeriNode *exp2,
                       int mtype, int line, int col )
{
  assert( exp1->tok_type() == vtEXP && exp2->tok_type() == vtEXP );

  int re;
  VeriMultiExp * mexp = NULL;
  if( exp1->detail_type() == veEXP_MULTI )
  {
    mexp = static_cast<VeriMultiExp *>(exp1);
    mexp->append_child( exp2 );
    re = mexp->check_last_child_validation( );
    if( re == OK )
      exp2->set_father( mexp );
    return mexp;
  }
  else
  {
    mexp = new VeriMultiExp( _node_pool.next_id( ), mtype, line, col, _mod );
    _node_pool.insert( mexp );
    mexp->append_child( exp1 );
    mexp->append_child( exp2 );
    re = mexp->check_child_validation();
    if( re == OK )
    {
      exp1->set_father( mexp );
      exp2->set_father( mexp );
    }
    return mexp;
  }
}

VeriNode *
VeriParser::gen_concat_exp( VeriNode *mexp )
{
  VeriMultiExp *mul_exp = NULL;
  if( mexp->tok_type() == vtEXP )
  {
    if( mexp->detail_type() == veEXP_MULTI )
    {
      mul_exp = static_cast<VeriMultiExp *>(mexp);
      mul_exp->set_multi_type( vemCONCAT_EXP );
    }
    else
    {
      mul_exp = new VeriMultiExp( _node_pool.next_id( ), vemCONCAT_EXP,
                                  mexp->line(), mexp->col(), _mod );
      _node_pool.insert( mul_exp );
      mul_exp->append_child( mexp );
      int re = mul_exp->check_last_child_validation( );
      if( re == OK )
        mexp->set_father( mul_exp );
    }
  }
  return mul_exp;
}

VeriNode *
VeriParser::gen_delay_exp( int line, int col, VeriNode *d1, VeriNode *dextra )
{
  VeriDelayExp * delay = NULL;
  switch( d1->detail_type() )
  {
    case veEXP_PRI :
    case veEXP_BIN :
    case veEXP_SIGN :
    case veEXP_UNSIGN :
    case veEXP_MINTY :
    case veEXP_QUES :
      delay = new VeriDelayExp( _node_pool.next_id( ), line, col,
                                d1, dextra, NULL, _mod );
      _node_pool.insert( delay );
      d1->set_father( delay );
      if( dextra != NULL )
        dextra->set_father( delay );
      break;

    case veEXP_MULTI :
    {
      VeriMultiExp * mexp = static_cast<VeriMultiExp *>(d1);
      NodeList& list = mexp->exp();
      assert( list.size() == 2 );

      NodeList::iterator it = list.begin();
      delay = new VeriDelayExp( _node_pool.next_id( ), line, col,
                                list.front(), *(++it), dextra, _mod );
      _node_pool.insert( delay );
      list.front()->set_father( delay );
      (*it)->set_father( delay );
      if( dextra != NULL )
        dextra->set_father( delay );
      break;
    }

    default:
      break;
  }

  if( delay != NULL )
    delay->check_child_validation( );

  return delay;
}

VeriNode *
VeriParser::gen_drive_exp( int line, int col, int drive_from, int drive_to)
{
  VeriDriveExp * drive = new VeriDriveExp( _node_pool.next_id( ), line, col,
                                           drive_from, drive_to, _mod  );
  _node_pool.insert( drive );

  drive->check_child_validation( );
  return drive;
}

VeriNode *
VeriParser::gen_signed_exp( int line, int col, VeriNode *exp )
{
  VeriSignExp * sign = new VeriSignExp( _node_pool.next_id( ), line, col,
                                        exp, _mod );
  _node_pool.insert( sign );

  int re = sign->check_child_validation( );
  if( re == OK )
    exp->set_father( sign );
  return sign;
}

VeriNode *
VeriParser::gen_unsigned_exp( int line, int col, VeriNode *exp )
{
  VeriUnsignExp * unsign = new VeriUnsignExp( _node_pool.next_id( ),
                                              line, col, exp, _mod );
  _node_pool.insert( unsign );

  int re = unsign->check_child_validation( );
  if( re == OK )
    exp->set_father( unsign );

  return unsign;
}

VeriNode *
VeriParser::gen_loop_stmt( int line, int col, VeriNode *init,
                           VeriNode *judge, VeriNode *iter,
                           VeriNode *content )
{
  VeriStmt * stmt = new VeriLoopStmt( _node_pool.next_id( ), line, col, init,
                                       judge, iter, content, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
  {
    init ->set_father( stmt );
    judge->set_father( stmt );
    iter ->set_father( stmt );
    content->set_father( stmt );
  }
  return stmt;
}

VeriNode *
VeriParser::gen_func_loop_stmt( int line, int col, VeriNode *init,
                                VeriNode *judge, VeriNode *iter,
                                VeriNode *content )
{
  VeriStmt * stmt = new VeriFuncLoopStmt( _node_pool.next_id( ), line, col, init,
                                           judge, iter, content, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
  {
    init ->set_father( stmt );
    judge->set_father( stmt );
    iter ->set_father( stmt );
    content->set_father( stmt );
  }
  return stmt;
}

VeriNode *
VeriParser::gen_gen_loop_stmt( int line, int col, VeriNode *init,
                               VeriNode *judge, VeriNode *iter,
                               const char *label, int label_line,
                               int label_col, VeriNode *content )
{
  String ilabel = "";
  if( label != NULL )
    ilabel = label;

  VeriInst * stmt = new VeriGenLoop( _node_pool.next_id( ), line, col, init,
                                     judge, iter, ilabel, label_line,
                                     label_col, content, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
  {
    init ->set_father( stmt );
    judge->set_father( stmt );
    iter ->set_father( stmt );
    content->set_father( stmt );
  }
  return stmt;
}

VeriNode *
VeriParser::gen_default_case_item( int line, int col, VeriNode *item )
{
  VeriStmt * stmt = new VeriDefaultCaseItem( _node_pool.next_id( ),
                                             line, col, item, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
    item->set_father( stmt );
  return stmt;
}

VeriNode *
VeriParser::gen_func_default_case_item( int line, int col, VeriNode *item )
{
  VeriStmt * stmt = new VeriFuncDefaultCaseItem( _node_pool.next_id( ),
                                                 line, col, item, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
    item->set_father( stmt );

  return stmt;
}

VeriNode *
VeriParser::gen_default_gen_case_item( int line, int col, VeriNode *item )
{
  VeriInst * stmt = new VeriDefaultGenCaseItem( _node_pool.next_id( ),
                                                line, col, item, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
    item->set_father( stmt );

  return stmt;
}

VeriNode *
VeriParser::gen_case_item( VeriNode *label, VeriNode *item )
{
  VeriCaseItem * stmt = new VeriCaseItem( _node_pool.next_id( ),
                                          label->line(),
                                          label->col(), item, _mod );
  _node_pool.insert( stmt );

  switch( label->detail_type() )
  {
    case veEXP_MULTI :
    {
      VeriMultiExp *mexp = static_cast<VeriMultiExp *>(label);
      NodeList &labels = mexp->exp();
      NodeList::iterator it = labels.begin();
      for( ; it != labels.end(); ++it )
      {
        stmt->append_label( *it );
        (*it)->set_father( stmt );
      }
      break;
    }

    case veEXP_ARRAY  : case veEXP_PRI : case veEXP_UNA : case veEXP_BIN :
    case veEXP_CALLER : case veEXP_MCONCAT : case veEXP_QUES :
    case veEXP_SIGN   : case veEXP_UNSIGN : case veEXP_MINTY :
      stmt->append_label( label );
      label->set_father( stmt );
      break;

    default:
      break;
  }

  int re = stmt->check_child_validation();
  if( re == OK )
    item ->set_father( stmt );

  return stmt;
}

VeriNode *
VeriParser::gen_func_case_item( VeriNode *label, VeriNode *item )
{
  VeriFuncCaseItem * stmt = new VeriFuncCaseItem( _node_pool.next_id( ),
                                                   label->line(),
                                                   label->col(), item, _mod );
  _node_pool.insert( stmt );

  switch( label->detail_type() )
  {
    case veEXP_MULTI :
    {
      VeriMultiExp *mexp = static_cast<VeriMultiExp *>(label);
      NodeList &labels = mexp->exp();
      NodeList::iterator it = labels.begin();
      for( ; it != labels.end(); ++it )
      {
        stmt->append_label( *it );
        (*it)->set_father( stmt );
      }
      break;
    }

    case veEXP_ARRAY  : case veEXP_PRI : case veEXP_UNA : case veEXP_BIN :
    case veEXP_CALLER : case veEXP_MCONCAT : case veEXP_QUES :
    case veEXP_SIGN   : case veEXP_UNSIGN : case veEXP_MINTY :
      stmt->append_label( label );
      label->set_father( stmt );
      break;

    default:
      break;
  }

  int re = stmt->check_child_validation();
  if( re == OK )
    item ->set_father( stmt );

  return stmt;
}

VeriNode *
VeriParser::gen_gen_case_item( VeriNode *label, VeriNode *item )
{
  VeriGenCaseItem * stmt = new VeriGenCaseItem( _node_pool.next_id( ),
                                                 label->line(),
                                                 label->col(), item, _mod );
  _node_pool.insert( stmt );

  switch( label->detail_type() )
  {
    case veEXP_MULTI :
    {
      VeriMultiExp *mexp = static_cast<VeriMultiExp *>(label);
      NodeList &labels = mexp->exp();
      NodeList::iterator it = labels.begin();
      for( ; it != labels.end(); ++it )
      {
        stmt->append_label( *it );
        (*it)->set_father( stmt );
      }
      break;
    }

    case veEXP_ARRAY  : case veEXP_PRI : case veEXP_UNA : case veEXP_BIN :
    case veEXP_CALLER : case veEXP_MCONCAT : case veEXP_QUES :
    case veEXP_SIGN   : case veEXP_UNSIGN : case veEXP_MINTY :
      stmt->append_label( label );
      label->set_father( stmt );
      break;

    default:
      break;
  }

  int re = stmt->check_child_validation();
  if( re == OK )
    item ->set_father( stmt );

  return stmt;
}

VeriNode *
VeriParser::gen_func_case_item_list( VeriNode *item )
{
  VeriFuncCaseItemList * stmt
    = new VeriFuncCaseItemList( _node_pool.next_id( ), item->line(),
                                item->col(), _mod );
  _node_pool.insert( stmt );

  switch( item->detail_type() )
  {
    case vsSTMT_FUNC_DEFAULT_CASE_ITEM:
      stmt->set_default( item );
      break;

    case vsSTMT_FUNC_CASE_ITEM:
      stmt->append_item( item );
      break;

    default:
      break;
  }

  int re = stmt->check_child_validation();
  if( re == OK )
    item ->set_father( stmt );
  return stmt;
}

VeriNode *
VeriParser::gen_case_item_list( VeriNode *item )
{
  VeriCaseItemList * stmt
    = new VeriCaseItemList( _node_pool.next_id( ), item->line(),
                             item->col(), _mod);
  _node_pool.insert( stmt );

  switch( item->detail_type() )
  {
    case vsSTMT_DEFAULT_CASE_ITEM:
      stmt->set_default( item );
      break;

    case vsSTMT_CASE_ITEM:
      stmt->append_item( item );
      break;

    default:
      break;
  }

  int re = stmt->check_child_validation();
  if( re == OK )
    item ->set_father( stmt );
  return stmt;
}

VeriNode *
VeriParser::gen_gen_case_item_list( VeriNode *item )
{
  VeriGenCaseItemList * stmt
    = new VeriGenCaseItemList( _node_pool.next_id( ), item->line(),
                                item->col(), _mod);
  _node_pool.insert( stmt );

  switch( item->detail_type() )
  {
    case vsINST_GEN_DEFAULT_CASE_ITEM:
      stmt->set_default( item );
      break;

    case vsINST_GEN_CASE_ITEM:
      stmt->append_item( item );
      break;

    default:
      break;
  }

  int re = stmt->check_child_validation();
  if( re == OK )
    item ->set_father( stmt );
  return stmt;
}

VeriNode *
VeriParser::append_func_case_item_list( VeriNode *list, VeriNode *item )
{
  if( list->detail_type() == vsSTMT_FUNC_CASE_ITEM_LIST )
  {
    VeriFuncCaseItemList *item_list
      = static_cast<VeriFuncCaseItemList *>(list);

    if( item->detail_type( ) == vsSTMT_FUNC_CASE_ITEM )
      item_list->append_item( item );
    else if( item->detail_type( ) == vsSTMT_FUNC_DEFAULT_CASE_ITEM )
      item_list->set_default( item );

    item_list->check_last_child_validation( );
    int re = item_list->check_last_child_validation( );
    if( re == OK )
      item->set_father( list );
    return list;
  }
  
  return list;
}

VeriNode *
VeriParser::append_case_item_list( VeriNode *list, VeriNode *item )
{
  if( list->detail_type() == vsSTMT_CASE_ITEM_LIST )
  {
    VeriCaseItemList *item_list
      = static_cast<VeriCaseItemList *>(list);

    if( item->detail_type( ) == vsSTMT_CASE_ITEM )
      item_list->append_item( item );
    else if( item->detail_type( ) == vsSTMT_DEFAULT_CASE_ITEM )
      item_list->set_default( item );

    item_list->check_last_child_validation( );
    int re = item_list->check_last_child_validation( );
    if( re == OK )
      item->set_father( list );
    return list;
  }
 
  return list;
}

VeriNode *
VeriParser::append_gen_case_item_list( VeriNode *list, VeriNode *item )
{
  if( list->detail_type() == vsINST_GEN_CASE_ITEM_LIST )
  {
    VeriGenCaseItemList *item_list
      = static_cast<VeriGenCaseItemList *>(list);

    if( item->detail_type( ) == vsINST_GEN_CASE_ITEM )
      item_list->append_item( item );
    else if( item->detail_type( ) == vsINST_GEN_DEFAULT_CASE_ITEM )
      item_list->set_default( item );

    item_list->check_last_child_validation( );
    int re = item_list->check_last_child_validation( );
    if( re == OK )
      item->set_father( list );
    return list;
  }
  
  return list;
}

VeriNode *
VeriParser::gen_func_case_stmt( int line, int col, int case_type,
                                VeriNode *switch_value, VeriNode *item )
{
  VeriStmt * stmt
    = new VeriFuncCaseStmt( _node_pool.next_id( ), line, col, case_type,
                            switch_value, item, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
  {
    switch_value->set_father( stmt );
    item->set_father( stmt );
  }
  return stmt;
}

VeriNode *
VeriParser::gen_case_stmt( int line, int col, int case_type,
                           VeriNode *switch_value, VeriNode *item )
{
  VeriStmt * stmt = new VeriCaseStmt( _node_pool.next_id( ),
                                       line, col, case_type,
                                       switch_value, item, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
  {
    switch_value->set_father( stmt );
    item->set_father( stmt );
  }
  return stmt;
}

VeriNode *
VeriParser::gen_gen_case_stmt( int line, int col,
                               VeriNode *switch_value, VeriNode *item )
{
  VeriInst * stmt = new VeriGenCase( _node_pool.next_id( ), line, col,
                                     switch_value, item, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
  {
    switch_value->set_father( stmt );
    item->set_father( stmt );
  }
  return stmt;
}

VeriNode *
VeriParser::gen_func_cond_stmt( int line, int col, VeriNode *if_exp,
                                VeriNode *if_stmt, VeriNode *else_stmt )
{
  VeriStmt * stmt
    = new VeriFuncCondStmt( _node_pool.next_id( ), line, col, if_exp,
                             if_stmt, else_stmt, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
  {
    if_exp->set_father( stmt );
    if_stmt->set_father( stmt );
    if( else_stmt != NULL )
      else_stmt->set_father( stmt );
  }
  return stmt;
}

VeriNode *
VeriParser::gen_cond_stmt( int line, int col, VeriNode *if_exp,
                           VeriNode *if_stmt, VeriNode *else_stmt )
{
  VeriStmt * stmt = new VeriCondStmt( _node_pool.next_id( ),
                                       line, col, if_exp,
                                       if_stmt, else_stmt, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
  {
    if_exp->set_father( stmt );
    if_stmt->set_father( stmt );
    if( else_stmt != NULL )
      else_stmt->set_father( stmt );
  }
  return stmt;
}

VeriNode *
VeriParser::gen_gen_cond_stmt( int line, int col, VeriNode *if_exp,
                               VeriNode *if_stmt, VeriNode *else_stmt )
{
  VeriInst * stmt = new VeriGenCond( _node_pool.next_id( ),
                                      line, col, if_exp,
                                      if_stmt, else_stmt, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
  {
    if_exp->set_father( stmt );
    if_stmt->set_father( stmt );
    if( else_stmt != NULL )
      else_stmt->set_father( stmt );
  }
  return stmt;
}

VeriNode *
VeriParser::gen_proc_timing_control_stmt( VeriNode *event_control,
                                          VeriNode *statement )
{
  VeriStmt * stmt = new VeriProcTimeContStmt( _node_pool.next_id( ),
                                              event_control->line(),
                                              event_control->col(),
                                              event_control,
                                              statement, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
  {
    event_control->set_father( stmt );
    statement->set_father( stmt );
  }
  return stmt;
}

VeriNode *
VeriParser::gen_disable_stmt( const char *label, int line, int col )
{
  VeriStmt * stmt = new VeriDisableStmt( _node_pool.next_id( ),
                                          line, col, label, _mod );
  _node_pool.insert( stmt );
  return stmt;
}

VeriNode *
VeriParser::gen_event_stmt( int line, int col, int is_all, VeriNode *event_list)
{
  int re = OK;
  VeriEventStmt * stmt = new VeriEventStmt( _node_pool.next_id( ),
                                             line, col, is_all, _mod );
  _node_pool.insert( stmt );

  if( event_list != NULL )
  {
    if( event_list->tok_type() == vtEXP )
    {
      switch( event_list->detail_type() )
      {
        case veEXP_PRI:
        case veEXP_EVENT:
          stmt->append_event( event_list );
          event_list->set_father( stmt );
          break;

        case veEXP_MULTI:
        {
          VeriMultiExp *mexp = static_cast<VeriMultiExp * >(event_list);
          re = mexp->check_pure_event_multi();
          if( re == OK )
          {
            NodeList &list = mexp->exp();
            NodeList::iterator it = list.begin();
            for( ; it != list.end(); ++it )
            {
              stmt->append_event( *it );
              (*it)->set_father( stmt );
            }
          }
          break;
        }

        default:
          break;
      }
    }
    else
      return NULL;
  }

  return stmt;
}

VeriNode *
VeriParser::gen_func_blocking_assign_stmt( VeriNode *assign_exp )
{
  assert( assign_exp->detail_type() == veEXP_ASSIGN );

  VeriAssignExp * aexp = static_cast<VeriAssignExp *>(assign_exp);

  VeriStmt * stmt = new VeriFuncBlockAssignStmt( _node_pool.next_id( ),
                                                 assign_exp->line(),
                                                 assign_exp->col(),
                                                 aexp->lval(),
                                                 aexp->rval(), _mod );
  _node_pool.insert( stmt );
  int re = stmt->check_child_validation( );
  if( re == OK )
    assign_exp->set_father( stmt );
  return stmt;
}

VeriNode *
VeriParser::gen_sys_caller_stmt( int line, int col, const char * name,
                            VeriNode *call_exp )
{
  int re = OK;
  VeriMultiExp * arg = NULL;
  if( call_exp != NULL )
  {
    switch( call_exp->detail_type() )
    {
      case veEXP_PRI :
      case veEXP_UNA :
      case veEXP_ARRAY :
      case veEXP_SIGN :
      case veEXP_UNSIGN :
      case veEXP_BIN :
      case veEXP_MCONCAT :
        arg = new VeriMultiExp( _node_pool.next_id( ),
                                 vemCOMMA_EXP, call_exp->line(),
                                 call_exp->col(), _mod );
        _node_pool.insert( arg );
        arg->append_child( call_exp );
        call_exp->set_father( arg );
        break;

      case veEXP_CALLER :
      {
        VeriCallerExp * caller = static_cast<VeriCallerExp *>(call_exp);
        if( caller->call_type() == vtTASK_CALL )
        {
          arg = new VeriMultiExp( _node_pool.next_id( ),
                                   vemCOMMA_EXP, call_exp->line(),
                                   call_exp->col(), _mod );
          _node_pool.insert( arg );
          arg->append_child( call_exp );
          call_exp->set_father( arg );
        }
        break;
      }

      case veEXP_MULTI :
      {
        VeriMultiExp * mexp = static_cast<VeriMultiExp *>(call_exp);
        if(    mexp->multi_type() == vemCOMMA_EXP
            || mexp->multi_type() == vemCONCAT_EXP )
          arg = mexp;
        break;
      }

      default:
        break;
    }
  }

  VeriStmt * stmt
    = new VeriSysCallerStmt( _node_pool.next_id( ), line, col, name, arg, _mod );
  _node_pool.insert( stmt );
  re = stmt->check_child_validation();
  if( re == OK )
    arg->set_father( stmt );

  return stmt;
}

VeriNode *
VeriParser::gen_dummy_stmt( int line, int col )
{
  VeriStmt * stmt
    = new VeriDummyStmt( _node_pool.next_id( ), line, col, _mod );
  _node_pool.insert( stmt );
  return stmt;
}

VeriNode *
VeriParser::gen_dummy_inst( int line, int col )
{
  VeriInst * inst
    = new VeriDummyInst( _node_pool.next_id( ), line, col, _mod );
  _node_pool.insert( inst );
  return inst;
}

VeriNode *
VeriParser::gen_gen_block_stmt( int line, int col, const char *name,
                                int label_line, int label_col,
                                VeriNode *item_list )
{
  VeriPrimaryExp *pexp = NULL;

  String iname = "";
  if( name != NULL )
    iname = name;

  VeriGenBlock * stmt = new VeriGenBlock( _node_pool.next_id( ), line, col,
                                          iname, label_line, label_col,
                                          item_list, _mod );
  _node_pool.insert( stmt );
  int re = stmt->check_child_validation();
  if( re == OK )
  {
    item_list->set_father( stmt );
    if( pexp != NULL )
      pexp->set_father( stmt );
  }

  return stmt;
}

VeriNode *
VeriParser::gen_seq_block_stmt( int line, int col, const char *name,
                                int label_line, int label_col,
                                VeriNode *decl_list, VeriNode *stmt_list )
{
  String iname = "";
  if( name != NULL )
    iname = name;

  VeriStmt * stmt = new VeriSeqBlock( _node_pool.next_id( ), line, col, iname,
                                       label_line, label_col,
                                      decl_list, stmt_list, _mod );
  _node_pool.insert( stmt );
  int re = stmt->check_child_validation();
  if( re == OK )
  {
    if( decl_list != NULL ) decl_list->set_father( stmt );
    if( stmt_list != NULL ) stmt_list->set_father( stmt );
  }

  return stmt;
}

VeriNode *
VeriParser::gen_func_seq_block_stmt( int line, int col, const char *name,
                                     int label_line, int label_col,
                                     VeriNode *decl_list, VeriNode *stmt_list )
{
  String iname = "";
  if( name != NULL )
    iname = name;

  VeriStmt * stmt = new VeriFuncSeqBlock( _node_pool.next_id( ), line, col,
                                           iname, label_line, label_col,
                                           decl_list, stmt_list, _mod );
  _node_pool.insert( stmt );
  int re = stmt->check_child_validation();
  if( re == OK )
  {
    if( decl_list != NULL ) decl_list->set_father( stmt );
    if( stmt_list != NULL ) stmt_list->set_father( stmt );
  }

  return stmt;
}

VeriNode *
VeriParser::gen_stmt_list( VeriNode *item )
{
  VeriStmtList * stmt = new VeriStmtList( _node_pool.next_id( ),
                                          item->line(), item->col(), _mod );
  _node_pool.insert( stmt );
  stmt->append_stmt( item );

  int re = stmt->check_child_validation();
  if( re == OK )
    item->set_father( stmt );

  return stmt;
}

VeriNode *
VeriParser::append_stmt_list( VeriNode *list, VeriNode *stmt )
{
  switch( list->detail_type() )
  {
    case vsSTMT_STMT_LIST :
    {
      VeriStmtList *item_list = static_cast<VeriStmtList *>(list);
      item_list->append_stmt( stmt );
      int re = item_list->check_last_child_validation( );
      if( re == OK )
        stmt->set_father( list );
      break;
    }

    case vsSTMT_DUMMY :
      break;

    default:
      break;
  }
  return list;
}

VeriNode *
VeriParser::gen_mod_item_list( VeriNode *item )
{
  VeriModuleItemList * stmt = new VeriModuleItemList( _node_pool.next_id( ),
                                                      item->line(),
                                                      item->col(), _mod );
  _node_pool.insert( stmt );
  stmt->append_item( item );

  int re = stmt->check_child_validation();
  if( re == OK )
    item->set_father( stmt );

  return stmt;
}

VeriNode *
VeriParser::append_mod_item_list( VeriNode *list, VeriNode *item )
{
  if( list->detail_type() == vsSTMT_MOD_ITEM_LIST )
  {
    VeriModuleItemList *item_list
      = static_cast<VeriModuleItemList *>(list);
    item_list->append_item( item );
    int re = item_list->check_last_child_validation( );
    if( re == OK )
      item->set_father( list );
  }

  return list;
}

VeriNode *
VeriParser::gen_func_stmt_list( VeriNode *item )
{
  VeriFuncStmtList * stmt
    = new VeriFuncStmtList( _node_pool.next_id( ),
                             item->line(), item->col() , _mod);
  _node_pool.insert( stmt );
  stmt->append_stmt( item );

  int re = stmt->check_child_validation();
  if( re == OK )
    item->set_father( stmt );

  return stmt;
}

VeriNode *
VeriParser::append_func_stmt_list( VeriNode *list, VeriNode *item )
{
  switch( list->detail_type() )
  {
    case vsSTMT_FUNC_STMT_LIST :
    {
      VeriFuncStmtList *item_list
        = static_cast<VeriFuncStmtList *>(list);
      item_list->append_stmt( item );
      int re = item_list->check_last_child_validation( );
      if( re == OK )
        item->set_father( list );

      break;
    }

    case vsSTMT_DUMMY :
      break;

    default:
      break;
  }
  return list;
}

VeriNode *
VeriParser::gen_gen_item_list( VeriNode *item )
{
  VeriGenItemList * stmt
    = new VeriGenItemList( _node_pool.next_id( ), item->line(),
                            item->col(), _mod );
  _node_pool.insert( stmt );
  stmt->append_item( item );

  int re = stmt->check_child_validation();
  if( re == OK )
    item->set_father( stmt );

  return stmt;
}

VeriNode *
VeriParser::append_gen_item_list( VeriNode *list, VeriNode *item )
{
  switch( list->detail_type() )
  {
    case vsINST_GEN_ITEM_LIST :
    {
      VeriGenItemList *item_list
        = static_cast<VeriGenItemList *>(list);
      item_list->append_item( item );
      int re = item_list->check_last_child_validation( );
      if( re == OK )
        item->set_father( list );
      break;
    }

    case vsINST_DUMMY :
      break;

    default:
      break;
  }
  return list;
}

VeriNode *
VeriParser::gen_block_or_non_or_caller_stmt( VeriNode *node )
{
  if( node == NULL || node->tok_type() != vtEXP )
    return node;

  int re;
  VeriStmt * stmt = NULL;
  switch( node->detail_type() )
  {
    case veEXP_CALLER :
      stmt = new VeriCallerStmt( _node_pool.next_id( ), node->line(),
                                 node->col(), node, _mod );
      _node_pool.insert( stmt );
      re = stmt->check_child_validation( );
      if( re == OK )
        node->set_father( stmt );
     break;

    case veEXP_ASSIGN :
    {
      VeriAssignExp * assign = static_cast< VeriAssignExp *>(node);
      stmt = new VeriBlockStmt( _node_pool.next_id( ),
                                 node->line(), node->col(),
                                 assign->lval(), NULL, assign->rval(), _mod );
      _node_pool.insert( stmt );
      re = stmt->check_child_validation( );
      if( re == OK )
      {
        assign->lval()->set_father( stmt );
        assign->rval()->set_father( stmt );
      }
     break;
    }

    case veEXP_BIN :
    {
      VeriBinaryExp * bexp = static_cast< VeriBinaryExp *>(node);
      if( bexp->op() != vtBLE )
        return stmt;

      stmt = new VeriNonBlockStmt( _node_pool.next_id( ),
                                    node->line(), node->col(),
                                    bexp->lval(), NULL, bexp->rval(), _mod );
      _node_pool.insert( stmt );
      re = stmt->check_child_validation( );
      if( re == OK )
      {
        bexp->lval()->set_father( stmt );
        bexp->rval()->set_father( stmt );
      }
      break;
    }

    default:
      break;
  }
  return stmt;
}

VeriNode *
VeriParser::gen_blocking_stmt( VeriNode *lval,
                               VeriNode *event, VeriNode *rval )
{
  VeriStmt * stmt = new VeriBlockStmt( _node_pool.next_id( ),
                                        lval->line(), lval->col(),
                                        lval, event, rval, _mod );
  _node_pool.insert( stmt );
  int re = stmt->check_child_validation( );
  if( re == OK )
  {
    lval->set_father( stmt );
    rval->set_father( stmt );
    if( event != NULL )
      event->set_father( stmt );
  }

  return stmt;
}

VeriNode *
VeriParser::gen_non_blocking_stmt( VeriNode *lval, VeriNode *event,
                                   VeriNode *rval )
{
  VeriStmt * stmt = new VeriNonBlockStmt( _node_pool.next_id( ),
                                           lval->line(), lval->col(),
                                           lval, event, rval, _mod );
  _node_pool.insert( stmt );
  int re = stmt->check_child_validation( );
  if( re == OK )
  {
    lval->set_father( stmt );
    rval->set_father( stmt );
    if( event != NULL )
      event->set_father( stmt );
  }

  return stmt;
}

VeriNode *
VeriParser::gen_continuous_assign_stmt( int line, int col,
                        VeriNode *delay, VeriNode *drive, VeriNode *exp )
{
  VeriDelayExp  * idelay = NULL;
  VeriDriveExp  * idrive = NULL;
  VeriStmt * stmt   = NULL;
  
  if( delay == NULL )
  {
    idelay = new VeriDelayExp( _node_pool.next_id( ),
                               _node_pool.next_id( ),
                               line, col, _mod );
    _node_pool.insert( idelay->delay1() );
    _node_pool.insert( idelay );
  }
  else
    idelay = static_cast<VeriDelayExp *>(delay);

  if( drive == NULL )
  {
    idrive = new VeriDriveExp( _node_pool.next_id( ), line, col, _mod );
    _node_pool.insert( idrive );
  }
  else
    idrive = static_cast<VeriDriveExp *>(drive);

  VeriMultiExp *mexp = NULL;
  if( exp->detail_type() != veEXP_MULTI )
  {
    mexp = new VeriMultiExp( _node_pool.next_id( ), vemCOMMA_EXP,
                             exp->line(), exp->col(), _mod );
    _node_pool.insert( mexp );
    mexp->append_child( exp );
    exp->set_father( mexp );
  }
  else
    mexp = static_cast<VeriMultiExp *>(exp);

  stmt = new VeriContAssignStmt( _node_pool.next_id( ), line, col,
                                  idelay, idrive, mexp, _mod );
  _node_pool.insert( stmt );

  int re = stmt->check_child_validation();
  if( re == OK )
  {
    idelay->set_father( stmt );
    idrive->set_father( stmt );
    mexp->set_father( stmt );
  }

  return stmt;
}

VeriNode *
VeriParser::gen_init_stmt( int line, int col, VeriNode *stmt )
{
  VeriStmt * init = new VeriInitStmt( _node_pool.next_id( ),
                                       line, col, stmt, _mod );
  _node_pool.insert( init );
  int re = init->check_child_validation( );
  if( re == OK )
    stmt->set_father( init );
  return init;
}

VeriNode *
VeriParser::gen_always_stmt( int line, int col, VeriNode *stmt )
{
  VeriStmt * always = new VeriAlwaysStmt( _node_pool.next_id( ),
                                           line, col, stmt, _mod );
  _node_pool.insert( always );
  int re = always->check_child_validation( );
  if( re == OK )
    stmt->set_father( always );

  return always;
}

VeriModInstance *
VeriParser::gen_mod_instance( VeriNode *mod_inst )
{
  if( mod_inst == NULL || mod_inst->detail_type() != veEXP_CALLER )
    return NULL;
  
  VeriCallerExp * call_exp = static_cast<VeriCallerExp *>(mod_inst);
  VeriModInstance * stmt
    = new VeriModInstance( _node_pool.next_id( ),
                            mod_inst->line(), mod_inst->col(),
                            call_exp->caller(), call_exp->arg(), _mod );
  _node_pool.insert( stmt );
  call_exp->caller()->set_father( stmt );
  call_exp->arg()->set_father( stmt );
  return stmt;
}

VeriNode *
VeriParser::gen_mod_instantiation( int line, int col, const char * mod_name,
                                   VeriNode *param, VeriNode *list )
{
  if( list == NULL || list->tok_type() != vtEXP )
    return NULL;

  String name = "";
  if( mod_name != NULL )
    name = mod_name;

  if( param != NULL && param->detail_type() != veEXP_MULTI )
  {
    VeriMultiExp *mexp = new VeriMultiExp( _node_pool.next_id( ), vemCOMMA_EXP,
                                           param->line(), param->col(), _mod );
    _node_pool.insert( mexp );
    mexp->append_child( param );
    param->set_father( mexp );
    param = mexp;
  }

  VeriModInstantiation * mod
    = new VeriModInstantiation( _node_pool.next_id( ), line, col,
                                 name, param, _mod );
  _node_pool.insert( mod );
  int re = mod->check_child_validation( );
  if( re == OK )
  {
    if( param != NULL )
      param->set_father( mod );
  }

  _mod_instantiations.push_back( mod );

  VeriModInstance * mod_inst = NULL;
  switch( list->detail_type() )
  {
    case veEXP_CALLER :
      mod_inst = gen_mod_instance( list );
      if( mod_inst != NULL )
      {
        mod->append_mod_inst( mod_inst );
        mod_inst->set_father( mod );
      }
      break;

    case veEXP_MULTI :
    {
      VeriMultiExp * mexp = static_cast<VeriMultiExp *>(list);
      switch( mexp->multi_type() )
      {
        case vemCOMMA_EXP :
        case vemPORT_EXP :
        case vemPORT_INST_EXP :
        {
          NodeList &caller_list = mexp->exp();
          NodeList::iterator it = caller_list.begin();
          for( ; it != caller_list.end(); ++it )
          {
            mod_inst = gen_mod_instance( *it );
            if( mod_inst != NULL )
            {
              mod->append_mod_inst( mod_inst );
              mod_inst->set_father( mod );
            }
          }
          break;
        }

        default:
          return NULL;
      }
      break;
    }

    default:
      break;
  }
  return mod;
}

VeriGateInstance *
VeriParser::gen_gate_instance( VeriNode *gate_inst )
{
  if( gate_inst == NULL )
    return NULL;

  VeriCallerExp * call_exp = NULL;
  VeriMultiExp  * multi_exp = NULL;

  VeriGateInstance *stmt = NULL;
  switch( gate_inst->detail_type() )
  {
    case veEXP_CALLER :
      call_exp = static_cast<VeriCallerExp *>(gate_inst);
      stmt = new VeriGateInstance( _node_pool.next_id( ), gate_inst->line(),
                                   gate_inst->col(), call_exp->caller(), 
                                   call_exp->arg(), _mod );
      _node_pool.insert( stmt );
      gate_inst->set_father( stmt );
      call_exp->caller()->set_father( stmt );
      call_exp->arg()->set_father( stmt );
      break;

    case veEXP_PRI :
      stmt = new VeriGateInstance( _node_pool.next_id( ), gate_inst->line(),
                                   gate_inst->col(), gate_inst,
                                   NULL, _mod );
      _node_pool.insert( stmt );
      gate_inst->set_father( stmt );
      break;

    case veEXP_MULTI :
      multi_exp = static_cast<VeriMultiExp *>(gate_inst);
      switch( multi_exp->multi_type() )
      {
        case vemCOMMA_EXP : case vemPORT_EXP :
          stmt = new VeriGateInstance( _node_pool.next_id( ), gate_inst->line(),
                                       gate_inst->col(), NULL, 
                                       gate_inst, _mod );
          _node_pool.insert( stmt );
          gate_inst->set_father( stmt );
          break;

        default:
          break;
      }
      break;

    default:
      break;
  }
  
  return stmt;
}

VeriNode *
VeriParser::gen_gate_instantiation( int line, int col, 
                            int gate_type, VeriNode *drive,
                            VeriNode *delay, VeriNode *list )
{
  VeriDelayExp * idelay = NULL;
  VeriDriveExp * idrive = NULL;
  
  if( delay == NULL )
  {
    idelay = new VeriDelayExp( _node_pool.next_id( ),
                               _node_pool.next_id( ),
                                line, col, _mod );
    _node_pool.insert( idelay->delay1() );
    _node_pool.insert( idelay );
  }
  else
    idelay = static_cast<VeriDelayExp *>(delay);

  if( drive == NULL )
  {
    idrive = new VeriDriveExp( _node_pool.next_id( ), line, col, _mod );
    _node_pool.insert( idrive );
  }
  else
    idrive = static_cast<VeriDriveExp *>(drive);

  VeriGateInstantiation * gate
    = new VeriGateInstantiation( _node_pool.next_id( ),
                                  line, col, gate_type,
                                  idrive, idelay, _mod );
  _node_pool.insert( gate );
  int re = gate->check_child_validation();
  if( re != OK )
    return gate;
  else
  {
    idelay->set_father( gate );
    idrive->set_father( gate );
  }

  if( list == NULL || list->tok_type() != vtEXP )
    return gate;

  VeriGateInstance *gate_inst = NULL;
  switch( list->detail_type() )
  {
    case veEXP_ARRAY  : case veEXP_PRI : case veEXP_UNA : case veEXP_BIN :
    case veEXP_CALLER : case veEXP_MCONCAT : case veEXP_QUES :
    case veEXP_SIGN   : case veEXP_UNSIGN : case veEXP_MINTY :
      gate_inst = gen_gate_instance( list );
      if( gate_inst != NULL )
      {
        gate->append_gate( gate_inst );
        gate_inst->set_father( gate );
      }
      break;

    case veEXP_MULTI :
    {
      VeriMultiExp * mexp = static_cast<VeriMultiExp *>(list);
      NodeList & exp_list = mexp->exp();
      NodeList::iterator it = exp_list.begin();
      for( ; it != exp_list.end(); ++it )
      {
        gate_inst = gen_gate_instance( *it );
        if( gate_inst != NULL )
        {
          gate->append_gate( gate_inst );
          gate_inst->set_father( gate );
        }
      }
      break;
    }

    default:
      break;
  }
  return gate;
}

VeriNode *
VeriParser::gen_tf_declaration( int line, int col, int inout, int type,
                                  int is_signed, VeriNode *range,
                                  VeriNode *port_ids )
{
  VeriTfDecl * tf_decl = new VeriTfDecl( _node_pool.next_id( ),
                                          line, col, inout,
                                          type, is_signed, range, _mod );
  _node_pool.insert( tf_decl );

  switch( port_ids->detail_type() )
  {
    case veEXP_PRI :
      tf_decl->append_port( port_ids );
      port_ids->set_father( tf_decl );
      break;
      
    case veEXP_MULTI :
    {
      VeriMultiExp *mexp = static_cast<VeriMultiExp *>(port_ids);
      if( mexp->multi_type() == vemCOMMA_EXP )
      {
        NodeList &ids = mexp->exp();
        NodeList::iterator it = ids.begin();
        for( ; it != ids.end(); ++it )
        {
          tf_decl->append_port( *it );
          (*it)->set_father( tf_decl );
        }
      }
      break;
    }

    default:
      break;
  }

  int re = tf_decl->check_child_validation();
  if( re == OK )
  {
    if( range != NULL )
      range->set_father( tf_decl );
  }
  return tf_decl;
}

VeriNode *
VeriParser::gen_tp_list( VeriNode *port )
{
  VeriTaskPortList * tp_list
    = new VeriTaskPortList( _node_pool.next_id( ), port->line(), port->col(), _mod );
  _node_pool.insert( tp_list );

  tp_list->append_port( port );
  int re = tp_list->check_last_child_validation();
  if( re == OK )
    port->set_father( tp_list );

  return tp_list;
}

VeriNode *
VeriParser::append_tp_list( VeriNode *list, VeriNode *port )
{
  VeriTaskPortList * tp_list
    = static_cast<VeriTaskPortList *>(list);
  tp_list->append_port( port );

  int re = tp_list->check_last_child_validation();
  if( re == OK )
    port->set_father( list );
  return tp_list;
}

VeriNode *
VeriParser::gen_fp_list( VeriNode *port )
{
  VeriFuncPortList * fp_list
    = new VeriFuncPortList( _node_pool.next_id( ),
                             port->line(), port->col(), _mod );
  _node_pool.insert( fp_list );

  fp_list->append_port( port );
  int re = fp_list->check_last_child_validation();
  if( re == OK )
    port->set_father( fp_list );
  return fp_list;
}

VeriNode *
VeriParser::append_fp_list( VeriNode *list, VeriNode *port )
{
  VeriFuncPortList * fp_list
    = static_cast< VeriFuncPortList *>(list);
  fp_list->append_port( port );

  int re = fp_list->check_last_child_validation();
  if( re == OK )
    port->set_father( list );
  return fp_list;
}

VeriNode *
VeriParser::gen_task_item_list( VeriNode *item )
{
  VeriTaskItemDeclList * list
    = new VeriTaskItemDeclList( _node_pool.next_id( ),
                                 item->line(), item->col(), _mod );
  _node_pool.insert( list );
  list->append_item( item );

  int re = list->check_last_child_validation();
  if( re == OK )
    item->set_father( list );
  return list;
}

VeriNode *
VeriParser::append_task_item_list( VeriNode *list, VeriNode *item )
{
  VeriTaskItemDeclList * tlist
    = static_cast<VeriTaskItemDeclList *>(list);
  tlist->append_item( item );

  int re = tlist->check_last_child_validation();
  if( re == OK )
    item->set_father( list );
  return list;
}

VeriNode *
VeriParser::gen_block_item_list( VeriNode *item )
{
  VeriBlockItemDeclList * list
    = new VeriBlockItemDeclList( _node_pool.next_id( ),
                                  item->line(), item->col(), _mod );
  _node_pool.insert( list );
  list->append_item( item );

  int re = list->check_last_child_validation();
  if( re == OK )
    item->set_father( list );
  return list;
}

VeriNode *
VeriParser::append_block_item_list( VeriNode *list, VeriNode *item )
{
  VeriBlockItemDeclList * tlist
    = static_cast<VeriBlockItemDeclList *>(list);
  tlist->append_item( item );

  int re = tlist->check_last_child_validation();
  if( re == OK )
    item->set_father( list );
  return list;
}

VeriNode *
VeriParser::gen_func_item_list( VeriNode *item )
{
  VeriFuncItemDeclList * list
    = new VeriFuncItemDeclList( _node_pool.next_id( ),
                                 item->line(), item->col(), _mod );
  _node_pool.insert( list );
  list->append_item( item );

  int re = list->check_last_child_validation();
  if( re == OK )
    item->set_father( list );
  return list;
}

VeriNode *
VeriParser::append_func_item_list( VeriNode *list, VeriNode *item )
{
  VeriFuncItemDeclList * tlist
    = static_cast<VeriFuncItemDeclList *>(list);
  tlist->append_item( item );

  int re = tlist->check_last_child_validation();
  if( re == OK )
    item->set_father( list );
  return list;
}

VeriNode *
VeriParser::gen_param_decl_list( VeriNode *item )
{
  VeriParamDeclList * list
    = new VeriParamDeclList( _node_pool.next_id( ),
                              item->line(), item->col(), _mod );
  _node_pool.insert( list );
  list->append_param( item );

  int re = list->check_last_child_validation();
  if( re == OK )
    item->set_father( list );
  return list;
}

VeriNode *
VeriParser::append_param_decl_list( VeriNode *list, VeriNode *item )
{
  VeriParamDeclList * tlist
    = static_cast<VeriParamDeclList *>(list);
  tlist->append_param( item );

  int re = tlist->check_last_child_validation();
  if( re == OK )
    item->set_father( list );
  return list;
}

VeriNode *
VeriParser::gen_port_decl_list( VeriNode *item )
{
  VeriPortDeclList * list
    = new VeriPortDeclList( _node_pool.next_id( ),
                             item->line(), item->col(), _mod );
  _node_pool.insert( list );
  list->append_port( item );

  int re = list->check_last_child_validation();
  if( re == OK )
    item->set_father( list );
  return list;
}

VeriNode *
VeriParser::append_port_decl_list( VeriNode *list, VeriNode *item )
{
  VeriPortDeclList * tlist
    = static_cast<VeriPortDeclList *>(list);
  tlist->append_port( item );

  int re = tlist->check_last_child_validation();
  if( re == OK )
    item->set_father( list );
  return list;
}

VeriNode *
VeriParser::gen_task_decl( int line, int col, int is_auto, const char *tname,
                         VeriNode *task_port, VeriNode *task_item,
                         VeriNode *stmt )
{
  String iname = "";
  if( tname != NULL ) iname = tname;

  VeriTaskDecl *decl
    = new VeriTaskDecl( _node_pool.next_id( ), line, col, iname, is_auto,
                        task_port, task_item, stmt, _mod );
  _node_pool.insert( decl );

  int re = decl->check_child_validation();
  if( re == OK )
  {
    if( task_port != NULL ) task_port->set_father( decl );
    if( task_item != NULL ) task_item->set_father( decl );
    stmt->set_father( decl );
  }
  return decl;
}

VeriNode *
VeriParser::gen_func_decl( int line, int col, int is_auto, int is_signed,
                         int is_int, VeriNode *range, const char *name,
                         VeriNode *port_list, VeriNode *item_decl,
                         VeriNode *stmt )
{
  assert( is_int == IS_FALSE || range == NULL );
  
  String iname = "";
  if( name != NULL )
    iname = name;

  VeriDecl * func = new VeriFuncDecl( _node_pool.next_id( ), line, col,
                                       is_auto, is_signed, is_int, range,
                                       iname, port_list, item_decl,
                                       stmt, _mod);
  _node_pool.insert( func );
  int re = func->check_child_validation();
  if( re != OK )
  {
    if( port_list != NULL )
      port_list->set_father( func );
    item_decl->set_father( func );
    stmt->set_father( func );
  }
  return func;
}

VeriNode *
VeriParser::gen_var_decl( int line, int col, int inout, int type,
                          int is_signed, int vector_scalar, VeriNode *drive,
                          VeriNode *range, VeriNode *delay, VeriNode * list )
{
  if( list == NULL || list->tok_type() != vtEXP )
    return list;

  int re;
  if( range != NULL )
    vector_scalar = vpVECTOR;

  VeriVarDecl * var = new VeriVarDecl( _node_pool.next_id( ),
                                       line, col, inout, type,
                                       is_signed, vector_scalar, drive,
                                       range, delay, _mod );
  _node_pool.insert( var );
  re = var->check_child_validation( );
  if( re == OK )
  {
    if( drive != NULL ) drive->set_father( var );
    if( range != NULL ) range->set_father( var );
    if( delay != NULL ) delay->set_father( var );
  }

  switch( list->detail_type() )
  {
    case veEXP_PRI :
    case veEXP_ASSIGN :
    case veEXP_ARRAY :
      var->append_var( list );
      re = var->check_last_child_validation( );
      if( re == OK )
        list->set_father( var );
        
      break;

    case veEXP_MULTI:
    {
      VeriMultiExp * mexp = static_cast<VeriMultiExp *>(list);
      if( mexp->multi_type() == vemCOMMA_EXP )
      {
        NodeList & vlist = mexp->exp();
        NodeList::iterator it = vlist.begin();
        for( ; it != vlist.end(); ++it )
        {
          var->append_var( *it );
          re = var->check_last_child_validation( );
          if( re == OK )
            (*it)->set_father( var );
        }
      }
      break;
    }

    default:
      break;
  }
  return var;
}

VeriNode *
VeriParser::gen_param_decl( int line, int col, int is_signed, int is_int,
                          int is_local, VeriNode *range, VeriNode *list )
{
  if( list == NULL || list->tok_type() != vtEXP )
    return list;

  int re;
  VeriParamDecl * var = new VeriParamDecl( _node_pool.next_id( ),
                                            line, col, is_signed,
                                            is_int, is_local, range, _mod );
  _node_pool.insert( var );
  re = var->check_child_validation( );
  if( re == OK )
  {
    if( range != NULL )
      range->set_father( var );
  }

  switch( list->detail_type() )
  {
    case veEXP_ASSIGN:
      var->append_var( list );
      re = var->check_last_child_validation( );
      if( re == OK )
        list->set_father( var );
      break;

    case veEXP_MULTI:
    {
      VeriMultiExp * mexp = static_cast<VeriMultiExp *>(list);
      if( mexp->multi_type() == vemCOMMA_EXP )
      {
        NodeList & vlist = mexp->exp();
        NodeList::iterator it = vlist.begin();
        for( ; it != vlist.end(); ++it )
        {
          var->append_var( *it );
          re = var->check_last_child_validation( );
          if( re == OK )
            (*it)->set_father( var );
        }
      }
      break;
    }

    default:
      break;
  }
  return var;
}

VeriNode *
VeriParser::gen_int_decl( int line, int col, VeriNode *list )
{
  if( list == NULL || list->tok_type() != vtEXP )
    return list;

  int re;
  VeriIntDecl * int_decl = new VeriIntDecl( _node_pool.next_id( ),
                                            line, col, _mod );
  _node_pool.insert( int_decl );

  switch( list->detail_type() )
  {
    case veEXP_PRI :
    case veEXP_ARRAY :
    case veEXP_ASSIGN:
      int_decl->append_var( list );
      re = int_decl->check_last_child_validation( );
      if( re == OK )
        list->set_father( int_decl );
      break;

    case veEXP_MULTI:
    {
      VeriMultiExp * mexp = static_cast<VeriMultiExp *>(list);
      if( mexp->multi_type() == vemCOMMA_EXP )
      {
        NodeList & vlist = mexp->exp();
        NodeList::iterator it = vlist.begin();
        for( ; it != vlist.end(); ++it )
        {
          int_decl->append_var( *it );
          re = int_decl->check_last_child_validation( );
          if( re == OK )
            (*it)->set_father( int_decl );
        }
      }
      break;
    }

    default:
      break;
  }
  return int_decl;
}

VeriNode *
VeriParser::gen_genvar_decl( int line, int col, VeriNode *list )
{
  if( list == NULL || list->tok_type() != vtEXP )
    return list;

  int re;
  VeriGenVarDecl * genvar = new VeriGenVarDecl( _node_pool.next_id( ),
                                                 line, col, _mod );
  _node_pool.insert( genvar );

  switch( list->detail_type() )
  {
    case veEXP_PRI :
    case veEXP_ARRAY :
    case veEXP_ASSIGN:
      genvar->append_var( list );
      re = genvar->check_last_child_validation( );
      if( re == OK )
        list->set_father( genvar );
      break;

    case veEXP_MULTI:
    {
      VeriMultiExp * mexp = static_cast<VeriMultiExp *>(list);
      if( mexp->multi_type() == vemCOMMA_EXP )
      {
        NodeList & vlist = mexp->exp();
        NodeList::iterator it = vlist.begin();
        for( ; it != vlist.end(); ++it )
        {
          genvar->append_var( *it );
          re = genvar->check_last_child_validation( );
          if( re == OK )
            (*it)->set_father( genvar );
        }
      }
      break;
    }

    default:
      break;
  }
  return genvar;
}

VeriNode *
VeriParser::gen_mod( int line, int col, const char *name, VeriNode *param,
                       VeriNode *port, VeriNode *item_list )
{
  if( _mod == NULL )
    prep_for_new_mod( );

  _mod->set_line( line );
  _mod->set_col( col );

  String iname = "";
  if( name != NULL ) iname = name;
  _mod->set_name( iname );

  if( _top_mod_name.empty() )
    _top_mod_name = iname;

  if(    port != NULL
      && port->tok_type( ) == vtEXP
      && port->detail_type( ) != veEXP_MULTI )
  {
    VeriMultiExp *mexp = new VeriMultiExp( _node_pool.next_id( ), vemCOMMA_EXP,
                                           port->line(), port->col(), _mod );
    _node_pool.insert( mexp );
    mexp->append_child( port );
    port->set_father( mexp );
    port = mexp;
  }

  _mod->set_param( param );
  _mod->set_port( port );
  _mod->set_item( item_list );
  _mod->addModInstantiated();

  _mod->set_fname( _fname, _fline );

  if( param != NULL ) param->set_father( _mod );
  if( port != NULL ) port->set_father( _mod );
  if( item_list != NULL ) item_list->set_father( _mod );

  append_module( _mod );

  _mods->add_mod_conn( _mod, _mod_instantiations );

  prep_for_new_mod( );
  
  return _mod;
}

int
VeriParser::clear_syntree( void )
{
  _node_pool.clean_up( );
  return OK;
}

void
VeriParser::dump_ast( CString& dir )
{
  _mods->dump_all_modules( dir, &_drawer );
}

void
VeriParser::append_err( int lvl, int line, int col, int err )
{
  String other = "";
  _err->append_err( lvl, line, col, err, other );
}

};
