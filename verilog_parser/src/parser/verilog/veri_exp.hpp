/* 
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : veri_node.hpp
 * Summary  : verilog syntax analyzer true node class : expression
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2013.1.28
 */

#ifndef PARSER_VERILOG_VERI_EXP_HPP_
#define PARSER_VERILOG_VERI_EXP_HPP_

#include "general_err.hpp"
#include "veri_node.hpp"

#include <string>
#include <list>
#include <cstring>

namespace rtl
{

class VeriNum;
class VeriPrimaryExp;

/*
 * class VeriExp :
 *     Verilog syntax analyzer true node : Expression
 */
class VeriExp : public VeriNode
{
  public:
    VeriExp( Uint idx, int exp_type, int line, int col,
             VeriMod *mod )
      :  VeriNode( idx, vtEXP, exp_type, line, col, mod )
      , _calc_value( NULL )
      , _is_signed( IS_FALSE )
    {}

    virtual ~VeriExp() {};

    // accessors
    VeriNum * calc_value( void ) { return _calc_value; }
    short is_signed( void ) { return _is_signed; }

    // mutators
    void set_calc_value( VeriNum *result ) { _calc_value = result; }
    void set_signed( short is_signed ) { _is_signed = is_signed; }

    virtual void clean_up( void ) = 0;
    virtual int  check_child_validation( void ) = 0;

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue ) = 0;
    virtual int  pretty_dot_label( Ofstream& ofile, int depth ) = 0;

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info ) = 0;
    virtual int  push_for_replacement( NodeVec& mstack ) = 0;
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl ) = 0;

  protected:
    VeriExp( Uint idx, const VeriExp *exp_copy )
      :  VeriNode( idx, exp_copy )
      , _calc_value( NULL )
      , _is_signed( exp_copy->_is_signed )
    {}

    int check_primary_not_other( VeriPrimaryExp *pexp );

  private:
    VeriNum *_calc_value;
    short    _is_signed;
};

class VeriDummyPort : public VeriExp
{
  public:
    VeriDummyPort( Uint idx, int line, int col, VeriMod *mod )
      :  VeriExp( idx, veEXP_DUMMY_PORT, line, col, mod )
    {}

    virtual ~VeriDummyPort( ) { clear(); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    VeriDummyPort( Uint idx, const VeriDummyPort *copy )
      :  VeriExp( idx, copy )
    {}

};

/*
 * class VeriPortRefExp :
 *     Verilog syntax analyzer true node : Port-Reference-Expression
 */
class VeriPortRefExp : public VeriExp
{
  public:
    VeriPortRefExp( Uint idx, int line, int col, CString& port_ref,
             VeriMod *mod )
      :  VeriExp( idx, veEXP_PORT_REF, line, col, mod )
      , _port_ref( port_ref )
    {}

    virtual ~VeriPortRefExp( ) { clear(); }

    // accessors
    CString& port_ref( void ) const { return _port_ref; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    VeriPortRefExp( Uint idx, const VeriPortRefExp *copy )
      :  VeriExp( idx, copy )
      , _port_ref( copy->_port_ref )
    {}

  private:
    String  _port_ref;
};

/*
 * class VeriNamedParamExp :
 *     Verilog syntax analyzer true node : Named-Parameter-Expression
 */
class VeriNamedParamExp : public VeriExp
{
  public:
    VeriNamedParamExp( Uint idx, int line, int col,
                       CString& ref, VeriNode *port_exp, VeriMod *mod )
      :  VeriExp( idx, veEXP_NAMED_PARAM, line, col, mod )
      , _ref( ref )
      , _port_ref( NULL )
      , _port_exp( port_exp )
    {}

    virtual ~VeriNamedParamExp( ) { clear(); }

    // accessors
    CString  & ref( void ) const { return _ref; }
    VeriNode * port_ref( void ) { return _port_ref; }
    VeriNode * port_exp( void ) { return _port_exp; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );
  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriNamedParamExp( Uint idx, const VeriNamedParamExp *copy )
      :  VeriExp( idx, copy )
      , _ref( copy->_ref )
      , _port_ref( NULL )
      , _port_exp( NULL )
    {}

  private:
    String    _ref;
    VeriNode *_port_ref;
    VeriNode *_port_exp;
};

/*
 * class VeriPrimaryExp :
 *     Verilog syntax analyzer true node : Primary-expression
 *   Primary-expression has one expression child, no operator contained
 */
class VeriPrimaryExp : public VeriExp
{
  typedef std::vector<int> IVec;

  public:
    VeriPrimaryExp( Uint idx, int line, int col,
                    CString& str, int primary_type, VeriMod *mod )
      : VeriExp( idx, veEXP_PRI, line, col, mod )
      , _str( str  )
      , _var( NULL )
      , _pri_type( primary_type )
    {}

    virtual ~VeriPrimaryExp() { clear(); }

    // accessors
    CString&   str( void ) const { return _str; }
    int        pri_type( void ) const { return _pri_type; }
    VeriNode * var( void ) { return _var; }

    // mutators
    void set_var( VeriNode *var ) { _var = var; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int process_id( String& help_info );

  protected:
    VeriPrimaryExp( Uint idx, const VeriPrimaryExp *copy )
      :  VeriExp( idx, copy )
      , _str( copy->_str )
      , _var( NULL )
      , _pri_type( copy->_pri_type )
    {}

  private:
    String    _str;
    VeriNode *_var;
    int       _pri_type;
};

/*
 * class VeriUnaryExp :
 *     Verilog syntax analyzer true node : Unary-expression
 *   Unary-expression has one expression child, one operator child
 */
class VeriUnaryExp : public VeriExp
{
  public:
    VeriUnaryExp( Uint idx, int line, int col, int op, VeriNode *exp,
             VeriMod *mod )
      : VeriExp( idx, veEXP_UNA, line, col, mod )
      , _op( op )
      , _exp( exp )
    {}

    virtual ~VeriUnaryExp() { clear(); }

    // accessors
    VeriNode * exp_child( void ) { return _exp; }
    int op( void ) const { return _op;  }

    // mutators
    void set_exp_child( VeriNode *exp ) { _exp = exp; }
    void set_op ( int op  ) { _op  = op;  }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int process_unary_op( void );

  protected:
    VeriUnaryExp( Uint idx, const VeriUnaryExp *copy )
      :  VeriExp( idx, copy )
      , _op( copy->_op )
      , _exp( NULL )
    {}

  private:
    int        _op;
    VeriNode  *_exp;
};

/*
 * class VeriEventExp :
 *     Verilog syntax analyzer true node : Event-expression
 *   Event-expression has one expression child, one event-edge type
 *     1, event-edge could be pos-edge, neg-edge, or none
 *     2, expression must be primary-var
 */
class VeriEventExp : public VeriExp
{
  public:
    VeriEventExp( Uint idx, int line, int col, int etype, VeriNode *exp,
             VeriMod *mod )
      : VeriExp( idx, veEXP_EVENT, line, col, mod )
      , _etype( etype )
      , _exp( exp )
    {}

    virtual ~VeriEventExp() { clear(); }

    // accessors
    VeriNode * exp_child( void ) { return _exp; }
    int etype( void ) { return _etype;  }

    // mutators
    void set_exp_child( VeriNode *exp ) { _exp = exp; }
    void set_etype ( int etype  ) { _etype  = etype;  }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );
  protected:
    int push_children( NodeVec& mstack );
    int check_sensitivity_list( void );

  protected:
    VeriEventExp( Uint idx, const VeriEventExp *copy )
      :  VeriExp( idx, copy )
      , _etype( copy->_etype )
      , _exp( NULL )
    {}

  private:
    int        _etype;
    VeriNode  *_exp;
};


/*
 * class VeriArrayExp :
 *     Verilog syntax analyzer true node : Array-expression
 *   Array-expression has two expression children :
 *       1, primary->var
 *       2, range list ( making array-expression recursive )
 */
class VeriArrayExp : public VeriExp
{
  typedef std::vector<int> IVec;

  public:
    VeriArrayExp( Uint idx, int line, int col, VeriNode *id, VeriMod *mod )
      : VeriExp( idx, veEXP_ARRAY, line, col, mod )
      , _id( id )
      , _name("")
      , _not_calc(IS_TRUE)
    {}
  
    virtual ~VeriArrayExp() { clear(); }
  
    // accessors
    VeriNode * id   ( void ) { return _id;    }
    NodeList & range( void ) { return _range; }

    CString  & name( void ) { return _name;  }
    int        range_depth( void ) const { return _range.size(); }
    int        not_calc( void ) const { return _not_calc; }
    int        is_whole( void ) const { return _is_whole; }
    int        dstart  ( void ) const { return _dstart;   }
    int        dend    ( void ) const { return _dend;     }
    const IVec & dimen ( void ) const { return _dimen;    }

    IList    & range_from( void ) { return _range_from; }
    IList    & range_to  ( void ) { return _range_to;   }
  
    // mutators
    void append_range( VeriNode *range ) { _range.push_back( range ); }
  
    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );
 
  protected:
    int push_children( NodeVec& mstack );
    int calc_range( void );
    int type_deduction( String& help_info );

  protected:
    VeriArrayExp( Uint idx, const VeriArrayExp *copy )
      :  VeriExp( idx, copy )
      , _id( NULL )
      , _name( copy->_name )
      , _not_calc(IS_TRUE)
    {}

  private:
    VeriNode  *_id;
    String     _name;
    NodeList   _range;
    IList      _range_from;
    IList      _range_to;

    int        _not_calc;
    int        _is_whole;
    int        _dstart;
    int        _dend;
    IVec       _dimen;
};


/*
 * class VeriCallerExp :
 *     Verilog syntax analyzer true node : Caller-expression
 *   Caller-expression has two expression children :
 *       1, caller : array identifier expression
 *       2, arg    : augument list ( must be MultiExp )
 */
class VeriCallerExp : public VeriExp
{
  public:
    VeriCallerExp( Uint idx, int line, int col, VeriNode *caller, VeriNode *arg,
             VeriMod *mod )
      : VeriExp( idx, veEXP_CALLER, line, col, mod )
      , _caller_name( "" )
      , _caller( caller )
      , _arg( arg )
      , _call_type( NONE )
      , _hive( NULL )
    {}
  
    virtual ~VeriCallerExp() { clear(); }
  
    // accessors
    VeriNode * caller( void ) { return _caller; }
    VeriNode * arg   ( void ) { return _arg; }
    VeriNode * hive  ( void ) { return _hive; }
    int call_type( void ) { return _call_type; }
  
    // mutators
    void set_caller( VeriNode *exp ) { _caller = exp; }
    void set_arg   ( VeriNode *exp ) { _arg    = exp; }
    void set_call_type( int type   ) { _call_type = type; }
  
    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );
    
    int check_func_call_child_validation( void );
  
  protected:
    int push_children( NodeVec& mstack );
    int unfolding( NodeVec& mstack, String& help_info );
    int get_ret_value( void );

  protected:
    VeriCallerExp( Uint idx, const VeriCallerExp *copy )
      :  VeriExp( idx, copy )
      , _caller_name( copy->_caller_name )
      , _caller( NULL )
      , _arg( NULL )
      , _call_type( copy->_call_type )
      , _hive( NULL )
    {}

  private:
    String     _caller_name;
    VeriNode  *_caller;
    VeriNode  *_arg;
    int        _call_type;

    VeriNode  *_hive;

    NodeList   _ports;
};

/*
 * class VeriBinaryExp :
 *     Verilog syntax analyzer true node : Binary-expression
 *   Binary-expression has two expression children, one operator child
 */
class VeriBinaryExp : public VeriExp
{
  public:
    VeriBinaryExp( Uint idx, int line, int col,
                   VeriNode *left, int op, VeriNode *right,
                        VeriMod *mod )
      : VeriExp( idx, veEXP_BIN, line, col, mod )
      , _left( left )
      , _op( op )
      , _right( right )
    {}

    virtual ~VeriBinaryExp() { clear(); }

    // accessors
    VeriNode * lval( void ) { return _left; }
    int          op( void ) { return _op;   }
    VeriNode * rval( void ) { return _right; }

    // mutators
    void set_lval( VeriNode *exp ) { _left  = exp; }
    void set_op  ( int op )        { _op    = op;  }
    void set_rval( VeriNode *exp ) { _right = exp; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int process_binary_op( void );

    int push_short_cut_left( NodeVec& mstack );
    int process_short_cut_left( NodeVec& mstack );

  protected:
    VeriBinaryExp( Uint idx, const VeriBinaryExp *copy )
      :  VeriExp( idx, copy )
      , _left( NULL )
      , _op( copy->_op )
      , _right( NULL )
    {}

    int check_child_validation( VeriNode *child );

  private:
    VeriNode *_left;
    int       _op;
    VeriNode *_right;
};

/*
 * class VeriAssignExp :
 *     Verilog syntax analyzer true node : Assign-expression
 *   Assign-expression has two expression children
 *     1, left-value should be primary->var
 *     2, right-value should be expression
 */
class VeriAssignExp : public VeriExp
{
  typedef std::vector<int> IVec;

  public:
    VeriAssignExp( Uint idx, int line, int col,
                   VeriNode *lval, VeriNode *rval,
                        VeriMod *mod )
      : VeriExp( idx, veEXP_ASSIGN, line, col, mod )
      , _lval( lval )
      , _rval( rval )
    {}

    virtual ~VeriAssignExp() { clear(); }

    // accessors
    VeriNode * lval( void ) { return _lval; }
    VeriNode * rval( void ) { return _rval; }

    // mutators
    void set_lval( VeriNode *exp ) { _lval = exp; }
    void set_rval( VeriNode *exp ) { _rval = exp; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int check_child_validation( VeriNode *child );

    int push_children( NodeVec& mstack );
    int process_assign_op( void );

  protected:
    VeriAssignExp( Uint idx, const VeriAssignExp *copy )
      :  VeriExp( idx, copy )
      , _lval( NULL )
      , _rval( NULL )
    {}

  private:
    VeriNode *_lval;
    VeriNode *_rval;
};

/*
 * class VeriMultiConcatExp :
 *     Verilog syntax analyzer true node : MultiConcat-expression
 *   MultiConcat-expression has two expression children
 *     1, first expression is single constant expression
 *     2, second expression is multi-expression( concat type )
 */
class VeriMultiConcatExp : public VeriExp
{
  public:
    VeriMultiConcatExp( Uint idx, int line, int col,
                   VeriNode *exp1, VeriNode *exp2,
                        VeriMod *mod )
      : VeriExp( idx, veEXP_MCONCAT, line, col, mod )
      , _exp1( exp1 )
      , _exp2( exp2 )
    {}

    virtual ~VeriMultiConcatExp() { clear(); }

    // accessors
    VeriNode * exp1( void ) { return _exp1; }
    VeriNode * exp2( void ) { return _exp2; }

    // mutators
    void set_exp1( VeriNode *exp ) { _exp1 = exp; }
    void set_exp2( VeriNode *exp ) { _exp2 = exp; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int process_mconcat( void );

  protected:
    VeriMultiConcatExp( Uint idx, const VeriMultiConcatExp *copy )
      :  VeriExp( idx, copy )
      , _exp1( NULL )
      , _exp2( NULL )
    {}

  private:
    VeriNode *_exp1;
    VeriNode *_exp2;
};

/*
 * class VeriRangeExp :
 *     Verilog syntax analyzer true node : Range-expression
 *   Range-expression has two expression children, one operator child
 */
class VeriRangeExp : public VeriExp
{
  public:
    VeriRangeExp( Uint idx, int line, int col,
                   VeriNode *left, int rtype, VeriNode *right,
                        VeriMod *mod )
      : VeriExp( idx, veEXP_RANGE, line, col, mod )
      , _left( left )
      , _rtype( rtype )
      , _right( right )
      , _vleft( RANGE_DEFAULT )
      , _vright( RANGE_DEFAULT )
      , _not_calc( IS_TRUE )
    {}

    virtual ~VeriRangeExp() { clear(); }

    // accessors
    VeriNode * left ( void ) { return _left; }
    VeriNode * right( void ) { return _right; }

    int        rtype( void ) const { return _rtype; }

    int   range_from( void ) const { return _vleft; }
    int   range_to  ( void ) const { return _vright;}
    int   not_calc  ( void ) const { return _not_calc; }

    // mutators
    void set_left ( VeriNode *exp ) { _left  = exp;   }
    void set_rtype( int rtype     ) { _rtype = rtype; }
    void set_right( VeriNode *exp ) { _right = exp;   }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int assign_range( void );

  protected:
    VeriRangeExp( Uint idx, const VeriRangeExp *copy )
      :  VeriExp( idx, copy )
      , _left( NULL )
      , _rtype( copy->_rtype )
      , _right( NULL )
      , _vleft( RANGE_DEFAULT )
      , _vright( RANGE_DEFAULT )
      , _not_calc( IS_TRUE )
    {}

   int check_child_validation( VeriNode *child );

  private:
    VeriNode *_left;
    int       _rtype;
    VeriNode *_right;
    int       _vleft;
    int       _vright;
    int       _not_calc;
};

/*
 * class VeriMintypmaxExp :
 *     Verilog syntax analyzer true node : Mintypmax-expression
 *   Mintypmax-expression has three expression children, two operator children
 *     !, there are only 2-types of ternary expression
 *         1, mintypmax expression
 */
class VeriMintypmaxExp : public VeriExp
{
  public:
    VeriMintypmaxExp( Uint idx, int line, int col,
                      VeriNode *exp1, VeriNode *exp2, VeriNode *exp3,
                      VeriMod *mod )
      : VeriExp( idx, veEXP_MINTY, line, col, mod )
      , _not_calc( IS_TRUE )
    {
      _exp[0] = exp1;
      _exp[1] = exp2;
      _exp[2] = exp3;
      _re[0] = _re[1] = _re[2] = 0;
    }

    virtual ~VeriMintypmaxExp() { clear(); }

    // accessors
    VeriNode * exp_child1( void ) { return _exp[0]; }
    VeriNode * exp_child2( void ) { return _exp[1]; }
    VeriNode * exp_child3( void ) { return _exp[2]; }
    int re1( void ) const { return _re[0]; }
    int re2( void ) const { return _re[1]; }
    int re3( void ) const { return _re[2]; }
    int not_calc( void ) const { return _not_calc; }

    // mutators
    void set_exp_child1( VeriNode *exp ) { _exp[0] = exp; }
    void set_exp_child2( VeriNode *exp ) { _exp[1] = exp; }
    void set_exp_child3( VeriNode *exp ) { _exp[2] = exp; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int process_mintypmax( void );

  protected:
    VeriMintypmaxExp( Uint idx, const VeriMintypmaxExp *copy )
      :  VeriExp( idx, copy )
      , _not_calc( IS_TRUE )
    {
      _exp[0] = NULL;
      _exp[1] = NULL;
      _exp[2] = NULL;
      _re[0] = _re[1] = _re[2] = 0;
    }

   int check_child_validation( VeriNode *child );

  private:
    VeriNode *_exp[3];
    int       _re[3];
    int       _not_calc;
};

/*
 * class VeriQuesExp :
 *     Verilog syntax analyzer true node : Ques-expression
 *   Ques-expression has three expression children, two operator children
 *     !, there are only 2-types of ternary expression
 *         1, mintypmax expression
 */
class VeriQuesExp : public VeriExp
{
  public:
    VeriQuesExp( Uint idx, int line, int col, VeriNode *ques,
                 VeriNode *choice1, VeriNode *choice2, VeriMod *mod )
      : VeriExp( idx, veEXP_QUES, line, col, mod )
      , _ques( ques )
      , _choice1( choice1 )
      , _choice2( choice2 )
      , _choice(0)
    { }

    virtual ~VeriQuesExp() { clear(); }

    // accessors
    VeriNode * ques( void ) { return _ques; }
    VeriNode * choice1( void ) { return _choice1; }
    VeriNode * choice2( void ) { return _choice2; }

    VeriNode * choice ( void )
    {
      switch( _choice )
      {
        case 1 : return _choice1;
        case 2 : return _choice2;
        default : return NULL;
      }
    }

    // mutators
    void set_ques   ( VeriNode *exp ) { _ques = exp; }
    void set_choice1( VeriNode *exp ) { _choice1 = exp; }
    void set_choice2( VeriNode *exp ) { _choice2 = exp; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_condition( NodeVec& mstack );
    int choose_children( NodeVec& mstack );
    int get_answer( void );

  protected:
    VeriQuesExp( Uint idx, const VeriQuesExp *copy )
      :  VeriExp( idx, copy )
      , _ques(NULL)
      , _choice1(NULL)
      , _choice2(NULL)
      , _choice(0)
    { }

   int check_child_validation( VeriNode *child );

  private:
    VeriNode *_ques;
    VeriNode *_choice1;
    VeriNode *_choice2;
    int       _choice;
};

/*
 * class VeriMultiExp :
 *     Verilog syntax analyzer true node : Multi-expression
 *   Multi-expression has multi-exp-type, multiple-expression children
 */
class VeriMultiExp : public VeriExp
{
  public:
    VeriMultiExp( Uint idx, int mul_type, int line, int col, VeriMod *mod )
      : VeriExp( idx, veEXP_MULTI, line, col, mod )
      , _multi_type( mul_type )
      , _exp_num( 0 )
    {}

    virtual ~VeriMultiExp() { clear(); }

    // accessors
    NodeList& exp( void ) { return _exp; }
    int  multi_type( void ) const { return _multi_type; }
    int  exp_num( void ) const { return _exp_num; }

    // mutators
    void set_multi_type( int type ) { _multi_type = type; }
    void append_child( VeriNode *n ) { _exp.push_back( n ); ++_exp_num; }

    int check_pure_event_multi( void );

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_children( NodeVec& mstack );
    int get_answer( void );

  protected:
    VeriMultiExp( Uint idx, const VeriMultiExp *copy )
      :  VeriExp( idx, copy )
      , _multi_type( copy->_multi_type )
      , _exp_num( copy->_exp_num )
    {}

   int check_child_validation( VeriNode *child );

  private:
    int      _multi_type;
    int      _exp_num;
    NodeList _exp;
};

/*
 * class VeriDelayExp :
 *     Verilog syntax analyzer true node : Delay-expression
 *   Delay-expression has 2 or 3 children.
 *   Each children is Primary-expression or Mintypmax-expression
 */
class VeriDelayExp : public VeriExp
{
  public:
    VeriDelayExp( Uint idx, int line, int col, VeriNode *delay1,
                  VeriNode *delay2, VeriNode *delay3, VeriMod *mod )
      : VeriExp( idx, veEXP_DELAY, line, col, mod )
      , _delay1( delay1 )
      , _delay2( delay2 )
      , _delay3( delay3 )
      , _not_calc( IS_TRUE )
    {
      memset( _delay, 0, sizeof(int) * 9 );
    }

    VeriDelayExp( Uint idx1, Uint idx2, int line, int col, VeriMod *mod )
      : VeriExp( idx1, veEXP_DELAY, line, col, mod )
      , _delay2( NULL )
      , _delay3( NULL )
      , _not_calc( IS_TRUE )
    {
      _delay1 = new VeriPrimaryExp( idx2, line, col, "0", veNUM, mod );
      memset( _delay, 0, sizeof(int) * 9 );
    }

    virtual ~VeriDelayExp() { clear(); }

    // accessors
    VeriNode * delay1( void ) { return _delay1; }
    VeriNode * delay2( void ) { return _delay2; }
    VeriNode * delay3( void ) { return _delay3; }

    int delay_num( void ) const ;
    void delay_value( int (&delay)[9] )
    { memcpy( delay, _delay, sizeof(int) * 9 ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int  check_child_validation( VeriNode *child );

  protected:
    int push_children( NodeVec& mstack );
    int get_delay_val( void );
    int get_one_val( VeriExp *delay_exp, int idx );

  protected:
    VeriDelayExp( Uint idx, const VeriDelayExp *copy )
      :  VeriExp( idx, copy )
      , _delay1( NULL )
      , _delay2( NULL )
      , _delay3( NULL )
      , _not_calc( IS_TRUE )
    {
      memset( _delay, 0, sizeof(int) * 9 );
    }

  private:
    VeriNode * _delay1;
    VeriNode * _delay2;
    VeriNode * _delay3;
    int        _delay[9];
    int        _not_calc;
};

class VeriDriveExp : public VeriExp
{
  public:
    VeriDriveExp( Uint idx, int line, int col, int drive_from, int drive_to,
             VeriMod *mod )
      : VeriExp( idx, veEXP_DRIVE, line, col, mod )
      , _drive_from( drive_from )
      , _drive_to( drive_to )
    {}

    VeriDriveExp( Uint idx, int line, int col, VeriMod *mod )
      : VeriExp( idx, veEXP_DRIVE, line, col, mod )
      , _drive_from( DEFAULT_DRIVE_FROM )
      , _drive_to( DEFAULT_DRIVE_TO )
    {}

    virtual ~VeriDriveExp() { clear(); }

    // accessors
    int drive_from( void ) { return _drive_from; }
    int drive_to( void ) { return _drive_to; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    VeriDriveExp( Uint idx, const VeriDriveExp *copy )
      :  VeriExp( idx, copy )
      , _drive_from( copy->_drive_from )
      , _drive_to( copy->_drive_to )
    {}

  private:
    int _drive_from;
    int _drive_to;
};

class VeriSignExp : public VeriExp
{
  public:
    VeriSignExp( Uint idx, int line, int col, VeriNode *exp,
             VeriMod *mod )
      : VeriExp( idx, veEXP_SIGN, line, col, mod )
      , _exp( exp )
    {}

    virtual ~VeriSignExp() { clear(); }

    // accessors
    VeriNode *exp( void ) { return _exp; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );
    
  protected:
    int push_children( NodeVec& mstack );
    int process_sign( void );

  protected:
    VeriSignExp( Uint idx, const VeriSignExp *copy )
      :  VeriExp( idx, copy )
      , _exp( NULL )
    {}

  private:
    VeriNode *_exp;
};

class VeriUnsignExp : public VeriExp
{
  public:
    VeriUnsignExp( Uint idx, int line, int col, VeriNode *exp,
             VeriMod *mod )
      : VeriExp( idx, veEXP_UNSIGN, line, col, mod )
      , _exp( exp )
    {}

    virtual ~VeriUnsignExp() { clear(); }

    // accessors
    VeriNode *exp( void ) { return _exp; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int process_unsign( void );

  protected:
    VeriUnsignExp( Uint idx, const VeriUnsignExp *copy )
      :  VeriExp( idx, copy )
      , _exp( NULL )
    {}

  private:
    VeriNode *_exp;
};

};
#endif // PARSER_VERILOG_VERI_EXP_HPP_
