/*
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : veri_var_container.cpp
 * Summary  : verilog syntax analyzer :: variable container functions
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2012.11.20
 */
#include "general_err.hpp"

#include "veri_node.hpp"
#include "veri_symbol_table.hpp"

#ifdef DEBUG_PARSER
#include <iostream>
using std::cout;
using std::endl;
#endif

namespace rtl
{

int
VeriSymbolTable::init( Uint size, CString& mod_name )
{
  if( size == 0 )
    return OK;

  return hash_create( size, 0, NULL, mod_name, vMOD_SCOPE );
}

int
VeriSymbolTable::hash_create( Uint size, int depth, SymbolTable *floater,
                              CString& name, int table_type )
{
  if( size != 0 )
  {
    _depth   = depth;
    _size    = size;
    _name    = name;
    _table_type = table_type;
    _floater = floater;
    _hash_list = new Symbol*[size];
    for( Uint i = 0 ; i < size ; ++i )
      _hash_list[i] = NULL;
    return OK;
  }
  else
    return ERR;
}

void
VeriSymbolTable::clean_up( void )
{
  if( !_deeper.empty() )
  { 
    ContList::iterator it = _deeper.begin();
    for( ; it != _deeper.end(); ++it )
      delete *it;
    _deeper.clear();
  }

  Symbol *p;
  Symbol *q;
  if( _hash_list != NULL )
  {
    for( Uint i = 0 ; i < _size ; ++i )
    {
      p = _hash_list[i];
      if( p != NULL )
      {
        while( p != NULL )
        {
          q = p;
          p = p->_next;
          q->_next = NULL;
          q->_data = NULL;
          delete q;
          q = NULL;
        }
      }
    }
  }
}

VeriSymbolTable *
VeriSymbolTable::append_scope( CString& name, int table_type )
{
  if( _size != 0 )
  {
    SymbolTable *deeper = new SymbolTable;
    deeper->hash_create( _size, _depth + 1, this, name, table_type );
    _deeper.push_back( deeper );
    return deeper;
  }
  else
    return NULL;
}

VeriSymbolTable *
VeriSymbolTable::finish_scope( void )
{
  int symbol_cnt = 0;

  Symbol *p = NULL;
  Symbol *q = NULL;

  if( _hash_list != NULL )
  {
    for( Uint i = 0 ; i < _size ; ++i )
    {
      p = _hash_list[i];
      if( p != NULL )
      {
        symbol_cnt = 1;
        break;
      }
    }
    if(     symbol_cnt == 0 )
//        || _table_type == vTASK_SCOPE
//        || _table_type == vFUNC_SCOPE )
    {
      for( Uint i = 0 ; i < _size ; ++i )
      {
        p = _hash_list[i];
        if( p != NULL )
        {
          while( p != NULL )
          {
            q = p;
            p = p->_next;
            q->_next = NULL;
            q->_data = NULL;
            delete q;
            q = NULL;
          }
        }
        _hash_list[i] = NULL;
      }
    }
  }
  return _floater;
}

int
VeriSymbolTable::insert( CString& name, VeriNode* data)
{
  Symbol *p, *q;
  Uint hash_key = hash_key33( name.c_str() ) % _size;

  if(hash_key > _size)
    return NOT_EXIST;
  p = _hash_list[hash_key];

  if( p == NULL )
  {
    p = new Symbol;
    p->_name = name;
    p->_data = data;
    p->_next = NULL;
    _hash_list[hash_key] = p;
  }
  else
  {
    while( p != NULL )
    {
      if( hash_cmp33( p->_name.c_str() , name.c_str() ) ) // duplicated
        return ERR;

      q = p;
      p = p->_next;
    }
    p = new Symbol;
    p->_name = name;
    p->_data = data;
    p->_next = NULL;
    q->_next = p;
  }
  return OK;
}

int
VeriSymbolTable::hash_delete( CString& name )
{
  Symbol *p;
  Symbol *q;
  Uint hash_key = hash_key33( name.c_str() ) % _size;

  p = _hash_list[hash_key];
  q = p;

  if( hash_key > _size || NULL == p )
    return NOT_EXIST;

  while( p != NULL )
  {
    if( hash_cmp33( p->_name.c_str() , name.c_str() ) )
    {
      if(p != q)
      {
        q->_next = p->_next;
        delete p;
        p = NULL;
        break;
      }
      else
      {
        delete p;
        _hash_list[hash_key] = NULL;
        break;
      }
    }
    q = p;
    p = p->_next;
  }

  return OK;
}

VeriNode *
VeriSymbolTable::value( CString& name )
{
#ifdef DEBUG_PARSER
//  cout << "symbal_table::value(name) : " << name << endl;
#endif

  Uint hash_key = hash_key33( name.c_str() ) % _size;

  if(hash_key > _size)
    return NULL;

  Symbol  *p = NULL;
  SymbolTable *ptr = this;

  while( 1 )
  {
    if( ptr->_hash_list != NULL )
    {
      p = ptr->_hash_list[hash_key];
      while( p != NULL )
      {
        if( hash_cmp33( p->_name.c_str() , name.c_str() ) )
          return p->_data;
        p = p->_next;
      }
    }

    if( ptr->_floater != NULL )
      ptr = ptr->_floater;
    else
      break;
  }
  return NULL;
}

VeriNode *
VeriSymbolTable::value_same_level( CString& name )
{
#ifdef DEBUG_PARSER
//  cout << "symbal_table::value_within_module(name) : " << name << endl;
#endif

  Uint hash_key = hash_key33( name.c_str() ) % _size;

  if(hash_key > _size)
    return NULL;

  Symbol  *p = NULL;

  if( _hash_list != NULL )
  {
    p = _hash_list[hash_key];
    while( p != NULL )
    {
      if( hash_cmp33( p->_name.c_str() , name.c_str() ) )
        return p->_data;
      p = p->_next;
    }
  }
  
  return NULL;
}

int
VeriSymbolTable::replace_value_same_level( CString& name, VeriNode *data )
{
#ifdef DEBUG_PARSER
//  cout << "symbal_table::value_within_module(name) : " << name << endl;
#endif

  Uint hash_key = hash_key33( name.c_str() ) % _size;

  if(hash_key > _size)
    return ERR;

  Symbol  *p = NULL;

  if( _hash_list != NULL )
  {
    p = _hash_list[hash_key];
    while( p != NULL )
    {
      if( hash_cmp33( p->_name.c_str() , name.c_str() ) )
      {
        p->_data = data;
        return OK;
      }
      p = p->_next;
    }
  }
  
  return ERR;
}

VeriNode *
VeriSymbolTable::value_within_module( CString& name )
{
#ifdef DEBUG_PARSER
//  cout << "symbal_table::value_within_module(name) : " << name << endl;
#endif

  Uint hash_key = hash_key33( name.c_str() ) % _size;

  if(hash_key > _size)
    return NULL;

  Symbol  *p = NULL;
  SymbolTable *ptr = this;

  while( 1 )
  {
    if( ptr->_hash_list != NULL )
    {
      p = ptr->_hash_list[hash_key];
      while( p != NULL )
      {
        if( hash_cmp33( p->_name.c_str() , name.c_str() ) )
          return p->_data;
        p = p->_next;
      }
    }

    if( ptr->_table_type != vMOD_SCOPE && ptr->_floater != NULL )
      ptr = ptr->_floater;
    else
      break;
  }
  return NULL;
}

void 
VeriSymbolTable::clear()
{
  clean_up();
  delete[] _hash_list;
  _hash_list = NULL;
}

int
VeriSymbolTable::pretty_dot_output( Ofstream& ofile, int depth )
{
  Symbol *p;
  if( _hash_list == NULL )
    return OK;

  for( Uint i = 0 ; i < _size ; ++i )
  {
    p = _hash_list[i];
    while( p != NULL )
    {
      if(      p->_data->tok_type( ) != vtMOD
          && ( p->_data->tok_type( ) != vtTASK || _table_type != vTASK_SCOPE )
          && ( p->_data->tok_type( ) != vtFUNC || _table_type != vFUNC_SCOPE )
        )
      {
        p->_data->pretty_dot_label( ofile, depth );
      }
      p = p->_next;
    }
  }
  return OK;
}

int
VeriSymbolTable::push_more_table( SymbolTableQueue & queue )
{
  ContList::iterator it = _deeper.begin();
  for( ; it != _deeper.end(); ++it )
    queue.push( *it );

  return OK;
}

#ifdef debug_VeriSymbolTable
void VeriSymbolTable::hash_print( )
{
  hash_print_recur( 0 );
}

void
VeriSymbolTable::hash_print_recur( int depth )
{
  int count = 0;
  Symbol *p;
  int j, k;

  cout << endl;
  for( j = 0 ; j < depth ; ++j )
    cout << "  ";
  cout << "type hash_table summarization : " << endl;

  for( j = 0 ; j < depth ; ++j )
    cout << "  ";
  cout << "size is " << _size << endl;

  for( Uint i = 0 ; i < _size ; ++i )
  {
    p = _hash_list[i];
    while( p != NULL )
    {
      count++;
      for( j = 0 ; j < depth ; ++j )
        cout << "  ";
    //  if( p->data()->id() == ctype_FUNC )
    //  {
    //    cout << "func::";
    //    k = -6;
    //  }
    //  else
    //    k = 0;

    //  cout << p->_name;
    //  k += 35 - p->_name.size() - depth * 2;
      for( j = 0 ; j < k ; ++j )
        cout << " ";
      p = p->_next;
    }
    if( count > 0 )
    {
      for( j = 0 ; j < depth ; ++j )
        cout << "  ";
      cout << "node " << i << " = " << count << endl;
    }
    count = 0;
  }
  ContList::iterator ictl = _deeper.begin();
  for( ; ictl != _deeper.end() ; ++ictl )
    ictl->hash_print_recur( depth + 1 );
}
#endif

};
