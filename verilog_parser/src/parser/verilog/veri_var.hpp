/* 
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : veri_var.hpp
 * Summary  : verilog syntax analyzer true node class : variable
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2013.1.28
 */

#ifndef PARSER_VERILOG_VERI_VAR_HPP_
#define PARSER_VERILOG_VERI_VAR_HPP_

#include "veri_node.hpp"

#include <vector>
#include <string>

namespace rtl
{

class VeriNum;

struct VeriSymbolRangeRec
{
  VeriSymbolRangeRec( int f, int t, int b )
    : from(f)
    , to(t)
    , bits(b)
  {}

  int from;
  int to;
  int bits;
};

/*
 * class VeriSysVar :
 *     Verilog syntax analyzer true node : system variable
 *     system variable is prefixed by '$'
 */
class VeriSysVar : public VeriNode
{
  typedef std::string  String;
  typedef const String CString;
  typedef std::ofstream Ofstream;

  public:
    VeriSysVar( Uint idx, int line, int col, CString& name,
                VeriMod *mod )
      :  VeriNode( idx, vtSYS_VAR, vtSYS_VAR, line, col, mod )
      ,  _name(name)
    {}

    virtual ~VeriSysVar() { clear(); }

    // accessors
    CString& name( void ) const { return _name; }

    // mutators
    void set_name( CString& v ) { _name = v; }

    virtual void clean_up( void );

    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    VeriSysVar() {};

  private:
    String _name;
};


/*
 * class VeriVar :
 *     Verilog syntax analyzer true node : variable
 */
class VeriVar : public VeriNode
{
  typedef std::string      String;
  typedef const String     CString;
  typedef std::vector<int> IVec;
  typedef std::vector< VeriSymbolRangeRec > SymbolRanges;

  public:
    VeriVar( Uint idx, int line, int col, CString& name, short inout,
             short net_type, short is_signed, short vec_scalar,
             short drive_from, short drive_to, int dstart, int dend,
             VeriNode *decl, VeriNum **val, int total_cnt, VeriMod *mod )
      :
         VeriNode( idx, vtVAR, vtVAR, line, col, mod )
      , _name(name)
      , _inout(inout)
      , _net_type(net_type)
      , _is_signed(is_signed)
      , _vec_scalar(vec_scalar)
      , _drive_from(drive_from)
      , _drive_to(drive_to)
      , _dstart(dstart)
      , _dend(dend)
      , _dlen(abs(dend-dstart)+1)
      , _decl(decl)
      , _val(val)
      , _total_cnt(total_cnt)
    { }

    virtual ~VeriVar() { clear(); }

    // accessors
    CString& name   ( void ) const { return _name;        }
    short inout     ( void ) const { return _inout;       }
    short net_type  ( void ) const { return _net_type;    }
    short is_signed ( void ) const { return _is_signed;   }
    short vec_scalar( void ) const { return _vec_scalar;  }
    short drive_from( void ) const { return _drive_from;  }
    short drive_to  ( void ) const { return _drive_to;    }
    int   dstart    ( void ) const { return _dstart;      }
    int   dend      ( void ) const { return _dend;        }
    int   dlen      ( void ) const { return _dlen;        }

    // depth starts from 1
    void  get_range ( unsigned int depth, int& start, int& end, int& bits );
    int   range_depth( void ) const { return _range.size(); }

    VeriNum * get_one_num( const IVec& dimen, int is_whole, int dstart, int dend );

    // mutators
    void set_name       ( CString& v ) { _name        = v; }
    void set_inout      ( short v    ) { _inout       = v; }
    void set_net_type   ( short v    ) { _net_type    = v; }
    void set_is_signed  ( short v    ) { _is_signed   = v; }
    void set_vec_scalar ( short v    ) { _vec_scalar  = v; }
    void set_drive_from ( short v    ) { _drive_from  = v; }
    void set_drive_to   ( short v    ) { _drive_to    = v; }

    int  set_one_num( const IVec& dimen, int is_whole, int dstart,
                      int dend, VeriNum *val );

    void add_range( int start, int end )
    {
      _range.push_back( SymbolRanges::value_type(
                              VeriSymbolRangeRec( start,
                                                  end,
                                                  abs(end - start) + 1 )
                                                )
                      );
    }

    virtual void clear( void );

    virtual void clean_up( void );

    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    VeriVar() {};

    int calc_dimen_cnt( int start_lvl );
    int determin_idx( const IVec& dimen );

  private:
    String _name;

    short  _inout;
    short  _net_type;

    short  _is_signed;
    short  _vec_scalar;

    short  _drive_from;
    short  _drive_to;

    int    _dstart;
    int    _dend;
    int    _dlen;

    SymbolRanges _range;

    VeriNode  *_decl;
    VeriNum  **_val;
    int        _total_cnt;
};

/*
 * class VeriTfVar :
 *     Verilog syntax analyzer true node : tf-variable
 */
class VeriTfVar : public VeriNode
{
  typedef std::string  String;
  typedef const String CString;

  public:
    VeriTfVar( Uint idx, int line, int col, CString& name, short inout,
               short net_type, short is_signed, VeriNode *decl, int dstart,
               int dend, VeriNum *val, VeriMod *mod )
      :  VeriNode( idx, vtTFVAR, vtTFVAR, line, col, mod )
      , _name(name)
      , _inout(inout)
      , _net_type(net_type)
      , _is_signed(is_signed)
      , _dstart(dstart)
      , _dend(dend)
      , _dlen(abs(dend-dstart)+1)
      , _decl(decl)
      , _val(val)
    {}

    virtual ~VeriTfVar() { clear(); }

    // accessors
    CString& name  ( void ) const { return _name;        }
    short inout    ( void ) const { return _inout;       }
    short net_type ( void ) const { return _net_type;    }
    short is_signed( void ) const { return _is_signed;   }
    VeriNum *get_val( void );

    int   dstart( void ) const { return _dstart; }
    int   dend  ( void ) const { return _dend;   }
    int   dlen  ( void ) const { return _dlen;   }
 
    // mutators
    void set_name     ( CString& v ) { _name      = v; }
    void set_inout    ( short v    ) { _inout     = v; }
    void set_net_type ( short v    ) { _net_type  = v; }
    void set_is_signed( short v    ) { _is_signed = v; }
    void set_dstart( int v ) { _dstart = v; }
    void set_dend( int v ) { _dend = v; }
    void set_dlen( int v ) { _dlen = v; }
    void set_val( VeriNum *val );

    virtual void clean_up( void );

    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    VeriTfVar() {};

  private:
    String _name;

    short  _inout;
    short  _net_type;
    short  _is_signed;

    int    _dstart;
    int    _dend;
    int    _dlen;

    VeriNode  *_decl;
    VeriNum   *_val;
};


/*
 * class VeriParam :
 *     Verilog syntax analyzer true node : parameter
 */
class VeriParam : public VeriNode
{
  typedef std::string  String;
  typedef const String CString;

  public:
    VeriParam( Uint idx, int line, int col, CString& name,
               short is_int, short is_local, short is_signed,
               VeriNode *decl, int dstart, int dend,
               VeriNum *val, VeriMod *mod )
      :  VeriNode( idx, vtPARAM, vtPARAM, line, col, mod )
      , _name(name)
      , _is_int(is_int)
      , _is_local(is_local)
      , _is_signed(is_signed)
      , _is_head_override( IS_FALSE)
      , _dstart(dstart)
      , _dend(dend)
      , _dlen(abs(dend-dstart)+1)
      , _decl(decl)
      , _val(val)
    {}

    VeriParam( Uint idx, int line, int col, CString& name,
               VeriNode *decl, int dstart, int dend,
               VeriNum *val, VeriMod *mod )
      :  VeriNode( idx, vtPARAM, vtPARAM, line, col, mod )
      , _name(name)
      , _is_int(IS_FALSE)
      , _is_local(IS_FALSE)
      , _is_signed(IS_FALSE)
      , _is_head_override(IS_TRUE)
      , _dstart(dstart)
      , _dend(dend)
      , _dlen(abs(dend-dstart)+1)
      , _decl(decl)
      , _val(val)
    {}

    virtual ~VeriParam() { clear(); }

    // accessors
    CString& name  ( void ) const { return _name;        }
    short is_int   ( void ) const { return _is_int;      }
    short is_local ( void ) const { return _is_local;    }
    short is_signed( void ) const { return _is_signed;   }
    short is_head_override( void ) const { return _is_head_override; }
    VeriNum *get_val( void );

    int   dstart( void ) const { return _dstart; }
    int   dend  ( void ) const { return _dend;   }
    int   dlen  ( void ) const { return _dlen;   }
 
    // mutators
    void set_name     ( CString& v ) { _name      = v; }
    void set_is_int   ( short v    ) { _is_int    = v; }
    void set_is_local ( short v    ) { _is_local  = v; }
    void set_is_signed( short v    ) { _is_signed = v; }
    void set_no_override( void ) { _is_head_override = IS_FALSE; }
    void set_dstart( int v ) { _dstart = v; }
    void set_dend( int v ) { _dend = v; }
    void set_dlen( int v ) { _dlen = v; }
    void set_val( VeriNum *val );

    virtual void clean_up( void );

    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    VeriParam() {};

  private:
    String _name;

    short  _is_int;
    short  _is_local;
    short  _is_signed;

    short  _is_head_override;

    int    _dstart;
    int    _dend;
    int    _dlen;

    VeriNode  *_decl;
    VeriNum   *_val;
};

/*
 * class VeriInt :
 *     Verilog syntax analyzer true node : integer
 */
class VeriInt : public VeriNode
{
  typedef std::string       String;
  typedef const String      CString;
  typedef std::vector<int > IVec;
  typedef std::vector< VeriSymbolRangeRec > SymbolRanges;

  public:
    VeriInt( Uint idx, int line, int col, CString& name,
             VeriNode *decl, VeriNum **val, int total_cnt, VeriMod *mod )
      :  VeriNode( idx, vtINT, vtINT, line, col, mod )
      , _name(name)
      , _decl(decl)
      , _val(val)
      , _total_cnt(total_cnt)
    {}

    virtual ~VeriInt() { clear(); }

    // accessors
    CString& name  ( void ) const { return _name; }
 
    // depth starts from 1
    void  get_range( unsigned int depth, int& start, int& end, int& bits );
    int   range_depth( void ) const { return _range.size(); }

    VeriNum * get_one_num( const IVec& dimen, int is_whole, int dstart, int dend );

    // mutators
    void set_name (CString& v) { _name = v; }

    void add_range( int start, int end )
    {
      _range.push_back( SymbolRanges::value_type(
                              VeriSymbolRangeRec( start,
                                                  end,
                                                  abs(end - start + 1) )
                                                )
                      );
    }

    int  set_one_num( const IVec& dimen, int is_whole, int dstart,
                      int dend, const VeriNum *val );

    virtual void clear( void );

    virtual void clean_up( void );

    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    VeriInt() {};

    int calc_dimen_cnt( int start_lvl );
    int determin_idx( const IVec& dimen );

  private:
    String _name;

    SymbolRanges _range;

    VeriNode  *_decl;
    VeriNum  **_val;
    int        _total_cnt;
};

/*
 * class VeriGenVar :
 *     Verilog syntax analyzer true node : genvar
 */
class VeriGenVar : public VeriNode
{
  typedef std::string  String;
  typedef const String CString;

  public:
    VeriGenVar( Uint idx, int line, int col, CString& name,
                VeriNode *decl, VeriNum *val, VeriMod *mod )
      :  VeriNode( idx, vtGENVAR, vtGENVAR, line, col, mod )
      , _name(name)
      , _decl(decl)
      , _val(val)
    {}

    virtual ~VeriGenVar() { clear(); }

    // accessors
    CString& name  ( void ) const { return _name;        }
    VeriNum *get_val ( void );

    // mutators
    void set_name( CString& v ) { _name = v; }
    void set_val( VeriNum *node );

    virtual void clean_up( void );

    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    VeriGenVar() {};

  private:
    String _name;

    VeriNode  *_decl;
    VeriNum   *_val;
};

class VeriLabelId : public VeriNode
{
  typedef std::string  String;
  typedef const String CString;

  public:
    VeriLabelId( Uint idx, int line, int col, CString& name,
               VeriNode *block, VeriMod *mod )
      :  VeriNode( idx, vtLABEL, vtLABEL, line, col, mod )
      , _name( name )
      , _label_block( block )
    {}

    virtual ~VeriLabelId() { clear(); }

    // accessors
    CString  & name ( void ) const { return _name;   }
    VeriNode * label( void ) { return _label_block;  }

    // mutators
    void set_name ( CString & v ) { _name = v; }
    void set_block( VeriNode *v ) { _label_block = v; }

    virtual void clean_up( void );

    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    VeriLabelId() {};

  private:
    String    _name;
    VeriNode *_label_block;
};

};
#endif // PARSER_VERILOG_VERI_NODE_HPP_
