/* 
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : veri_node.hpp
 * Summary  : verilog syntax analyzer true node class : declaration
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2013.1.28
 */

#ifndef PARSER_VERILOG_VERI_DECL_HPP_
#define PARSER_VERILOG_VERI_DECL_HPP_

#include "veri_node.hpp"
#include "veri_stmt.hpp"

#include <vector>
#include <string>

namespace rtl
{

class VeriExp;
class VeriVar;
class VeriNum;
class VeriVar;
class VeriTfVar;
class VeriParam;
class VeriGenVar;
class VeriInt;
class VeriFunc;

/*
 * class VeriDecl :
 *     Verilog syntax analyzer true node : declaration
 */
class VeriDecl : public VeriNode
{
  public:
    VeriDecl( Uint idx, int line, int col, int decl_type,
              VeriMod *mod )
      :  VeriNode( idx, vtDECL, decl_type, line, col, mod )
      , _decl_status( vSTART_S )
    {}

    virtual ~VeriDecl() {};

    int decl_status( void ) { return _decl_status; }
    void set_decl_status( int s ) { _decl_status = s; }

    virtual void clean_up( void ) = 0;
    virtual int  check_child_validation( void ) = 0;

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue ) = 0;
    virtual int  pretty_dot_label( Ofstream& ofile, int depth ) = 0;

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info ) = 0;
    virtual int  push_for_replacement( NodeVec& mstack ) = 0;
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl ) = 0;

  protected:
    VeriDecl( Uint idx, const VeriDecl *decl_copy )
      :  VeriNode( idx, decl_copy )
    {}

  private:
    int   _decl_status;
};

class VeriTfDecl : public VeriDecl
{
  public:
    VeriTfDecl( Uint idx, int line, int col, int inout, int net_type,
                int is_signed, VeriNode *range, VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_TF_DECL, mod )
      , _net_type( net_type )
      , _inout( inout )
      , _is_signed( is_signed )
      , _range( range )
    {}

    virtual ~VeriTfDecl() { clear(); }

    // accessors
    int net_type ( void ) { return _net_type;  }
    int inout    ( void ) { return _inout;     }
    int is_signed( void ) { return _is_signed; }
    VeriNode * range( void ) { return _range;  }
    NodeList & port_ids( void ) { return _port_ids; }

    // mutators
    void append_port( VeriNode *node ) { _port_ids.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_children( NodeVec& mstack );
    int declare_tf_symbol( void );

    VeriTfVar * reg_tfvar_symbol( int line, int col, CString& name,
                              int dstart, int dend, VeriNode *var );

  protected:
    VeriTfDecl( Uint idx, const VeriTfDecl *copy )
      :  VeriDecl( idx, copy )
      , _net_type( copy->_net_type )
      , _inout( copy->_inout )
      , _is_signed( copy->_is_signed )
      , _range( NULL )
    {}

  private:
    int       _net_type;
    int       _inout;
    int       _is_signed;
    VeriNode *_range;
    NodeList  _port_ids;
};

class VeriTaskPortList : public VeriDecl
{
  public:
    VeriTaskPortList( Uint idx, int line, int col,
             VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_TASK_PORT_LIST, mod )
    {}

    virtual ~VeriTaskPortList() { clear(); }

    // accessors
    NodeList & task_port( void ) { return _task_port; }

    // mutators
    void append_port( VeriNode * node ) { _task_port.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriTaskPortList( Uint idx, const VeriTaskPortList *copy )
      : VeriDecl( idx, copy )
    {}

  private:
    NodeList _task_port;
};

class VeriFuncPortList : public VeriDecl
{
  public:
    VeriFuncPortList( Uint idx, int line, int col,
             VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_FUNC_PORT_LIST, mod )
    {}

    virtual ~VeriFuncPortList() { clear(); }

    // accessors
    NodeList & func_port( void ) { return _func_port; }

    // mutators
    void append_port( VeriNode * node ) { _func_port.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriFuncPortList( Uint idx, const VeriFuncPortList *copy )
      : VeriDecl( idx, copy )
    {}

  private:
    NodeList _func_port;
};

class VeriTaskItemDeclList : public VeriDecl
{
  public:
    VeriTaskItemDeclList( Uint idx, int line, int col,
             VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_TASK_ITEM_DECL_LIST, mod )
    {}

    virtual ~VeriTaskItemDeclList() { clear(); }

    // accessors
    NodeList & task_item( void ) { return _task_item; }

    // mutators
    void append_item( VeriNode * node ) { _task_item.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriTaskItemDeclList( Uint idx, const VeriTaskItemDeclList *copy )
      : VeriDecl( idx, copy )
    {}

  private:
    NodeList _task_item;
};

class VeriBlockItemDeclList : public VeriDecl
{
  public:
    VeriBlockItemDeclList( Uint idx, int line, int col,
             VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_BLOCK_ITEM_DECL_LIST, mod )
    {}

    virtual ~VeriBlockItemDeclList() { clear(); }

    // accessors
    NodeList & block_item( void ) { return _block_item; }

    // mutators
    void append_item( VeriNode * node ) { _block_item.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriBlockItemDeclList( Uint idx, const VeriBlockItemDeclList *copy )
      : VeriDecl( idx, copy )
    {}

  private:
    NodeList _block_item;
};

class VeriFuncItemDeclList : public VeriDecl
{
  public:
    VeriFuncItemDeclList( Uint idx, int line, int col,
             VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_FUNC_ITEM_DECL_LIST, mod )
    {}

    virtual ~VeriFuncItemDeclList() { clear(); }

    // accessors
    NodeList & func_item( void ) { return _func_item; }

    // mutators
    void append_item( VeriNode * node ) { _func_item.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriFuncItemDeclList( Uint idx, const VeriFuncItemDeclList *copy )
      : VeriDecl( idx, copy )
    {}

  private:
    NodeList _func_item;
};

class VeriTaskDecl : public VeriDecl
{
  typedef std::vector<int> IVec;

  public:
    VeriTaskDecl( Uint idx, int line, int col, CString& task_name,
                  int is_auto, VeriNode *task_port, VeriNode *item_decl,
                  VeriNode *stmt,
                       VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_TASK_DECL, mod )
      , _task_name( task_name )
      , _is_auto( is_auto )
      , _task_port( task_port )
      , _item_decl( item_decl )
      , _stmt( stmt )
      , _act_port_num( 0 )
    {}

    virtual ~VeriTaskDecl() { clear(); }

    // accessors
    CString& task_name( void ) { return _task_name; }
    int      is_auto  ( void ) { return _is_auto; }

    VeriNode *task_port( void ) { return _task_port; }
    VeriNode *item_decl( void ) { return _item_decl; }
    VeriNode *stmt     ( void ) { return _stmt; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    virtual int  search_for_symbol( NodeVec& seq, String& help_info );

  protected:
    int reg_task_symbol( void );

    int push_ports( NodeVec& mstack );

    int get_call_exp( void );
    int prep_ports_for_check_type( void );
    int check_type_ref( VeriExp *exp, VeriNode *port, int no_assign );

    int push_contents( NodeVec& mstack );
    
    int finish_scope( void );

  protected:
    VeriTaskDecl( Uint idx, const VeriTaskDecl *copy )
      :  VeriDecl( idx, copy )
      , _task_name( copy->_task_name )
      , _is_auto( copy->_is_auto )
      , _task_port( NULL )
      , _item_decl( NULL )
      , _stmt( NULL )
      , _act_port_num( 0 )
    {}

  private:
    String    _task_name;
    int       _is_auto;
    VeriNode *_task_port;
    VeriNode *_item_decl;
    VeriNode *_stmt;

    NodeList  _act_ports;
    int       _act_port_num;
};

/*
 * range cannot co-exist with integer
 */
class VeriFuncDecl : public VeriDecl
{
  typedef std::vector<int> IVec;

  public:
    VeriFuncDecl( Uint idx, int line, int col, int is_auto, int is_signed,
                  int is_int, VeriNode *range, CString& func_name,
                  VeriNode *port_list, VeriNode *item_decl, VeriNode *stmt,
                  VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_FUNC_DECL, mod )
      , _is_auto( is_auto )
      , _is_signed( is_signed )
      , _is_int( is_int )
      , _range( range )
      , _func_name( func_name )
      , _port_list( port_list )
      , _item_decl( item_decl )
      , _stmt( stmt )
      , _ret( NULL )
      , _act_port_num( 0 )
    {}

    virtual ~VeriFuncDecl() { clear(); }

    // accessors
    CString&  func_name( void ) { return _func_name; }
    int       is_auto  ( void ) { return _is_auto;   }
    int       is_signed( void ) { return _is_signed; }
    int       is_int   ( void ) { return _is_int;    }

    VeriNode *range    ( void ) { return _range;     }
    VeriNode *port_list( void ) { return _port_list; }
    VeriNode *item_decl( void ) { return _item_decl; }
    VeriNode *stmt     ( void ) { return _stmt; }
    VeriFunc *ret_value( void ) { return _ret; }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    virtual int  search_for_symbol( NodeVec& seq, String& help_info );

  protected:
    int reg_func_symbol( void );

    int push_ports( NodeVec& mstack );

    int get_call_exp( void );
    int prep_ports_for_check_type( void );
    int check_type_ref( VeriExp *exp, VeriNode *port, int no_assign );
    
    int push_contents( NodeVec& mstack );

    int collect_ret_value( void );
    int finish_scope( void );

  protected:
    VeriFuncDecl( Uint idx, const VeriFuncDecl *copy )
      :  VeriDecl( idx, copy )
      , _is_auto( copy->_is_auto )
      , _is_signed( copy->_is_signed )
      , _is_int( copy->_is_int )
      , _range( NULL )
      , _func_name( copy->_func_name )
      , _port_list( NULL )
      , _item_decl( NULL )
      , _stmt( NULL )
      , _ret( NULL )
      , _act_port_num( 0 )
    {}

  private:
    int       _is_auto;
    int       _is_signed;
    int       _is_int;
    VeriNode *_range;
    String    _func_name;
    VeriNode *_port_list;
    VeriNode *_item_decl;
    VeriNode *_stmt;

    VeriFunc *_ret;

    NodeList  _act_ports;
    int       _act_port_num;
};

class VeriVarDecl : public VeriDecl
{
  public:
    VeriVarDecl( Uint idx, int line, int col, short inout, short net_type,
                 short is_signed, short vector_scalar, VeriNode *drive,
                 VeriNode *range, VeriNode *delay, VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_VAR, mod )
      , _inout( inout )
      , _net_type( net_type )
      , _is_signed( is_signed )
      , _vec_scalar( vector_scalar )
      , _drive( drive )
      , _range( range )
      , _delay( delay )
    {}

    virtual ~VeriVarDecl() { clear(); }

    // accessors
    short inout        ( void ) { return _inout; }
    short net_type     ( void ) { return _net_type; }
    short is_signed    ( void ) { return _is_signed; }
    short vector_scalar( void ) { return _vec_scalar; }
    VeriNode* drive( void ) { return _drive; }
    VeriNode* range( void ) { return _range; }
    VeriNode* delay( void ) { return _delay; }
    NodeList& vars( void ) { return _vars; }

    // mutators
    void append_var( VeriNode *node ) { _vars.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int    check_last_child_validation( void );

  protected:
    VeriVar * reg_var_symbol( int line, int col, CString& name,
                              short drive_from, short drive_to,
                              int dstart, int dend,
                              IList& range_from, IList& range_to,
                              VeriNode *var, VeriNum **init, int total_cnt );

    int push_children( NodeVec& mstack );

    int declare_var_symbols( void );
    int do_nothing( void );

  protected:
    VeriVarDecl( Uint idx, const VeriVarDecl *copy )
      :  VeriDecl( idx, copy )
      , _inout( copy->_inout )
      , _net_type( copy->_net_type )
      , _is_signed( copy->_is_signed )
      , _vec_scalar( copy->_vec_scalar )
      , _drive( NULL )
      , _range( NULL )
      , _delay( NULL )
    {}

  private:
    short     _inout;
    short     _net_type;
    short     _is_signed;
    short     _vec_scalar;
    VeriNode *_drive;
    VeriNode *_range;
    VeriNode *_delay;
    NodeList  _vars;
};

class VeriParamDecl : public VeriDecl
{
  public:
    VeriParamDecl( Uint idx, int line, int col, short is_signed, short is_int,
                   short is_local, VeriNode *range, VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_PARAM, mod )
      , _is_signed( is_signed )
      , _is_int( is_int )
      , _is_local( is_local )
      , _range( range )
    {}

    virtual ~VeriParamDecl() { clear(); }

    // accessors
    short is_signed( void ) { return _is_signed; }
    short is_local ( void ) { return _is_local; }
    short is_int   ( void ) { return _is_int; }
    VeriNode* range( void ) { return _range; }
    NodeList& vars ( void ) { return _vars; }

    // mutators
    void append_var( VeriNode *node ) { _vars.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int    check_last_child_validation( void );

  protected:
    VeriParam * reg_param_symbol( int line, int col, VeriNode *decl,
                                  CString& name, int dstart, int dend,
                                  VeriNum  *init );

    int push_children( NodeVec& mstack );

    int declare_param_symbols( void );
    int do_nothing( void );

  protected:
    VeriParamDecl( Uint idx, const VeriParamDecl *copy )
      :  VeriDecl( idx, copy )
      , _is_signed( copy->_is_signed )
      , _is_int( copy->_is_int )
      , _is_local( copy->_is_local )
      , _range( NULL )
    {}

  private:
    short     _is_signed;
    short     _is_int;
    short     _is_local;
    VeriNode *_range;
    NodeList  _vars;
};

class VeriIntDecl : public VeriDecl
{
  public:
    VeriIntDecl( Uint idx, int line, int col, VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_INT, mod )
    {}

    virtual ~VeriIntDecl() { clear(); }

    // accessors
    NodeList& vars ( void ) { return _vars; }

    // mutators
    void append_var( VeriNode *node ) { _vars.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int    check_last_child_validation( void );

  protected:
    VeriInt * reg_int_symbol( int line, int col, VeriNode *decl,
                              CString& name, IList& range_from,
                              IList& range_to, VeriNum **init,
                              int total_cnt );

    int push_children( NodeVec& mstack );

    int declare_int_symbols( void );
    int do_nothing( void );

  protected:
    VeriIntDecl( Uint idx, const VeriIntDecl *copy )
      :  VeriDecl( idx, copy )
    {}

  private:
    NodeList  _vars;
};

class VeriGenVarDecl : public VeriDecl
{
  public:
    VeriGenVarDecl( Uint idx, int line, int col, VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_GENVAR, mod )
    {}

    virtual ~VeriGenVarDecl() { clear(); }

    // accessors
    NodeList& vars ( void ) { return _vars; }

    // mutators
    void append_var( VeriNode *node ) { _vars.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int    check_last_child_validation( void );

  protected:
    VeriGenVar * reg_genvar_symbol( int line, int col, VeriNode *decl,
                                    CString& name, VeriNum *init );

    int push_children( NodeVec& mstack );

    int declare_genvar_symbols( void );
    int do_nothing( void );

  protected:
    VeriGenVarDecl( Uint idx, const VeriGenVarDecl *copy )
      :  VeriDecl( idx, copy )
    {}

  private:
    NodeList  _vars;
};

class VeriParamDeclList : public VeriDecl
{
  public:
    VeriParamDeclList( Uint idx, int line, int col,
             VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_PARAM_DECL_LIST, mod )
    {}

    virtual ~VeriParamDeclList() { clear(); }

    // accessors
    NodeList & param_list( void ) { return _param_list; }

    // mutators
    void append_param( VeriNode * node ) { _param_list.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriParamDeclList( Uint idx, const VeriParamDeclList *copy )
      :  VeriDecl( idx, copy )
    {}

  private:
    NodeList _param_list;
};

class VeriPortDeclList : public VeriDecl
{
  public:
    VeriPortDeclList( Uint idx, int line, int col, VeriMod *mod )
      :  VeriDecl( idx, line, col, vsDECL_PORT_DECL_LIST, mod )
    {}

    virtual ~VeriPortDeclList() { clear(); }

    // accessors
    NodeList & port_list( void ) { return _port_list; }

    // mutators
    void append_port( VeriNode * node ) { _port_list.push_back( node ); }

    virtual void clean_up( void );
    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

    int check_last_child_validation( void );

  protected:
    int push_children( NodeVec& mstack );
    int do_nothing( void );

  protected:
    VeriPortDeclList( Uint idx, const VeriPortDeclList *copy )
      :  VeriDecl( idx, copy )
    {}

  private:
    NodeList _port_list;
};

};
#endif // PARSER_VERILOG_VERI_DECL_HPP_
