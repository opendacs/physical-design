/*
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : veri_module.hpp
 * Summary  : verilog syntax analyzer :: module
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2012.12.20
 */

#ifndef PARSER_VERILOG_VERI_MODULE_
#define PARSER_VERILOG_VERI_MODULE_

#include <cstdlib>
#include <string>
#include <list>
#include <fstream>
#include <map>

#include "veri_node.hpp"
#include "veri_symbol_table.hpp"

namespace rtl
{

class VeriExp;
class VeriDecl;
class VeriGateInstantiation;
class VeriModInstantiation;

/*
 * class VeriMod :
 *     Verilog syntax-analyzer Module class.
 *
 *     call init before using.
 */
// I/P: VeriNode* , M: VeriMod*
#define FOREACH_ITEMS_OF_MODULE(I, M) for(auto (I) : ((rtl::VeriModuleItemList*)((M)->item()))->item_list())
#define FOREACH_PORTS_OF_MODULE(P, M) for(auto (P) : ((rtl::VeriMultiExp*)((M)->port()))->exp())
#define FOREACH_INSTS_OF_MODULE(I, M) for(auto (I) : ((M)->modInstantiated()))

class VeriMod : public VeriNode
{
  typedef VeriSymbolTable                SymbolTable;
  typedef std::list< String >            SList;
  typedef std::map< String, VeriNode * > NodeMap;
  typedef std::vector<int>               IVec;

  public:
    VeriMod( Uint idx )
      :  VeriNode( idx, vtMOD, vtMOD, 0, 0, this )
      , _mod_name("")
      , _param(NULL)
      , _port(NULL)
      , _item(NULL)
      , _act_port_num( 0 )
      , _symbols(NULL)
    {}

    ~VeriMod( void ) { clear(); };
 
    void init( int symbol_table_size );

    // assessors
    CString& name ( void ) const { return _mod_name; }
    const SList& fname( void ) const { return _fname; }
    const IList& fline( void ) const { return _fline; }
    VeriNode * param( void ) { return _param; }
    VeriNode * port( void ) { return _port; }
    VeriNode * item( void ) { return _item; }
    const NodeList& modInstantiated( void ) { return _modInstantiated; }

    // mutators for lists to be output to dfg
    void set_name ( CString&  name  ) { _mod_name = name; }
    void set_fname( const SList& fnames, const IList& flines  );
    void set_param( VeriNode *param ) { _param = param; }
    void set_port ( VeriNode *port  ) { _port  = port;  }
    void set_item ( VeriNode *item  ) { _item  = item;  }
    void addModInstantiated ( void );

    SymbolTable *symbol_table( void ) { return _symbols; }

    // clear inner data, back to status after initialization
    virtual void clear( void );

    // clear inner data, back to status after construction
    virtual void clean_up( void );

    virtual int  check_child_validation( void );

    virtual int  push_dot_children( NodeQueue& mqueue, IQueue& iqueue );
    virtual int  pretty_dot_label( Ofstream& ofile, int depth );

    // 2nd round parse
    virtual int  process_node_2nd( NodeVec& mstack, String& help_info );
    virtual int  push_for_replacement( NodeVec& mstack );
    virtual int  replace_node( Uint idx, NodeList& node_list,
                               NodeList::iterator& inl );

  protected:
    int push_everything( NodeVec& mstack );

    int prep_ports_for_map( void );
    int check_type_ref( VeriExp *exp, VeriVar *port );
    int map_ports( void );

    int finish_scope( void );

  protected:
    VeriMod( Uint idx, const VeriMod *copy )
      :  VeriNode( idx, copy )
      , _mod_name( copy->_mod_name )
      , _fname( copy->_fname.begin(), copy->_fname.end() )
      , _fline( copy->_fline.begin(), copy->_fline.end() )
      , _param( NULL )
      , _port( NULL )
      , _item( NULL )
      , _act_port_num( 0 )
      , _symbols( NULL )
    {
    }

  private:
    String       _mod_name;
    SList        _fname;
    IList        _fline;

    // syntax tree
    VeriNode    *_param;
    VeriNode    *_port;
    VeriNode    *_item;
    NodeList    _modInstantiated;

    NodeList  _act_ports;
    int       _act_port_num;

    // variable container
    SymbolTable *_symbols;
};

};

#endif // PARSER_VERILOG_VERI_MODULE_
