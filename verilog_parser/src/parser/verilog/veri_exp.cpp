/*
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : veri_exp.cpp
 * Summary  : verilog syntax analyzer :: expression
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2012.11.20
 */

#include <assert.h>

#include "general_err.hpp"
#include "veri_err_token.hpp"

#include "veri_symbol_table.hpp"

#include "veri_num.hpp"
#include "veri_tfmg.hpp"

#include "veri_exp.hpp"
#include "veri_decl.hpp"

#include "veri_module.hpp"
#include "veri_parser.hpp"

namespace rtl
{

int
VeriExp::check_primary_not_other( VeriPrimaryExp *pexp )
{
  if( pexp->var() == NULL )
    return OK;

  switch( pexp->var()->tok_type() )
  {
    case vtVAR :
      if( static_cast<VeriVar *>(pexp->var())->range_depth() != 0 )
      {
        veri_err->append_err( ERR_ERROR, pexp, vSEC_PRI_NOT_OTHER );
        return vSEC_PRI_NOT_OTHER;
      }
      return OK;
      break;

    case vtTFVAR :
    case vtPARAM :
    case vtGENVAR :
    case vtFUNC :
      return OK; break;

    case vtINT :
      if( static_cast<VeriInt *>(pexp->var())->range_depth() != 0 )
      {
        veri_err->append_err( ERR_ERROR, pexp, vSEC_PRI_NOT_OTHER );
        return vSEC_PRI_NOT_OTHER;
      }
      return OK;
      break;

      return OK; break;

    default:
      veri_err->append_err( ERR_ERROR, pexp, vSEC_PRI_NOT_OTHER );
      return vSEC_PRI_NOT_OTHER;
  }
}

    /*************************
     * Dummy-Port Expression *
     *************************/

void
VeriDummyPort::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );
}

int
VeriDummyPort::push_dot_children( NodeQueue& /*mqueue*/, IQueue& /*iqueue*/ )
{
  return OK;
}

int
VeriDummyPort::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection

  return OK;
}

int
VeriDummyPort::check_child_validation( void )
{
  return OK;
}

int
VeriDummyPort::process_node_2nd( NodeVec& /*mstack*/, String& /*help_info*/ )
{
  return OK;
}

int
VeriDummyPort::push_for_replacement( NodeVec& /*mstack*/ )
{
  return OK;
}

int
VeriDummyPort::replace_node( Uint idx, NodeList& /*node_list*/,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriDummyPort *tmp = new VeriDummyPort( idx, this );

  // begin replacement

  *inl = tmp;
  return OK;
}

    /*****************************
     * Port-Reference Expression *
     *****************************/

void
VeriPortRefExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );
}

int
VeriPortRefExp::push_dot_children( NodeQueue& /*mqueue*/, IQueue& /*iqueue*/ )
{
  return vNOT_KNOWN_ERR;
}

int
VeriPortRefExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_name( _port_ref );
  ofile << dot_str_tail( tok_type() );

  // connection

  return vNOT_KNOWN_ERR;
}

int
VeriPortRefExp::check_child_validation( void )
{
  return vNOT_KNOWN_ERR;
}

int
VeriPortRefExp::process_node_2nd( NodeVec& /*mstack*/, String& /*help_info*/ )
{
  return vNOT_KNOWN_ERR;
}

int
VeriPortRefExp::push_for_replacement( NodeVec& /*mstack*/ )
{
  return vNOT_KNOWN_ERR;
}

int
VeriPortRefExp::replace_node( Uint idx, NodeList& /*node_list*/,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriPortRefExp *tmp = new VeriPortRefExp( idx, this );

  // begin replacement

  *inl = tmp;
  return OK;
}

    /******************************
     * Named-Parameter Expression *
     ******************************/

void
VeriNamedParamExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );
}

int
VeriNamedParamExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;
  mqueue.push( _port_exp );
  iqueue.push( depth );

  if( calc_value() != NULL )
  {
    mqueue.push( calc_value() );
    iqueue.push( depth );
  }

  return OK;
}

int
VeriNamedParamExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_var_conn( _ref, _port_ref );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _port_exp );

  if( _port_ref != NULL )
    ofile << dot_str_symbol_conn( idx(), _port_ref );

  return OK;
}

int
VeriNamedParamExp::check_child_validation( void )
{
  if( _port_exp == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_NAMED_PARAM_EXP );
    return vCHILD_TYPE_NAMED_PARAM_EXP;
  }
   
  if( _port_exp->tok_type() != vtEXP )
  {
    veri_err->append_err( ERR_ERROR, _port_exp->line(),
                         _port_exp->col(), vCHILD_TYPE_NAMED_PARAM_EXP );
    return vCHILD_TYPE_NAMED_PARAM_EXP;
  }

  return OK;
}

int
VeriNamedParamExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return do_nothing( );
      break;
  }
}

int
VeriNamedParamExp::push_children( NodeVec& mstack )
{
  if( _port_exp != NULL )
  {
    //_port_exp->clean_up( );
    _port_exp->set_is_decl( is_decl() );
    _port_exp->set_is_trash( is_trash() );
    _port_exp->set_scope( scope() );
    mstack.push_back( _port_exp );
  }

  return OK;
}

int
VeriNamedParamExp::do_nothing( void )
{
  return OK;
}

int
VeriNamedParamExp::push_for_replacement( NodeVec& mstack )
{
  if( _port_exp != NULL )
    mstack.push_back( _port_exp );
  return OK;
}

int
VeriNamedParamExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriNamedParamExp *tmp = new VeriNamedParamExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl; 
  if( _port_exp != NULL )
  {
    tmp->_port_exp = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
  }

  *inl = tmp;
  return OK;
}

    /**********************
     * Primary Expression *
     **********************/

void
VeriPrimaryExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );
}

int
VeriPrimaryExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  if( calc_value() != NULL )
  {
    mqueue.push( calc_value() );
    iqueue.push( iqueue.front() + 1 );
  }
  return OK;
}

int
VeriPrimaryExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_primary_type( _pri_type );
  ofile << dot_str_name( _str );
  ofile << dot_str_tail( tok_type() );

  // connection
  if( calc_value() != NULL )
    ofile << dot_str_conn( idx(), calc_value() );

  if( _var != NULL )
    ofile << dot_str_symbol_conn( idx(), _var );

  return OK;
}

int
VeriPrimaryExp::check_child_validation( void )
{
  return OK;
}

int
VeriPrimaryExp::process_node_2nd( NodeVec& /*mstack*/, String& help_info )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = process_id( help_info );
      if( re == vSEARCH_DECL )
        set_pstatus( vSEARCH_DECL_S );
      else
        set_pstatus( vEND_S );
      return re;
      break;

    case vSEARCH_DECL_S :
      re = process_id( help_info );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return OK;
      break;
  }
}

int
VeriPrimaryExp::process_id( String& help_info )
{
  int re = OK;
  IVec dummy_dimen;
  switch( _pri_type )
  {
    case veVAR :
      if( is_decl() == IS_FALSE )
      {
        _var = scope()->value_within_module( _str );
        if( _var == NULL )
        {
          if( pstatus() == vSTART_S )
          {
            help_info = _str;
            return vSEARCH_DECL;
          }
          else
          {
            veri_err->append_err( ERR_ERROR, this, vSEC_SYMBOL_NOT_FOUND );
            return vSEC_SYMBOL_NOT_FOUND;
          }
        }
        else
        {
          switch( _var->tok_type( ) )
          {
            case vtPARAM  : case vtVAR : case vtTFVAR : case vtINT :
            case vtGENVAR : case vtFUNC :
              break;

            default:
              _var = NULL;
              veri_err->append_err( ERR_ERROR, this, vSEC_SYMBOL_MIS_MATCH );
              return vSEC_SYMBOL_MIS_MATCH;
              break;
          }
        }

        VeriNum *val = NULL;
        switch( _var->tok_type() )
        {
          case vtPARAM :
          {
            VeriParam *param = static_cast<VeriParam *>(_var);
            val = param->get_val();
            break;
          }

          case vtTFVAR :
          {
            VeriTfVar *tfvar = static_cast<VeriTfVar *>(_var);
            val = tfvar->get_val();
            break;
          }

          case vtVAR :
//            val = (static_cast<VeriVar *>(_var))->get_one_num( dummy_dimen,
//                                                               IS_TRUE, 0, 0 );
            break;

          case vtINT :
            val = (static_cast<VeriInt *>(_var))->get_one_num( dummy_dimen,
                                                               IS_TRUE, 0, 0 );
            break;

          case vtGENVAR :
            val = (static_cast<VeriGenVar *>(_var))->get_val();
            break;

          case vtFUNC :
            val = (static_cast<VeriFunc *>(_var))->get_val();
            break;

          default:
            break;
        }

        if( val == NULL )
          return OK;

        set_calc_value( val );
        val->set_father( this );
        val->set_scope( scope( ) );
        return OK;
      }
      break;
      
    case veSTR:
      break;

    case veNUM:
    {
      VeriNum *num = new VeriNum( veri_parser->next_id(),
                                  line(), col(), birth_mod() );
      veri_parser->insert_node( num );
      num->set_father( this );
      num->set_scope( scope( ) );
      re = num->parse( _str.c_str(), _str.size() );

      if( is_signed() == IS_TRUE )
        num->set_signed( ); // ?

      if( re != OK )
      {
        veri_err->append_err( ERR_ERROR, line(), col(), re );
        veri_parser->toss_node( num->idx() );
        return re;
      }
      set_calc_value( num );
      break;
    }

    default:
      break;
  }

  return OK;
}

int
VeriPrimaryExp::push_for_replacement( NodeVec& /*mstack*/ )
{
  return OK;
}

int
VeriPrimaryExp::replace_node( Uint idx, NodeList& /*node_list*/,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriPrimaryExp *tmp = new VeriPrimaryExp( idx, this );

  // begin replacement
  *inl = tmp;
  return OK;
}

    /********************
     * Unary Expression *
     ********************/

void
VeriUnaryExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );
}

int
VeriUnaryExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;
  mqueue.push( _exp );
  iqueue.push( depth );

  if( calc_value() != NULL )
  {
    mqueue.push( calc_value() );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriUnaryExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_op( _op );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _exp );

  if( calc_value() != NULL )
    ofile << dot_str_conn( idx(), calc_value() );

  return OK;
}

int
VeriUnaryExp::check_child_validation( void )
{
  if( _exp == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_UNA_EXP );
    return vCHILD_TYPE_UNA_EXP;
  }
   
  if( _exp->tok_type() == vtEXP )
  {
    switch( _exp->detail_type() )
    {
      case veEXP_EVENT   :
      case veEXP_ASSIGN  :
      case veEXP_RANGE   :
      case veEXP_PORT_REF :
      case veEXP_NAMED_PARAM :
      case veEXP_DUMMY_PORT :
        veri_err->append_err( ERR_ERROR, _exp->line(),
                              _exp->col(), vCHILD_TYPE_UNA_EXP );
        return vCHILD_TYPE_UNA_EXP;

      case veEXP_CALLER  :
      {
        /* task caller shall not be here */;
        VeriCallerExp *cexp = static_cast<VeriCallerExp *>(_exp);
        if( cexp->call_type() == vtTASK_CALL )
        {
          veri_err->append_err( ERR_ERROR, _exp->line(),
                              _exp->col(), vCHILD_TYPE_UNA_EXP );
          return vCHILD_TYPE_UNA_EXP;
        }
        else
          return OK;
        break;
      }

      case veEXP_MINTY :
      {
        veri_err->append_err( ERR_ERROR, _exp->line(),
                            _exp->col(), vCHILD_TYPE_UNA_EXP );
        return vCHILD_TYPE_UNA_EXP;
        break;
      }

      case veEXP_MULTI :
      {
        VeriNode *save_exp = _exp;
        VeriMultiExp *mexp = static_cast<VeriMultiExp *>(_exp);
        switch( mexp->multi_type() )
        {
          case vemCONCAT_EXP :
            return OK;

          case vemCOMMA_EXP :
          {
            _exp = mexp->exp().back();
            int re = check_child_validation();
            _exp = save_exp;
            return re;
            break;
          }

          case vemEVENT_EXP :
          case vemPORT_EXP :
          case vemPORT_INST_EXP :
          default :
            return vCHILD_TYPE_UNA_EXP;
        }
        break;
      }

      default:
        return OK;
    }
  }
  veri_err->append_err( ERR_ERROR, line(),
                        col(), vCHILD_TYPE_UNA_EXP );
  return vCHILD_TYPE_UNA_EXP;
}

int
VeriUnaryExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return process_unary_op( );
      break;
  }
}

int
VeriUnaryExp::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  //_exp->clean_up( );
  _exp->set_is_trash( is_trash() );
  _exp->set_scope( scope() );
  static_cast<VeriExp *>(_exp)->set_signed( is_signed() );
  mstack.push_back( _exp );

  return OK;
}

int
VeriUnaryExp::process_unary_op( void )
{
  VeriExp * exp = static_cast<VeriExp *>(_exp);

  int re = OK;
  if( exp->detail_type( ) == veEXP_PRI )
  {
    re = check_primary_not_other( static_cast<VeriPrimaryExp *>(exp) );
    if( re != OK )
      return re;
  }

  if( exp->calc_value() != NULL )
  {
    VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value() );
    veri_parser->insert_node( num );
    num->set_father( this );
    num->set_scope( scope( ) );

    int re = num->unary_operation( _op );
    if( re != OK )
    {
      veri_err->append_err( ERR_ERROR, _exp, re );
      veri_parser->toss_node( num->idx() );
      return re;
    }
    set_calc_value( num );
  }
  return OK;
}

int
VeriUnaryExp::push_for_replacement( NodeVec& mstack )
{
  mstack.push_back( _exp );
  return OK;
}

int
VeriUnaryExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriUnaryExp *tmp = new VeriUnaryExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl; 
  tmp->_exp = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

    /********************
     * Event Expression *
     ********************/

void
VeriEventExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx( ) );
  set_calc_value( NULL );
}

int
VeriEventExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;
  if( _exp != NULL )
  {
    mqueue.push( _exp );
    iqueue.push( depth );
  }
  if( calc_value() != NULL )
  {
    mqueue.push( calc_value() );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriEventExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_op( _etype );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _exp );

  return OK;
}

int
VeriEventExp::check_child_validation( void )
{
  if( _exp == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_EVENT_EXP );
    return vCHILD_TYPE_EVENT_EXP;
  }

  if( _exp->detail_type() == veEXP_PRI )
  {
    VeriPrimaryExp *pexp = static_cast<VeriPrimaryExp *>(_exp);
    if( pexp->pri_type() == veVAR )
      return OK;
  }
  else if( _exp->detail_type() == veEXP_UNA )
  {
    VeriUnaryExp *una = static_cast<VeriUnaryExp *>(_exp);
    if( una->exp_child( )->detail_type( ) == veEXP_PRI )
    {
      VeriPrimaryExp *pexp = static_cast<VeriPrimaryExp *>(una->exp_child());
      if( pexp->pri_type() == veVAR )
        return OK;
    }
  }
  veri_err->append_err( ERR_ERROR, _exp->line(),
                        _exp->col(), vCHILD_TYPE_EVENT_EXP );
  return vCHILD_TYPE_EVENT_EXP;
}

int
VeriEventExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return check_sensitivity_list( );
      break;
  }
}

int
VeriEventExp::push_children( NodeVec& mstack )
{
  if( _exp != NULL )
  {
    //_exp->clean_up( );
    _exp->set_scope( scope() );
    _exp->set_is_trash( is_trash() );
    mstack.push_back( _exp );
  }
  return OK;
}

int
VeriEventExp::check_sensitivity_list( void )
{
  /* TODO */
  return OK;
}

int
VeriEventExp::push_for_replacement( NodeVec& mstack )
{
  mstack.push_back( _exp );
  return OK;
}

int
VeriEventExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriEventExp *tmp = new VeriEventExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl; 
  tmp->_exp = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

    /********************
     * Array Expression *
     ********************/

void
VeriArrayExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );

  _range_from.clear();
  _range_to.clear();
  _not_calc = IS_TRUE;
  _is_whole = IS_TRUE;
  _dstart   = 0;
  _dend     = 0;
  _dimen.clear( );
}

int
VeriArrayExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;

  mqueue.push( _id );
  iqueue.push( depth );

  NodeList::iterator it = _range.begin();
  for( ; it != _range.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }

  return OK;
}

int
VeriArrayExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_name( _name );
  ofile << dot_str_range( _range_from, _range_to );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _id );

  NodeList::iterator it = _range.begin();
  for( ; it != _range.end(); ++it )
    ofile << dot_str_conn( idx(), *it );
  
  return OK;
}

int
VeriArrayExp::check_child_validation( void )
{
  if( _id != NULL && _id->detail_type() == veEXP_PRI )
  {
    VeriPrimaryExp *pid = static_cast<VeriPrimaryExp *>(_id);
    if( pid->pri_type() == veVAR )
    {
      NodeList::iterator it = _range.begin();
      for( ; it != _range.end(); ++it )
      {
        if( (*it)->detail_type() != veEXP_RANGE )
        {
          veri_err->append_err( ERR_ERROR, _id->line(),
                      _id->col(), vCHILD_TYPE_ARRAY_EXP );
          return vCHILD_TYPE_ARRAY_EXP;
        }
      }
      return OK;
    }
  }
  veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_ARRAY_EXP );
  return vCHILD_TYPE_ARRAY_EXP;
}

int
VeriArrayExp::process_node_2nd( NodeVec& mstack, String& help_info )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vARRAY_S );
      return re;
      break;

    case vARRAY_S:
      if( is_decl() == IS_TRUE )
        return calc_range( );
      else
      {
        re = type_deduction( help_info );
        if( re == vSEARCH_DECL )
          set_pstatus( vSEARCH_DECL_S );
        else
          set_pstatus( vEND_S );
        return re;
      }
      break;

    case vSEARCH_DECL_S :
      re = type_deduction( help_info );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return OK;
      break;
  }
}

int
VeriArrayExp::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  NodeList::reverse_iterator it = _range.rbegin();
  for( ; it != _range.rend(); ++it )
  {
    //(*it)->clean_up( );
    static_cast<VeriExp *>(*it)->set_signed( is_signed() );
    (*it)->set_scope( scope( ) );
    (*it)->set_is_trash( is_trash() );
    mstack.push_back( *it );
  }

  //_id->clean_up( );
  _id->set_is_trash( is_trash() );
  _id->set_is_decl( IS_TRUE );
  _id->set_scope( scope() );
  mstack.push_back( _id );

  return OK;
}

int
VeriArrayExp::calc_range( void )
{
  VeriPrimaryExp * pexp = static_cast<VeriPrimaryExp *>(_id);
  _name = pexp->str();

  VeriRangeExp *range_exp = NULL;
  _not_calc = IS_FALSE;
  NodeList::iterator it = _range.begin();
  for( ; it != _range.end(); ++it )
  {
    range_exp = static_cast<VeriRangeExp *>(*it);
    if( range_exp->not_calc( ) == IS_FALSE )
    {
      _range_from.push_front( range_exp->range_from() );
      _range_to.push_front( range_exp->range_to() );
    }
    else
      _not_calc = IS_TRUE;
  }

  if( _not_calc == IS_TRUE )
  {
    veri_err->append_err( ERR_ERROR, *it, vSEC_ARRAY_DECL_RANGE_UNKNOWN );
    return vSEC_ARRAY_DECL_RANGE_UNKNOWN;
  }

  return OK;
}

int
VeriArrayExp::type_deduction( String& help_info )
{
  VeriPrimaryExp *pid = static_cast<VeriPrimaryExp *>(_id);
  _name = pid->str();
  VeriNode *symbol = scope()->value_within_module( pid->str() );
  if( symbol == NULL )
  {
    if( pstatus( ) == vARRAY_S )
    {
      help_info = _name;
      return vSEARCH_DECL;
    }
    else
    {
      veri_err->append_err( ERR_ERROR, _id, vSEC_SYMBOL_NOT_FOUND );
      return vSEC_SYMBOL_NOT_FOUND;
    }
  }
  else
  {
    switch( symbol->tok_type( ) )
    {
      case vtVAR : case vtINT :
        pid->set_var( symbol );
        break;

      default:
        veri_err->append_err( ERR_ERROR, _id, vSEC_SYMBOL_MIS_MATCH );
        return vSEC_SYMBOL_MIS_MATCH;
        break;
    }
  }

  pid->set_var( symbol );
  if( _not_calc == IS_TRUE )
    return OK;

  unsigned int last, i;
  int dlen;
  NodeList::iterator it = _range.begin();
  VeriRangeExp *range_exp = NULL;

  switch( pid->var()->tok_type() )
  {
    case vtVAR :
    {
      VeriVar *var = static_cast<VeriVar *>(pid->var() );
      if( var->range_depth() == 0 )
      {
        _not_calc = IS_TRUE;
        veri_err->append_err( ERR_ERROR, pid, vSEC_VAR_RANGE_OVERLOAD );
        return vSEC_VAR_RANGE_OVERLOAD;
      }
      
      last = _range.size() - var->range_depth( );
      if( last != 0 && last != 1 )
      {
        _not_calc = IS_TRUE;
        veri_err->append_err( ERR_ERROR, pid, vSEC_VAR_RANGE_OVERLOAD );
        return vSEC_VAR_RANGE_OVERLOAD;
      }

      for( i = 0; i < _range.size() - last; ++i, ++it )
      {
        var->get_range( i+1, _dstart, _dend, dlen );
        range_exp = static_cast<VeriRangeExp *>(*it);
        if( range_exp->rtype() != vrSINGLE )
        {
          _not_calc = IS_TRUE;
          veri_err->append_err( ERR_ERROR, _id, vSEC_ARRAY_DECL_RANGE_UNKNOWN );
          return vSEC_ARRAY_DECL_RANGE_UNKNOWN;
        }
        else if(    (    range_exp->range_from() <= _dend
                      && range_exp->range_from() >= _dstart )
                 || (    range_exp->range_from() >= _dend
                      && range_exp->range_from() <= _dstart ) )
        {
          // this is ok
          _dimen.push_back( range_exp->range_from( ) );
        }
        else
        {
          _not_calc = IS_TRUE;
          veri_err->append_err( ERR_ERROR, _id, vSEC_TYPE_MISMATCH );
          return vSEC_TYPE_MISMATCH;
        }
      }
      
      if( last == 1 )
      {
        _is_whole = IS_FALSE;
        var->get_range( i+1, _dstart, _dend, dlen );
        range_exp = static_cast<VeriRangeExp *>(*it);
        if(      range_exp->rtype() != vrSINGLE
            && (    (    range_exp->range_from() <= _dend
                      && range_exp->range_from() >= _dstart
                      && range_exp->range_to()   <= _dend
                      && range_exp->range_to()   >= _dstart )
                 || (    range_exp->range_from() >= _dend
                      && range_exp->range_from() <= _dstart
                      && range_exp->range_to()   >= _dend
                      && range_exp->range_to()   <= _dstart ) ) )
        {
          _dstart = range_exp->range_from();
          _dend   = range_exp->range_to();
        }
        else if(      range_exp->rtype() == vrSINGLE
                 && (    (    range_exp->range_from() <= _dend
                           && range_exp->range_from() >= _dstart )
                      || (    range_exp->range_from() >= _dend
                           && range_exp->range_from() <= _dstart ) ) )
        {
          _dstart = range_exp->range_from();
          _dend   = range_exp->range_from();
        }
        else
        {
          _not_calc = IS_TRUE;
          veri_err->append_err( ERR_ERROR, _id, vSEC_TYPE_MISMATCH );
          return vSEC_TYPE_MISMATCH;
        }
      }
//      set_calc_value( var->get_one_num( dimen, _is_whole, _dstart, _dend );
      break;
    }
  
    case vtINT :
    {
      VeriInt *int_var = static_cast<VeriInt *>(pid->var() );
      if( int_var->range_depth() == 0 )
      {
        _not_calc = IS_TRUE;
        veri_err->append_err( ERR_ERROR, pid, vSEC_VAR_RANGE_OVERLOAD );
        return vSEC_VAR_RANGE_OVERLOAD;
      }
      
      last = _range.size() - int_var->range_depth( );
      if( last != 0 && last != 1 )
      {
        _not_calc = IS_TRUE;
        veri_err->append_err( ERR_ERROR, pid, vSEC_VAR_RANGE_OVERLOAD );
        return vSEC_VAR_RANGE_OVERLOAD;
      }

      for( i = 0; i < _range.size() - last; ++i, ++it )
      {
        int_var->get_range( i+1, _dstart, _dend, dlen );
        range_exp = static_cast<VeriRangeExp *>(*it);
        if( range_exp->rtype() != vrSINGLE )
        {
          _not_calc = IS_TRUE;
          veri_err->append_err( ERR_ERROR, _id, vSEC_ARRAY_DECL_RANGE_UNKNOWN );
          return vSEC_ARRAY_DECL_RANGE_UNKNOWN;
        }
        else if(    (    range_exp->range_from() <= _dend
                      && range_exp->range_from() >= _dstart )
                 || (    range_exp->range_from() >= _dend
                      && range_exp->range_from() <= _dstart ) )
        {
          // this is ok
          _dimen.push_back( range_exp->range_from() );
        }
        else
        {
          _not_calc = IS_TRUE;
          veri_err->append_err( ERR_ERROR, _id, vSEC_TYPE_MISMATCH );
          return vSEC_TYPE_MISMATCH;
        }
      }
      
      if( last == 1 )
      {
        _is_whole = IS_FALSE;
        int_var->get_range( i+1, _dstart, _dend, dlen );
        range_exp = static_cast<VeriRangeExp *>(*it);
        if(      range_exp->rtype() != vrSINGLE
            && (    (    range_exp->range_from() <= _dend
                      && range_exp->range_from() >= _dstart
                      && range_exp->range_to()   <= _dend
                      && range_exp->range_to()   >= _dstart )
                 || (    range_exp->range_from() >= _dend
                      && range_exp->range_from() <= _dstart
                      && range_exp->range_to()   >= _dend
                      && range_exp->range_to()   <= _dstart ) ) )
        {
          _dstart = range_exp->range_from( );
          _dend   = range_exp->range_to( );
        }
        else if(      range_exp->rtype() == vrSINGLE
                 && (    (    range_exp->range_from() <= _dend
                           && range_exp->range_from() >= _dstart )
                      || (    range_exp->range_from() >= _dend
                           && range_exp->range_from() <= _dstart ) ) )
        {
          _dstart = range_exp->range_from();
          _dend   = range_exp->range_from();
        }
        else
        {
          _not_calc = IS_TRUE;
          veri_err->append_err( ERR_ERROR, _id, vSEC_TYPE_MISMATCH );
          return vSEC_TYPE_MISMATCH;
        }
      }

      set_calc_value( int_var->get_one_num( _dimen, _is_whole, _dstart, _dend ) );
      break;
    }

    default:
      veri_err->append_err( ERR_ERROR, pid, vSEC_ARRAY_NOT_OTHER );
      return vSEC_ARRAY_NOT_OTHER;
      break;
  }
  
  return OK;
}

int
VeriArrayExp::push_for_replacement( NodeVec& mstack )
{
  mstack.push_back( _id );

  NodeList::iterator it = _range.begin();
  for( ; it != _range.end(); ++it )
    mstack.push_back( *it );
  
  return OK;
}

int
VeriArrayExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriArrayExp *tmp = new VeriArrayExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl;
  NodeList::iterator it = _range.begin();
  for( ; it != _range.end(); ++it )
  {
    tmp->_range.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  tmp->_id = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

    /*********************
     * Caller Expression *
     *********************/

void
VeriCallerExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );

  _hive = NULL;
}

int
VeriCallerExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;

  mqueue.push( _caller );
  iqueue.push( depth );

  if( _arg != NULL )
  {
    mqueue.push( _arg );
    iqueue.push( depth );
  }
  
  if( _hive != NULL )
  {
    mqueue.push( _hive );
    iqueue.push( depth );
  }

  if( calc_value( ) != NULL )
  {
    mqueue.push( calc_value( ) );
    iqueue.push( depth );
  }

  return OK;
}

int
VeriCallerExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _caller );
  ofile << dot_str_conn( idx(), _arg );
  ofile << dot_str_conn( idx(), _hive );
  ofile << dot_str_conn( idx(), calc_value() );

  return OK;
}

int
VeriCallerExp::check_child_validation( void )
{
  if( _caller == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_CALL_EXP );
    return vCHILD_TYPE_CALL_EXP;
  }

  switch( _caller->detail_type() )
  {
    case veEXP_PRI :
    case veEXP_ARRAY :
      break;

    default:
      veri_err->append_err( ERR_ERROR, _caller->line(),
                _caller->col(), vCHILD_TYPE_CALL_EXP );
      return vCHILD_TYPE_CALL_EXP;
      break;
  }

  if( _arg != NULL )
  {
    if( _arg->detail_type() != veEXP_MULTI )
    {
      veri_err->append_err( ERR_ERROR, _arg->line(),
                _arg->col(), vCHILD_TYPE_CALL_EXP );
      return vCHILD_TYPE_CALL_EXP;
    }
    else
    {
      VeriMultiExp * mexp = static_cast<VeriMultiExp *>(_arg);
      switch( mexp->multi_type() )
      {
        case vemCOMMA_EXP:
        case vemPORT_EXP:
        case vemPORT_INST_EXP :
          break;

        default:
          veri_err->append_err( ERR_ERROR, _arg->line(),
                       _arg->col(), vCHILD_TYPE_CALL_EXP );
          return vCHILD_TYPE_CALL_EXP;
      }
    }
  }

  return OK;
}

int
VeriCallerExp::check_func_call_child_validation( void )
{
  if( _caller == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_CALL_EXP );
    return vCHILD_TYPE_CALL_EXP;
  }

  if( _caller->detail_type() != veEXP_PRI )
  {
    veri_err->append_err( ERR_ERROR, _caller->line(),
                    _caller->col(), vCHILD_TYPE_CALL_EXP );
    return vCHILD_TYPE_CALL_EXP;
  }

  if( _arg != NULL )
  {
    if( _arg->detail_type() != veEXP_MULTI )
    {
      veri_err->append_err( ERR_ERROR, _arg->line(),
                    _arg->col(), vCHILD_TYPE_CALL_EXP );
      return vCHILD_TYPE_CALL_EXP;
    }
    else
    {
      VeriMultiExp * mexp = static_cast<VeriMultiExp *>(_arg);
      if( mexp->multi_type() != vemCOMMA_EXP )
      {
        veri_err->append_err( ERR_ERROR, _arg->line(),
                       _arg->col(), vCHILD_TYPE_CALL_EXP );
        return vCHILD_TYPE_CALL_EXP;
      }
    }
  }

  return OK;
}

int
VeriCallerExp::process_node_2nd( NodeVec& mstack, String& help_info )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      set_pstatus( vCALL_S );
      return push_children( mstack );
      break;

    case vCALL_S:
      re = unfolding( mstack, help_info );
      if( re == vSEARCH_DECL )
        set_pstatus( vSEARCH_DECL_S );
      else
        set_pstatus( vEND_S );
      return re;
      break;

    case vSEARCH_DECL_S :
      re = unfolding( mstack, help_info );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return get_ret_value( );
      break;
  }
}

int
VeriCallerExp::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  if( _arg != NULL )
  {
    //_arg->clean_up( );
    _arg->set_scope( scope() );
    static_cast<VeriExp *>(_arg)->set_signed( is_signed() );
    mstack.push_back( _arg );
  }

  //_caller->clean_up( );
  _caller->set_is_decl( IS_TRUE );
  _caller->set_scope( scope() );
  mstack.push_back( _caller );

  return OK;
}

int
VeriCallerExp::unfolding( NodeVec& mstack, String& help_info )
{
  if( _hive != NULL )
    return vSEC_CALLER_REUNFOLD;

  VeriNode *tf = NULL;
  switch( _caller->detail_type() )
  {
    case veEXP_PRI:
    {
      VeriPrimaryExp *pexp = static_cast<VeriPrimaryExp *>(_caller);
      _caller_name = pexp->str();
      tf = scope()->value(pexp->str());
      if( tf == NULL )
      {
        if( pstatus( ) == vCALL_S )
        {
          help_info = _caller_name;
          return vSEARCH_DECL;
        }
        else
        {
          veri_err->append_err( ERR_ERROR, _caller, vSEC_TF_NOT_FOUND );
          return vSEC_TF_NOT_FOUND;
        }
      }
      break;
    }

    case veEXP_ARRAY:
    {
      VeriArrayExp *aexp = static_cast<VeriArrayExp *>(_caller);
      _caller_name = aexp->name();
      tf = scope()->value(aexp->name());
      if( tf == NULL )
      {
        if( pstatus( ) == vCALL_S )
        {
          help_info = _caller_name;
          return vSEARCH_DECL;
        }
        else
        {
          veri_err->append_err( ERR_ERROR, _caller, vSEC_TF_NOT_FOUND );
          return vSEC_TF_NOT_FOUND;
        }
      }
      break;
    }

    default:
      break;
  }

  switch( tf->tok_type( ) )
  {
    case vtTASK :
      tf = (static_cast<VeriTask *>(tf))->task();
      break;

    case vtFUNC :
      tf = (static_cast<VeriFunc *>(tf))->func();
      break;

    default:
      veri_err->append_err( ERR_ERROR, tf, vSEC_TF_NOT_FOUND );
      return vSEC_SYMBOL_NOT_FOUND;
      break;
  }

  NodeVec  node_vec;

  NodeVec tstack;
  tstack.reserve( 397 );
  tstack.push_back( tf );

  while( !tstack.empty( ) )
  {
    tf = tstack.back( );
    tstack.pop_back( );
    node_vec.push_back( tf );
    tf->push_for_replacement( tstack );
  }

  NodeList node_list( node_vec.rbegin(), node_vec.rend() );
  node_vec.clear();
  NodeList::iterator inl = node_list.begin();
  for( ; inl != node_list.end(); ++inl )
  {
    tf = *inl;
    tf->replace_node( veri_parser->next_id(), node_list, inl );
    veri_parser->insert_node( *inl );
    tstack.push_back( *inl );
  }

  mstack.push_back( this );

  _hive = tstack.back();
  _hive->set_father( this );
  _hive->set_scope( scope( ) );
  mstack.push_back( _hive );

  tstack.clear();
  return OK;
}

int
VeriCallerExp::get_ret_value( void )
{
  if( _call_type == vtTASK_CALL )
    return OK;

  VeriNode * ret = scope()->value_within_module( _caller_name );
  if( ret == NULL )
  {
    veri_err->append_err( ERR_ERROR, this, vSEC_CALLER_MISS_RET );
    return vSEC_CALLER_MISS_RET;
  }
  assert( ret->tok_type( ) == vtFUNC );

  VeriFunc *func_ret = static_cast<VeriFunc *>(ret);

  VeriNum *num = func_ret->get_val();
  set_calc_value( num );
  return OK;
}

int
VeriCallerExp::push_for_replacement( NodeVec& mstack )
{
  mstack.push_back( _caller );

  if( _arg != NULL )
    mstack.push_back( _arg );

  return OK;
}

int
VeriCallerExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriCallerExp *tmp = new VeriCallerExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl;
  if( _arg != NULL )
  {
    tmp->_arg = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  tmp->_caller = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

    /*********************
     * Binary Expression *
     *********************/

void
VeriBinaryExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );
}

int
VeriBinaryExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;
  mqueue.push( _left );
  mqueue.push( _right );
  iqueue.push( depth );
  iqueue.push( depth );

  if( calc_value() != NULL )
  {
    mqueue.push( calc_value() );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriBinaryExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_op( _op );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _left );
  ofile << dot_str_conn( idx(), _right );

  if( calc_value() != NULL )
    ofile << dot_str_conn( idx(), calc_value() );

  return OK;
}

int
VeriBinaryExp::check_child_validation( VeriNode *child )
{
  if( child != NULL && child->tok_type() == vtEXP )
  {
    switch( child->detail_type() )
    {
      case veEXP_EVENT   :
      case veEXP_ASSIGN  :
      case veEXP_RANGE   :
      case veEXP_PORT_REF :
      case veEXP_NAMED_PARAM :
      case veEXP_DUMMY_PORT :
        veri_err->append_err( ERR_ERROR, child->line(),
                       child->col(), vCHILD_TYPE_BIN_EXP );
        return vCHILD_TYPE_BIN_EXP;

      case veEXP_CALLER :
      {
        /* task caller shall not be here */;
        VeriCallerExp *cexp = static_cast<VeriCallerExp *>(child);
        if( cexp->call_type() == vtTASK_CALL )
        {
          veri_err->append_err( ERR_ERROR, child->line(),
                       child->col(), vCHILD_TYPE_BIN_EXP );
          return vCHILD_TYPE_BIN_EXP;
        }
        break;
      }

      case veEXP_MINTY :
      {
        veri_err->append_err( ERR_ERROR, child->line(),
                     child->col(), vCHILD_TYPE_BIN_EXP );
        return vCHILD_TYPE_BIN_EXP;
        break;
      }

      case veEXP_MULTI :
      {
        VeriMultiExp *mexp = static_cast<VeriMultiExp *>(child);
        switch( mexp->multi_type() )
        {
          case vemCONCAT_EXP :
            return OK;

          case vemCOMMA_EXP :
            return check_child_validation( mexp->exp().back() );
            break;

          case vemEVENT_EXP :
          case vemPORT_EXP :
          case vemPORT_INST_EXP :
          default :
            veri_err->append_err( ERR_ERROR, child->line(),
                       child->col(), vCHILD_TYPE_BIN_EXP );
            return vCHILD_TYPE_BIN_EXP;
        }
        break;
      }

      default:
        break;
    }
    return OK;
  }
  veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_BIN_EXP );
  return vCHILD_TYPE_BIN_EXP;
}

int
VeriBinaryExp::check_child_validation( void )
{
  int re;
  re = check_child_validation( _left  );
  if( re != OK )
    return re;

  re = check_child_validation( _right );
  if( re != OK )
    return re;
  return OK;
}

int
VeriBinaryExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      switch( _op )
      {
        case vtBDOR :
        case vtBDAND :
          re = push_short_cut_left( mstack );
          set_pstatus( vSHORT_CUT_S );
          return re;
          break;

        default:
          re = push_children( mstack );
          set_pstatus( vEND_S );
          return re;
      }
      break;

    case vSHORT_CUT_S :
      re = process_short_cut_left( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return process_binary_op( );
      break;
  }
}

int
VeriBinaryExp::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  //_right->clean_up( );
  _right->set_is_trash( is_trash() );
  _right->set_scope( scope() );
  mstack.push_back( _right );

  //_left->clean_up( );
  _left->set_is_trash( is_trash() );
  _left->set_scope( scope() );
  mstack.push_back( _left );
  return OK;
}

int
VeriBinaryExp::push_short_cut_left( NodeVec& mstack )
{
  mstack.push_back( this );

  //_left->clean_up( );
  _left->set_is_trash( is_trash() );
  _left->set_scope( scope() );
  mstack.push_back( _left );

  return OK;
}

int
VeriBinaryExp::process_short_cut_left( NodeVec& mstack )
{
  VeriExp * left  = static_cast<VeriExp *>(_left);
  if( left->calc_value( ) != NULL && _op == vtBDOR )
  {
    VeriNum *num = new VeriNum( veri_parser->next_id(), left->calc_value() );
    veri_parser->insert_node( num );
    num->set_father( this );
    num->set_scope( scope( ) );

    num->unary_operation( vtUOR );
    if( num->one_bit_value( ) == V1 )
    {
      set_calc_value( num );
      return OK;
    }
    veri_parser->toss_node( num->idx() );
  }
  else if( left->calc_value( ) != NULL && _op == vtBDAND )
  {
    VeriNum *num = new VeriNum( veri_parser->next_id(), left->calc_value() );
    veri_parser->insert_node( num );
    num->set_father( this );
    num->set_scope( scope( ) );

    num->unary_operation( vtUOR );
    if( left->calc_value()->one_bit_value( ) == V0 )
    {
      set_calc_value( num );
      return OK;
    }
    veri_parser->toss_node( num->idx() );
  }

  mstack.push_back( this );

  //_right->clean_up( );
  _right->set_is_trash( is_trash() );
  _right->set_scope( scope() );
  mstack.push_back( _right );
  
  return OK;
}

int
VeriBinaryExp::process_binary_op( void )
{
  VeriExp * left  = static_cast<VeriExp *>(_left);
  VeriExp * right = static_cast<VeriExp *>(_right);

  int re = OK;

  // type check
  if( left->detail_type( ) == veEXP_PRI )
  {
    re = check_primary_not_other( static_cast<VeriPrimaryExp*>(left) );
    if( re != OK )
      return re;
  }

  if( right->detail_type( ) == veEXP_PRI )
  {
    re = check_primary_not_other( static_cast<VeriPrimaryExp*>(right) );
    if( re != OK )
      return re;
  }

  if( right->calc_value( ) != NULL && _op == vtBDOR )
  {
    VeriNum *num = new VeriNum( veri_parser->next_id(), right->calc_value() );
    veri_parser->insert_node( num );
    num->set_father( this );
    num->set_scope( scope( ) );

    num->unary_operation( vtUOR );
    if( right->calc_value()->one_bit_value( ) == V1 )
    {
      set_calc_value( num );
      return OK;
    }
    veri_parser->toss_node( num->idx() );
  }
  else if( right->calc_value( ) != NULL && _op == vtBDAND )
  {
    VeriNum *num = new VeriNum( veri_parser->next_id(), right->calc_value() );
    veri_parser->insert_node( num );
    num->set_father( this );
    num->set_scope( scope( ) );

    num->unary_operation( vtUOR );
    if( right->calc_value()->one_bit_value( ) == V0 )
    {
      set_calc_value( num );
      return OK;
    }
    veri_parser->toss_node( num->idx() );
  }

  if( left->calc_value() != NULL && right->calc_value() != NULL )
  {
    VeriNum *num = new VeriNum( veri_parser->next_id(), left->calc_value() );
    veri_parser->insert_node( num );
    num->set_father( this );
    num->set_scope( scope( ) );

    re = num->binary_operation( _op, right->calc_value() );
    if( re != OK )
    {
      veri_err->append_err( ERR_ERROR, right, re );
      veri_parser->toss_node( num->idx() );
      return re;
    }
    set_calc_value( num );
  }
  return OK;
}

int
VeriBinaryExp::push_for_replacement( NodeVec& mstack )
{
  mstack.push_back( _left );
  mstack.push_back( _right );
  return OK;
}

int
VeriBinaryExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriBinaryExp *tmp = new VeriBinaryExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl;

  tmp->_right = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );
  itl = inl;

  tmp->_left = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

    /*********************
     * Assign Expression *
     *********************/

void
VeriAssignExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );
}

int
VeriAssignExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;
  mqueue.push( _lval );
  mqueue.push( _rval );
  iqueue.push( depth );
  iqueue.push( depth );
  return OK;
}

int
VeriAssignExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _lval );
  ofile << dot_str_conn( idx(), _rval );

  return OK;
}

int
VeriAssignExp::check_child_validation( VeriNode *child )
{
  if( child != NULL && child->tok_type() == vtEXP )
  {
    switch( child->detail_type() )
    {
      case veEXP_EVENT   :
      case veEXP_RANGE   :
      case veEXP_PORT_REF :
      case veEXP_NAMED_PARAM :
      case veEXP_DUMMY_PORT :
        veri_err->append_err( ERR_ERROR, child->line(),
                    child->col(), vCHILD_TYPE_ASSIGN_EXP );
        return vCHILD_TYPE_ASSIGN_EXP;

      case veEXP_CALLER :
      {
        /* task caller shall not be here */;
        VeriCallerExp *cexp = static_cast<VeriCallerExp *>(child);
        if( cexp->call_type() == vtTASK_CALL )
        {
          veri_err->append_err( ERR_ERROR, child->line(),
                    child->col(), vCHILD_TYPE_ASSIGN_EXP );
          return vCHILD_TYPE_ASSIGN_EXP;
        }
        else
          return OK;
        break;
      }

      case veEXP_MINTY :
      {
        veri_err->append_err( ERR_ERROR, child->line(),
                  child->col(), vCHILD_TYPE_ASSIGN_EXP );
        return vCHILD_TYPE_ASSIGN_EXP;
        break;
      }

      case veEXP_MULTI :
      {
        VeriMultiExp *mexp = static_cast<VeriMultiExp *>(child);
        switch( mexp->multi_type() )
        {
          case vemCONCAT_EXP :
            return OK;

          case vemCOMMA_EXP :
            return check_child_validation( mexp->exp().back() );
            break;

          case vemEVENT_EXP :
          case vemPORT_EXP :
          case vemPORT_INST_EXP :
          default :
            veri_err->append_err( ERR_ERROR, child->line(),
                        child->col(), vCHILD_TYPE_ASSIGN_EXP );
            return vCHILD_TYPE_ASSIGN_EXP;
        }
        break;
      }

      default:
        return OK;
    }
    return OK;
  }
  veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_ASSIGN_EXP );
  return vCHILD_TYPE_ASSIGN_EXP;
}

int
VeriAssignExp::check_child_validation( void )
{
  if( _lval != NULL && _rval != NULL )
  {
    if(    _lval->detail_type() != veEXP_PRI
        && _lval->detail_type() != veEXP_ARRAY )
    {
      veri_err->append_err( ERR_ERROR, _lval->line(),
                  _lval->col(), vCHILD_TYPE_ASSIGN_EXP );
      return vCHILD_TYPE_ASSIGN_EXP;
    }

    if( _lval->detail_type() == veEXP_ARRAY )
    {
      VeriArrayExp *aexp = static_cast<VeriArrayExp *>(_lval);
      VeriRangeExp *rexp = NULL;
      NodeList& range = aexp->range();
      NodeList::reverse_iterator it = range.rbegin();

      if( aexp->range().size() == (range.size() + 1) )
        ++it;
      else if( aexp->range().size() != range.size() )
      {
        veri_err->append_err( ERR_ERROR, _lval->line(),
                    _lval->col(), vCHILD_TYPE_ASSIGN_EXP );
        return vCHILD_TYPE_ASSIGN_EXP;
      }

      for( ++it; it != range.rend(); ++it )
      {
        if( (*it) == NULL || (*it)->detail_type() != veEXP_RANGE )
        {
          veri_err->append_err( ERR_ERROR, _lval->line(),
                      _lval->col(), vCHILD_TYPE_ASSIGN_EXP );
          return vCHILD_TYPE_ASSIGN_EXP;
        }
        rexp = static_cast<VeriRangeExp *>(*it);
        if( rexp->rtype() != vrSINGLE )
        {
          veri_err->append_err( ERR_ERROR, (*it)->line(),
                      (*it)->col(), vCHILD_TYPE_ASSIGN_EXP );
          return vCHILD_TYPE_ASSIGN_EXP;
        }
      }
    }

    if( _rval->tok_type() != vtEXP )
    {
      veri_err->append_err( ERR_ERROR, _rval->line(),
                  _rval->col(), vCHILD_TYPE_ASSIGN_EXP );
      return vCHILD_TYPE_ASSIGN_EXP;
    }

    return check_child_validation( _rval );
  }
  
  veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_ASSIGN_EXP );
  return vCHILD_TYPE_ASSIGN_EXP;
}

int
VeriAssignExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      // assuming no array-initialization
      return process_assign_op( );
      break;
  }
}

int
VeriAssignExp::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  //_rval->clean_up( );
  _rval->set_is_trash( is_trash() );
  _rval->set_scope( scope() );
  mstack.push_back( _rval );

  //_lval->clean_up( );
  _lval->set_is_decl( is_decl() );
  _lval->set_is_trash( is_trash() );
  _lval->set_scope( scope() );
  mstack.push_back( _lval );
  return OK;
}

int
VeriAssignExp::process_assign_op( void )
{
  VeriExp * lval = static_cast<VeriExp *>(_lval);
  VeriExp * rval = static_cast<VeriExp *>(_rval);

  if( rval->calc_value() != NULL )
  {
    VeriPrimaryExp *pexp = NULL;
    VeriArrayExp   *aexp = NULL;

    IVec dummy_dimen;

    switch( lval->detail_type( ) )
    {
      case veEXP_PRI :
      {
        pexp = static_cast<VeriPrimaryExp *>(lval);
        if( pexp->var() == NULL )
          return vSEC_SYMBOL_NOT_FOUND;
         
        int re = check_primary_not_other( pexp );
        if( re != OK )
          return re;

        switch( pexp->var()->tok_type( ) )
        {
          case vtVAR :
          {
            VeriVar *var = static_cast<VeriVar *>(pexp->var());
            VeriNum *num = new VeriNum( veri_parser->next_id(),
                                        rval->calc_value() );
            veri_parser->insert_node( num );
            num->truncate_assign( var->dlen() );
            num->set_father( this );
            num->set_scope( scope() );
            var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
            break;
          }

          case vtTFVAR :
          {
            VeriTfVar *tfvar = static_cast<VeriTfVar *>(pexp->var());
            VeriNum *num = new VeriNum( veri_parser->next_id(),
                                        rval->calc_value() );
            veri_parser->insert_node( num );
            num->truncate_assign( tfvar->dlen() );
            num->set_father( this );
            num->set_scope( scope() );
            tfvar->set_val( num );
            break;
          }

          case vtFUNC :
          {
            VeriFunc *func_ret = static_cast<VeriFunc *>(pexp->var());
            VeriNum *num = new VeriNum( veri_parser->next_id(),
                                        rval->calc_value() );
            veri_parser->insert_node( num );
            num->truncate_assign( func_ret->dlen() );
            num->set_father( this );
            num->set_scope( scope() );
            func_ret->set_val( num );
            break;
          }

          case vtPARAM :
          {
            VeriParam *param = static_cast<VeriParam *>(pexp->var());
            VeriNum *num = new VeriNum( veri_parser->next_id(),
                                        rval->calc_value() );
            veri_parser->insert_node( num );
            num->truncate_assign( param->dlen() );
            num->set_father( this );
            num->set_scope( scope() );
            param->set_val( num );
            break;
          }
          
          case vtINT :
          {
            VeriInt *int_var = static_cast<VeriInt *>(pexp->var());
            VeriNum *num = new VeriNum( veri_parser->next_id(),
                                        rval->calc_value() );
            veri_parser->insert_node( num );
            num->truncate_assign( 32 );
            num->set_father( this );
            num->set_scope( scope() );
            int_var->set_one_num( dummy_dimen, IS_TRUE, 0, 0, num );
            break;
          }

          case vtGENVAR :
          {
            VeriGenVar *genvar = static_cast<VeriGenVar *>(pexp->var());
            VeriNum *num = new VeriNum( veri_parser->next_id(),
                                        rval->calc_value() );
            veri_parser->insert_node( num );
            num->truncate_assign( 32 );
            num->set_father( this );
            num->set_scope( scope() );
            genvar->set_val( num );
            break;
          }

          default:
            return vSEC_SYMBOL_NOT_FOUND;
            break;
        }
        break;
      }

      case veEXP_ARRAY :
      {
        aexp = static_cast<VeriArrayExp *>(lval);
        if(    aexp->id() == NULL
            || aexp->id()->detail_type() != veEXP_PRI )
          return vSEC_TYPE_MISMATCH;
        
        if( aexp->not_calc() == IS_TRUE )
          return OK;

        pexp = static_cast<VeriPrimaryExp *>(aexp->id());
        if( pexp->var() == NULL )
          return vSEC_TYPE_MISMATCH;

        int dlen = abs(aexp->dend() - aexp->dstart()) + 1;
        switch( pexp->var()->tok_type( ) )
        {
          case vtVAR :
          {
            VeriVar *var = static_cast<VeriVar *>(pexp->var());
            VeriNum *num = new VeriNum( veri_parser->next_id(),
                                        rval->calc_value() );
            veri_parser->insert_node( num );
            num->truncate_assign( dlen );
            num->set_father( this );
            num->set_scope( scope() );
            var->set_one_num( aexp->dimen(), aexp->is_whole(),
                              aexp->dstart(), aexp->dend(), num );
            break;
          }

          case vtINT :
          {
            VeriInt *int_var = static_cast<VeriInt *>(pexp->var());
            VeriNum *num = new VeriNum( veri_parser->next_id(),
                                        rval->calc_value() );
            veri_parser->insert_node( num );
            num->truncate_assign( 32 );
            num->set_father( this );
            num->set_scope( scope() );
            int_var->set_one_num( aexp->dimen(), aexp->is_whole(),
                                  aexp->dstart(), aexp->dend(), num );
            break;
          }

          default:
            break;
        }
        break;
      }

      default:
        break;
    }
    return OK;
  }

  return OK;
}

int
VeriAssignExp::push_for_replacement( NodeVec& mstack )
{
  mstack.push_back( _lval );
  mstack.push_back( _rval );
  return OK;
}

int
VeriAssignExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriAssignExp *tmp = new VeriAssignExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl;

  tmp->_rval = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );
  itl = inl;

  tmp->_lval = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

    /*************************************
     * Multiple-Concatenation Expression *
     *************************************/

void
VeriMultiConcatExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );
}

int
VeriMultiConcatExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;
  mqueue.push( _exp1 );
  mqueue.push( _exp2 );
  iqueue.push( depth );
  iqueue.push( depth );

  if( calc_value() != NULL )
  {
    mqueue.push( calc_value() );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriMultiConcatExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _exp1 );
  ofile << dot_str_conn( idx(), _exp2 );

  return OK;
}

int
VeriMultiConcatExp::check_child_validation( void )
{
  if(    _exp1 != NULL && _exp1->tok_type() == vtEXP
      && _exp2 != NULL && _exp2->tok_type() == vtEXP )
  {
    switch( _exp1->detail_type() )
    {
      case veEXP_EVENT   :
      case veEXP_RANGE   :
      case veEXP_MULTI   :
      case veEXP_PORT_REF :
      case veEXP_NAMED_PARAM :
      case veEXP_DUMMY_PORT :
        veri_err->append_err( ERR_ERROR, _exp1->line(),
                      _exp1->col(), vCHILD_TYPE_MCONCAT_EXP );
        return vCHILD_TYPE_MCONCAT_EXP;

      case veEXP_CALLER :
      {
        /* task caller shall not be here */;
        VeriCallerExp *cexp = static_cast<VeriCallerExp *>(_exp1);
        if( cexp->call_type() == vtTASK_CALL )
        {
          veri_err->append_err( ERR_ERROR, _exp1->line(),
                      _exp1->col(), vCHILD_TYPE_MCONCAT_EXP );
          return vCHILD_TYPE_MCONCAT_EXP;
        }
        else
          return OK;
        break;
      }

      default:
        return OK;
    }

    if( _exp2->detail_type() != veEXP_MULTI )
    {
      VeriMultiExp *mexp = static_cast<VeriMultiExp *>(_exp2);
      if( mexp->multi_type() == vemCONCAT_EXP )
        return OK;
      veri_err->append_err( ERR_ERROR, _exp2->line(),
                     _exp2->col(), vCHILD_TYPE_MCONCAT_EXP );
      return vCHILD_TYPE_MCONCAT_EXP;
    }
  }
  veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_MCONCAT_EXP );
  return vCHILD_TYPE_MCONCAT_EXP;
}

int
VeriMultiConcatExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return process_mconcat( );
      break;
  }
}

int
VeriMultiConcatExp::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  //_exp2->clean_up( );
  _exp2->set_is_trash( is_trash( ) );
  _exp2->set_scope( scope( ) );
  static_cast<VeriExp *>(_exp2)->set_signed( is_signed() );
  mstack.push_back( _exp2 );

  //_exp1->clean_up( );
  _exp1->set_is_trash( is_trash( ) );
  _exp1->set_scope( scope( ) );
  static_cast<VeriExp *>(_exp1)->set_signed( is_signed() );
  mstack.push_back( _exp1 );

  return OK;
}

int
VeriMultiConcatExp::process_mconcat( void )
{
  return OK;
}

int
VeriMultiConcatExp::push_for_replacement( NodeVec& mstack )
{
  mstack.push_back( _exp1 );
  mstack.push_back( _exp2 );
  return OK;
}

int
VeriMultiConcatExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriMultiConcatExp *tmp = new VeriMultiConcatExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl;

  tmp->_exp2 = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );
  itl = inl;

  tmp->_exp1 = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

    /********************
     * Range Expression *
     ********************/

void
VeriRangeExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );

  _vleft  = RANGE_DEFAULT;
  _vright = RANGE_DEFAULT;
  _not_calc = IS_TRUE;
}

int
VeriRangeExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;
  mqueue.push( _left );
  iqueue.push( depth );
  if( _right != NULL )
  {
    mqueue.push( _right );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriRangeExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );

  IList  range_from( 1, _vleft );
  IList  range_to( 1, _vright );

  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_range_type( _rtype );
  ofile << dot_str_range( range_from, range_to ); 
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _left );
  ofile << dot_str_conn( idx(), _right );

  return OK;
}

int
VeriRangeExp::check_child_validation( VeriNode *child )
{
  if( child != NULL && child->tok_type() == vtEXP )
  {
    switch( child->detail_type() )
    {
      case veEXP_EVENT   :
      case veEXP_RANGE   :
      case veEXP_PORT_REF :
      case veEXP_NAMED_PARAM :
      case veEXP_DUMMY_PORT :
        veri_err->append_err( ERR_ERROR, child->line(),
                  child->col(), vCHILD_TYPE_RANGE_EXP );
        return vCHILD_TYPE_RANGE_EXP;

      case veEXP_CALLER :
      {
        /* task caller shall not be here */;
        VeriCallerExp *cexp = static_cast<VeriCallerExp *>(child);
        if( cexp->call_type() == vtTASK_CALL )
        {
          veri_err->append_err( ERR_ERROR, child->line(),
                  child->col(), vCHILD_TYPE_RANGE_EXP );
          return vCHILD_TYPE_RANGE_EXP;
        }
        break;
      }

      case veEXP_MULTI :
      {
        VeriMultiExp *mexp = static_cast<VeriMultiExp *>(child);
        switch( mexp->multi_type() )
        {
          case vemCONCAT_EXP :
            return OK;

          case vemCOMMA_EXP :
            return check_child_validation( mexp->exp().back() );
            break;

          case vemEVENT_EXP :
          case vemPORT_EXP  :
          case vemPORT_INST_EXP :
          default :
            veri_err->append_err( ERR_ERROR, child->line(),
                     child->col(), vCHILD_TYPE_RANGE_EXP );
            return vCHILD_TYPE_RANGE_EXP;
        }
        break;
      }

      default:
        break;
    }
    return OK;
  }
  veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_RANGE_EXP );
  return vCHILD_TYPE_RANGE_EXP;
}

int
VeriRangeExp::check_child_validation( void )
{
  int re;
  re = check_child_validation( _left  );
  if( re != OK )
    return re;

  if( _right != NULL )
    return check_child_validation( _right );

  if( _right == NULL && _rtype != vrSINGLE )
    return check_child_validation( _right );

  return OK;
}

int
VeriRangeExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return assign_range( );
      break;
  }
}

int
VeriRangeExp::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  if( _right != NULL )
  {
    //_right->clean_up( );
    _right->set_is_trash( is_trash( ) );
    _right->set_scope( scope( ) );
    static_cast<VeriExp *>(_right)->set_signed( is_signed() );
    mstack.push_back( _right );
  }

  //_left->clean_up( );
  _left->set_is_trash( is_trash( ) );
  _left->set_scope( scope( ) );
  static_cast<VeriExp *>(_left)->set_signed( is_signed() );
  mstack.push_back( _left );

  return OK;
}

int
VeriRangeExp::assign_range( void )
{
  VeriExp * left  = static_cast<VeriExp *>(_left);
  VeriExp * right = static_cast<VeriExp *>(_right);

  if( left->calc_value() != NULL )
  {
    _vleft = left->calc_value()->get_first_int();
    _not_calc = IS_FALSE;
  }

  if( right != NULL && right->calc_value() != NULL )
  {
    _vright = right->calc_value()->get_first_int();
    _not_calc = IS_FALSE;
  }
  
  return OK;
}

int
VeriRangeExp::push_for_replacement( NodeVec& mstack )
{
  mstack.push_back( _left );
  if( _right != NULL )
    mstack.push_back( _right );
  return OK;
}

int
VeriRangeExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriRangeExp *tmp = new VeriRangeExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl;

  if( _right != NULL )
  {
    tmp->_right = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  tmp->_left = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

    /************************
     * Mintypmax Expression *
     ************************/

void
VeriMintypmaxExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );

  memset( _re, 0, sizeof(int) * 3 );
  _not_calc = IS_TRUE;
}

int
VeriMintypmaxExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;
  mqueue.push( _exp[0] );
  mqueue.push( _exp[1] );
  mqueue.push( _exp[2] );
  iqueue.push( depth );
  iqueue.push( depth );
  iqueue.push( depth );

  if( calc_value( ) != NULL )
  {
    mqueue.push( calc_value() );
    iqueue.push( depth );
  }

  return OK;
}

int
VeriMintypmaxExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _exp[0] );
  ofile << dot_str_conn( idx(), _exp[1] );
  ofile << dot_str_conn( idx(), _exp[2] );

  if( calc_value() != NULL )
    ofile << dot_str_conn( idx(), calc_value() );

  return OK;
}

int
VeriMintypmaxExp::check_child_validation( VeriNode *child )
{
  if( child != NULL && child->tok_type() == vtEXP )
  {
    switch( child->detail_type() )
    {
      case veEXP_EVENT   :
      case veEXP_RANGE   :
      case veEXP_PORT_REF :
      case veEXP_NAMED_PARAM :
      case veEXP_DUMMY_PORT :
        veri_err->append_err( ERR_ERROR, child->line(),
                      child->col(), vCHILD_TYPE_MINTY_EXP );
        return vCHILD_TYPE_MINTY_EXP;

      case veEXP_CALLER :
      {
        /* task caller shall not be here */;
        VeriCallerExp *cexp = static_cast<VeriCallerExp *>(child);
        if( cexp->call_type() == vtTASK_CALL )
        {
          veri_err->append_err( ERR_ERROR, child->line(),
                        child->col(), vCHILD_TYPE_MINTY_EXP );
          return vCHILD_TYPE_MINTY_EXP;
        }
        break;
      }

      case veEXP_MULTI :
      {
        VeriMultiExp *mexp = static_cast<VeriMultiExp *>(child);
        switch( mexp->multi_type() )
        {
          case vemCONCAT_EXP :
            return OK;

          case vemCOMMA_EXP :
            return check_child_validation( mexp->exp().back() );
            break;

          case vemEVENT_EXP :
          case vemPORT_EXP  :
          case vemPORT_INST_EXP :
          default :
            veri_err->append_err( ERR_ERROR, child->line(),
                          child->col(), vCHILD_TYPE_MINTY_EXP );
            return vCHILD_TYPE_MINTY_EXP;
        }
        break;
      }

      default:
        break;
    }
    return OK;
  }
  veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_MINTY_EXP );
  return vCHILD_TYPE_MINTY_EXP;
}

int
VeriMintypmaxExp::check_child_validation( void )
{
  int re;
  for( int i = 0; i < 3; ++i )
  {
    re = check_child_validation( _exp[i] );
    if( re != OK )
      return re;
  }
  return OK;
}

int
VeriMintypmaxExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return process_mintypmax( );
      break;
  }
}

int
VeriMintypmaxExp::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  //_exp[2]->clean_up( );
  _exp[2]->set_is_trash( is_trash( ) );
  _exp[2]->set_scope( scope( ) );
  static_cast<VeriExp *>(_exp[2])->set_signed( is_signed() );
  mstack.push_back( _exp[2] );

  //_exp[1]->clean_up( );
  _exp[1]->set_is_trash( is_trash( ) );
  _exp[1]->set_scope( scope( ) );
  static_cast<VeriExp *>(_exp[1])->set_signed( is_signed() );
  mstack.push_back( _exp[1] );

  //_exp[0]->clean_up( );
  _exp[0]->set_is_trash( is_trash( ) );
  _exp[0]->set_scope( scope( ) );
  static_cast<VeriExp *>(_exp[0])->set_signed( is_signed() );
  mstack.push_back( _exp[0] );

  return OK;
}

int
VeriMintypmaxExp::process_mintypmax( void )
{
  VeriExp * exp = NULL;

  exp = static_cast<VeriExp* >(_exp[0]);

  _not_calc = IS_FALSE;

  if( exp->calc_value( ) != NULL )
    _re[0] = exp->calc_value()->get_first_int( );
  else
    _not_calc = IS_TRUE;

  exp = static_cast<VeriExp* >(_exp[1]);
  if( exp->calc_value( ) != NULL )
    _re[1] = exp->calc_value()->get_first_int( );
  else
    _not_calc = IS_TRUE;

  exp = static_cast<VeriExp* >(_exp[2]);
  if( exp->calc_value( ) != NULL )
    _re[2] = exp->calc_value()->get_first_int( );
  else
    _not_calc = IS_TRUE;

 return OK;
}

int
VeriMintypmaxExp::push_for_replacement( NodeVec& mstack )
{
  mstack.push_back( _exp[0] );
  mstack.push_back( _exp[1] );
  mstack.push_back( _exp[2] );
  return OK;
}

int
VeriMintypmaxExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriMintypmaxExp *tmp = new VeriMintypmaxExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl;

  tmp->_exp[2] = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );
  itl = inl;

  tmp->_exp[1] = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );
  itl = inl;

  tmp->_exp[0] = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

    /***********************
     * Question Expression *
     ***********************/

void
VeriQuesExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );

  _choice = 0;
}

int
VeriQuesExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;
  mqueue.push( _ques );
  mqueue.push( _choice1 );
  mqueue.push( _choice2 );
  iqueue.push( depth );
  iqueue.push( depth );
  iqueue.push( depth );

  if( calc_value( ) != NULL )
  {
    mqueue.push( calc_value() );
    iqueue.push( depth );
  }

  return OK;
}

int
VeriQuesExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _ques );
  ofile << dot_str_conn( idx(), _choice1 );
  ofile << dot_str_conn( idx(), _choice2 );

  if( calc_value() != NULL )
    ofile << dot_str_conn( idx(), calc_value() );

  return OK;
}

int
VeriQuesExp::check_child_validation( VeriNode *child )
{
  if( child != NULL && child->tok_type() == vtEXP )
  {
    switch( child->detail_type() )
    {
      case veEXP_EVENT   :
      case veEXP_RANGE   :
      case veEXP_PORT_REF :
      case veEXP_NAMED_PARAM :
      case veEXP_DUMMY_PORT :
        veri_err->append_err( ERR_ERROR, child->line(),
                      child->col(), vCHILD_TYPE_QUES_EXP );
        return vCHILD_TYPE_QUES_EXP;

      case veEXP_CALLER :
      {
        /* task caller shall not be here */;
        VeriCallerExp *cexp = static_cast<VeriCallerExp *>(child);
        if( cexp->call_type() == vtTASK_CALL )
        {
          veri_err->append_err( ERR_ERROR, child->line(),
                        child->col(), vCHILD_TYPE_QUES_EXP );
          return vCHILD_TYPE_QUES_EXP;
        }
        break;
      }

      case veEXP_MINTY :
      {
        veri_err->append_err( ERR_ERROR, child->line(),
                     child->col(), vCHILD_TYPE_QUES_EXP );
        return vCHILD_TYPE_QUES_EXP;
        break;
      }

      case veEXP_MULTI :
      {
        VeriMultiExp *mexp = static_cast<VeriMultiExp *>(child);
        switch( mexp->multi_type() )
        {
          case vemCONCAT_EXP :
            return OK;

          case vemCOMMA_EXP :
            return check_child_validation( mexp->exp().back() );
            break;

          case vemEVENT_EXP :
          case vemPORT_EXP  :
          case vemPORT_INST_EXP :
          default :
            veri_err->append_err( ERR_ERROR, child->line(),
                          child->col(), vCHILD_TYPE_QUES_EXP );
            return vCHILD_TYPE_QUES_EXP;
        }
        break;
      }

      default:
        break;
    }
    return OK;
  }
  veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_QUES_EXP );
  return vCHILD_TYPE_QUES_EXP;
}

int
VeriQuesExp::check_child_validation( void )
{
  int re;
  re = check_child_validation( _ques );
  if( re != OK )
    return re;
  
  re = check_child_validation( _choice1 );
  if( re != OK )
    return re;

  return check_child_validation( _choice2 );
}

int
VeriQuesExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_condition( mstack );
      set_pstatus( vIF_S );
      return re;
      break;

    case vIF_S :
      re = choose_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return get_answer( );
      break;
  }
}

int
VeriQuesExp::push_condition( NodeVec& mstack )
{
  mstack.push_back( this );

  //_ques->clean_up( );
  _ques->set_is_trash( is_trash( ) );
  _ques->set_scope( scope( ) );
  static_cast<VeriExp *>(_ques)->set_signed( is_signed() );
  mstack.push_back( _ques );

  return OK;
}

int
VeriQuesExp::choose_children( NodeVec& mstack )
{
  VeriExp *ques = static_cast<VeriExp *>(_ques);
  
  if( ques->calc_value( ) != NULL )
  {
    VeriNum *num = new VeriNum( veri_parser->next_id(), ques->calc_value() );
    veri_parser->insert_node( num );
    num->set_father( this );
    num->set_scope( scope( ) );
    num->unary_operation( vtUOR );
    int re = num->one_bit_value( );

    switch( re )
    {
      case V0 :
        mstack.push_back( this );

        //_choice2->clean_up( );
        _choice2->set_is_trash( is_trash( ) );
        _choice2->set_scope( scope( ) );
        static_cast<VeriExp *>(_choice2)->set_signed( is_signed() );
        mstack.push_back( _choice2 );
        _choice = 2;

        return OK;
        break;

      case V1 :
        mstack.push_back( this );

        //_choice1->clean_up( );
        _choice1->set_is_trash( is_trash( ) );
        _choice1->set_scope( scope( ) );
        static_cast<VeriExp *>(_choice1)->set_signed( is_signed() );
        mstack.push_back( _choice1 );
        _choice = 1;

        return OK;
        break;

      default:
        break;
    }
  }

  mstack.push_back( this );

  //_choice2->clean_up( );
  _choice2->set_is_trash( is_trash( ) );
  _choice2->set_scope( scope( ) );
  static_cast<VeriExp *>(_choice2)->set_signed( is_signed() );
  mstack.push_back( _choice2 );

  //_choice1->clean_up( );
  _choice1->set_is_trash( is_trash( ) );
  _choice1->set_scope( scope( ) );
  static_cast<VeriExp *>(_choice1)->set_signed( is_signed() );
  mstack.push_back( _choice1 );

  return OK;
}

int
VeriQuesExp::get_answer( void )
{
  VeriExp * choice = NULL;
  switch( _choice )
  {
    case 1 :
    {
      choice = static_cast<VeriExp *>(_choice1);
      VeriNum *num = new VeriNum( veri_parser->next_id(),
                                  choice->calc_value( ) );
      veri_parser->insert_node( num );
      num->set_father( this );
      num->set_scope( scope( ) );
      set_calc_value( num );
      break;
    }

    case 2 :
    {
      choice = static_cast<VeriExp *>(_choice2);
      VeriNum *num = new VeriNum( veri_parser->next_id(),
                                  choice->calc_value( ) );
      veri_parser->insert_node( num );
      num->set_father( this );
      num->set_scope( scope( ) );
      set_calc_value( num );
      break;
    }

    case 0 :
    default:
      break;
  }
  return OK;
}

int
VeriQuesExp::push_for_replacement( NodeVec& mstack )
{
  mstack.push_back( _ques );
  mstack.push_back( _choice1 );
  mstack.push_back( _choice2 );
  return OK;
}

int
VeriQuesExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriQuesExp *tmp = new VeriQuesExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl;

  tmp->_choice2 = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );
  itl = inl;

  tmp->_choice1 = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );
  itl = inl;

  tmp->_ques = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

    /***********************
     * Multiple Expression *
     ***********************/

void
VeriMultiExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );
}

int
VeriMultiExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;
  NodeList::iterator it = _exp.begin();
  for( ; it != _exp.end(); ++it )
  {
    mqueue.push( *it );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriMultiExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_multi_exp_type( _multi_type );
  ofile << dot_str_tail( tok_type() );

  // connection
  NodeList::iterator it = _exp.begin();
  for( ; it != _exp.end(); ++it )
    ofile << dot_str_conn( idx(), *it );

  return OK;
}

int
VeriMultiExp::check_child_validation( VeriNode *child )
{
  if( child != NULL && child->tok_type() == vtEXP )
  {
    switch( child->detail_type() )
    {
      case veEXP_DUMMY_PORT :
      case veEXP_NAMED_PARAM :
        _multi_type = vemPORT_INST_EXP;
        break;

      case veEXP_EVENT   :
      case veEXP_RANGE   :
      case veEXP_PORT_REF :
        veri_err->append_err( ERR_ERROR, child->line(),
                     child->col(), vCHILD_TYPE_MULTI_EXP );
        return vCHILD_TYPE_MULTI_EXP;

      case veEXP_CALLER :
      {
        /* task caller shall not be here */;
        VeriCallerExp *cexp = static_cast<VeriCallerExp *>(child);
        if( cexp->call_type() == vtTASK_CALL )
        {
          veri_err->append_err( ERR_ERROR, child->line(),
                     child->col(), vCHILD_TYPE_MULTI_EXP );
          return vCHILD_TYPE_MULTI_EXP;
        }
        break;
      }

      case veEXP_MULTI :
      {
        VeriMultiExp *mexp = static_cast<VeriMultiExp *>(child);
        switch( mexp->multi_type() )
        {
          case vemCONCAT_EXP :
            return OK;

          case vemCOMMA_EXP :
            return check_child_validation( mexp->exp().back() );
            break;

          case vemEVENT_EXP :
          case vemPORT_EXP  :
          case vemPORT_INST_EXP :
          default :
            veri_err->append_err( ERR_ERROR, child->line(),
                         child->col(), vCHILD_TYPE_MULTI_EXP );
            return vCHILD_TYPE_MULTI_EXP;
        }
        break;
      }

      default:
        break;
    }
    return OK;
  }
  veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_MULTI_EXP );
  return vCHILD_TYPE_MULTI_EXP;
}

int
VeriMultiExp::check_child_validation( void )
{
  if( _exp.empty() )
  {
    veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_MULTI_EXP );
    return vCHILD_TYPE_MULTI_EXP;
  }

  NodeList::iterator it = _exp.begin();
  int re;

  switch( _multi_type )
  {
    case vemCOMMA_EXP :
      for( ; it != _exp.end(); ++it )
      {
        if( *it == NULL || (*it)->tok_type() != vtEXP )
        {
          veri_err->append_err( ERR_ERROR, (*it)->line(),
                      (*it)->col(), vCHILD_TYPE_MULTI_EXP );
          return vCHILD_TYPE_MULTI_EXP;
        }
        else
        {
          re = check_child_validation( *it );
          if( re != OK )
            return re;
        }
      }
      break;

    case vemEVENT_EXP :
      for( ; it != _exp.end(); ++it )
      {
        if( *it == NULL || (*it)->tok_type() != vtEXP )
          return vCHILD_TYPE_MULTI_EXP;
        else
        {
          switch( (*it)->detail_type() )
          {
            case veEXP_PRI    :
            case veEXP_EVENT  :
            case veEXP_SIGN   :
            case veEXP_UNSIGN :
              break;

            default:
              veri_err->append_err( ERR_ERROR, (*it)->line(),
                        (*it)->col(), vCHILD_TYPE_MULTI_EXP );
              return vCHILD_TYPE_MULTI_EXP;
          }
        }
      }
      return OK;
      break;

    case vemCONCAT_EXP :
      for( ; it != _exp.end(); ++it )
      {
        if( *it == NULL || (*it)->tok_type() != vtEXP )
        {
          veri_err->append_err( ERR_ERROR, (*it)->line(),
                        (*it)->col(), vCHILD_TYPE_MULTI_EXP );
          return vCHILD_TYPE_MULTI_EXP;
        }
        else
        {
          switch( (*it)->detail_type() )
          {
            case veEXP_MULTI:
            {
              VeriMultiExp *mexp = static_cast<VeriMultiExp *>(*it);
              if( mexp->multi_type() != vemCONCAT_EXP )
              {
                veri_err->append_err( ERR_ERROR, (*it)->line(),
                           (*it)->col(), vCHILD_TYPE_MULTI_EXP );
                return vCHILD_TYPE_MULTI_EXP;
              }
              break;
            }

            case veEXP_PRI    :  case veEXP_ARRAY  : case veEXP_UNA :
            case veEXP_SIGN   :  case veEXP_BIN    : case veEXP_MINTY :
            case veEXP_UNSIGN :  case veEXP_QUES   :
              break;

            default:
              veri_err->append_err( ERR_ERROR, (*it)->line(),
                           (*it)->col(), vCHILD_TYPE_MULTI_EXP );
              return vCHILD_TYPE_MULTI_EXP;
          }
        }
      }
      break;

    case vemPORT_EXP :
      for( ; it != _exp.end(); ++it )
      {
        if( *it == NULL || (*it)->detail_type() != veEXP_NAMED_PARAM )
        {
          veri_err->append_err( ERR_ERROR, (*it)->line(),
                           (*it)->col(), vCHILD_TYPE_MULTI_EXP );
          return vCHILD_TYPE_MULTI_EXP;
        }
      }
      break;

    case vemPORT_INST_EXP :
      for( ; it != _exp.end(); ++it )
      {
        if( *it == NULL || (*it)->tok_type() != vtEXP )
          return vCHILD_TYPE_MULTI_EXP;
        else
        {
          switch( (*it)->detail_type() )
          {
            case veEXP_PRI    :  case veEXP_UNA :  case veEXP_QUES :
            case veEXP_ARRAY  :  case veEXP_BIN :  case veEXP_MINTY :
            case veEXP_SIGN   :
            case veEXP_UNSIGN :
            case veEXP_DUMMY_PORT : case veEXP_NAMED_PARAM :
              break;

            default:
              veri_err->append_err( ERR_ERROR, (*it)->line(),
                        (*it)->col(), vCHILD_TYPE_MULTI_EXP );
              return vCHILD_TYPE_MULTI_EXP;
          }
        }
      }
      return OK;
      break;

    default:
      veri_err->append_err( ERR_ERROR, (*it)->line(),
                        (*it)->col(), vCHILD_TYPE_MULTI_EXP );
      return vCHILD_TYPE_MULTI_EXP;
      break;
  }
  return OK;
}

int
VeriMultiExp::check_last_child_validation( void )
{
  if( _exp.empty() )
  {
    veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_MULTI_EXP );
    return vCHILD_TYPE_MULTI_EXP;
  }

  VeriNode *last = _exp.back();
  if( last == NULL || last->tok_type() != vtEXP )
  {
    veri_err->append_err( ERR_ERROR, last->line(),
                        last->col(), vCHILD_TYPE_MULTI_EXP );
    return vCHILD_TYPE_MULTI_EXP;
  }

  int re;
  switch( _multi_type )
  {
    case vemCOMMA_EXP :
      re = check_child_validation( last );
      if( re != OK )
        return re;
    break;

    case vemEVENT_EXP :
      if(    last->detail_type() != veEXP_EVENT
          && last->detail_type() != veEXP_PRI )
      {
        veri_err->append_err( ERR_ERROR, last->line(),
                        last->col(), vCHILD_TYPE_MULTI_EXP );
        return vCHILD_TYPE_MULTI_EXP;
      }
      break;

    case vemCONCAT_EXP :
      switch( last->detail_type() )
      {
        case veEXP_MULTI:
        {
          VeriMultiExp *mexp = static_cast<VeriMultiExp *>(last);
          if( mexp->multi_type() != vemCONCAT_EXP )
          {
            veri_err->append_err( ERR_ERROR, last->line(),
                            last->col(), vCHILD_TYPE_MULTI_EXP );
            return vCHILD_TYPE_MULTI_EXP;
          }
          break;
        }

        case veEXP_PRI  : case veEXP_UNA   : case veEXP_BIN  :
        case veEXP_QUES : case veEXP_ARRAY : case veEXP_SIGN :
        case veEXP_UNSIGN : case veEXP_MINTY :
          break;

        default:
          veri_err->append_err( ERR_ERROR, line(),
                                col(), vCHILD_TYPE_MULTI_EXP );
          return vCHILD_TYPE_MULTI_EXP;
      }
      break;

    case vemPORT_EXP :
      if( last->detail_type() != veEXP_NAMED_PARAM )
      {
        veri_err->append_err( ERR_ERROR, last->line(),
                            last->col(), vCHILD_TYPE_MULTI_EXP );
        return vCHILD_TYPE_MULTI_EXP;
      }
      break;

    case vemPORT_INST_EXP :
      switch( last->detail_type() )
      {
        case veEXP_PRI    : case veEXP_BIN  : case veEXP_UNA :
        case veEXP_SIGN   : case veEXP_QUES : case veEXP_MINTY :
        case veEXP_UNSIGN :
        case veEXP_DUMMY_PORT : case veEXP_NAMED_PARAM :
          break;

        default:
          veri_err->append_err( ERR_ERROR, last->line(),
                    last->col(), vCHILD_TYPE_MULTI_EXP );
          return vCHILD_TYPE_MULTI_EXP;
      }
      break;

    default:
      veri_err->append_err( ERR_ERROR, line(),
                            col(), vCHILD_TYPE_MULTI_EXP );
      return vCHILD_TYPE_MULTI_EXP;
      break;
  }
  return OK;
}

int
VeriMultiExp::check_pure_event_multi( void )
{
  if( _exp.empty() )
  {
    veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_MULTI_EXP );
    return vCHILD_TYPE_MULTI_EXP;
  }

  NodeList::iterator it = _exp.begin();
  for( ; it != _exp.end(); ++it )
  {
    if(         (*it) == NULL
        || (    (*it)->detail_type() != veEXP_EVENT
             && (*it)->detail_type() != veEXP_PRI ) )
    {
      veri_err->append_err( ERR_ERROR, (*it)->line(),
                          (*it)->col(), vCHILD_TYPE_MULTI_EXP );
      return vCHILD_TYPE_MULTI_EXP;
    }
  }
  return OK;
}

int
VeriMultiExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return get_answer( );
      break;
  }
}

int
VeriMultiExp::push_children( NodeVec& mstack )
{
  NodeList::reverse_iterator it = _exp.rbegin();
  for( ; it != _exp.rend(); ++it )
  {
    //(*it)->clean_up( );
    (*it)->set_is_trash( is_trash( ) );
    (*it)->set_scope( scope( ) );
    static_cast<VeriExp *>(*it)->set_signed( is_signed() );
    mstack.push_back( *it );
  }
  return OK;
}

int
VeriMultiExp::get_answer( void )
{
  VeriExp *exp = static_cast<VeriExp *>(_exp.back());
  if( exp->calc_value( ) != NULL )
  {
    VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value() );
    veri_parser->insert_node( num );
    num->set_father( this );
    num->set_scope( scope( ) );
    set_calc_value( num );
  }
  return OK;
}

int
VeriMultiExp::push_for_replacement( NodeVec& mstack )
{
  NodeList::iterator it = _exp.begin();
  for( ; it != _exp.end(); ++it )
    mstack.push_back( *it );
  return OK;
}

int
VeriMultiExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriMultiExp *tmp = new VeriMultiExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl;
  NodeList::iterator it = _exp.begin();
  for( ; it != _exp.end(); ++it )
  {
    tmp->_exp.push_front( *(--itl) );
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  *inl = tmp;
  return OK;
}

    /********************
     * Delay Expression *
     ********************/

void
VeriDelayExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );

  memset( _delay, 0, sizeof(int) * 9 );
  _not_calc = IS_TRUE;
}

int
VeriDelayExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front( ) + 1;

  if( _delay1 != NULL )
  {
    mqueue.push( _delay1 );
    iqueue.push( depth );
  }
  if( _delay2 != NULL )
  {
    mqueue.push( _delay2 );
    iqueue.push( depth );
  }
  if( _delay3 != NULL )
  {
    mqueue.push( _delay3 );
    iqueue.push( depth );
  }
  return OK;
}

int
VeriDelayExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _delay1 );
  ofile << dot_str_conn( idx(), _delay2 );
  ofile << dot_str_conn( idx(), _delay3 );

  return OK;
}

int
VeriDelayExp::check_child_validation( VeriNode *child )
{
  if( child == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_DELAY_EXP );
    return vCHILD_TYPE_DELAY_EXP;
  }

  switch( child->detail_type() )
  {
    case veEXP_PRI :
    case veEXP_ARRAY :
    case veEXP_SIGN :
    case veEXP_UNSIGN :
      break;

    default:
      veri_err->append_err( ERR_ERROR, child->line(),
                          child->col(), vCHILD_TYPE_DELAY_EXP );
      return vCHILD_TYPE_DELAY_EXP;
      break;
  }
  return OK;
}

int
VeriDelayExp::check_child_validation( void )
{
  if( _delay1 == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_DELAY_EXP );
    return vCHILD_TYPE_DELAY_EXP;
  }

  int re;

  re = check_child_validation( _delay1 );
  if( re != OK )
    return re;

  if( _delay2 != NULL )
  {
    re = check_child_validation( _delay2 );
    if( re != OK )
      return re;
  }

  if( _delay3 != NULL )
    re = check_child_validation( _delay3 );

  return re;
}

int
VeriDelayExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return get_delay_val( );
      break;
  }
}

int
VeriDelayExp::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  if( _delay3 != NULL )
  {
    //_delay3->clean_up( );
    _delay3->set_is_trash( is_trash( ) );
    _delay3->set_scope( scope( ) );
    static_cast<VeriExp *>(_delay3)->set_signed( is_signed() );
    mstack.push_back( _delay3 );
  }

  if( _delay2 != NULL )
  {
    //_delay2->clean_up( );
    _delay2->set_is_trash( is_trash( ) );
    _delay2->set_scope( scope( ) );
    static_cast<VeriExp *>(_delay2)->set_signed( is_signed() );
    mstack.push_back( _delay2 );
  }

  if( _delay1 != NULL )
  {
    //_delay1->clean_up( );
    _delay1->set_is_trash( is_trash( ) );
    _delay1->set_scope( scope( ) );
    static_cast<VeriExp *>(_delay1)->set_signed( is_signed() );
    mstack.push_back( _delay1 );
  }

  return OK;
}

int
VeriDelayExp::delay_num( void ) const
{
  int num = 0;
  if( _delay1 != NULL ) ++num; else return num;
  if( _delay2 != NULL ) ++num; else return num;
  if( _delay3 != NULL ) ++num; else return num;

  return num;
}

/*
 * assuming delay_exp is not null
 */
int
VeriDelayExp::get_one_val( VeriExp *delay_exp, int idx )
{
  if( delay_exp->detail_type( ) == veEXP_MINTY )
  {
    VeriMintypmaxExp *pmax
      = static_cast<VeriMintypmaxExp *>(delay_exp);

    if( pmax->not_calc( ) == IS_TRUE )
    {
      _not_calc = IS_TRUE;
      veri_err->append_err( ERR_ERROR, delay_exp, vSEC_DELAY_NOT_CALC );
      return vSEC_DELAY_NOT_CALC;
    }
    else
    {
      _delay[(idx-1)*3+0] = pmax->re1();
      _delay[(idx-1)*3+1] = pmax->re2();
      _delay[(idx-1)*3+2] = pmax->re3();
    }
  }
  else
  {
    if( delay_exp->calc_value( ) == NULL )
    {
      _not_calc = IS_TRUE;
      veri_err->append_err( ERR_ERROR, delay_exp, vSEC_DELAY_NOT_CALC );
      return vSEC_DELAY_NOT_CALC;
    }
    else
      _delay[(idx-1)*3+0] = delay_exp->calc_value()->get_first_int();
  }

  return OK;
}

int
VeriDelayExp::get_delay_val( void )
{
  VeriExp * delay1 = static_cast<VeriExp *>(_delay1);
  VeriExp * delay2 = static_cast<VeriExp *>(_delay2);
  VeriExp * delay3 = static_cast<VeriExp *>(_delay3);

  _not_calc = IS_FALSE;
  if( delay1 != NULL ) get_one_val( delay1, 1 );
  if( delay2 != NULL ) get_one_val( delay2, 2 );
  if( delay3 != NULL ) get_one_val( delay2, 3 );

  return OK;
}

int
VeriDelayExp::push_for_replacement( NodeVec& mstack )
{
  if( _delay1 != NULL ) mstack.push_back( _delay1 );
  if( _delay2 != NULL ) mstack.push_back( _delay2 );
  if( _delay3 != NULL ) mstack.push_back( _delay3 );
  return OK;
}

int
VeriDelayExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriDelayExp *tmp = new VeriDelayExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl;

  if( _delay3 != NULL )
  {
    tmp->_delay3 = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  if( _delay2 != NULL )
  {
    tmp->_delay2 = *(--itl);
    (*itl)->set_father( tmp );
    node_list.erase( itl );
    itl = inl;
  }

  tmp->_delay1 = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

    /********************
     * Drive Expression *
     ********************/

void
VeriDriveExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );
}

int
VeriDriveExp::push_dot_children( NodeQueue& /*mqueue*/, IQueue& /*iqueue*/ )
{
  return OK;
}

int
VeriDriveExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_drive_from( _drive_from );
  ofile << dot_str_drive_to( _drive_to );
  ofile << dot_str_tail( tok_type() );

  // connection
  
  return OK;
}

int
VeriDriveExp::check_child_validation( void )
{
  if( _drive_from == vpHIGHZ1 && _drive_to == vpHIGHZ0 )
  {
    veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_DRIVE_EXP );
    return vCHILD_TYPE_DRIVE_EXP;
  }

  if( _drive_from == vpHIGHZ0 && _drive_to == vpHIGHZ1 )
  {
    veri_err->append_err( ERR_ERROR, line(), col(), vCHILD_TYPE_DRIVE_EXP );
    return vCHILD_TYPE_DRIVE_EXP;
  }
  return OK;
}

int
VeriDriveExp::process_node_2nd( NodeVec& /*mstack*/, String& /*help_info*/ )
{
  return OK;
}

int
VeriDriveExp::push_for_replacement( NodeVec& /*mstack*/ )
{
  return OK;
}

int
VeriDriveExp::replace_node( Uint idx, NodeList& /*node_list*/,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriDriveExp *tmp = new VeriDriveExp( idx, this );

  // begin replacement

  *inl = tmp;
  return OK;
}

    /*********************
     * Signed Expression *
     *********************/

void
VeriSignExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );
}

int
VeriSignExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  mqueue.push( _exp );
  iqueue.push( iqueue.front() + 1 );

  if( calc_value() != NULL )
  {
    mqueue.push( calc_value() );
    iqueue.push( iqueue.front() + 1 );
  }
  return OK;
}

int
VeriSignExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _exp );

  if( calc_value() != NULL )
    ofile << dot_str_conn( idx(), calc_value() );

  return OK;
}

int
VeriSignExp::check_child_validation( void )
{
  if( _exp == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_SIGN_FUNC_EXP );
    return vCHILD_TYPE_SIGN_FUNC_EXP;
  }
   
  if( _exp->tok_type() != vtEXP )
  {
    veri_err->append_err( ERR_ERROR, _exp->line(),
                          _exp->col(), vCHILD_TYPE_SIGN_FUNC_EXP );
    return vCHILD_TYPE_SIGN_FUNC_EXP;
  }
  return OK;
}

int
VeriSignExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return process_sign( );
      break;
  }
}

int
VeriSignExp::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  //_exp->clean_up( );
  _exp->set_is_trash( is_trash( ) );
  _exp->set_scope( scope( ) );
  static_cast<VeriExp *>(_exp)->set_signed( is_signed() );
  mstack.push_back( _exp );
  return OK;
}

int
VeriSignExp::process_sign( void )
{
  VeriExp *exp = static_cast<VeriExp *>(_exp);
  if( exp->calc_value() != NULL )
  {
    VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value() );
    veri_parser->insert_node( num );
    num->set_signed( );
    num->set_father( this );
    num->set_scope( scope( ) );
    set_calc_value( num );
  }
  return OK;
}

int
VeriSignExp::push_for_replacement( NodeVec& mstack )
{
  mstack.push_back( _exp );
  return OK;
}

int
VeriSignExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriSignExp *tmp = new VeriSignExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl;
  tmp->_exp = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

    /***********************
     * Unsigned Expression *
     ***********************/

void
VeriUnsignExp::clean_up( void )
{
  set_pstatus( vSTART_S );
  if( calc_value() != NULL )
    veri_parser->toss_node( calc_value()->idx() );
  set_calc_value( NULL );
}

int
VeriUnsignExp::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  mqueue.push( _exp );
  iqueue.push( iqueue.front() + 1 );

  if( calc_value() != NULL )
  {
    mqueue.push( calc_value() );
    iqueue.push( iqueue.front() + 1 );
  }
  return OK;
}

int
VeriUnsignExp::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  ofile << dot_str_detail_type( detail_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _exp );

  if( calc_value() != NULL )
    ofile << dot_str_conn( idx(), calc_value() );

  return OK;
}

int
VeriUnsignExp::check_child_validation( void )
{
  if( _exp == NULL )
  {
    veri_err->append_err( ERR_ERROR, line(),
                          col(), vCHILD_TYPE_UNSIGN_FUNC_EXP );
    return vCHILD_TYPE_UNSIGN_FUNC_EXP;
  }
   
  if( _exp->tok_type() != vtEXP )
  {
    veri_err->append_err( ERR_ERROR, _exp->line(),
                          _exp->col(), vCHILD_TYPE_UNSIGN_FUNC_EXP );
    return vCHILD_TYPE_UNSIGN_FUNC_EXP;
  }
  return OK;
}

int
VeriUnsignExp::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_children( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return process_unsign( );
      break;
  }
}

int
VeriUnsignExp::push_children( NodeVec& mstack )
{
  mstack.push_back( this );

  //_exp->clean_up( );
  _exp->set_is_trash( is_trash( ) );
  _exp->set_scope( scope( ) );
  static_cast<VeriExp *>(_exp)->set_signed( is_signed() );
  mstack.push_back( _exp );
  return OK;
}

int
VeriUnsignExp::process_unsign( void )
{
  VeriExp *exp = static_cast<VeriExp *>(_exp);
  if( exp->calc_value() != NULL )
  {
    VeriNum *num = new VeriNum( veri_parser->next_id(), exp->calc_value() );
    veri_parser->insert_node( num );
    num->set_unsigned( );
    num->set_father( this );
    num->set_scope( scope( ) );
    set_calc_value( num );
  }
  return OK;
}

int
VeriUnsignExp::push_for_replacement( NodeVec& mstack )
{
  mstack.push_back( _exp );
  return OK;
}

int
VeriUnsignExp::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriUnsignExp *tmp = new VeriUnsignExp( idx, this );

  // begin replacement
  NodeList::iterator itl = inl;
  tmp->_exp = *(--itl);
  (*itl)->set_father( tmp );
  node_list.erase( itl );

  *inl = tmp;
  return OK;
}

};
