/*
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : veri_module.cpp
 * Summary  : verilog syntax analyzer :: module functions
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2012.11.20
 */

#ifdef DEBUG_PARSER
#include <iostream>
#endif

#include "general_err.hpp"
#include "veri_err_token.hpp"

#include "veri_num.hpp"

#include "veri_var.hpp"
#include "veri_exp.hpp"
#include "veri_decl.hpp"
#include "veri_inst.hpp"
#include "veri_tfmg.hpp"
#include "veri_stmt.hpp"

#include "veri_module.hpp"
#include "veri_parser.hpp"

namespace rtl
{

void
VeriMod::init( int symbol_table_size )
{
  _symbols = new SymbolTable;
  _symbols->init( symbol_table_size, _mod_name );
  set_scope( _symbols );
}

void
VeriMod::clean_up( void )
{
  set_pstatus( vSTART_S );
  _symbols->clean_up( );
  if( _symbols != NULL )
    delete _symbols;
  _symbols = NULL;

  _act_ports.clear( );
  _act_port_num = 0;
}

void
VeriMod::clear( void )
{
  set_pstatus( vSTART_S );
  if( _symbols != NULL )
    delete _symbols;
  _symbols = NULL;

  _act_ports.clear( );
  _act_port_num = 0;
}

int
VeriMod::push_dot_children( NodeQueue& mqueue, IQueue& iqueue )
{
  int depth = iqueue.front() + 1;

  if( _param != NULL )
  {
    mqueue.push( _param );
    iqueue.push( depth );
  }

  if( _port != NULL )
  {
    mqueue.push( _port );
    iqueue.push( depth );
  }

  if( _item != NULL )
  {
    mqueue.push( _item );
    iqueue.push( depth );
  }

  return OK;
}

int
VeriMod::pretty_dot_label( Ofstream& ofile, int depth )
{
  // rank
  ofile << dot_str_rank( idx(), depth );
  
  // content
  ofile << dot_str_head( idx() );
  ofile << dot_str_tok_type( tok_type() );
  if( scope() != NULL )  ofile << dot_str_scope( scope()->depth() );
  ofile << dot_str_name( _mod_name );
  ofile << dot_str_tail( tok_type() );

  // connection
  ofile << dot_str_conn( idx(), _param );
  ofile << dot_str_conn( idx(), _port  );
  ofile << dot_str_conn( idx(), _item  );

  return OK;
}

int
VeriMod::check_child_validation( void )
{
  if( _port->detail_type( ) == veEXP_MULTI )
  {
    VeriMultiExp *port_list = static_cast<VeriMultiExp *>(_port);
    NodeList& exp_list = port_list->exp();

    NodeList::iterator iport = exp_list.begin();

    for( ; iport != exp_list.end(); ++iport )
    {
      if( (*iport) == NULL )
      {
        veri_err->append_err( ERR_ERROR, line(), col(),
                          vCHILD_TYPE_MODULE_PORT_ID );
        return vCHILD_TYPE_MODULE_PORT_ID;
      }

      if( (*iport)->detail_type( ) != veEXP_PRI )
      {
        veri_err->append_err( ERR_ERROR, (*iport)->line(), (*iport)->col(),
                          vCHILD_TYPE_MODULE_PORT_ID );
        return vCHILD_TYPE_MODULE_PORT_ID;
      }
    }
  }
  else if( _port->detail_type( ) == vsDECL_PORT_DECL_LIST )
  {
    VeriPortDeclList *port_list = static_cast<VeriPortDeclList *>(_port);
    NodeList & port_decl_list = port_list->port_list( );

    NodeList::iterator idecl = port_decl_list.begin( );
    NodeList::iterator iport;

    for( ; idecl != port_decl_list.end( ); ++idecl )
    {
      if( (*idecl) == NULL )
      {
        veri_err->append_err( ERR_ERROR, line(), col(),
                              vCHILD_TYPE_MODULE_PORT_ID );
        return vCHILD_TYPE_MODULE_PORT_ID;
      }
      
      if( (*idecl)->detail_type( ) != vsDECL_VAR )
      {
        veri_err->append_err( ERR_ERROR, (*idecl)->line(), (*idecl)->col(),
                          vCHILD_TYPE_MODULE_PORT_ID );
        return vCHILD_TYPE_MODULE_PORT_ID;
      }
    }
  }

  return OK;
}

int
VeriMod::process_node_2nd( NodeVec& mstack, String& /*help_info*/ )
{
  int re;
  switch( pstatus( ) )
  {
    case vSTART_S :
      re = push_everything( mstack );
      set_pstatus( vEND_S );
      return re;
      break;

    default:
      return map_ports( );
      break;
  }
}

int
VeriMod::push_everything( NodeVec& mstack )
{
  if( father() != NULL )
    set_scope( scope()->append_scope( _mod_name, vMOD_SCOPE ) );

  scope()->insert( _mod_name, this );

  if( _port->detail_type( ) == vsDECL_PORT_DECL_LIST )
  {
    if( _item != NULL )
    {
      _item->set_scope( scope( ) );
      mstack.push_back( _item );
    }

    if( _port != NULL )
    {
      _port->set_is_decl( IS_TRUE );
      _port->set_is_trash( IS_TRUE );
      _port->set_scope( scope( ) );
      mstack.push_back( _port );
    }

    if( _param != NULL )
    {
      _param->set_is_decl( IS_TRUE );
      _param->set_is_trash( IS_TRUE );
      _param->set_scope( scope( ) );
      mstack.push_back( _param );
    }

  }
  else if( _port->detail_type( ) == veEXP_MULTI )
  {
    if( _port != NULL )
    {
      _port->set_is_trash( IS_TRUE );
      _port->set_scope( scope( ) );
      mstack.push_back( _port );
    }

    if( _item != NULL )
    {
      _item->set_scope( scope( ) );
      mstack.push_back( _item );
    }

    if( _param != NULL )
    {
      _param->set_is_decl( IS_TRUE );
      _param->set_is_trash( IS_TRUE );
      _param->set_scope( scope( ) );
      mstack.push_back( _param );
    }
  }

  return OK;
}

int
VeriMod::check_type_ref( VeriExp *exp, VeriVar *port )
{
  int re = OK;

  IVec dummy_dimen;

  VeriVar    *var     = NULL;
  VeriInt    *int_var = NULL;

  switch( exp->detail_type( ) )
  {
    case veEXP_PRI :
    {
      VeriPrimaryExp *pexp = static_cast<VeriPrimaryExp *>(exp);
      if( pexp->var( ) == NULL )
      {
        if( pexp->pri_type( ) == veNUM && port->range_depth( ) != 0 )
        {
          veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
          return vSEC_MOD_PORT_MISMATCH;
        }
        return OK;
      }

      switch( pexp->var()->tok_type( ) )
      {
        case vtVAR :
          var = static_cast<VeriVar *>(pexp->var());
          if( var->range_depth() != port->range_depth() )
          {
            veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
            return vSEC_MOD_PORT_MISMATCH;
          }
          else
            return OK;
          break;

        case vtTFVAR :
          if( 0 != port->range_depth() )
          {
            veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
            return vSEC_MOD_PORT_MISMATCH;
          }
          else
            return OK;
          break;


        case vtPARAM :
          if( 0 != port->range_depth() )
          {
            veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
            return vSEC_MOD_PORT_MISMATCH;
          }
          else
            return OK;
          break;

        case vtINT :
          int_var = static_cast<VeriInt *>(pexp->var());
          if( int_var->range_depth() != port->range_depth() )
          {
            veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
            return vSEC_MOD_PORT_MISMATCH;
          }
          else
            return OK;
          break;

        case vtGENVAR :
          if( 0 != port->range_depth() )
          {
            veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
            return vSEC_MOD_PORT_MISMATCH;
          }
          else
            return OK;
          break;

        default:
          break;
      }
      break;
    
    }

    case veEXP_UNA :
    case veEXP_BIN :
    case veEXP_SIGN :
    case veEXP_UNSIGN :
      if( port->range_depth( ) != 0 )
      {
        veri_err->append_err( ERR_ERROR, exp, vSEC_MOD_PORT_MISMATCH );
        return vSEC_MOD_PORT_MISMATCH;
      }
      return OK;
      break;

    case veEXP_ARRAY :
    {
      VeriArrayExp *aexp = static_cast<VeriArrayExp *>(exp);
      VeriPrimaryExp *pexp = static_cast<VeriPrimaryExp *>(aexp->id());

      if( pexp->var() != NULL )
        return vSEC_MOD_PORT_MISMATCH;

      if( aexp->range_depth( ) != port->range_depth( ) )
      {
        veri_err->append_err( ERR_ERROR, exp, vSEC_MOD_PORT_MISMATCH );
        return vSEC_MOD_PORT_MISMATCH;
      }
      return OK;
      break;
    }

    case veEXP_NAMED_PARAM :
    {
      VeriNamedParamExp * nexp = static_cast<VeriNamedParamExp *>(exp);
      if( port->name( ) != nexp->ref() )
      {
        veri_err->append_err( ERR_ERROR, exp, vSEC_MOD_PORT_MISMATCH );
        return vSEC_MOD_PORT_MISMATCH;
      }
      return OK;
      break;

      VeriExp *inner_exp = static_cast<VeriExp *>(nexp->port_exp());
      return check_type_ref( inner_exp, port );
      break;
    }

    case veEXP_CALLER :
    {
      VeriCallerExp *cexp = static_cast<VeriCallerExp *>(exp);
      if( cexp->call_type() == vtTASK_CALL )
      {
        veri_err->append_err( ERR_ERROR, exp, vSEC_MOD_PORT_MISMATCH );
        return vSEC_MOD_PORT_MISMATCH;
      }
      else
      {
        if( port->range_depth() != 0 )
        {
          veri_err->append_err( ERR_ERROR, exp, vSEC_MOD_PORT_MISMATCH );
          return vSEC_MOD_PORT_MISMATCH;
        }
      }
      break;
    }

    case veEXP_MINTY :
    {
      VeriMintypmaxExp *pmax = static_cast<VeriMintypmaxExp *>(exp);
      VeriExp *exp1 = static_cast<VeriExp *>(pmax->exp_child1( ) );
      VeriExp *exp2 = static_cast<VeriExp *>(pmax->exp_child2( ) );
      VeriExp *exp3 = static_cast<VeriExp *>(pmax->exp_child3( ) );

      re = check_type_ref( exp1, port );
      if( re != OK )
        return re;

      re = check_type_ref( exp2, port );
      if( re != OK )
        return re;
         
      return check_type_ref( exp3, port );
      break;
    }

    case veEXP_QUES :
    {
      VeriQuesExp *ques = static_cast<VeriQuesExp *>(exp);
      VeriExp  *choice1 = static_cast<VeriExp *>(ques->choice1( ) );
      VeriExp  *choice2 = static_cast<VeriExp *>(ques->choice2( ) );

      re = check_type_ref( choice1, port );
      if( re != OK )
        return re;
         
      return check_type_ref( choice2, port );
      break;
    }

    case veEXP_MULTI :
    {
      VeriMultiExp *mexp = static_cast<VeriMultiExp *>(exp);
      NodeList & exp_list = mexp->exp();
      NodeList::iterator it = exp_list.begin();
      for( ; it != exp_list.end(); ++it )
      {
        re = check_type_ref( static_cast<VeriExp *>(*it), port );
        if( re != OK )
          return re;
      }
      return OK;
      break;
    }

    case veEXP_MCONCAT :
      /* TODO */;
      return OK;
      break;

    default:
      return OK;
      break;
  }
  return OK;
}

int
VeriMod::prep_ports_for_map( )
{
  if( _port->detail_type( ) == veEXP_MULTI )
  {
    VeriMultiExp *port_list = static_cast<VeriMultiExp *>(_port);
    NodeList& exp_list = port_list->exp();

    NodeList::iterator iport = exp_list.begin();

    VeriPrimaryExp *pexp = NULL;
    for( ; iport != exp_list.end(); ++iport )
    {
      pexp = static_cast<VeriPrimaryExp *>(*iport);
      if( pexp->var( ) != NULL )
        _act_ports.push_back( pexp->var( ) );
    }
    return OK;
  }
  else if( _port->detail_type( ) == vsDECL_PORT_DECL_LIST )
  {
    VeriPortDeclList *port_list = static_cast<VeriPortDeclList *>(_port);
    NodeList & port_decl_list = port_list->port_list( );

    VeriVarDecl *var_decl = NULL;

    NodeList::iterator idecl = port_decl_list.begin( );
    NodeList::iterator iport;

    VeriPrimaryExp *pexp = NULL;
    VeriArrayExp   *aexp = NULL;
    for( ; idecl != port_decl_list.end( ); ++idecl )
    {
      var_decl = static_cast<VeriVarDecl *>(*idecl);
      NodeList &vars = var_decl->vars( );
      for( iport = vars.begin(); iport != vars.end(); ++iport )
      {
        if( (*iport)->detail_type( ) == veEXP_PRI )
          pexp = static_cast<VeriPrimaryExp *>(*iport);
        else if( (*iport)->detail_type( ) == veEXP_ARRAY )
        {
          aexp = static_cast<VeriArrayExp *>(*iport);
          pexp = static_cast<VeriPrimaryExp *>(aexp->id());
        }
        if( pexp->var( ) != NULL )
          _act_ports.push_back( pexp->var( ) );
      }
    }
    return OK;
  }

  return vSEC_MOD_PORT_PARSE_NOT_SUPPORT;
}

int
VeriMod::map_ports( void )
{
  scope()->finish_scope();

  int re = prep_ports_for_map( );
  if( re != OK )
    return re;
  
  if( _port == NULL )
    return OK;;

  VeriModInstance *father_mod_inst = static_cast<VeriModInstance *>(father());
  if( father_mod_inst == NULL )
    return OK;

  if( father_mod_inst->arg()->detail_type() == veEXP_MULTI )
  {
    VeriMultiExp *mexp = static_cast<VeriMultiExp *>(father_mod_inst->arg());
    NodeList & exp_list = mexp->exp();

    NodeList::iterator iarg  = exp_list.begin();
    NodeList::iterator iport = _act_ports.begin();

    VeriExp *arg  = NULL;
    VeriVar *port = NULL;
    for( ; iarg != exp_list.end(); ++iarg, ++iport )
    {
      if( (*iport)->tok_type( ) == vtVAR )
      {
        arg  = static_cast<VeriExp *>(*iarg);
        port = static_cast<VeriVar *>(*iport);
        re = check_type_ref( arg, port );
        if( re != OK )
          return re;
      }
      else
      {
        veri_err->append_err( ERR_ERROR, this, vSEC_MOD_PORT_MISMATCH );
        return vSEC_MOD_PORT_MISMATCH;
      }
    }
  }
  return OK;
}

int
VeriMod::push_for_replacement( NodeVec& mstack )
{
  if( _param != NULL )
    mstack.push_back( _param );

  if( _port != NULL )
    mstack.push_back( _port );

  if( _item != NULL )
    mstack.push_back( _item );

  return OK;
}

int
VeriMod::replace_node( Uint idx, NodeList& node_list,
                          NodeList::iterator& inl)
{
  // spawn duplicant
  VeriMod *tmp = new VeriMod( idx, this );

  // begin replacement
  NodeList::iterator itl = inl; 

  if( _item != NULL )
  {
    tmp->_item = *(--itl);
    node_list.erase( itl );
    itl = inl;
  }

  if( _port != NULL )
  {
    tmp->_port = *(--itl);
    node_list.erase( itl );
    itl = inl;
  }

  if( _param != NULL )
  {
    tmp->_param = *(--itl);
    node_list.erase( itl );
  }

  *inl = tmp;
  return OK;
}

void
VeriMod::set_fname( const SList& fnames, const IList& flines )
{
  _fname.assign( fnames.begin(), fnames.end() );
  _fline.assign( flines.begin(), flines.end() );
#ifdef DEBUG_PARSER
  //std::cout << fnames.back( ) << std::endl;
#endif
}

void
VeriMod::addModInstantiated( void )
{
  for (auto iter : ((rtl::VeriModuleItemList*)item())->item_list()) {
    if (iter->tok_type() == vtINST && iter->detail_type() == vsINST_MOD_INSTANTIATION) {
      _modInstantiated.push_back(iter);
    }
  }
}

};
