/* 
 * Copyright (c) 2012 Fudan University ICCAD Lab
 * All rights reserved.
 *
 * Filename : util_err_token.hpp
 * Summary  : token definitions of rtl-synthesis utilities' errors
 *
 * Version  : 1 ( Creation )
 * Author   : Pavio Yang ( 11212020059@fudan.edu.cn )
 * Date     : 2012.10.28
 */

#ifndef ERR_UTIL_ERR_UTIL_ERR_TOKEN_HPP_
#define ERR_UTIL_ERR_UTIL_ERR_TOKEN_HPP_

namespace rtl
{

/*
 *  directed acyclic graph class error token difinitions
 */
enum DFA_DAG_ERR
{
  DAG_VAR_DUPLICATED  = 5000,
};

};

#endif // ERR_UTIL_ERR_UTIL_ERR_TOKEN_HPP_
