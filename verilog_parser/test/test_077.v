

// This design checks working of relational operators. It instantiates
// modules that mimicks relational operators but does not uses that
// operators. In short it provides the functioning of the relational
// operators without using them.

module test(in1, in2, out1, out2, out3, out4, out5, out6, out7, out8, out9, out10);

parameter WIDTH = 16;

input signed [0:WIDTH-1] in1;
input [0:WIDTH-1] in2;
output reg out1, out3, out5, out7, out9;
output out2, out4, out6, out8, out10;

always @(in1, in2)
begin
	out1 = (in1 < in2);
	out3 = (in1 > in2);
	out5 = (in1 == in2);
	out7 = (in1 <= in2);
	out9 = (in1 >= in2);
end

less_than #(.WIDTH(WIDTH)) lt (in1, in2, out2);
greater_than #(.WIDTH(WIDTH)) gt (in1, in2, out4);
equal #(.WIDTH(WIDTH)) eq (in1, in2, out6);
less_than_or_equal #(.WIDTH(WIDTH)) le (in1, in2, out8);
greater_than_or_equal #(.WIDTH(WIDTH)) ge (in1, in2, out10);

endmodule

module less_than(l, r, out);  	// out=1 if l<r, 0 otherwise

parameter WIDTH = 16;

input signed [0:WIDTH-1] l, r;
output reg out;

reg signed [0:WIDTH-1] t1;

always@(l, r)
begin
	t1 = (l-r);
	out = ((t1[0])?(1'b1):(1'b0));
end

endmodule

module greater_than(l, r, out);  	// out=1 if l>r, 0 otherwise

parameter WIDTH = 16;

input signed [0:WIDTH-1] l, r;
output out;

less_than #(.WIDTH(WIDTH)) lt1 (r, l, out);

endmodule

module equal(l, r, out);	// out=1, if l==r, 0 otherwise

parameter WIDTH = 16;

input signed [0:WIDTH-1] l, r;
output out;

wire t1, t2;

less_than #(.WIDTH(WIDTH)) lt1 (l, r, t1);
greater_than #(.WIDTH(WIDTH)) lt2 (l, r, t2);

assign out = ~(t1 | t2);

endmodule

module less_than_or_equal(l, r, out);	// out=1, if l<=r, 0 otherwise

parameter WIDTH = 16;

input signed [0:WIDTH-1] l, r;
output out;

wire t1, t2;

less_than #(.WIDTH(WIDTH)) lt1 (l, r, t1);
equal #(.WIDTH(WIDTH)) lt2 (l, r, t2);

assign out = (t1 | t2);

endmodule

module greater_than_or_equal(l, r, out);	// out=1, if l>=r, 0 otherwise

parameter WIDTH = 16;

input signed [0:WIDTH-1] l, r;
output out;

wire t1, t2;

greater_than #(.WIDTH(WIDTH)) lt1 (l, r, t1);
equal #(.WIDTH(WIDTH)) lt2 (l, r, t2);

assign out = (t1 | t2);

endmodule

